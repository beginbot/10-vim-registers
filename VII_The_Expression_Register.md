# The Expression Register

## Key "=

## Useful For

- Quick calculations you want to insert
- Calling external commands

```vimrc
=108 * 420

=system('ls ~/stream/Stream/Samples/')

=system('ls')

=system('fancy b')
```



