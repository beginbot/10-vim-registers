# The Small Delete

## Key: "-

This register contains text from commands that delete less than one line.

## Useful For

- When you know you just deleted multiple lines, and just want to small
  delete from before
- ????

## Not that predictable

s works
x and X work
c and C work
d and D work

S does NOT not work
dd does NOT work

Also if the line is too long, it cuts off the end but does save the whole thing.
I have not found a limit so far to how long it can be.  War and Peace did not work.
But I got up to 300,000 characters long and it still saved.
