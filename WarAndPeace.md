<!DOCTYPE html>
<html lang="en">
<!-- __ _ _ _ __| |_ (_)__ _____
    / _` | '_/ _| ' \| |\ V / -_)
    \__,_|_| \__|_||_|_| \_/\___| -->
  <head>
    <title>Full text of &quot;War And Peace&quot;</title>

          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
        <meta name="google-site-verification" content="Q2YSouphkkgHkFNP7FgAkc4TmBs1Gmag3uGNndb53B8" />
    <meta name="google-site-verification" content="bpjKvUvsX0lxfmjg19TLblckWkDpnptZEYsBntApxUk" />

    <script>
/* @licstart  The following is the entire license notice for the
 * JavaScript code in this page.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 */
</script>
        <script>window.archive_setup=[]</script>
    <meta charset="UTF-8">
    <script src="//archive.org/includes/jquery-1.10.2.min.js?v1.10.2" type="text/javascript"></script>
            <script src="//archive.org/includes/analytics.js?v=1fffaadd" type="text/javascript"></script>
    <script>
      'use strict';
      if ('archive_analytics' in window) {
                  archive_analytics.service = "ao_2";
        
        archive_analytics.send_pageview_on_load({"mediaType":"texts"});

        archive_analytics.process_url_events(window.location);
      }
    </script>
        <script src="//archive.org/includes/build/npm/jquery-ui.min.js?v1.12.1" type="text/javascript"></script>
    <script src="//archive.org/includes/bootstrap.min.js?v3.0.0" type="text/javascript"></script>
    <script src="//archive.org/components/npm/clipboard/dist/clipboard.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/components/npm/@babel/polyfill/dist/polyfill.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/ie-dom-node-remove-polyfill.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/polyfill.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/components/npm/@webcomponents/webcomponentsjs/webcomponents-bundle.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/more-facets.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/radio-player-controller.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/ia-wayback-search.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/ia-topnav.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/npm/react/umd/react.production.min.js?v16.7.0" type="text/javascript"></script>
    <script src="//archive.org/includes/build/npm/react-dom/umd/react-dom.production.min.js?v16.7.0" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/archive.min.js?v=1fffaadd" type="text/javascript"></script>
    <script src="//archive.org/includes/build/js/areact.min.js?v=1fffaadd" type="text/javascript"></script>
    <link href="//archive.org/includes/build/css/archive.min.css?v=1fffaadd" rel="stylesheet" type="text/css"/>
    <link rel="SHORTCUT ICON" href="https://archive.org/images/glogo.jpg"/>
      </head>
  <body class="navia ">
    <a href="#maincontent" class="hidden-for-screen-readers">Skip to main content</a>

    <!-- Wraps all page content -->
    <div id="wrap"
          >
                    <div class="ia-banners">
              <div 
        class="ia-banner hidden new-lending-mechanism"
        data-campaign="new-lending-mechanism">
        <p><a href="http://blog.archive.org/2020/06/10/temporary-national-emergency-library-to-close-2-weeks-early-returning-to-traditional-controlled-digital-lending/" rel="nofollow">See what's new with book lending at the Internet Archive</a></p>
        <form class="banner-close" action="" method="get" data-action="ia-banner-close">
          <fieldset>
            <button type="submit"></button>
          </fieldset>
        </form>
      </div>
          </div>
                    
        <div id="topnav">
                              <ia-topnav config='{"baseHost":"archive.org","screenName":false,"username":"","uploadURL":"https:\/\/archive.org\/create","eventCategory":"TopNav","waybackPagesArchived":"451 billion","isAdmin":0,"hideSearch":false}' menus="eyJhdWRpbyI6eyJoZWFkaW5nIjoiSW50ZXJuZXQgQXJjaGl2ZSBBdWRpbyIsImljb25MaW5rcyI6W3siaWNvbiI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvc2VydmljZXNcL2ltZ1wvZXRyZWUiLCJ0aXRsZSI6IkxpdmUgTXVzaWMgQXJjaGl2ZSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvZXRyZWUifSx7Imljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlcnZpY2VzXC9pbWdcL2xpYnJpdm94YXVkaW8iLCJ0aXRsZSI6IkxpYnJpdm94IEZyZWUgQXVkaW8iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2xpYnJpdm94YXVkaW8ifV0sImZlYXR1cmVkTGlua3MiOlt7InRpdGxlIjoiQWxsIGF1ZGlvIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpbyJ9LHsidGl0bGUiOiJUaGlzIEp1c3QgSW4iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlYXJjaC5waHA/cXVlcnk9bWVkaWF0eXBlOmF1ZGlvJnNvcnQ9LXB1YmxpY2RhdGUifSx7InRpdGxlIjoiR3JhdGVmdWwgRGVhZCIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvR3JhdGVmdWxEZWFkIn0seyJ0aXRsZSI6Ik5ldGxhYmVscyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvbmV0bGFiZWxzIn0seyJ0aXRsZSI6Ik9sZCBUaW1lIFJhZGlvIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9vbGR0aW1lcmFkaW8ifSx7InRpdGxlIjoiNzggUlBNcyBhbmQgQ3lsaW5kZXIgUmVjb3JkaW5ncyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvNzhycG0ifV0sImxpbmtzIjpbeyJ0aXRsZSI6IkF1ZGlvIEJvb2tzICYgUG9ldHJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpb19ib29rc3BvZXRyeSJ9LHsidGl0bGUiOiJDb21tdW5pdHkgQXVkaW8iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL29wZW5zb3VyY2VfYXVkaW8ifSx7InRpdGxlIjoiQ29tcHV0ZXJzLCBUZWNobm9sb2d5IGFuZCBTY2llbmNlIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpb190ZWNoIn0seyJ0aXRsZSI6Ik11c2ljLCBBcnRzICYgQ3VsdHVyZSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvYXVkaW9fbXVzaWMifSx7InRpdGxlIjoiTmV3cyAmIFB1YmxpYyBBZmZhaXJzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpb19uZXdzIn0seyJ0aXRsZSI6Ik5vbi1FbmdsaXNoIEF1ZGlvIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpb19mb3JlaWduIn0seyJ0aXRsZSI6IlNwaXJpdHVhbGl0eSAmIFJlbGlnaW9uIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hdWRpb19yZWxpZ2lvbiJ9LHsidGl0bGUiOiJRIGFuZCBKIFRoZSBPQU0gTmV0d29yayIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvcG9kY2FzdF9xLWotdGhlLW9hbS1uZXR3b3JrXzEwODczOTU1NDAifSx7InRpdGxlIjoiS2F5bGEgTGFpbidzIFBvZGNhc3QiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3BvZGNhc3Rfa2F5bGEtbGFpbnMtcG9kY2FzdF84MDgyNzgyMjMifSx7InRpdGxlIjoiUG9kY2FzdHMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3BvZGNhc3RzIn1dfSwiaW1hZ2VzIjp7ImhlYWRpbmciOiJJbWFnZXMiLCJpY29uTGlua3MiOlt7Imljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlcnZpY2VzXC9pbWdcL21ldHJvcG9saXRhbm11c2V1bW9mYXJ0LWdhbGxlcnkiLCJ0aXRsZSI6Ik1ldHJvcG9saXRhbiBNdXNldW0iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL21ldHJvcG9saXRhbm11c2V1bW9mYXJ0LWdhbGxlcnkifSx7Imljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlcnZpY2VzXC9pbWdcL2Jyb29rbHlubXVzZXVtIiwidGl0bGUiOiJCcm9va2x5biBNdXNldW0iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2Jyb29rbHlubXVzZXVtIn1dLCJmZWF0dXJlZExpbmtzIjpbeyJ0aXRsZSI6IkFsbCBpbWFnZXMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2ltYWdlIn0seyJ0aXRsZSI6IlRoaXMgSnVzdCBJbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvc2VhcmNoLnBocD9xdWVyeT1tZWRpYXR5cGU6aW1hZ2Umc29ydD0tcHVibGljZGF0ZSJ9LHsidGl0bGUiOiJGbGlja3IgQ29tbW9ucyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvZmxpY2tyY29tbW9ucyJ9LHsidGl0bGUiOiJPY2N1cHkgV2FsbCBTdHJlZXQgRmxpY2tyIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9mbGlja3Itb3dzIn0seyJ0aXRsZSI6IkNvdmVyIEFydCIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvY292ZXJhcnRhcmNoaXZlIn0seyJ0aXRsZSI6IlVTR1MgTWFwcyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvbWFwc191c2dzIn1dLCJsaW5rcyI6W3sidGl0bGUiOiJOQVNBIEltYWdlcyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvbmFzYSJ9LHsidGl0bGUiOiJTb2xhciBTeXN0ZW0gQ29sbGVjdGlvbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvc29sYXJzeXN0ZW1jb2xsZWN0aW9uIn0seyJ0aXRsZSI6IkFtZXMgUmVzZWFyY2ggQ2VudGVyIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hbWVzcmVzZWFyY2hjZW50ZXJpbWFnZWxpYnJhcnkifV19LCJtb3JlIjpbeyJ0aXRsZSI6IkFib3V0IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9hYm91dFwvIn0seyJ0aXRsZSI6IkJsb2ciLCJ1cmwiOiJodHRwczpcL1wvYmxvZy5hcmNoaXZlLm9yZ1wvIn0seyJ0aXRsZSI6IlByb2plY3RzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9wcm9qZWN0c1wvIn0seyJ0aXRsZSI6IkhlbHAiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2Fib3V0XC9mYXFzLnBocCJ9LHsidGl0bGUiOiJEb25hdGUiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RvbmF0ZSJ9LHsidGl0bGUiOiJDb250YWN0IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9hYm91dFwvY29udGFjdC5waHAifSx7InRpdGxlIjoiSm9icyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvYWJvdXRcL2pvYnMucGhwIn0seyJ0aXRsZSI6IlZvbHVudGVlciIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvYWJvdXRcL3ZvbHVudGVlcnBvc2l0aW9ucy5waHAifSx7InRpdGxlIjoiUGVvcGxlIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9hYm91dFwvYmlvcy5waHAifV0sInNvZnR3YXJlIjp7ImhlYWRpbmciOiJTb2Z0d2FyZSIsImljb25MaW5rcyI6W3siaWNvbiI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvc2VydmljZXNcL2ltZ1wvaW50ZXJuZXRhcmNhZGUiLCJ0aXRsZSI6IkludGVybmV0IEFyY2FkZSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvaW50ZXJuZXRhcmNhZGUifSx7Imljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlcnZpY2VzXC9pbWdcL2NvbnNvbGVsaXZpbmdyb29tIiwidGl0bGUiOiJDb25zb2xlIExpdmluZyBSb29tIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9jb25zb2xlbGl2aW5ncm9vbSJ9XSwiZmVhdHVyZWRMaW5rcyI6W3sidGl0bGUiOiJBbGwgc29mdHdhcmUiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3NvZnR3YXJlIn0seyJ0aXRsZSI6IlRoaXMgSnVzdCBJbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvc2VhcmNoLnBocD9xdWVyeT1tZWRpYXR5cGU6c29mdHdhcmUmc29ydD0tcHVibGljZGF0ZSJ9LHsidGl0bGUiOiJPbGQgU2Nob29sIEVtdWxhdGlvbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvdG9zZWMifSx7InRpdGxlIjoiTVMtRE9TIEdhbWVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zb2Z0d2FyZWxpYnJhcnlfbXNkb3NfZ2FtZXMifSx7InRpdGxlIjoiSGlzdG9yaWNhbCBTb2Z0d2FyZSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvaGlzdG9yaWNhbHNvZnR3YXJlIn0seyJ0aXRsZSI6IkNsYXNzaWMgUEMgR2FtZXMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2NsYXNzaWNwY2dhbWVzIn0seyJ0aXRsZSI6IlNvZnR3YXJlIExpYnJhcnkiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3NvZnR3YXJlbGlicmFyeSJ9XSwibGlua3MiOlt7InRpdGxlIjoiS29kaSBBcmNoaXZlIGFuZCBTdXBwb3J0IEZpbGUiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2tvZGlfYXJjaGl2ZSJ9LHsidGl0bGUiOiJDb21tdW5pdHkgU29mdHdhcmUiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL29wZW5fc291cmNlX3NvZnR3YXJlIn0seyJ0aXRsZSI6IlZpbnRhZ2UgU29mdHdhcmUiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3ZpbnRhZ2Vzb2Z0d2FyZSJ9LHsidGl0bGUiOiJBUEsiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2Fwa2FyY2hpdmUifSx7InRpdGxlIjoiTVMtRE9TIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zb2Z0d2FyZWxpYnJhcnlfbXNkb3MifSx7InRpdGxlIjoiQ0QtUk9NIFNvZnR3YXJlIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9jZC1yb21zIn0seyJ0aXRsZSI6IkNELVJPTSBTb2Z0d2FyZSBMaWJyYXJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9jZHJvbXNvZnR3YXJlIn0seyJ0aXRsZSI6IlNvZnR3YXJlIFNpdGVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zb2Z0d2FyZXNpdGVzIn0seyJ0aXRsZSI6IlR1Y293cyBTb2Z0d2FyZSBMaWJyYXJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC90dWNvd3MifSx7InRpdGxlIjoiU2hhcmV3YXJlIENELVJPTXMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2NkYmJzYXJjaGl2ZSJ9LHsidGl0bGUiOiJTb2Z0d2FyZSBDYXBzdWxlcyBDb21waWxhdGlvbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvc29mdHdhcmVjYXBzdWxlcyJ9LHsidGl0bGUiOiJDRC1ST00gSW1hZ2VzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9jZHJvbWltYWdlcyJ9LHsidGl0bGUiOiJaWCBTcGVjdHJ1bSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvc29mdHdhcmVsaWJyYXJ5X3p4X3NwZWN0cnVtIn0seyJ0aXRsZSI6IkRPT00gTGV2ZWwgQ0QiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2Rvb20tY2RzIn1dfSwidGV4dHMiOnsiaGVhZGluZyI6IkJvb2tzIiwiaWNvbkxpbmtzIjpbeyJ0aXRsZSI6IkJvb2tzIHRvIEJvcnJvdyIsImljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2ltYWdlc1wvYm9vay1sZW5kLnBuZyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvaW5saWJyYXJ5In0seyJ0aXRsZSI6Ik9wZW4gTGlicmFyeSIsImljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2ltYWdlc1wvd2lkZ2V0T0wucG5nIiwidXJsIjoiaHR0cHM6XC9cL29wZW5saWJyYXJ5Lm9yZ1wvIn1dLCJmZWF0dXJlZExpbmtzIjpbeyJ0aXRsZSI6IkFsbCBCb29rcyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvYm9va3MifSx7InRpdGxlIjoiQWxsIFRleHRzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC90ZXh0cyJ9LHsidGl0bGUiOiJUaGlzIEp1c3QgSW4iLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlYXJjaC5waHA/cXVlcnk9bWVkaWF0eXBlOnRleHRzJnNvcnQ9LXB1YmxpY2RhdGUifSx7InRpdGxlIjoiU21pdGhzb25pYW4gTGlicmFyaWVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zbWl0aHNvbmlhbiJ9LHsidGl0bGUiOiJGRURMSU5LIChVUykiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2ZlZGxpbmsifSx7InRpdGxlIjoiR2VuZWFsb2d5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9nZW5lYWxvZ3kifSx7InRpdGxlIjoiTGluY29sbiBDb2xsZWN0aW9uIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9saW5jb2xuY29sbGVjdGlvbiJ9XSwibGlua3MiOlt7InRpdGxlIjoiQW1lcmljYW4gTGlicmFyaWVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9hbWVyaWNhbmEifSx7InRpdGxlIjoiQ2FuYWRpYW4gTGlicmFyaWVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC90b3JvbnRvIn0seyJ0aXRsZSI6IlVuaXZlcnNhbCBMaWJyYXJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC91bml2ZXJzYWxsaWJyYXJ5In0seyJ0aXRsZSI6IkNvbW11bml0eSBUZXh0cyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvb3BlbnNvdXJjZSJ9LHsidGl0bGUiOiJQcm9qZWN0IEd1dGVuYmVyZyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvZ3V0ZW5iZXJnIn0seyJ0aXRsZSI6IkJpb2RpdmVyc2l0eSBIZXJpdGFnZSBMaWJyYXJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9iaW9kaXZlcnNpdHkifSx7InRpdGxlIjoiQ2hpbGRyZW4ncyBMaWJyYXJ5IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9pYWNsIn0seyJ0aXRsZSI6IkJvb2tzIGJ5IExhbmd1YWdlIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9ib29rc2J5bGFuZ3VhZ2UifSx7InRpdGxlIjoiQWRkaXRpb25hbCBDb2xsZWN0aW9ucyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvYWRkaXRpb25hbF9jb2xsZWN0aW9ucyJ9XX0sIndlYiI6eyJtb2JpbGVBcHBzTGlua3MiOlt7InVybCI6Imh0dHBzOlwvXC9hcHBzLmFwcGxlLmNvbVwvdXNcL2FwcFwvd2F5YmFjay1tYWNoaW5lXC9pZDEyMDE4ODgzMTMiLCJ0aXRsZSI6IldheWJhY2sgTWFjaGluZSAoaU9TKSIsImV4dGVybmFsIjp0cnVlfSx7InVybCI6Imh0dHBzOlwvXC9wbGF5Lmdvb2dsZS5jb21cL3N0b3JlXC9hcHBzXC9kZXRhaWxzP2lkPWNvbS5hcmNoaXZlLndheWJhY2ttYWNoaW5lJmhsPWVuX1VTIiwidGl0bGUiOiJXYXliYWNrIE1hY2hpbmUgKEFuZHJvaWQpIiwiZXh0ZXJuYWwiOnRydWV9XSwiYnJvd3NlckV4dGVuc2lvbnNMaW5rcyI6W3sidXJsIjoiaHR0cHM6XC9cL2FkZG9ucy5tb3ppbGxhLm9yZ1wvZW4tVVNcL2ZpcmVmb3hcL2FkZG9uXC93YXliYWNrLW1hY2hpbmVfbmV3XC8iLCJ0aXRsZSI6IkZpcmVmb3giLCJleHRlcm5hbCI6dHJ1ZX0seyJ1cmwiOiJodHRwczpcL1wvYXBwcy5hcHBsZS5jb21cL3VzXC9hcHBcL3dheWJhY2stbWFjaGluZVwvaWQxNDcyNDMyNDIyP210PTEyIiwidGl0bGUiOiJTYWZhcmkiLCJleHRlcm5hbCI6dHJ1ZX1dLCJhcmNoaXZlSXRMaW5rcyI6W3sidXJsIjoiaHR0cHM6XC9cL3d3dy5hcmNoaXZlLWl0Lm9yZ1wvZXhwbG9yZSIsInRpdGxlIjoiRXhwbG9yZSB0aGUgQ29sbGVjdGlvbnMifSx7InVybCI6Imh0dHBzOlwvXC93d3cuYXJjaGl2ZS1pdC5vcmdcL2Jsb2dcL2xlYXJuLW1vcmVcLyIsInRpdGxlIjoiTGVhcm4gTW9yZSJ9LHsidXJsIjoiaHR0cHM6XC9cL3d3dy5hcmNoaXZlLWl0Lm9yZ1wvY29udGFjdC11cyIsInRpdGxlIjoiQnVpbGQgQ29sbGVjdGlvbnMifV19LCJ2aWRlbyI6eyJoZWFkaW5nIjoiVmlkZW8iLCJpY29uTGlua3MiOlt7Imljb24iOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL3NlcnZpY2VzXC9pbWdcL3R2IiwidGl0bGUiOiJUViBOZXdzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC90diJ9LHsiaWNvbiI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvc2VydmljZXNcL2ltZ1wvOTExIiwidGl0bGUiOiJVbmRlcnN0YW5kaW5nIDlcLzExIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC85MTEifV0sImZlYXR1cmVkTGlua3MiOlt7InRpdGxlIjoiQWxsIHZpZGVvIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9tb3ZpZXMifSx7InRpdGxlIjoiVGhpcyBKdXN0IEluIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9zZWFyY2gucGhwP3F1ZXJ5PW1lZGlhdHlwZTptb3ZpZXMmc29ydD0tcHVibGljZGF0ZSJ9LHsidGl0bGUiOiJQcmVsaW5nZXIgQXJjaGl2ZXMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL3ByZWxpbmdlciJ9LHsidGl0bGUiOiJEZW1vY3JhY3kgTm93ISIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvZGVtb2NyYWN5X25vd192aWQifSx7InRpdGxlIjoiT2NjdXB5IFdhbGwgU3RyZWV0IiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9vY2N1cHl3YWxsc3RyZWV0In0seyJ0aXRsZSI6IlRWIE5TQSBDbGlwIExpYnJhcnkiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL25zYSJ9XSwibGlua3MiOlt7InRpdGxlIjoiQW5pbWF0aW9uICYgQ2FydG9vbnMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2FuaW1hdGlvbmFuZGNhcnRvb25zIn0seyJ0aXRsZSI6IkFydHMgJiBNdXNpYyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvYXJ0c2FuZG11c2ljdmlkZW9zIn0seyJ0aXRsZSI6IkNvbXB1dGVycyAmIFRlY2hub2xvZ3kiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL2NvbXB1dGVyc2FuZHRlY2h2aWRlb3MifSx7InRpdGxlIjoiQ3VsdHVyYWwgJiBBY2FkZW1pYyBGaWxtcyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvY3VsdHVyYWxhbmRhY2FkZW1pY2ZpbG1zIn0seyJ0aXRsZSI6IkVwaGVtZXJhbCBGaWxtcyIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvZXBoZW1lcmEifSx7InRpdGxlIjoiTW92aWVzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9tb3ZpZXNhbmRmaWxtcyJ9LHsidGl0bGUiOiJOZXdzICYgUHVibGljIEFmZmFpcnMiLCJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2RldGFpbHNcL25ld3NhbmRwdWJsaWNhZmZhaXJzIn0seyJ0aXRsZSI6IlNwaXJpdHVhbGl0eSAmIFJlbGlnaW9uIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zcGlyaXR1YWxpdHlhbmRyZWxpZ2lvbiJ9LHsidGl0bGUiOiJTcG9ydHMgVmlkZW9zIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9zcG9ydHMifSx7InRpdGxlIjoiVGVsZXZpc2lvbiIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wvdGVsZXZpc2lvbiJ9LHsidGl0bGUiOiJWaWRlb2dhbWUgVmlkZW9zIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC9nYW1ldmlkZW9zIn0seyJ0aXRsZSI6IlZsb2dzIiwidXJsIjoiaHR0cHM6XC9cL2FyY2hpdmUub3JnXC9kZXRhaWxzXC92bG9ncyJ9LHsidGl0bGUiOiJZb3V0aCBNZWRpYSIsInVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvZGV0YWlsc1wveW91dGhfbWVkaWEifV19LCJ1c2VyIjpbXSwic2lnbmVkT3V0IjpbeyJ1cmwiOiJodHRwczpcL1wvYXJjaGl2ZS5vcmdcL2FjY291bnRcL3NpZ251cCIsInRpdGxlIjoiU2lnbiB1cCBmb3IgZnJlZSIsImFuYWx5dGljc0V2ZW50IjoiQXZhdGFyTWVudS1TaWdudXAifSx7InVybCI6Imh0dHBzOlwvXC9hcmNoaXZlLm9yZ1wvYWNjb3VudFwvbG9naW4iLCJ0aXRsZSI6IkxvZyBpbiIsImFuYWx5dGljc0V2ZW50IjoiQXZhdGFyTWVudS1Mb2dpbiJ9XX0=">
            <noscript class="static-content">      <style scope="icon-hamburger-1">.icon-hamburger-1 svg.icon-hamburger {
        display: block;
      }

      .icon-hamburger-1 .fill-color.icon-hamburger {
        fill: #fff;
      }</style><!-- Shady DOM styles for login-button --><style scope="login-button-1">.login-button-1 .dropdown-toggle.login-button {
        display: block;
          height: 4rem;
          font-size: 1.6rem;
          text-transform: uppercase;
          text-decoration: none;
          color: #ccc;
          cursor: pointer;
      }

      .login-button-1 .dropdown-toggle.login-button .fill-color.login-button {
        fill: #999;
      }

      .login-button-1 .dropdown-toggle.login-button:active .fill-color.login-button,.login-button-1 .dropdown-toggle.login-button:focus .fill-color.login-button,.login-button-1 .dropdown-toggle.login-button:hover .fill-color.login-button {
        fill: #fff;
      }

      .login-button-1 .active.login-button {
        border-radius: 1rem 1rem 0 0;
          background: #333;
      }

      .login-button-1 .active.login-button .fill-color.login-button {
        fill: #fff;
      }

      .login-button-1 span.login-button {
        display: none;
          font-size: 1.4rem;
          text-transform: uppercase;
          color: #999;
      }

      .login-button-1 span.login-button a.login-button {
        color: inherit;
          text-decoration: none;
      }

      .login-button-1 a.login-button:hover,.login-button-1 a.login-button:active,.login-button-1 a.login-button:focus {
        color: #fff;
      }

      @media (min-width: 890px) {
      .login-button-1 .logged-out-toolbar.login-button {
        transform: translateY(-.5rem);
      }

      .login-button-1 .active.login-button {
        background: transparent;
      }

      .login-button-1 .dropdown-toggle.login-button {
        display: inline-block;
            vertical-align: middle;
      }

      .login-button-1 span.login-button {
        display: inline;
            vertical-align: middle;
      }

      }</style><!-- Shady DOM styles for nav-search --><style scope="nav-search-1">.nav-search-1 input.nav-search:focus {
        outline: none;
      }

      .nav-search-1 button.nav-search {
        background: none;
          color: inherit;
          border: none;
          font: inherit;
          cursor: pointer;
      }

      .nav-search-1 button.nav-search:focus {
        outline: none;
      }

      .nav-search-1 .search.nav-search {
        padding-top: 0;
          margin-right: .5rem;
      }

      .nav-search-1 .search.nav-search svg.nav-search {
        position: relative;
          top: -5px;
          right: -3px;
          fill:;
      }

      .nav-search-1 .search-activated.nav-search {
        display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          position: absolute;
          top: 0;
          right: 5rem;
          bottom: 0;
          left: 5rem;
          z-index: 3;
          padding: 0.5rem 0.2rem;
          border-radius: 1rem 1rem 0 0;
          background: #333;
      }

      .nav-search-1 .search-inactive.nav-search {
        display: none;
      }

      .nav-search-1 .search-activated.nav-search .highlight.nav-search,.nav-search-1 .search-activated.nav-search .search.nav-search {
        background: #fff;
          border-radius: 0.5rem;
      }

      .nav-search-1 .search-activated.nav-search .highlight.nav-search {
        display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          width: 100%;
          margin: 0 .5rem;
      }

      .nav-search-1 .search-activated.nav-search .search.nav-search {
        height: 100%;
          padding: 0;
          margin-right: 0;
          -ms-flex-item-align: center;
          -ms-grid-row-align: center;
          align-self: center;
      }

      .nav-search-1 .search-activated.nav-search .search-field.nav-search {
        width: 100%;
          height: 100%;
          box-sizing: border-box;
          padding-left: 1rem;
          border-radius: 0.5rem;
          border: none;
          font-size: 1.6rem;
          text-align: center;
      }

      .nav-search-1 .search-activated.nav-search .search-field.nav-search:focus {
        outline: none;
      }

      @keyframes fade-in-nav-search-1 {
      0% {
        opacity: 0;
      }

      100% {
        opacity: 1;
      }

      }

      .nav-search-1 .fade-in.nav-search {
        animation: fade-in-nav-search-1 .2s forwards;
      }

      @media (min-width: 890px) {
      .nav-search-1 .search.nav-search svg.nav-search {
        display: inline;
            width: 28px;
            height: 28px;
            vertical-align: -14px;
      }

      .nav-search-1 .search.nav-search path.nav-search {
        fill: #333;
      }

      .nav-search-1 .search-inactive.nav-search,.nav-search-1 .search-activated.nav-search {
        display: block;
            position: static;
            padding: 1.2rem .2rem;
            background: transparent;
      }

      .nav-search-1 .search-activated.nav-search .highlight.nav-search {
        width: 13rem;
            height: 2.8rem;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: reverse;
            -ms-flex-direction: row-reverse;
            flex-direction: row-reverse;
      }

      .nav-search-1 .search-activated.nav-search .search-field.nav-search {
        width: calc(100% - 28px);
            height: 100%;
            padding-left: 0;
            font-size: 1.4rem;
            text-align: left;
      }

      .nav-search-1 .search-activated.nav-search .search.nav-search svg.nav-search {
        width: 28px;
            height: 28px;
      }

      }</style><!-- Shady DOM styles for media-button --><style scope="media-button-1">.media-button-1 a.media-button {
        display: inline-block;
          text-decoration: none;
      }

      .media-button-1 .menu-item.media-button {
        display: inline-flex;
          width: 100%;
          padding: 0;
          font-size: 1.6rem;
          text-align: left;
          background: transparent;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
      }

      .media-button-1 .menu-item.media-button:focus {
        outline: none;
      }

      .media-button-1 .label.media-button {
        display: inline-block;
          padding: 0;
          font-weight: 400;
          color: #fff;
          text-align: left;
          vertical-align: middle;
      }

      .media-button-1 .menu-item.media-button > .icon.media-button {
        display: inline-flex;
          width: 42px;
          height: 42px;
          vertical-align: middle;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          justify-content: center;
      }

      .media-button-1 .menu-item.selected.media-button .icon.media-button {
        background-color: #333;
          border-radius: 1rem 0 0 1rem;
      }

      .media-button-1 .icon.media-button .fill-color.media-button {
        fill: #999;
      }

      .media-button-1 .icon.active.media-button .fill-color.media-button {
        fill: #fff;
      }

      .media-button-1 .donate.media-button .fill-color.media-button {
        fill: #f00;
      }

      @media (min-width: 890px) {
      .media-button-1 .menu-item.media-button {
        width: auto;
            height: 5rem;
            color: #999;
      }

      .media-button-1 .menu-item.media-button:hover,.media-button-1 .menu-item.media-button:active,.media-button-1 .menu-item.media-button:focus {
        color: #fff;
      }

      .media-button-1 .menu-item.media-button:hover .fill-color.media-button,.media-button-1 .menu-item.media-button:active .fill-color.media-button,.media-button-1 .menu-item.media-button:focus .fill-color.media-button {
        fill: #fff;
      }

      .media-button-1 .label.media-button {
        display: none;
      }

      .media-button-1 .label.media-button,.media-button-1 .web.media-button:after {
        padding-right: 1rem;
            font-size: 1.3rem;
            text-transform: uppercase;
            color: inherit;
      }

      .media-button-1 .web.media-button:after {
        display: none;
            content: "web";
      }

      .media-button-1 .donate.media-button,.media-button-1 .more.media-button {
        display: none;
      }

      .media-button-1 .menu-item.selected.media-button {
        background: #474747;
      }

      .media-button-1 .menu-item.selected.media-button .label.media-button,.media-button-1 .menu-item.selected.web.media-button:after {
        color: #fff;
      }

      .media-button-1 .menu-item.selected.media-button .icon.media-button {
        background: transparent;
      }

      .media-button-1 .web.selected.media-button .fill-color.media-button {
        fill: #ffcd27;
      }

      .media-button-1 .texts.selected.media-button .fill-color.media-button {
        fill: #faab3c;
      }

      .media-button-1 .video.selected.media-button .fill-color.media-button {
        fill: #f1644b;
      }

      .media-button-1 .audio.selected.media-button .fill-color.media-button {
        fill: #00adef;
      }

      .media-button-1 .software.selected.media-button .fill-color.media-button {
        fill: #9ecc4f;
      }

      .media-button-1 .images.selected.media-button .fill-color.media-button {
        fill: #aa99c9;
      }

      }

      @media (min-width: 1300px) {
      .media-button-1 .label.media-button,.media-button-1 .web.media-button:after {
        display: inline;
      }

      .media-button-1 .web.media-button .label.media-button {
        display: none;
      }

      }</style><!-- Shady DOM styles for media-menu --><style scope="media-menu-1">.media-menu-1 {
        outline: none;
      }

      .media-menu-1 .media-menu.media-menu {
        position: absolute;
          z-index: -1;
          top: -100vh;
          width: 100%;
          background-color: #222;
          margin: 0;
          overflow: hidden;
      }

      .media-menu-1 .media-menu.tx-slide.media-menu {
        transition-property: top;
          transition-duration: 0.2s;
          transition-timing-function: ease;
      }

      .media-menu-1 .media-menu.tx-slide.open.media-menu {
        top: 100%;
      }

      .media-menu-1 .media-menu.tx-slide.closed.media-menu {
        top: -100vh;
      }

      .media-menu-1 .media-menu.tx-slide.closed.media-menu {
        transition-duration: 0.2s;
      }

      .media-menu-1 .menu-group.media-menu {
        position: relative;
          line-height: normal;
      }

      @media (min-width: 890px) {
      .media-menu-1 .media-menu.media-menu {
        display: inline-block;
            position: static;
            width: auto;
            height: 5rem;
      }

      .media-menu-1 .media-menu.tx-slide.media-menu {
        transition-property: none;
      }

      .media-menu-1 .media-menu.tx-slide.open.media-menu,.media-menu-1 .media-menu.tx-slide.closed.media-menu {
        top: 0;
      }

      .media-menu-1 .menu-group.media-menu {
        font-size: 0;
      }

      }</style><!-- Shady DOM styles for primary-nav --><style scope="primary-nav-1">.primary-nav-1 button.primary-nav:focus,.primary-nav-1 a.primary-nav:focus,.primary-nav-1 input.primary-nav:focus {
        outline: none;
      }

      .primary-nav-1 nav.primary-nav {
        position: relative;
          display: -ms-grid;
          display: grid;
          height: 4rem;
          grid-template-areas: "hamburger empty search user";
          -ms-grid-columns: 4rem minmax(1rem, 100%) 4.3rem 5rem;
          grid-template-columns: 4rem auto 4.3rem 5rem;
          -ms-grid-rows: 100%;
          grid-template-rows: 100%;
          background: #222;
          border-bottom: 1px solid #333;
      }

      .primary-nav-1 button.primary-nav {
        background: none;
          color: inherit;
          border: none;
          font: inherit;
          cursor: pointer;
      }

      .primary-nav-1 .link-home.primary-nav {
        position: absolute;
          top: 50%;
          left: 50%;
          z-index: 2;
          text-decoration: none;
          -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
      }

      .primary-nav-1 media-menu.primary-nav {
        grid-column-start: hamburger-start;
          grid-column-end: user-end;
      }

      .primary-nav-1 .ia-logo.primary-nav {
        display: block;
      }

      .primary-nav-1 .ia-wordmark.primary-nav {
        display: none;
      }

      .primary-nav-1 .hamburger.primary-nav {
        -ms-grid-row: 1;
          -ms-grid-column: 1;
          grid-area: hamburger;
          padding: 0;
      }

      .primary-nav-1 .search-trigger.primary-nav {
        -ms-grid-row: 1;
          -ms-grid-column: 3;
          grid-area: search;
          position: relative;
          padding: 0;
          z-index: 1;
          width: 100%;
          text-align: right;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
      }

      .primary-nav-1 .search-trigger.primary-nav .fill-color.primary-nav {
        fill: #999;
      }

      .primary-nav-1 .search-activated.primary-nav {
        position: relative;
          z-index: 3;
      }

      .primary-nav-1 .upload.primary-nav {
        display: none;
      }

      .primary-nav-1 .upload.primary-nav span.primary-nav {
        display: none;
      }

      .primary-nav-1 .user-info.primary-nav {
        -ms-grid-row: 1;
          -ms-grid-column: 4;
          grid-area: user;
          -ms-grid-row-align: stretch;
          align-self: stretch;
          -ms-grid-column-align: end;
          justify-self: end;
      }

      .primary-nav-1 .username.primary-nav {
        display: none;
          font-size: 1.3rem;
          vertical-align: middle;
          text-transform: uppercase;
      }

      .primary-nav-1 .user-menu.primary-nav {
        height: 100%;
          padding: .5rem 1rem;
          color: #999;
      }

      .primary-nav-1 .user-menu.primary-nav:hover {
        color: #fff;
      }

      .primary-nav-1 .user-menu.active.primary-nav {
        border-radius: 1rem 1rem 0 0;
          background: #333;
      }

      .primary-nav-1 .user-menu.primary-nav img.primary-nav {
        display: block;
          width: 30px;
          height: 30px;
      }

      @media (min-width: 890px) {
      .primary-nav-1 {
        ;
      }

      .primary-nav-1 nav.primary-nav {
        display: block;
            z-index: 2;
            height: 5rem;
            padding-right: 1.5rem;
      }

      .primary-nav-1 .link-home.primary-nav {
        position: static;
            float: left;
            margin-top: 1rem;
            padding: 0 10px 0 13px;
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            transform: translate(0, 0);
      }

      .primary-nav-1 .ia-logo.primary-nav,.primary-nav-1 .ia-wordmark.primary-nav {
        display: inline-block;
            vertical-align: middle;
      }

      .primary-nav-1 .ia-wordmark.primary-nav {
        margin-left: 1rem;
      }

      .primary-nav-1 .hamburger.primary-nav,.primary-nav-1 .search-trigger.primary-nav {
        display: none;
      }

      .primary-nav-1 .user-info.primary-nav {
        float: right;
            padding-top: 1rem;
      }

      .primary-nav-1 .user-menu.primary-nav {
        padding-top: 0;
      }

      .primary-nav-1 .user-menu.active.primary-nav {
        background: transparent;
      }

      .primary-nav-1 .user-menu.primary-nav img.primary-nav {
        display: inline-block;
            vertical-align: middle;
      }

      .primary-nav-1 .upload.primary-nav {
        display: block;
            float: right;
            margin-top: 1rem;
            font-size: 1.4rem;
            text-transform: uppercase;
            text-decoration: none;
            color: #999;
      }

      .primary-nav-1 .upload.primary-nav:active,.primary-nav-1 .upload.primary-nav:focus,.primary-nav-1 .upload.primary-nav:hover {
        color: #fff;
      }

      .primary-nav-1 .upload.primary-nav svg.primary-nav {
        width: 32px;
            height: 32px;
            vertical-align: middle;
            fill: #999;
      }

      .primary-nav-1 .upload.primary-nav:hover svg.primary-nav,.primary-nav-1 .upload.primary-nav:focus svg.primary-nav,.primary-nav-1 .upload.primary-nav:active svg.primary-nav {
        fill: #fff;
      }

      .primary-nav-1 nav-search.primary-nav {
        float: right;
            margin-left: 1rem;
      }

      .primary-nav-1 login-button.primary-nav {
        display: block;
            margin-right: 1rem;
      }

      }

      @media (min-width: 990px) {
      .primary-nav-1 .username.primary-nav {
        display: inline-block;
      }

      .primary-nav-1 .upload.primary-nav span.primary-nav {
        display: inline;
      }

      }</style><!-- Shady DOM styles for user-menu --><!-- Shady DOM styles for search-menu --><style scope="search-menu-1">.search-menu-1 {
        ;
      }

      .search-menu-1 button.search-menu:focus,.search-menu-1 input.search-menu:focus {
        outline-color: #428bca;
          outline-width: 0.16rem;
          outline-style: auto;
      }

      .search-menu-1 .search-menu.search-menu {
        position: absolute;
          top: -800px;
          right: 0;
          left: 0;
          z-index: 1;
          padding: 0 4.5rem;
          font-size: 1.6rem;
          background-color: #333;
      }

      .search-menu-1 .tx-slide.search-menu {
        overflow: hidden;
          transition-property: top;
          transition-duration: 0.2s;
          transition-timing-function: ease;
      }

      .search-menu-1 .initial.search-menu,.search-menu-1 .closed.search-menu {
        top: -800px;
      }

      .search-menu-1 .closed.search-menu {
        transition-duration: 0.2s;
      }

      .search-menu-1 .open.search-menu {
        top: 4rem;
      }

      .search-menu-1 label.search-menu,.search-menu-1 a.search-menu {
        padding: 1rem;
          display: block;
      }

      .search-menu-1 .advanced-search.search-menu {
        text-decoration: none;
          color: #428bca;
      }

      @media (min-width: 890px) {
      .search-menu-1 .search-menu.search-menu {
        overflow: visible;
            top: -400px;
            right: 2rem;
            left: auto;
            z-index: 5;
            padding: 1rem 2rem;
            transition: opacity .2s ease-in-out;
            font-size: 1.4rem;
            color: #333;
            border-radius: 2px;
            background: #fff;
            box-shadow: 0 1px 2px 1px rgba(0, 0, 0, .15);
      }

      .search-menu-1 .search-menu.search-menu:after {
        position: absolute;
            right: 7px;
            top: -7px;
            width: 12px;
            height: 7px;
            box-sizing: border-box;
            color: #fff;
            content: "";
            border-bottom: 7px solid currentColor;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
      }

      .search-menu-1 .initial.search-menu,.search-menu-1 .closed.search-menu {
        opacity: 0;
            transition-duration: .2s;
      }

      .search-menu-1 .open.search-menu {
        top: 5.1rem;
            opacity: 1;
      }

      .search-menu-1 label.search-menu {
        padding: 0;
      }

      .search-menu-1 label.search-menu + label.search-menu {
        padding-top: 7px;
      }

      .search-menu-1 a.search-menu {
        padding: 1rem 0 0 0;
      }

      }</style><!-- Shady DOM styles for wayback-search --><!-- Shady DOM styles for more-slider --><!-- Shady DOM styles for media-subnav --><style scope="media-subnav-1">.media-subnav-1 a.media-subnav {
        text-decoration: none;
          color: #fff;
      }

      .media-subnav-1 img.media-subnav {
        display: block;
          width: 90px;
          height: 90px;
          margin: 0 auto 1rem auto;
          border-radius: 45px;
      }

      .media-subnav-1 h3.media-subnav {
        margin-top: 0;
          font-size: 1.8rem;
      }

      .media-subnav-1 h4.media-subnav {
        font-size: 1.6rem;
      }

      .media-subnav-1 ul.media-subnav {
        padding: 0;
          margin: 0;
          list-style: none;
      }

      .media-subnav-1 li.media-subnav + li.media-subnav {
        padding-top: 1.5rem;
      }

      .media-subnav-1 .icon-links.media-subnav {
        display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: space-evenly;
          -ms-flex-pack: space-evenly;
          justify-content: space-evenly;
          text-align: center;
      }

      .media-subnav-1 .icon-links.media-subnav a.media-subnav {
        display: inline-block;
          width: 120px;
          margin-bottom: 1.5rem;
          overflow: hidden;
          white-space: nowrap;
          text-align: center;
          text-overflow: ellipsis;
      }

      .media-subnav-1 .icon-links.media-subnav a.media-subnav + a.media-subnav {
        margin-left: 2rem;
      }

      .media-subnav-1 .featured.media-subnav h4.media-subnav {
        display: none;
      }

      @media (min-width: 890px) {
      .media-subnav-1 {
        display: -ms-grid;
            display: grid;
            -ms-grid-columns: 40% 20% 40%;
            grid-template-columns: 40% 20% 40%;
      }

      .media-subnav-1 .wayback-search.media-subnav {
        -ms-grid-column: 1;
            -ms-grid-column-span: 3;
            grid-column: 1 / 4;
      }

      .media-subnav-1 h3.media-subnav {
        display: none;
      }

      .media-subnav-1 h4.media-subnav {
        margin: 0 0 1rem 0;
            font-weight: 100;
      }

      .media-subnav-1 ul.media-subnav {
        font-size: 1.3rem;
      }

      .media-subnav-1 li.media-subnav {
        padding-bottom: .5rem;
      }

      .media-subnav-1 li.media-subnav + li.media-subnav {
        padding-top: 0;
      }

      .media-subnav-1 li.media-subnav a.media-subnav {
        display: block;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
      }

      .media-subnav-1 .icon-links.media-subnav {
        -ms-grid-column: 1;
      }

      .media-subnav-1 .icon-links.media-subnav a.media-subnav {
        padding-top: 3.5rem;
            max-width: 160px;
      }

      .media-subnav-1 .links.media-subnav {
        padding: 0 1.5rem;
      }

      .media-subnav-1 .featured.media-subnav {
        -ms-grid-column: 2;
      }

      .media-subnav-1 .featured.media-subnav h4.media-subnav {
        display: block;
      }

      .media-subnav-1 .top.media-subnav {
        -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav {
        display: -ms-grid;
            display: grid;
            -ms-grid-columns: 50% 3rem 50%;
            grid-template-columns: 50% 50%;
            -ms-grid-rows: (auto)[7];
            grid-template-rows: repeat(7, auto);
            grid-column-gap: 3rem;
            grid-auto-flow: column;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(1) {
        -ms-grid-row: 1;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(2) {
        -ms-grid-row: 2;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(3) {
        -ms-grid-row: 3;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(4) {
        -ms-grid-row: 4;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(5) {
        -ms-grid-row: 5;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(6) {
        -ms-grid-row: 6;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(7) {
        -ms-grid-row: 7;
            -ms-grid-column: 1;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(8) {
        -ms-grid-row: 1;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(9) {
        -ms-grid-row: 2;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(10) {
        -ms-grid-row: 3;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(11) {
        -ms-grid-row: 4;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(12) {
        -ms-grid-row: 5;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(13) {
        -ms-grid-row: 6;
            -ms-grid-column: 3;
      }

      .media-subnav-1 .top.media-subnav ul.media-subnav > *.media-subnav:nth-child(14) {
        -ms-grid-row: 7;
            -ms-grid-column: 3;
      }

      }</style><!-- Shady DOM styles for media-slider --><style scope="media-slider-1">.media-slider-1 .overflow-clip.media-slider {
        display: none;
          position: absolute;
          top: 4rem;
          right: 0;
          left: 4rem;
          height: 368px;
          overflow-x: hidden;
      }

      .media-slider-1 .information-menu.media-slider {
        position: absolute;
          top: 0;
          right: 0;
          left: 0;
          padding: 0;
          height: 368px;
          overflow-x: hidden;
          font-size: 1.4rem;
          background: #333;
      }

      .media-slider-1 .open.media-slider {
        display: block;
      }

      .media-slider-1 .info-box.media-slider {
        padding: 1rem;
      }

      @media (min-width: 890px) {
      .media-slider-1 .overflow-clip.media-slider {
        display: block;
            top: 0;
            left: 0;
            height: auto;
            overflow-x: visible;
            transform: translate(0, -100%);
            transition: transform .2s ease;
      }

      .media-slider-1 .information-menu.media-slider {
        left: 0;
            z-index: 1;
            height: auto;
            min-height: 21rem;
            background: #474747;
            transform: translate(0, -100%);
            transition: transform .2s ease;
      }

      .media-slider-1 .overflow-clip.open.media-slider {
        transform: translate(0, 8rem);
      }

      .media-slider-1 .information-menu.open.media-slider {
        transform: translate(0, 0);
      }

      .media-slider-1 .info-box.media-slider {
        max-width: 1000px;
            padding: 1.5rem 0;
            margin: 0 auto;
      }

      }</style><!-- Shady DOM styles for desktop-subnav --><style scope="desktop-subnav-1">.desktop-subnav-1 ul.desktop-subnav {
        position: relative;
          z-index: 3;
          padding: .8rem 0;
          margin: 0;
          font-size: 1.2rem;
          text-transform: uppercase;
          text-align: center;
          background: #333;
      }

      .desktop-subnav-1 li.desktop-subnav {
        display: inline-block;
          padding: 0 15px;
      }

      .desktop-subnav-1 a.desktop-subnav {
        text-decoration: none;
          color: #aaa;
      }

      .desktop-subnav-1 a.desktop-subnav:hover,.desktop-subnav-1 a.desktop-subnav:active,.desktop-subnav-1 a.desktop-subnav:focus {
        color: #fff;
      }

      .desktop-subnav-1 .donate.desktop-subnav svg.desktop-subnav {
        width: 16px;
          height: 16px;
          vertical-align: -4px;
          fill: #f00;
      }</style><!-- Shady DOM styles for signed-out-dropdown --><style scope="signed-out-dropdown-1">.signed-out-dropdown-1 {
        ;
      }

      .signed-out-dropdown-1 nav.signed-out-dropdown {
        position: absolute;
          top: -1500px;
          right: 0;
          z-index: 1;
          overflow: hidden;
          font-size: 1.6rem;
          background-color: #333;
          transition-property: top;
          transition-duration: 0.2s;
          transition-timing-function: ease;
      }

      .signed-out-dropdown-1 .initial.signed-out-dropdown,.signed-out-dropdown-1 .closed.signed-out-dropdown {
        top: -1500px;
      }

      .signed-out-dropdown-1 .closed.signed-out-dropdown {
        transition-duration: 0.5s;
      }

      .signed-out-dropdown-1 .open.signed-out-dropdown {
        top: 4rem;
          max-width: 100vw;
          overflow: auto;
      }

      .signed-out-dropdown-1 h3.signed-out-dropdown {
        padding: 0.6rem 2rem;
          margin: 0;
          font-size: inherit;
          overflow: hidden;
          text-overflow: ellipsis;
      }

      .signed-out-dropdown-1 ul.signed-out-dropdown {
        padding: 0.4rem 0 0.7rem 0;
          margin: 0;
          list-style: none;
          
          max-height: calc(100vh - 7.2rem + 1px);
          overflow: auto;
          box-sizing: border-box;
      }

      .signed-out-dropdown-1 .divider.signed-out-dropdown {
        margin: .5rem 0;
          border-bottom: 1px solid #666;
      }

      .signed-out-dropdown-1 a.signed-out-dropdown,.signed-out-dropdown-1 .info-item.signed-out-dropdown {
        display: block;
          color: #fff;
          text-decoration: none;
          padding: 1rem 2rem;
      }

      .signed-out-dropdown-1 .info-item.signed-out-dropdown {
        font-size: .8em;
          color: #999;
      }

      @media (min-width: 890px) {
      .signed-out-dropdown-1 nav.signed-out-dropdown {
        overflow: visible;
            top: calc(100% + 7px);
            left: auto;
            z-index: 5;
            transition: opacity .2s ease-in-out;
            font-size: 1.4rem;
            border-radius: 2px;
            background: #fff;
            box-shadow: 0 1px 2px 1px rgba(0, 0, 0, .15);
      }

      .signed-out-dropdown-1 nav.signed-out-dropdown:after {
        position: absolute;
            right: 7px;
            top: -7px;
            width: 12px;
            height: 7px;
            box-sizing: border-box;
            color: #fff;
            content: "";
            border-bottom: 7px solid currentColor;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
      }

      .signed-out-dropdown-1 h3.signed-out-dropdown {
        display: none;
      }

      .signed-out-dropdown-1 ul.signed-out-dropdown {
        max-height: calc(100vh - 8.5rem + 1px);
      }

      .signed-out-dropdown-1 .divider.signed-out-dropdown {
        border-bottom-color: #666;
      }

      .signed-out-dropdown-1 a.signed-out-dropdown {
        padding: .5rem 2rem;
            color: #333;
            transition: background .1s ease-out, color .1s ease-out;
      }

      .signed-out-dropdown-1 .info-item.signed-out-dropdown {
        padding: .5rem 2rem;
            font-size: .8em;
      }

      .signed-out-dropdown-1 a.signed-out-dropdown:hover,.signed-out-dropdown-1 a.signed-out-dropdown:active,.signed-out-dropdown-1 a.signed-out-dropdown:focus {
        color: #fff;
            background: #428bca;
      }

      .signed-out-dropdown-1 .initial.signed-out-dropdown,.signed-out-dropdown-1 .closed.signed-out-dropdown {
        opacity: 0;
            transition-duration: .2s;
      }

      .signed-out-dropdown-1 .open.signed-out-dropdown {
        top: 5.1rem;
            opacity: 1;
            overflow: visible;
      }

      }

      @media (min-width: 890px) {
      .signed-out-dropdown-1 .initial.signed-out-dropdown,.signed-out-dropdown-1 .closed.signed-out-dropdown,.signed-out-dropdown-1 .open.signed-out-dropdown {
        right: 33.7rem;
      }

      .signed-out-dropdown-1 .search-hidden.initial.signed-out-dropdown,.signed-out-dropdown-1 .search-hidden.closed.signed-out-dropdown,.signed-out-dropdown-1 .search-hidden.open.signed-out-dropdown {
        right: 18.3rem;
      }

      }

      @media (min-width: 990px) {
      .signed-out-dropdown-1 .initial.signed-out-dropdown,.signed-out-dropdown-1 .closed.signed-out-dropdown,.signed-out-dropdown-1 .open.signed-out-dropdown {
        right: 39.7rem;
      }

      .signed-out-dropdown-1 .search-hidden.initial.signed-out-dropdown,.signed-out-dropdown-1 .search-hidden.closed.signed-out-dropdown,.signed-out-dropdown-1 .search-hidden.open.signed-out-dropdown {
        right: 23.5rem;
      }

      }</style><!-- Shady DOM styles for ia-topnav --><style scope="ia-topnav-1">.ia-topnav-1 {
        ;

          color: #fff;
          font-size: 2rem;
          font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
      }

      .ia-topnav-1 #close-layer.ia-topnav {
        display: none;
          position: fixed;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 0;
      }

      .ia-topnav-1 #close-layer.visible.ia-topnav {
        display: block;
      }

      .ia-topnav-1 .topnav.ia-topnav {
        position: relative;
          z-index: 3;
      }

      @media (max-width: 889px) {
      .ia-topnav-1 desktop-subnav.ia-topnav {
        display: none;
      }

      }</style><style>body {transition: opacity ease-in 0.2s; } 
      body[unresolved] {opacity: 0; display: block; overflow: hidden; position: relative; } 
      </style>
          <div class="topnav style-scope ia-topnav">
        <primary-nav class="style-scope ia-topnav x-scope primary-nav-1"><!---->
      <nav class="style-scope primary-nav">
        <a class="link-home style-scope primary-nav" href="https://archive.org" data-event-click-tracking="BetaTopNav|NavHome"><!---->
  <svg class="ia-logo style-scope primary-nav" width="27" height="30" viewBox="0 0 27 30" xmlns="http://www.w3.org/2000/svg" aria-labelledby="logoTitleID logoDescID">
    <title id="logoTitleID" class="style-scope primary-nav">Internet Archive logo</title>
    <desc id="logoDescID" class="style-scope primary-nav">A line drawing of the Internet Archive headquarters building façade.</desc>
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" class="style-scope primary-nav">
      <mask id="mask-2" fill="white" class="style-scope primary-nav">
        <path d="M26.6666667,28.6046512 L26.6666667,30 L0,30 L0.000283687943,28.6046512 L26.6666667,28.6046512 Z M25.6140351,26.5116279 L25.6140351,28.255814 L1.05263158,28.255814 L1.05263158,26.5116279 L25.6140351,26.5116279 Z M3.62469203,7.6744186 L3.91746909,7.82153285 L4.0639977,10.1739544 L4.21052632,13.9963932 L4.21052632,17.6725617 L4.0639977,22.255044 L4.03962296,25.3421929 L3.62469203,25.4651163 L2.16024641,25.4651163 L1.72094074,25.3421929 L1.55031755,22.255044 L1.40350877,17.6970339 L1.40350877,14.0211467 L1.55031755,10.1739544 L1.68423854,7.80887484 L1.98962322,7.6744186 L3.62469203,7.6744186 Z M24.6774869,7.6744186 L24.9706026,7.82153285 L25.1168803,10.1739544 L25.2631579,13.9963932 L25.2631579,17.6725617 L25.1168803,22.255044 L25.0927809,25.3421929 L24.6774869,25.4651163 L23.2130291,25.4651163 L22.7736357,25.3421929 L22.602418,22.255044 L22.4561404,17.6970339 L22.4561404,14.0211467 L22.602418,10.1739544 L22.7369262,7.80887484 L23.0420916,7.6744186 L24.6774869,7.6744186 Z M9.94042303,7.6744186 L10.2332293,7.82153285 L10.3797725,10.1739544 L10.5263158,13.9963932 L10.5263158,17.6725617 L10.3797725,22.255044 L10.3556756,25.3421929 L9.94042303,25.4651163 L8.47583122,25.4651163 L8.0362015,25.3421929 L7.86556129,22.255044 L7.71929825,17.6970339 L7.71929825,14.0211467 L7.86556129,10.1739544 L8.00005604,7.80887484 L8.30491081,7.6744186 L9.94042303,7.6744186 Z M18.0105985,7.6744186 L18.3034047,7.82153285 L18.449948,10.1739544 L18.5964912,13.9963932 L18.5964912,17.6725617 L18.449948,22.255044 L18.425851,25.3421929 L18.0105985,25.4651163 L16.5460067,25.4651163 L16.1066571,25.3421929 L15.9357367,22.255044 L15.7894737,17.6970339 L15.7894737,14.0211467 L15.9357367,10.1739544 L16.0702315,7.80887484 L16.3753664,7.6744186 L18.0105985,7.6744186 Z M25.6140351,4.53488372 L25.6140351,6.97674419 L1.05263158,6.97674419 L1.05263158,4.53488372 L25.6140351,4.53488372 Z M13.0806755,0 L25.9649123,2.93331338 L25.4484139,3.8372093 L0.771925248,3.8372093 L0,3.1041615 L13.0806755,0 Z" id="path-1" class="style-scope primary-nav"></path>
      </mask>
      <use fill="#FFFFFF" xlink:href="#path-1" class="style-scope primary-nav"></use>
      <g mask="url(#mask-2)" fill="#FFFFFF" class="style-scope primary-nav">
        <path d="M0,0 L26.6666667,0 L26.6666667,30 L0,30 L0,0 Z" id="swatch" class="style-scope primary-nav"></path>
      </g>
    </g>
  </svg>
<!---->
  <svg class="ia-wordmark style-scope primary-nav" height="14" viewBox="0 0 183 14" width="183" xmlns="http://www.w3.org/2000/svg"><g fill="#fff" fill-rule="evenodd" class="style-scope primary-nav"><g transform="translate(107.24177)" class="style-scope primary-nav"><path d="m3.46567087 2.73592273c.09358964-.31350476.26547063-.95793121.26547063-1.08718317 0-.73655285-.57818604-.75488647-1.20271694-.86488814v-.40471447h2.93682509l3.81107838 12.38893815h-1.62387035l-1.17212033-3.82897484h-3.98340931l-1.17167038 3.82897484h-.96784292zm-.7032722 5.33783106h3.4520181l-1.78045302-5.37541496z" class="style-scope primary-nav"></path><path d="m13.7607138 2.88304997c0-1.82281935-.0319465-1.82281935-1.4843858-2.09919855v-.40471447h3.8425749c1.1721204 0 4.4050127 0 4.4050127 3.2954667 0 2.09828187-1.1554722 2.79816749-2.3896856 3.03696279l2.874282 6.05650866h-1.7957513l-2.7649442-5.72512863h-.9687428v5.72512863h-1.7183599zm1.7183599 3.29500836h.8909015c1.0146377 0 2.4364804-.33092169 2.4364804-2.39299467 0-1.98873853-.9687428-2.54103858-2.2812476-2.54103858-1.0461343 0-1.0461343.33138003-1.0461343 1.6573585z" class="style-scope primary-nav"></path><path d="m31.6158204 12.2524422c-.5304913.2768376-1.3903462.6998857-2.9989182.6998857-3.389925 0-4.6083902-2.4113283-4.6083902-6.55334954 0-3.35000921.8742533-6.07530059 4.3582178-6.07530059 1.3903462 0 2.4058839.27592086 3.1240044.55230005l.0940396 3.07500503h-.4530999c-.062543-.9395976-.921948-2.743625-2.6088113-2.743625-2.296096 0-2.6713545 2.88983555-2.6713545 5.19162051 0 2.90816916.8598549 5.52254214 3.0304147 5.52254214 1.4843858 0 2.2497511-.5523 2.7338976-.9386809z" class="style-scope primary-nav"></path><path d="m36.1784975 2.80971552c0-1.45568877-.0935896-1.62114962-1.5626771-1.80448573v-.40563116l3.2805871-.42258975v5.70633665h4.2484299v-5.50420858h1.7183599v12.38893815h-1.7183599v-6.01984144h-4.2484299v6.01984144h-1.71791z" class="style-scope primary-nav"></path><path d="m46.8631973 12.3083598c1.1086774 0 1.3273532-.2759209 1.3273532-1.711901v-7.91645358c0-1.43598014-.2186758-1.71144266-1.3273532-1.71144266v-.46109033h4.3735162v.46109033c-1.093829 0-1.3273532.27546252-1.3273532 1.71144266v7.91645358c0 1.4359801.2335242 1.711901 1.3273532 1.711901v.4597153h-4.3735162z" class="style-scope primary-nav"></path><path d="m56.1269544 2.73592273c-.4998947-1.67569211-.6407291-1.67569211-1.8902409-1.95207131v-.40471447h3.0304147l3.0614614 10.08806985h.0310465l2.9840699-10.08806985h1.0146377l-3.8106284 12.38893815h-1.3588497z" class="style-scope primary-nav"></path><path d="m68.8422796 2.88304997c0-1.82281935-.0467948-1.82281935-1.4839359-2.09919855v-.40471447h7.4826722l.0467949 2.7243747h-.4378016c0-1.47264736-.6555775-1.85948657-1.6868634-1.85948657h-1.2184652c-.7806636 0-.9835912.07333445-.9835912.95747287v3.66351396h1.8902409c1.0767309 0 1.1865187-.80942895 1.2805583-1.41672984h.4369017v3.86518369h-.4369017c0-.9941401-.3437619-1.58356571-1.2805583-1.58356571h-1.8902409v4.10535405c0 1.0308073.2029276 1.0679328.9835912 1.0679328h1.7345581c.8895516 0 1.311155-.3685056 1.4051946-1.8040274h.4378015l-.0476947 2.6689156h-6.2322605z" class="style-scope primary-nav"></path></g><g transform="translate(.24177 .277385)" class="style-scope primary-nav"><path d="m1.57302598 10.3699491v-8.03470531c0-1.45706379-.25962128-1.73802639-1.57302598-1.73802639v-.46659042h5.18252665v.46659042c-1.29540668 0-1.57302597.2809626-1.57302597 1.73802639v8.03470531c0 1.4575222.27761929 1.7366514 1.57302597 1.7366514v.4675071h-5.18252665v-.4675071c1.3134047 0 1.57302598-.2791292 1.57302598-1.7366514z" class="style-scope primary-nav"></path><path d="m10.0331719 2.39116131c0-1.73756805-.25872136-1.66240024-1.85064525-1.98003007v-.41113124h3.90556795l5.830455 10.1641543h.0368959v-10.1641543h1.1662709v12.5741076h-1.7773033l-6.1076243-10.63028642h-.0373459v10.63028642h-1.166271z" class="style-scope primary-nav"></path><path d="m25.9161467.87818h-1.2774087c-1.4618883 0-1.8875412.46704876-1.9991288 1.83198615h-.5178928l.0737919-2.71016615h9.4768516l.0742417 2.76562533h-.5183426c0-1.43873018-.5925845-1.88744533-2.0175768-1.88744533h-1.2580608v11.6959276h-2.0364747z" class="style-scope primary-nav"></path><path d="m36.505907 2.54103859c0-1.84986143-.0562438-1.84986143-1.7593054-2.12990735v-.41113124h8.8658192l.0557938 2.76562533h-.5183426c0-1.49510604-.777064-1.88744533-1.9982289-1.88744533h-1.4443403c-.9255476 0-1.1662709.07516781-1.1662709.97213976v3.71805646h2.2398522c1.2769587 0 1.4060944-.82272082 1.5176821-1.43964686h.5187926v3.92430959h-.5187926c0-1.00880698-.406755-1.60648273-1.5176821-1.60648273h-2.2398522v4.16631328c0 1.0463909.2407233 1.0830581 1.1662709 1.0830581h2.0544728c1.0555832 0 1.554578-.3735473 1.6652657-1.83061113h.5192426l-.0557938 2.70879113h-7.3845831z" class="style-scope primary-nav"></path><path d="m48.7037894 2.54103859c0-1.84986143-.0368959-1.84986143-1.7575055-2.12990735v-.41113124h4.5525963c1.3885464 0 5.2194226 0 5.2194226 3.34450912 0 2.13036568-1.3687486 2.8403348-2.8315368 3.08325515l3.4056733 6.14634333h-2.1287145l-3.2756376-5.81083823h-1.1478231v5.81083823h-2.0364747zm2.0364747 3.34405077h1.0555833c1.202267 0 2.8873305-.33550509 2.8873305-2.42782853 0-2.01898899-1.147823-2.57908083-2.7028509-2.57908083-1.2400629 0-1.2400629.33596344-1.2400629 1.68256722z" class="style-scope primary-nav"></path><path d="m62.1435345 2.39116131c0-1.73756805-.2596213-1.66240024-1.8510952-1.98003007v-.41113124h3.905118l5.830005 10.1641543h.0373458v-10.1641543h1.166271v12.5741076h-1.7764035l-6.1085242-10.63028642h-.0368959v10.63028642h-1.165821z" class="style-scope primary-nav"></path><path d="m75.9895846 2.54103859c0-1.84986143-.0553439-1.84986143-1.7584055-2.12990735v-.41113124h8.8658192l.0557938 2.76562533h-.5183426c0-1.49510604-.777964-1.88744533-1.9991289-1.88744533h-1.4438902c-.9250977 0-1.1658211.07516781-1.1658211.97213976v3.71805646h2.2394023c1.2774087 0 1.4060944-.82272082 1.5176821-1.43964686h.5187926v3.92430959h-.5187926c0-1.00880698-.406755-1.60648273-1.5176821-1.60648273h-2.2394023v4.16631328c0 1.0463909.2407234 1.0830581 1.1658211 1.0830581h2.0544727c1.0555832 0 1.5550279-.3735473 1.6661657-1.83061113h.5183426l-.0557938 2.70879113h-7.385033z" class="style-scope primary-nav"></path><path d="m90.2243917.87818h-1.2765088c-1.4623382 0-1.8879911.46704876-1.9995788 1.83198615h-.5178927l.0742418-2.71016615h9.4759517l.0742418 2.76562533h-.5178928c0-1.43873018-.5921344-1.88744533-2.0180267-1.88744533h-1.2585108v11.6959276h-2.0360247z" class="style-scope primary-nav"></path></g></g></svg>
<!----></a>

      <button class="search-trigger style-scope primary-nav" data-event-click-tracking="BetaTopNav|NavSearchOpen">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="searchTitleID searchDescID" class="style-scope primary-nav">
    <title id="searchTitleID" class="style-scope primary-nav">Search icon</title>
    <desc id="searchDescID" class="style-scope primary-nav">An illustration of a magnifying glass.</desc>
    <path class="fill-color style-scope primary-nav" d="m32.4526364 29.8875889-8.1719472-7.9751279c1.1046135-1.4876138 1.7652549-3.3102407 1.7652549-5.2846451 0-.101185-.0142895-.1981539-.030573-.2944743.0166158-.0976175.0309053-.196208.0309053-.2990145 0-4.9814145-4.152935-9.0343271-9.2572866-9.0343271-.0907218 0-.1781206.01394537-.2655193.02594487-.0880633-.0119995-.1747974-.02594487-.2655193-.02594487-5.1046839 0-9.25761889 4.0529126-9.25761889 9.0343271 0 .1011849.01395722.1981539.03057294.2947985-.01694804.0976176-.03090525.1958838-.03090525.2986903 0 4.9814145 4.1526027 9.0346514 9.2572866 9.0346514.0907218 0 .1777882-.0139454.2658516-.0262692.0873987.0123238.1741328.0262692.265187.0262692 1.7306942 0 3.3467399-.4747911 4.7338208-1.2852439l8.2882574 8.0886366c.3652137.3564177.843082.53414 1.3212826.53414.4782007 0 .9567336-.1780467 1.3212827-.53414.7294304-.7118622.7294304-1.8660845-.0003323-2.5782711zm-15.9526364-7.8875889c-.0832667-.0118703-.1652765-.0253024-.2513711-.0253024-2.8781993 0-5.2197212-2.3278242-5.2197212-5.1891862 0-.0974612-.013197-.1908615-.0289077-.2836371.0160249-.0940251.0292219-.1889874.0292219-.2880105 0-2.861362 2.3418361-5.1891861 5.2200354-5.1891861.0854662 0 .1677902-.0131198.2510569-.0246777.0826383.0115579.1649623.0246777.2510569.0246777 2.8781993 0 5.2197212 2.3278241 5.2197212 5.1891861 0 .0974612.0135112.1908616.0289077.2839496-.0157107.0940251-.0295361.1886749-.0295361.287698 0 2.861362-2.3415219 5.1891862-5.2197212 5.1891862-.0860946 0-.1684187.0134321-.2507427.0253024z" fill-rule="evenodd"></path>
  </svg>

      </button>
      <nav-search class="style-scope primary-nav x-scope nav-search-1"><!----><div class="search-activated fade-in search-inactive style-scope nav-search">
      <form id="nav-search" class="highlight style-scope nav-search" method="get" action="https://archive.org/search.php" data-event-submit-tracking="BetaTopNav|NavSearchSubmit">
        <input type="text" name="query" class="search-field style-scope nav-search" placeholder="Search" autocomplete="off">
        <input type="hidden" name="sin" class="style-scope nav-search" value="">
        <button type="submit" class="search style-scope nav-search" data-event-click-tracking="BetaTopNav|NavSearchClose">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="searchTitleID searchDescID" class="style-scope nav-search">
    <title id="searchTitleID" class="style-scope nav-search">Search icon</title>
    <desc id="searchDescID" class="style-scope nav-search">An illustration of a magnifying glass.</desc>
    <path class="fill-color style-scope nav-search" d="m32.4526364 29.8875889-8.1719472-7.9751279c1.1046135-1.4876138 1.7652549-3.3102407 1.7652549-5.2846451 0-.101185-.0142895-.1981539-.030573-.2944743.0166158-.0976175.0309053-.196208.0309053-.2990145 0-4.9814145-4.152935-9.0343271-9.2572866-9.0343271-.0907218 0-.1781206.01394537-.2655193.02594487-.0880633-.0119995-.1747974-.02594487-.2655193-.02594487-5.1046839 0-9.25761889 4.0529126-9.25761889 9.0343271 0 .1011849.01395722.1981539.03057294.2947985-.01694804.0976176-.03090525.1958838-.03090525.2986903 0 4.9814145 4.1526027 9.0346514 9.2572866 9.0346514.0907218 0 .1777882-.0139454.2658516-.0262692.0873987.0123238.1741328.0262692.265187.0262692 1.7306942 0 3.3467399-.4747911 4.7338208-1.2852439l8.2882574 8.0886366c.3652137.3564177.843082.53414 1.3212826.53414.4782007 0 .9567336-.1780467 1.3212827-.53414.7294304-.7118622.7294304-1.8660845-.0003323-2.5782711zm-15.9526364-7.8875889c-.0832667-.0118703-.1652765-.0253024-.2513711-.0253024-2.8781993 0-5.2197212-2.3278242-5.2197212-5.1891862 0-.0974612-.013197-.1908615-.0289077-.2836371.0160249-.0940251.0292219-.1889874.0292219-.2880105 0-2.861362 2.3418361-5.1891861 5.2200354-5.1891861.0854662 0 .1677902-.0131198.2510569-.0246777.0826383.0115579.1649623.0246777.2510569.0246777 2.8781993 0 5.2197212 2.3278241 5.2197212 5.1891861 0 .0974612.0135112.1908616.0289077.2839496-.0157107.0940251-.0295361.1886749-.0295361.287698 0 2.861362-2.3415219 5.1891862-5.2197212 5.1891862-.0860946 0-.1684187.0134321-.2507427.0253024z" fill-rule="evenodd"></path>
  </svg>

        </button>
      </form>
    </div><!----></nav-search>

        <a class="upload style-scope primary-nav" href="https://archive.org/create/">

  <svg width="40" height="41" viewBox="0 0 40 41" xmlns="http://www.w3.org/2000/svg" aria-labelledby="uploadTitleID uploadDescID" class="style-scope primary-nav">
    <title id="uploadTitleID" class="style-scope primary-nav">Upload icon</title>
    <desc id="uploadDescID" class="style-scope primary-nav">An illustration of a horizontal line over an up pointing arrow.</desc>
    <path class="fill-color style-scope primary-nav" d="m20 12.8 8 10.4h-4.8v8.8h-6.4v-8.8h-4.8zm12-4.8v3.2h-24v-3.2z" fill-rule="evenodd"></path>
  </svg>

          <span class="style-scope primary-nav">Upload</span>
        </a>
        <div class="user-info style-scope primary-nav">

      <login-button class="style-scope primary-nav x-scope login-button-1"><!---->
      <div class="logged-out-toolbar style-scope login-button">
        <a class="dropdown-toggle style-scope login-button" data-event-click-tracking="BetaTopNav|NavLoginIcon">

  <svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="userTitleID userDescID" class="style-scope login-button">
    <title id="userTitleID" class="style-scope login-button">User icon</title>
    <desc id="userDescID" class="style-scope login-button">An illustration of a person's head and chest.</desc>
    <path class="fill-color style-scope login-button" d="m20.7130435 18.0434783c-3.5658385 0-6.4565218-2.9198821-6.4565218-6.5217392 0-3.60185703 2.8906833-6.5217391 6.4565218-6.5217391s6.4565217 2.91988207 6.4565217 6.5217391c0 3.6018571-2.8906832 6.5217392-6.4565217 6.5217392zm-12.9130435 16.9565217c0-7.9240855 5.7813665-14.3478261 12.9130435-14.3478261s12.9130435 6.4237406 12.9130435 14.3478261z" fill-rule="evenodd"></path>
  </svg>

        </a>
        <span class="style-scope login-button">
          <a class="style-scope login-button" href="https://archive.org/account/signup">Sign up</a>
          |
          <a class="style-scope login-button" href="https://archive.org/account/login">Log in</a>
        </span>
      </div>
    <!----></login-button>

        </div>
        <media-menu class="style-scope primary-nav x-scope media-menu-1" tabindex="-1"><!---->
      <nav class="media-menu tx-slide closed style-scope media-menu" aria-hidden="true" aria-expanded="false">
        <div class="menu-group style-scope media-menu">
          <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="web"><!---->
      <a class="menu-item web  style-scope media-button" href="https://archive.org/web/" data-event-click-tracking="BetaTopNav|NavMenuWeb">

      <span class="icon  style-scope media-button">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="webTitleID webDescID" class="style-scope media-button">
    <title id="webTitleID" class="style-scope media-button">Web icon</title>
    <desc id="webDescID" class="style-scope media-button">An illustration of a computer application window</desc>
    <path class="fill-color style-scope media-button" d="m8 28.7585405v-8.1608108-9.3577297h24v9.3577297 8.1608108zm14.2702703-15.8863783h-12.43243246v2.6114594h12.43243246zm7.7837838 14.0365946v-7.0727027-1.8497838h-20.21621626v1.8497838 7.0727027zm-3.7837838-14.0365946h-2.7027027v2.6114594h2.7027027zm4 0h-2.7027027v2.6114594h2.7027027z" fill-rule="evenodd"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Wayback Machine<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="texts"><!---->
      <a class="menu-item texts  style-scope media-button" href="https://archive.org/details/texts" data-event-click-tracking="BetaTopNav|NavMenuTexts">

      <span class="icon  style-scope media-button">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="textsTitleID textsDescID" class="style-scope media-button">
    <title id="textsTitleID" class="style-scope media-button">Texts icon</title>
    <desc id="textsDescID" class="style-scope media-button">An illustration of an open book.</desc>
    <path class="fill-color style-scope media-button" d="m10.3323235 11.0007023h6.9060825c.8851083 0 1.5847122.3064258 2.0988114.9192774v14.4324451h-.6460032c-.1435563-.120323-.3528315-.2434552-.6278257-.3693964-.2749942-.1259413-.5201585-.2191097-.7354929-.2795053l-.3048241-.1081503h-5.7042647c-.3108832 0-.5621067-.0601615-.7536705-.1804846-.0717781-.0599274-.1256117-.1439663-.1615008-.2521166-.0358891-.1081502-.0598928-.2043619-.0720112-.2886348v-13.8741368zm19.1752505 0v13.603761c-.0717781.3361555-.2211606.5943584-.4481473.7746089-.0717781.0599274-.1733862.1079162-.304824.1439663-.1314379.0360501-.2451643.0601615-.3411793.0723343h-5.5965975c-.9568865.2640552-1.5068748.5164059-1.649965.757052h-.6634817v-14.4324451c.5140992-.6128516 1.2076439-.9192774 2.0806339-.9192774h6.92426zm1.3814961.6489017-.1796783 15.2976474c-.0955489 0-1.0342578.0119386-2.8161268.035816-1.7818691.0238773-3.3006293.0898911-4.5562806.1980414-1.2556514.1081503-1.9613144.2884008-2.1169891.5407514-.0955488.1924233-.5439291.273419-1.345141.2429871-.8012118-.0304319-1.3155441-.1776755-1.5429969-.4417308-.334654-.3843783-3.4558378-.5765674-9.36355164-.5765674v-15.3875385l-.96830576.3960828v16.2702977c6.4096947-.2041278 9.7760429-.0840388 10.0990445.3602669.2391051.276228.9864833.414342 2.2421347.414342.1915638 0 .4187835-.0210682.6816593-.0632047s.4810068-.0870821.6543929-.1348367c.1733862-.0477547.2719646-.0838048.2957353-.1081503.0838965-.1563732.9599161-.2675666 2.6280587-.3335805 1.6681426-.0660138 3.3213703-.0931684 4.9596831-.0814638l2.4392915.0182591v-16.2344816z"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Books<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="video"><!---->
      <a class="menu-item video  style-scope media-button" href="https://archive.org/details/movies" data-event-click-tracking="BetaTopNav|NavMenuVideo">

      <span class="icon  style-scope media-button">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="videoTitleID videoDescID" class="style-scope media-button">
    <title id="videoTitleID" class="style-scope media-button">Video icon</title>
    <desc id="videoDescID" class="style-scope media-button">An illustration of two cells of a film strip.</desc>
    <path class="fill-color style-scope media-button" d="m31.0117647 12.0677966c0 .4067797-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711864-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067797-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711864-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711865-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.3898305c0 .4067797-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711864-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711865-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.2711865-.7058823-.6779661v-.6779661c0-.4067797.2823529-.6779661.7058823-.6779661h1.2705883c.4235294 0 .7058823.2711864.7058823.6779661zm-4.0941176-10.440678c0 .5423729-.4235295.9491525-.9882353.9491525h-11.5764706c-.5647059 0-.9882353-.4067796-.9882353-.9491525v-6.9152542c0-.5423729.4235294-.9491526.9882353-.9491526h11.5764706c.5647058 0 .9882353.4067797.9882353.9491526zm-.1411765 11.2542373c0 .5423729-.4235294.9491525-.9882353.9491525h-11.5764706c-.5647059 0-.9882353-.4067796-.9882353-.9491525v-6.9152542c0-.5423729.4235294-.9491526.9882353-.9491526h11.5764706c.5647059 0 .9882353.4067797.9882353.9491526zm-14.9647059-17.220339c0 .4067797-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711864-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067797-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711864-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711865-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.3898305c0 .4067797-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711864-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711865-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm0 3.2542373c0 .4067796-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.2711865-.70588236-.6779661v-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661zm20.0470588-20.4745763h-.8470588v.27118644.6779661c0 .40677966-.2823529.6779661-.7058823.6779661h-1.2705883c-.4235294 0-.7058823-.27118644-.7058823-.6779661v-.6779661-.27118644h-16.5176471v.27118644.6779661c0 .40677966-.2823529.6779661-.7058823.6779661h-1.27058828c-.42352941 0-.70588236-.27118644-.70588236-.6779661v-.6779661-.27118644h-1.12941176v24h1.12941176v-.2711864-.6779661c0-.4067797.28235295-.6779661.70588236-.6779661h1.27058828c.4235294 0 .7058823.2711864.7058823.6779661v.6779661.2711864h16.6588235v-.2711864-.6779661c0-.4067797.282353-.6779661.7058824-.6779661h1.2705882c.4235294 0 .7058824.2711864.7058824.6779661v.6779661.2711864h.8470588v-24z" fill-rule="evenodd"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Video<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="audio"><!---->
      <a class="menu-item audio  style-scope media-button" href="https://archive.org/details/audio" data-event-click-tracking="BetaTopNav|NavMenuAudio">

      <span class="icon  style-scope media-button">

  <svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" aria-labelledby="audioTitleID audioDescID" class="style-scope media-button">
    <title id="audioTitleID" class="style-scope media-button">Audio icon</title>
    <desc id="audioDescID" class="style-scope media-button">An illustration of an audio speaker.</desc>
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" class="style-scope media-button">
      <g transform="translate(10, 8)" class="fill-color style-scope media-button">
        <path d="M19.4264564,11.8585048 L19.4264564,20.7200433 C19.4264564,22.3657576 18.8838179,23.2519114 16.8489237,23.2519114 C12.2364969,23.125318 7.75972977,23.125318 3.14730298,23.2519114 C1.24806842,23.2519114 0.569770368,22.492351 0.569770368,20.7200433 L0.569770368,2.74377955 C0.569770368,1.09806526 1.11240881,0.211911416 3.14730298,0.211911416 C7.75972977,0.338504822 12.2364969,0.338504822 16.8489237,0.211911416 C18.7481583,0.211911416 19.4264564,0.971471855 19.4264564,2.74377955 C19.2907967,5.78202131 19.4264564,8.82026306 19.4264564,11.8585048 L19.4264564,11.8585048 Z M10.0659432,2.74377955 C8.16670861,2.74377955 6.67445288,4.13630702 6.67445288,5.90861471 C6.67445288,7.6809224 8.16670861,9.07344988 10.0659432,9.07344988 C11.9651777,9.07344988 13.4574335,7.6809224 13.4574335,5.90861471 C13.4574335,4.13630702 11.8295181,2.74377955 10.0659432,2.74377955 L10.0659432,2.74377955 Z M10.0659432,11.4787246 C7.21709133,11.4787246 5.04653754,13.6308125 5.04653754,16.1626806 C5.04653754,18.8211422 7.35275094,20.8466367 10.0659432,20.8466367 C12.914795,20.8466367 15.0853488,18.6945488 15.0853488,16.1626806 C15.0853488,13.6308125 12.7791354,11.4787246 10.0659432,11.4787246 L10.0659432,11.4787246 Z" class="style-scope media-button"></path>
        <ellipse cx="10.2016028" cy="16.5690777" rx="1.35659611" ry="1.34075134" class="style-scope media-button"></ellipse>
      </g>
    </g>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Audio<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="software"><!---->
      <a class="menu-item software  style-scope media-button" href="https://archive.org/details/software" data-event-click-tracking="BetaTopNav|NavMenuSoftware">

      <span class="icon  style-scope media-button">

  <svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="softwareTitleID softwareDescID" class="style-scope media-button">
    <title id="softwareTitleID" class="style-scope media-button">Software icon</title>
    <desc id="softwareDescID" class="style-scope media-button">An illustration of a 3.5" floppy disk.</desc>
    <path class="fill-color style-scope media-button" d="m32 30.6900373v-21.44521088c0-.82988428-.4156786-1.24482642-1.2470357-1.24482642h-21.50592858c-.83135715 0-1.24703572.4221795-1.24703572 1.26653851v21.44521089c0 .8588337.41567857 1.2882506 1.24703572 1.2882506h21.48327168c.8458575 0 1.2687863-.4366542 1.2687863-1.3099627zm-5.9950155-20.4410268v6.114667c0 .6694561-.3428744 1.0041841-1.0286232 1.0041841h-10.1294464c-.2622159 0-.4773054-.0802141-.6452685-.2406423s-.2519447-.3642806-.2519447-.6115572v-6.1363791l.0217506-.1311772h12.0326259zm-4.9437353.8295827v5.0010178h3.0405558v-5.0010178zm-9.7134658 18.8035735v-7.753025c0-.5241057.1604108-.9025595.4812325-1.1353613.1897138-.1453504.4011782-.2180256.6343932-.2180256h14.7451099c.3208217 0 .5905898.1091636.8093044.3274907s.3280719.5023936.3280719.8521995v7.8181612l-.0217506.1094652h-16.9772676z"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Software<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="images"><!---->
      <a class="menu-item images  style-scope media-button" href="https://archive.org/details/image" data-event-click-tracking="BetaTopNav|NavMenuImages">

      <span class="icon  style-scope media-button">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="imagesTitleID imagesDescID" class="style-scope media-button">
    <title id="imagesTitleID" class="style-scope media-button">Images icon</title>
    <desc id="imagesDescID" class="style-scope media-button">An illustration of two photographs.</desc>
    <path class="fill-color style-scope media-button" d="m20.8219178 15.3769871c0 1.1136708-.8767123 1.8932404-1.8630137 1.8932404s-1.9726027-.8909367-1.9726027-1.8932404c0-1.0023038.8767123-1.8932404 1.9726027-1.8932404.9863014 0 1.8630137.8909366 1.8630137 1.8932404zm-5.9178082-3.7864808h15.4520548v6.0138225l-1.9726028-3.3410125-2.6301369 6.3479237-2.1917809-2.67281-6.1369863 5.1228859h-2.5205479zm-1.7534247-1.6705063v14.9231892h18.8493151v-14.9231892zm-2.9589041 7.2388604c.2191781 0 1.9726028-.3341012 1.9726028-.3341012v-2.0046075l-4.1643836.5568354c.43835616 4.7887846.87671233 9.9116704 1.31506849 14.700455 6.02739731-.5568354 13.26027401-1.5591391 19.39726031-2.1159746-.1095891-.5568354-.1095891-2.0046075-.2191781-2.67281-.4383562.1113671-1.4246575 0-1.8630137.1113671v.8909367c-5.1506849.4454683-10.3013699 1.1136708-15.4520548 1.6705062.109589-.111367-.5479452-7.0161262-.9863014-10.8026071z" fill-rule="evenodd"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Images<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="donate"><!---->
      <a class="menu-item donate  style-scope media-button" href="https://archive.org/donate/" data-event-click-tracking="BetaTopNav|NavMenuDonate">

      <span class="icon  style-scope media-button">

  <svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="donateTitleID donateDescID" class="style-scope media-button">
    <title id="donateTitleID" class="style-scope media-button">Donate icon</title>
    <desc id="donateDescID" class="style-scope media-button">An illustration of a heart shape</desc>
    <path class="fill-color style-scope media-button" d="m30.0120362 11.0857287c-1.2990268-1.12627221-2.8599641-1.65258786-4.682812-1.57894699-.8253588.02475323-1.7674318.3849128-2.8262192 1.08047869-1.0587873.6955659-1.89622 1.5724492-2.512298 2.63065-.591311-1.0588196-1.4194561-1.9357029-2.4844351-2.63065-1.0649791-.69494706-2.0039563-1.05510663-2.8169316-1.08047869-1.2067699-.04950647-2.318187.17203498-3.3342513.66462439-1.0160643.4925893-1.82594378 1.2002224-2.42963831 2.1228992-.60369453.9226769-.91173353 1.9629315-.92411701 3.1207641-.03715043 1.9202322.70183359 3.7665141 2.21695202 5.5388457 1.2067699 1.4035084 2.912594 3.1606786 5.1174721 5.2715107 2.2048782 2.1108321 3.7565279 3.5356901 4.6549492 4.2745742.8253588-.6646243 2.355647-2.0647292 4.5908647-4.2003145s3.9747867-3.9171994 5.218707-5.3448422c1.502735-1.7723316 2.2355273-3.6186135 2.1983769-5.5388457-.0256957-1.7608832-.6875926-3.2039968-1.9866194-4.3302689z"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->Donate<!----></span>

      </a>
    <!----></media-button>
      <!---->
        <media-button class="style-scope media-menu x-scope media-button-1" data-mediatype="more"><!---->
      <a class="menu-item more  style-scope media-button" href="https://archive.org/about/" data-event-click-tracking="BetaTopNav|NavMenuMore">

      <span class="icon  style-scope media-button">

  <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="ellipsesTitleID ellipsesDescID" class="style-scope media-button">
    <title id="ellipsesTitleID" class="style-scope media-button">Ellipses icon</title>
    <desc id="ellipsesDescID" class="style-scope media-button">An illustration of text ellipses.</desc>
    <path class="fill-color style-scope media-button" d="m10.5 17.5c1.3807119 0 2.5 1.1192881 2.5 2.5s-1.1192881 2.5-2.5 2.5c-1.38071187 0-2.5-1.1192881-2.5-2.5s1.11928813-2.5 2.5-2.5zm9.5 0c1.3807119 0 2.5 1.1192881 2.5 2.5s-1.1192881 2.5-2.5 2.5-2.5-1.1192881-2.5-2.5 1.1192881-2.5 2.5-2.5zm9.5 0c1.3807119 0 2.5 1.1192881 2.5 2.5s-1.1192881 2.5-2.5 2.5-2.5-1.1192881-2.5-2.5 1.1192881-2.5 2.5-2.5z" fill-rule="evenodd"></path>
  </svg>

      </span>
      <span class="label style-scope media-button"><!---->More<!----></span>

      </a>
    <!----></media-button>
      <!---->
        </div>
      </nav>
    <!----></media-menu>
        <button class="hamburger style-scope primary-nav" tabindex="1" data-event-click-tracking="BetaTopNav|NavHamburger">
          <icon-hamburger class="style-scope primary-nav x-scope icon-hamburger-1"><!---->
      <svg height="40" viewBox="0 0 40 40" width="40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="hamburgerTitleID hamburgerDescID" class="style-scope icon-hamburger">
        <title id="hamburgerTitleID" class="style-scope icon-hamburger">Hamburger icon</title>
        <desc id="hamburgerDescID" class="style-scope icon-hamburger">An icon used to represent a menu that can be toggled by interacting with this icon.</desc>
        <path d="m30.5 26.5c.8284271 0 1.5.6715729 1.5 1.5s-.6715729 1.5-1.5 1.5h-21c-.82842712 0-1.5-.6715729-1.5-1.5s.67157288-1.5 1.5-1.5zm0-8c.8284271 0 1.5.6715729 1.5 1.5s-.6715729 1.5-1.5 1.5h-21c-.82842712 0-1.5-.6715729-1.5-1.5s.67157288-1.5 1.5-1.5zm0-8c.8284271 0 1.5.6715729 1.5 1.5s-.6715729 1.5-1.5 1.5h-21c-.82842712 0-1.5-.6715729-1.5-1.5s.67157288-1.5 1.5-1.5z" fill="#999" fill-rule="evenodd" class="style-scope icon-hamburger"></path>
      </svg>
    <!----></icon-hamburger>
        </button>
      </nav>
    <!----></primary-nav>
        <media-slider class="style-scope ia-topnav x-scope media-slider-1"><!---->
      <div class="overflow-clip closed style-scope media-slider">
        <div class="information-menu closed style-scope media-slider">
          <div class="info-box style-scope media-slider">
            <media-subnav class="style-scope media-slider x-scope media-subnav-1"><!----><!----></media-subnav>
          </div>
        </div>
      </div>
    <!----></media-slider>
      </div>
      <desktop-subnav class="style-scope ia-topnav x-scope desktop-subnav-1"><!---->
      <ul class="style-scope desktop-subnav">
        <!---->
        <li class="style-scope desktop-subnav">
          <a class="about style-scope desktop-subnav" href="https://archive.org/about/"><!---->About<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="blog style-scope desktop-subnav" href="https://blog.archive.org/"><!---->Blog<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="projects style-scope desktop-subnav" href="https://archive.org/projects/"><!---->Projects<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="help style-scope desktop-subnav" href="https://archive.org/about/faqs.php"><!---->Help<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="donate style-scope desktop-subnav" href="https://archive.org/donate/"><!---->Donate<!---->
  <svg width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" aria-labelledby="donateTitleID donateDescID" class="style-scope desktop-subnav">
    <title id="donateTitleID" class="style-scope desktop-subnav">Donate icon</title>
    <desc id="donateDescID" class="style-scope desktop-subnav">An illustration of a heart shape</desc>
    <path class="fill-color style-scope desktop-subnav" d="m30.0120362 11.0857287c-1.2990268-1.12627221-2.8599641-1.65258786-4.682812-1.57894699-.8253588.02475323-1.7674318.3849128-2.8262192 1.08047869-1.0587873.6955659-1.89622 1.5724492-2.512298 2.63065-.591311-1.0588196-1.4194561-1.9357029-2.4844351-2.63065-1.0649791-.69494706-2.0039563-1.05510663-2.8169316-1.08047869-1.2067699-.04950647-2.318187.17203498-3.3342513.66462439-1.0160643.4925893-1.82594378 1.2002224-2.42963831 2.1228992-.60369453.9226769-.91173353 1.9629315-.92411701 3.1207641-.03715043 1.9202322.70183359 3.7665141 2.21695202 5.5388457 1.2067699 1.4035084 2.912594 3.1606786 5.1174721 5.2715107 2.2048782 2.1108321 3.7565279 3.5356901 4.6549492 4.2745742.8253588-.6646243 2.355647-2.0647292 4.5908647-4.2003145s3.9747867-3.9171994 5.218707-5.3448422c1.502735-1.7723316 2.2355273-3.6186135 2.1983769-5.5388457-.0256957-1.7608832-.6875926-3.2039968-1.9866194-4.3302689z"></path>
  </svg>
<!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="contact style-scope desktop-subnav" href="https://archive.org/about/contact.php"><!---->Contact<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="jobs style-scope desktop-subnav" href="https://archive.org/about/jobs.php"><!---->Jobs<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="volunteer style-scope desktop-subnav" href="https://archive.org/about/volunteerpositions.php"><!---->Volunteer<!----><!----></a>
        </li>
      <!---->
        <li class="style-scope desktop-subnav">
          <a class="people style-scope desktop-subnav" href="https://archive.org/about/bios.php"><!---->People<!----><!----></a>
        </li>
      <!---->
      </ul>
    <!----></desktop-subnav>
      <search-menu class="style-scope ia-topnav x-scope search-menu-1" tabindex="-1"><!---->
      <div class="search-menu tx-slide closed style-scope search-menu" aria-hidden="true" aria-expanded="false">
        <!---->
        <label class="style-scope search-menu">
          <input form="nav-search" type="radio" name="sin" class="style-scope search-menu" value="" checked="">
          Search Metadata
        </label>
      <!---->
        <label class="style-scope search-menu">
          <input form="nav-search" type="radio" name="sin" class="style-scope search-menu" value="TXT">
          Search text contents
        </label>
      <!---->
        <label class="style-scope search-menu">
          <input form="nav-search" type="radio" name="sin" class="style-scope search-menu" value="TV">
          Search TV news captions
        </label>
      <!----><!---->
        <label class="style-scope search-menu">
          <input form="nav-search" type="radio" name="sin" class="style-scope search-menu" value="WEB">
          Search archived websites
        </label>
      <!---->
        <a class="advanced-search style-scope search-menu" href="https://archive.org/advancedsearch.php" data-event-click-tracking="BetaTopNav|NavAdvancedSearch">Advanced Search</a>
      </div>
    <!----></search-menu>

      <signed-out-dropdown class="style-scope ia-topnav x-scope signed-out-dropdown-1" tabindex="-1"><!---->
      <nav class="initial style-scope signed-out-dropdown" aria-hidden="true" aria-expanded="false">
        <ul class="style-scope signed-out-dropdown">
          <!---->
        <li class="style-scope signed-out-dropdown"><!----><a class="style-scope signed-out-dropdown" href="https://archive.org/account/signup" data-event-click-tracking="BetaTopNav|NavSignUpDropdown"><!---->Sign up for free<!----></a><!----></li>
      <!---->
        <li class="style-scope signed-out-dropdown"><!----><a class="style-scope signed-out-dropdown" href="https://archive.org/account/login" data-event-click-tracking="BetaTopNav|NavLogInDropdown"><!---->Log in<!----></a><!----></li>
      <!---->
        </ul>
      </nav>
    <!----></signed-out-dropdown>

    </noscript>
          </ia-topnav>
        </div>
            <input class="js-tvsearch" type="hidden" value='{"ands":[],"minday":"06/04/2009","maxday":"07/19/2020"}'/>
    
        <!-- Begin page content -->
        <main id="maincontent">
                    <div class="container container-ia">

                  <h1>
          Full text of "<a href="/details/WarAndPeace_201804">War And Peace</a>"
        </h1>
        <h2 class="pull-right">
          <small><a href="/details/WarAndPeace_201804">See other formats</a></small>
        </h2>
        <br class="clearfix" clear="right"/>
      <pre>A Primeira Guerra Mundial. 
Na Batalha de La Lys 

Judite Gonsalves de Freitas, Joao Casqueira Cardoso e Pedro Reis 





A Primeira Guerra Mundial. 
Na Batalha de La Lys 


Judite Gon9alves de Freitas, Joao Casqueira Cardoso e Pedro Reis 

(EDITORES) 


Porto - 2019 


FICHATECNICA 

Tl'tulo: 

A Primeira Guerra Mundial. Na Batalha de La Lys 

Editores: 

Judite Gonsalves de Freitas, Joao Casqueira Cardoso e Pedro Reis 

© 2019 - Universidade Fernando Pessoa Edi(;ao 
Suporte (Formato): Eletrdnico (PDF) 

Edi9oes Universidade Fernando Pessoa 

Praqa 9 de Abril, 349 • 4249-004 Porto 

Tlf. +351 22 507 1300 • Fax. +351 22 550 8269 

E-mail: edicoes@ufp.edu.pt ■ https://edicoes.ufp.pt/ 

Capa e Paginagao: Ofieina Grafica da UFP 
ISBN: 978-989-643-151-8 

CATALOGAgAO NA PUBLICAgAO 

A Primeira Guerra Mundial [Doeumento eletrdnico] : Na Batalha de 
Fa Lys / eds. Judite Gonpalves de Freitas, Joao Casqueira Cardoso, 
Pedro Reis. - eBook. - Porto : Ediqoes Universidade Fernando Pessoa, 
2019. - 269 p. 

ISBN 978-989-643-151-8 

Flistdria - Guerra Mundial - 1914-1918 / Batalha de La Lys - 1918 / 
Portugal - Histdria Militar 

GDU 94(100) 

94(469) 

355.48 


A Primeira Guerra Mundial. 
Na Batalha de La Lys 


Judite Gon9alves de Freitas, Joao Casqueira Cardoso e Pedro Reis 

(EDITORES) 


ediQoes UNIVERSIDADE FERNANDO PESSOA 



oil 

017 

023 

029 

031 

043 

055 

065 

083 

101 

109 

111 


Indice geral 

Apresentagao 

Presentation 

Avant-propos 

I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 

Helmut Bley 

The Impact of World War I on the non-European World 

Luis Alves de Fraga 

Portugal e a beliger^cia na Grande Guerra: Razdes de uma interven9ao 

Claudia Ramos 

Do paradigma da guerra ao paradigma da paz: O tratado de Versalhes 
e a genese de organiza9des internacionais intergovernamentais 

Antonio Fernando Cascais 

O gaseamento na imagem medica e na memoria 
historica da Primeira Guerra Mundial 

Gongalo Paixao 

O Servi90 Veterinario Militar na Primeira Guerra Mundial 

Teresa Toldy 

Ethical challenges posed hy faceless wars. In memory of a 
combatant in La Lys 

II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


Guilhermina Mota 

A Batalha de La Lys e a sua historia 


123 Luis Alves de Fraga 

O CEP na Batalha de La Lys: O hm de um objeetivo poh'tieo naeional 

135 Joao Casqueira Cardoso 

The Battle of La Lys: legal, politieal and soeial questions 

147 Manuel do Nascimento 

La partieipation du Portugal dans la Premiere Guerre mondiale 

159 III. A GUERRA NAS COLONIAS 

161 Helmut Bley 

German Mittel-Afriea plans and the eolonial 
situation in Angola and Mozambique 

171 Jose Antonio Rodrigues Pereira 

A Marinha na Grande Guerra. A Defesa Man'tima 
das Ilhas de Cabo Verde (1914-1918) 

185 Jose Soares Martins 

Para la do Rovuma mandam os que la estao: ou o desastre militar 
em Mo 9 ambique durante a primeira guerra mundial de 14-18 

191 IV. A HISTORIOGRAFIA DA GUERRA 

193 Antonio Paulo Duarte 

As imagens da 1“ Guerra Mundial, na Historia 
e na Historiograba, em Portugal 

207 Judite Gongalves de Freitas 

The war in History and the History of war: historiographie 
probles in the syntheses of Portugal 

215 Jorge Pedro Sousa 

Portugal na Guerra: uma revista de infopropaganda 
223 V POSTERS 
231 VI.OUTROS 


233 Joaquim Castro 

Phenomenological exploration of emigration and acculturation: 

War and peace between the individual position and States 

251 Mehdi Jendoubi 

La chute de 1’Empire ottoman et ses consequences pour le Proche-Orient 

261 Aurore Rouffelaers 

Amours suspendues: Correspondances de guerre 

267 Aurore Rouffelaers 

Exposition Racine: 19 memoires vivantes 



Apresenta 9 ao 


O livro presentemente disponibilizado em formato e-book, por op^ao dos editores, cons- 
titui o resultado dos trabalhos de autor apresentados e desenvolvidos no ambito do Con- 
gresso Internacional sobre a Primeira Guerra Mundial. No centendrio da Batalha de la 
Lys que reuniu, nas instala^oes da Faculdade de Ciencias Humanas e Sociais da Universi- 
dade Fernando Pessoa, nos passados dias 9 a 11 de abril de 2018, mais de duas dezenas de 
investigadores nacionais e estrangeiros com o intuito de proceder a uma analise multidis- 
ciplinar dos motivos, implica^oes, ocorrencias e analises de um dos maiores acontecimen- 
tos belicos da primeira metade do seculo XX. O congresso foi desenvolvido em associa^ao 
com o Instituto da Defesa Nacional. 

A estrutura de apresenta^ao dos textos obedece a uma subdivisao organica e tematica, 
partindo de unidades tematicas de ambito geral e problematizante para unidades tematicas 
especiftcas relacionadas, mormente com o contexto, o ambito e as dimensoes da partici- 
pa^ao portuguesa na Batalha de La Lys, estabelecendo uma rela^ao de complementarida- 
de entre ambas. Por conseguinte, a ordem de apresenta^ao principia com um I capitulo 
intitulado “A problematiza^ao da Primeira Grande Guerra” e um II capitulo, especibca- 
mente dedicado a participa^ao de “Portugal na Guerra - A Batalha de La Lys”. De igual 
modo, no III capitulo, intitulado “A guerra nas colonias” inserem-se todos os trabalhos 
que abordam, nomeadamente, a proje^ao do conflito nos dominios africanos portugueses, 
com destaque para a questao esclavagista e as amea^as angldfonas e germanicas, o papel de 
defesa da armada portuguesa em Cabo Verde, e, ftnalmente, a intimida^ao alema na regiao 
de Tanganica (Africa Oriental). 

OIV capitulo e inteiramente dedicado a historiograba da guerra, incluindo estudos sobre 
a constru^ao das imagens da participa^ao de Portugal no conflito, as mudan^as concetuais 
e as direcjoes investigativas do discurso historiograbco, ao longo do seculo XX, e por ulti¬ 
mo, as imagens da guerra na imprensa portuguesa. 

No V capitulo inserem-se os posters apresentados a congresso, abordando respetiva- 
mente o papel da Cruz Vermelha e o impacto da Primeira Guerra Mundial na cidade de 
Porto. Para fechar, no ultimo capitulo (Vl), foram reunidos todos os trabalhos que abor¬ 
dam problematicas diversas relacionadas com o fendmeno guerra, mas que se encontram 
de algum modo mais distanciadas do nucleo tematico principal do livro. 


Tratando-se de um encontro cientifico de ambito internacional foi concedida total liber- 
dade aos autores para redigirem os textos ftnais numa das tres Imguas obciais do encontro 
(portugues, frances on ingles). 


O primeiro texto de Helmut Bley precede a uma abordagem problematizante da guerra 
como um fenomeno mundial, matizando o fenomeno com o crescimento do fervor nacio- 
nalista e a crise do imperialismo europeu nos continentes africano e asiatico; prosseguindo 
com a analise das repercussoes da Revolu^ao Russa de 1917 no mundo, o impacto da guer¬ 
ra nas popula^oes civis europeias e extraeuropeias, bem como a instabilidade causada no 
Proximo Oriente. Por seu turno, Luis Alves de Fraga consigna o seu texto a avalia^ao das 
condicionantes internas e externas da entrada de Portugal na guerra, analisando especial- 
mente as dependencias pollticas de Portugal, mormente da Gra-Bretanha, e o malogro da 
polltica de beligerancia nacional na sequencia da falta de apoio britanico. Claudia Ramos 
salienta as contradi^oes do primeiro quartel do seculo XX, entre o legado de conflito aci- 
catado pelas crescentes rivalidades entre as principals potencias europeias e o delinear de 
uma nova ordem internacional no pds-guerra pautada por uma matriz de dialogo, enten- 
dimento e coopera^ao. Antonio Fernando de Caseais aflora a questao do uso pioneiro de ar- 
mamento quimico e biologico e as suas vertentes traumaticas e duradouras, quer do ponto 
de vista humano imediato quer do ponto de vista memorialistico e reincidente posterior. 
Por seu turno, Gon9alo Paixao procede a analise dos servi^os veterinarios do Exercito Por¬ 
tugues recorrendo a fontes do Arquivo Militar. Descrevendo os contingentes e as condi^oes 
de envio dos solipedes para as zonas de guerra na Europa e na Africa Portuguesa, esmiii^a 
as razoes da falta de organiza^ao dos servi^os veterinarios. No final deste capftulo, Teresa 
Toldy, em homenagem ao seu avo, combatente e sobrevivente da Batalha de La Lys, pro¬ 
cede a uma reflexao sobre as questoes eticas levantadas pela altera^ao dos padroes belicos, 
pelo recurso a mecanismos de “morte a disttacia”, mormente com os bombardeamen- 
tos aereos e a utiliza^ao de armas qufmicas (gaseamentos) que permitiram uma destrui^ao 
massiva de soldados e “pessoas sem rosto”. 

No primeiro texto do segundo capftulo, Guilhermina Mota disserta sobre a historia da 
Batalha de La Lys, sublinhando os aspetos da historiografta portuguesa acerca deste acon- 
tecimento. A autora enquadra esses aspetos, e contrasta os mesmos com elementos obje- 
tivos, documentos e evidencias conhecidas agora. A Batalha de La Lys, no dia 9 de abril de 
1918 e os dias seguintes, inseriu-se num conjunto de ofensivas alemas do final da Primeira 
guerra mundial, nas Elandres, nomeadamente na opera^ao alema chamada “Georgette”. 
A ofensiva foi caracterizada por durfssimos ataques nas frentes de combate, por parte de 
tropas bem treinadas e bem equipadas, contrariamente ao Exercito Portugues. As circuns- 
tancias que precederam a Batalha de La Lys, como as alteraqoes taticas que diziam respeito 
ao Gontingente Expedicionario Portugues, sao descritas e analisadas com mimicia. A du- 
reza da Batalha de La Lys e af patente, em particular quando comparada com outras opera- 
^Qes semelhantes. Luis Alves de Fraga, concentrando o olhar sobre as condi^oes polfticas, 
economicas e sociais do envolvimento portugues na Primeira Guerra Mundial, demonstra 
a forma como o Gontingente Expedicionario Portugues estava, de facto, abandonado pelo 
poder politico, no periodo em que a Batalha de La Lys ocorreu. Em Portugal, ja havia uma 


12 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


reticencia quase generalizada a entrada do pais na guerra. Apesar da propaganda a favor 
da entrada na guerra, a situa^ao nao foi alterada de forma substancial no prazo de um ano, 
entre abril f9f7 e abril f9f8. O proprio contexto da Batalha de La Lys, em que o Exercito 
Alemao eonhecia ao pormenor as fraquezas do Contingente Expedieionario Portugues, e o 
visou o mesmo especifteamente, expliea o mimero extremamente elevado de prisioneiros 
portugueses, assim o facto de, na fase pos-conflito - em que os soldados portugueses foram 
integrados nas unidades britanicas - ter sido contmuo o abandono politico dos soldados 
portugueses, e da propria guerra como um dos objetivos nacionais. Joao Casqueira Car¬ 
doso destaca alguns aspetos da participa^ao portuguesa na Primeira Guerra Mundial. O 
Exercito Portugues nao estava preparado para este tipo de conflito, nomeadamente num 
cenario europeu, devido ao atraso da sua forma^ao e do seu equipamento. Naquilo que 
iria constituir um exemplo das primeiras guerras modernas, o Contingente Expedieionario 
Portugues encontrou-se rapidamente ultrapassado. Para alem disso, a motiva^ao dos sol¬ 
dados, e dos seus oftciais, era na sua maioria fraca. Nao se percebia os objetivos e interesse 
da guerra, e sentia-se a sua crueldade, nas situaqoes em que vida nas trincheiras, a espera e 
o esgotamento nervoso imperavam. Apds a Batalha de La Lys, muitos soldados portugueses 
foram obrigados a trabalhos que nao deveriam ter cumpridos, se tivessem recebido o apoio 
das autoridades politicas nacionais. Note-se igualmente a forma como Portugal entrou mal 
das negocia^oes de paz, incapaz de fazer valer as suas preten^oes. 

Manuel do Nascimento indica pormenor es sob re a chegada do Contingente Expedicio- 
nario Portugues ao porto de Brest, em Eran^a, e a forma como a popula^ao francesa aco- 
Iheu com surpresa esses soldados, vindos de um pais relativamente pouco conhecido, e 
que pisavam o solo frances pela primeira vez. Os pormenores sao dados, igualmente, sob re 
aspetos logisticos do transporte do Contingente Expedieionario Portugues, e sobre as di- 
ftculdades que o Contingente Expedieionario Portugues encontrou ao relacionar-se com o 
Exercito Ingles que, por exemplo, nao providenciou navios para desloca^ao dos soldados 
portugueses. Nas Elandres, naquilo que iria constituir o local da Batalha de La Lys, o Exer¬ 
cito Portugues, sob comando ingles, foi sobrecarregado, colocado num setor pantanoso, 
onde a vida nas trincheiras era particularmente complexa. Ainda hoje, e discutido o mi- 
mero exato de baixas sofridas pelo Contingente Expedieionario Portugues na Batalha de 
La Lys, ou em consequencia da mesma - como e o caso dos soldados portugueses feitos 
prisioneiros. 

Alguns artigos incidem sobre a guerra nas coldnias portuguesas. Helmut Bley discute a 
situa^ao da for^a de trabalho e ate a continua^ao da escravidao em Angola e Mozambique, 
no contexto dos debates internacionais sobre escandalos colonials. O acordo anglo-alemao 
de 1898 e discutido no contexto da falencia esperada de Portugal e da Guerra dos Boe- 
res, sendo abordadas duas linhas de pensamento visfveis na Gra-Bretanha e na Alemanha: 
controlo territorial versus influencia econdmica informal. O impacto da guerra na Africa 
Oriental e igualmente discutido sublinhando-se o sofrimento da popula^ao africana, tanto 
na Tanzania como em Mozambique. Einalmente, sao referidos os pianos alemaes para a 
Africa Central, argumentando que, apds a derrota da Russia, especialmente os militares 
alemaes queriam uma expansao para a Europa Oriental e nao para territdrios ultramarinos. 

Jose Antonio Rodrigues Pereira refere o papel da Marinha Portuguesa que teve de tomar 
imediatamente medidas de defesa dos interesses nacionais no mar e nas coldnias, advo- 
gando que foi Cabo Verde quern recebeu maior atenzao da Armada Portuguesa. Neste con- 


APRESENTAZAO 


13 


texto, e assinalada a importancia estrategica do porto do Mindelo (Cabo Verde) que obrigou 
a signiftcativas medidas de defesa maritima daquele porto. 

Jose Soares Martins aborda a presen^a de Portugal nas colonias, mais concretamente em 
Mozambique, na defesa dos seus interesses colonials face a presen^a amea^adora da Ale- 
manha em terras do Tanganica, defendendo que viria a saldar-se num dos maiores desas- 
tres militares da nossa historia recente, dada a imprepara^ao dos militares portugueses. 
Deste modo, o autor pretende sobretudo lembrar os infelizes que aos milhares bcaram em 
valas comuns e cemiterios improvisados no norte de Mozambique. 

Por seu turno, Antonio Paulo Duarte aborda a questao da construzao das imagens em 
torno da participazao portuguesa na Primeira Guerra Mundial, mormente daquelas que a 
memoria coletiva retem e da construzao historica que dela faz. A explanazao e analise dessa 
construzao e executada em tres moment os: a leitura dos protagonistas, a fase do desfecho 
e resultados, e, bnalmente, a reconstruzao atual do conflito. Judite Gonzalves de Freitas 
questiona os modelos de analise historiograbcos da participazao portuguesa no primeiro 
conflito mundial, identibcando as principals mudanzas da nossa historiograba ao longo do 
seculo XX, e a evoluzao do autoconceito respetivo de Portugal. Para o efeito a autora ex- 
plana as carateristicas do discurso historiograbco das smteses de Portugal sob re a proble- 
matica da guerra e a alterazao das perspetivas historiograbcas dominantes, mormente nas 
distintas etapas da historiograba do Estado Novo e em tempos de democracia. Jorge Pedro 
Sousa trata das imagens da guerra na imprensa, abordando as questoes atinentes ao dis¬ 
curso iconograbco da revista Portugal na Guerra, com base numa abordagem qualitativa e 
quantitativa. Perspetiva-se o uso da iconograba para projetar e realzar signibcados sobre o 
conflito e, neste contexto, o autor conclm que a iconograba da supracitada revista espelha 
o esforzo propagandfstico de Portugal na promozao da ideia de justibcar a participazao no 
conflito, pugnando pelo controlo da opiniao publica - aquilo que o autor classibca como 
“infopropagranda”. 

Joao Casqueira Cardoso e Isabel Silva sublinham, num poster sobre o papel da Cruz Ver- 
melha durante a Primeira Guerra Mundial, alguns aspetos mais gerais, como a relevancia 
que a Gruz Vermelha ganha nesta altura, ou os servizos de apoio aos prisioneiros que co- 
mezam a emergir e a prestar um servizo essencial - como o encaminhamento de correio 
para os campos de prisioneiros. As normas internacionais sobre a guerra irao entao mudar, 
grazas a estes esforzos, bem com a regulamentazao sobre o uso de armas que, em bom 
rigor, nao deveriam ter sido usadas durante este conflito mundial. Sublinha-se os aspetos 
muito graves, em termos flsicos e psicologicos, da guerra de trincheiras, e do uso de gazes, 
para os combatentes. Catarina Nogueira Pereira e Diogo Guedes Vidal, num trabalho origi¬ 
nal realizado na base de varias fontes de informazao locals, apresentam um poster sobre o 
impacto, indireto, da Primeira Guerra Mundial na cidade de Porto. A pressao populacional 
para a emigratdria portuguesa para fora da Europa - nomeadamente para o Brasil - au- 
menta de forma drastica; os prezos locals e as dibculdades ligadas a falta de mao-de-obra, 
fazem-se igualmente sentir; e a admissao de soldados com problemas de saude e visfvel no 
aumento da populazao hospitalar. Mehdi Jendoubi mostra um outro aspeto da Primeira 
Guerra Mundial, frequentemente esquecido: a participazao dos soldados oriundos da Afri¬ 
ca do Norte, e maioritariamente integrados no Exercito frances, nos cenarios das batalhas 
ocorridas na Europa. Nao longe do numero de soldados africanos reclamados pelas auto- 
ridades franceses, 500.000, os soldados africanos (e do Medio Oriente), com algumas ca- 


14 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


rateristicas proprias (como o seu uniforme), irao dar um contributo decisive em batalhas, 
nomeadamente na Batalha da Marne. A fraca documenta^ao disponivel, e o relativamente 
escasso reconhecimento desses aspetos da historia da Primeira guerra mundial nao deixa 
de ter analogias com a menoriza^ao de que foram objetos os soldados portugueses, parente 
menor e menosprezado dos Aliados. 

Finalmente, no sexto capitulo sao disponibilizados tres trabalhos de natureza diferente. 
O primeiro de Joaquim de Castro apresenta um estudo exploratorio e qualitative no qual 
descreve as suas experiencias pessoais de emigra^ao, empregando o metodo de redu^ao 
fenomenologica e, portanto, procurando ser descritivo, em vez de normative e prescritivo. 
O foco e a identidade etnica e a experiencia de emigra^ao, em reflexao comparativa com os 
principals modelos de acultura^ao. 

Na comemora^ao do centenario do ftm da Primeira Guerra Mundial, Mehdi Jendoubi 
concentra-se no caminho feito pela Europa que, em um seculo, passou das rivalidades 
imperials e nacionalismos exacerbados a reconcilia^ao e a unibca^ao do continente. No 
entanto, essa conquista nao foi feita sem problemas, sendo a Segunda Guerra Mundial, 
ainda mais devastadora que a Primeira, a prova evidente disso. O autor centra-se igual- 
mente nas razoes das guerras que ocorreram no Proximo Oriente, abordando a encru- 
zilhada civilizacional deste ponto de encontro para a Europa, Africa e Asia, refletindo 
igualmente sobre as crises que abalam essa area de instabilidade que e o epicentre de 
tensoes internacionais atuais. 

Aurore Rouffelaers centra a sua aten^ao na analise de um fundo de correspondencia par¬ 
ticular trocada entre os soldados do Gorpo Expedicionario Portugues e as suas familias, 
amigos e amores. A autora refere que entre 1914 e 1918 mais de 20 bilioes de cartas foram 
trocadas entre soldados de cinquenta e duas nacionalidades e suas familias. Trocas pun- 
gentes, onde tragedia e ansiedade, vida quotidiana e esperan^a, intimidade e diivida se 
projetam. Esta pratica afeta todos os estratos socials, constituindo o linico elo desses sol¬ 
dados com o mundo longe das trincheiras. Por outro lado, a analise dessas cartas revela a 
intensidade e capacidade de observar, deslocando-se muito para alem do palco da batalha 
para expressar os mais profundos sentimentos de dor, de amor, de saudade, de diivida e 
de esperan^a. 

Numa contribui^ao complementar, a mesma autora, Comissaria da Exposi^ao, apre¬ 
senta a exposi^ao Bataille de La Lys - Exposition Racine: 19 memoire vivantes, e propoe 
pistas para continuar trabalhos de recolha de dados sobre este tema ao mesmo tempo 
publico e privado. 


Judite Gonsalves de Ereitas 
Joao Casqueira Gardoso 
Pedro Reis 


APRESENTAgAO 


15 



Presentation 


The book currently available in an e-book format, at the option of the editors, is the result 
of the author’s works presented and developed within the framework of the International 
Congress on World War I. In the centenary of the Battle of the Lys that reunited, in the 
facilities of the Faculty of Human and Social Sciences of the University Fernando Pessoa, on 
April 9-11 2018, more than two dozen national and foreign researchers with the intention 
of carrying out a multidisciplinary analysis of the motives, implications, and occurrences 
of one of the greatest war events of the hrst half of the twentieth century. The conference 
was developed in association with the Portuguese Institute of National Defense (IDN). 

The structure of presentation of the texts follows an organic and thematic subdivision, 
starting from thematic units of general and problematic scope to specihc thematic units, 
mainly related to the context, scope and dimensions of Portuguese participation in the 
Battle of La Lys, establishing a complementarity between the two kinds of thematic units. 
Therefore, the order of presentation begins with chapter 1 entitled “The problematization 
of the First World War” whereas chapter 11 is specihcally dedicated to the participation of 
“Portugal in the War - The Battle of La Lys“. Likewise, in chapter ill, entitled “War in the 
Colonies”, all the papers deal with the projection of the conflict in the Portuguese Afri¬ 
can domains, with emphasis on the slavery problem and the Anglophone and Germanic 
threats, the role of defense of the Portuguese navy in Cape Verde, and, hnally, the German 
intimidation in the region of Tanganyika (East Africa). 

Chapter IV is devoted entirely to the historiography of war, including studies on the 
construction of images of Portugal’s participation in the conflict, conceptual changes and 
investigative directions of historiographic discourse throughout the 20th century, and 
images of the war in the Portuguese press. 

Chapter V includes the posters presented to the congress, addressing respectively the 
role of the Red Cross and the impact of the First World War in the city of Porto. Final¬ 
ly, in the last chapter (Vl), all the papers that deal with various issues related to the war 
phenomenon have been gathered, but they are somehow more distanced from the main 
thematic core of the book. 

in the context of a scientihc meeting of international scope, authors were granted com¬ 
plete freedom to write the hnal texts in one of the three official languages of the meeting 
(Portuguese, French or English). 


The first text by Helmut Bley proceeds to a problematizing approach to war as a world 
phenomenon, tempering the phenomenon with the growth of nationalist fervor and the 
crisis of European imperialism in the African and Asian continents; the impact of the war 
on European and extra-European civilian populations as well as the instability caused in 
the Middle East. Luis Alves de Fraga, for his part, relates his text to the evaluation of the 
internal and external conditions of Portugal’s entry into the war, especially analyzing the 
political dependencies of Portugal, especially in relation to Great Britain, and the failure 
of the national belligerence policy following the lack of British support. Claudia Ramos 


stresses the contradictions of the hrst quarter of the twentieth century, between the lega¬ 
cy of conflict fueled hy the growing rivalries between the major European powers and the 
delineation of a new post-war international order based on a matrix of dialogue, under¬ 
standing and cooperation. Antonio Fernando de Cascais arises the issue of the pioneering 
use of chemical and biological weapons and their traumatic and enduring aspects, both 
from the immediate human point of view and from the point of view of memorialism and 
subsequent recidivism. Gon9alo Paixao, for his part, analyzes the veterinary services of 
the Portuguese Army using sources from the Military Archives, describing the contingents 
and the conditions solipeds were sent to the war zones in Europe and Portuguese Africa, 
analyzing the reasons for the lack of organization of the veterinary services. Manuel do 
Nascimento, for his part, reviews the Portuguese participation in the Eirst World War, giv¬ 
ing special emphasis to the ambiguity and hesitations of the Portuguese position in the face 
of the internal constraints, namely the implantation of the I Republic, and exogenous con¬ 
straints, e.g. the agreement between England and Germany for the sharing of Portuguese 
Africa. At the end of this chapter, Teresa Toldy, in homage to her grandfather, hghter and 
survivor of the Battle of La Lys, reflects on the ethical issues raised by changing war pat¬ 
terns, given the use of “distance death” mechanisms, mainly with aerial bombardments 
and the use of chemical weapons (gassing) which allowed a mass destruction of soldiers 
and “faceless people”. 

In the hrst article of the second chapter, Guilhermina Mota, in his article on the histo¬ 
ry of the Battle of La Lys, underlines the aspects of the Portuguese historiography about 
this event. The author frames these aspects, and contrasts them with objective elements, 
documents and evidence known nowadays. The Battle of La Lys, on April 9,1918 and the 
following days, was part of a series of German offensives at the end of the Eirst World War 
in Elanders, notably the German operation called “Georgette“. The offensive was charac¬ 
terized by harsh attacks on the battle fronts by well-trained and well-equipped troops, 
unlike the Portuguese Army. The circumstances that preceded the Battle of La Lys, such 
as the tactical changes related to the Expeditionary Portuguese Contingent, are described 
and analyzed in detail. The hardness of the Battle of La Lys is evident there, particular¬ 
ly when compared with other similar operations. Luis Alves de Fraga, concentrating on 
the political, economic and social conditions of Portuguese involvement in World War I, 
demonstrates how the Portuguese Expeditionary Contingent was, in fact, abandoned by 
the political power when the Battle of La Lys occurred. In Portugal, there was already an 
almost generalized reluctance to take part in the war. Despite propaganda for entry the 
war, the situation did not change substantially between April 1917 and April 1918. The very 
context of the Battle of La Lys , in which the German Army knew in detail the weakness¬ 
es of the Portuguese Expeditionary Contingent, and specihcally aimed at them, explains 
the extremely high number of Portuguese prisoners, as well as in the post-conflict phase 
- when Portuguese soldiers were integrated into the British units - the political abandon¬ 
ment of soldiers, and of the war itself as one of the national objectives. Joao Casqueira 
Cardoso highlights some aspects of Portuguese participation in the Eirst World War. The 
Portuguese Army was not prepared for this type of conflict, particularly in a European sce¬ 
nario, due to the backwardness of its training and its equipment. In what would become an 
example of modern wars, the Portuguese Expeditionary Contingent was quickly outdated. 
In addition, the motivation of the soldiers, and their officers, was mostly weak. The objec- 


18 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


tives and interests of the war were not perceived, and its cruelty was felt, in situations in 
which life in the trenches, waiting and nervous exhaustion prevailed. After the Battle of 
La Lys, many Portuguese soldiers were forced into tasks they should not have done if they 
had received the support of the national political authorities. It should also he noted how 
Portugal went wrong in peace negotiations, incapahle of asserting its claims. Manuel do 
Nascimento indicates details of the arrival of the Portuguese Expeditionary Contingent to 
the port of Brest in France and the way in which the French population welcomed these 
soldiers from a relatively little-known country, and who stepped on the French soil for 
the hrst time. The details are also given on the logistic aspects of the transport of the Ex¬ 
pedition Portuguese Contingent and on the difficulties that the Portuguese Expeditionary 
Conquest encountered when dealing with the English Army, which did not, for example, 
provide ships for the Portuguese soldiers to travel. In Flanders, which would he the site 
of the Battle of La Lys, the Portuguese Army, under English command, was overloaded, 
placed in a marshy sector, where life in the trenches was particularly complex. Even to¬ 
day, the exact number of casualties or Portuguese soldiers of the Expeditionary Contingent 
taken prisoners in the Battle of La Lys is discussed. 

Some articles focus on the war in the Portuguese colonies. Helmut Bley discusses the 
workforce situation and continuing slavery in Angola and Mozambique in the context of 
international debates on colonial scandals. The Anglo-German agreement of 1898 is dis¬ 
cussed in the context of the expected bankruptcy of Portugal and the Boer War, with two 
lines of thought perceptible in Britain and Germany: territorial control versus informal 
economic influence. The impact of the war in East Africa is also discussed by highlighting 
the plight of the African population, both in Tanzania and in Mozambique. Finally, the 
German plans for Central Africa are mentioned, arguing that after the defeat of Russia, 
especially the German military wanted an expansion to Eastern Europe and not to overseas 
territories. 

Jose Antonio Rodrigues Pereira refers to the role of the Portuguese Navy which had to 
take immediate measures to defend national interests at sea and in the colonies, argu¬ 
ing that it was Cape Verde that received the greatest attention from the Portuguese Navy. 
In this context, the strategic importance of the port of Mindelo (Cape Verde) is stressed, 
which has forced significant maritime defense measures in that port. 

Jose Soares Martins addresses Portugal’s presence in the colonies, more precisely in Mo¬ 
zambique, in the defense of its colonial interests in the face of the threatening presence of 
Germany in Tanganyika’s lands, arguing that it would become one of the greatest military 
disasters in our recent history, given the unpreparedness of the Portuguese military. In 
this way, the author intends, above all, to remind the unhappy people who have been left 
in mass graves and improvised cemeteries in northern Mozambique. 

Antonio Paulo Duarte, in turn, approaches the question of the construction of imag¬ 
es about the Portuguese participation in World War 1, especially those that the collective 
memory retains and of historical construction made from it. The explanation and analysis 
of this construction is carried out in three moments: the reading of the protagonists, the 
phase of the outcome and results, and, finally, the current reconstruction of the conflict. 
Judite Gon9alves de Freitas questions the models of historiographical analysis of the Por¬ 
tuguese participation in World War I, identifying the major changes of our historiogra¬ 
phy throughout the twentieth century, and the evolution of the respective self-concept of 


PRESENTATION 


19 


Portugal. For this purpose, the author explores the characteristics of the historiographic 
discourse of the Portuguese synthesis on the prohlematic of war and the alteration of the 
dominant historiographic perspective, especially in the different stages of the historiog¬ 
raphy of the dictatorial regime, “Estado Novo”, and in times of democracy. Jorge Pedro 
Sousa refers the images of the war in the press, addressing the issues pertaining to the 
iconographic discourse of the magazine Portugal na Guerra (Portugal in the war), based 
on a qualitative and quantitative approach. The iconography of the magazine reflects Por¬ 
tugal’s propagandistic effort in promoting the idea of justifying the participation in the 
conflict, to control the public opinion - which the author classihes as “infopropagranda". 

Joao Casqueira Cardoso and Isabel Silva underline in a poster about the role of the Red 
Cross during World War 1 some more general aspects, such as the relevance that the Red 
Cross was gaining at that time or the support services for the prisoners or an essential ser¬ 
vice - such as forwarding mail to prison camps. The international norms on war will then 
change, thanks to these efforts, as well as the regulations on the use of weapons that, in 
good faith, should not have been used during this world conflict. The very serious phys¬ 
ical and psychological aspects of trench warfare and the use of gasses for combatants are 
highlighted. Catarina Nogueira Pereira and Diogo Guedes Vidal present a poster about the 
indirect impact of the First World War in the city of Porto, based on several local sources 
of information. The Portuguese emigration pressure outside Europe - notably to Brazil - 
increases drastically; local prices and difficulties linked to the lack of labor force are also 
felt; and the admission of soldiers with health problems is visible in the increase of the 
hospital population. Mehdi Jendoubi mentions another aspect of the First World War, of¬ 
ten forgotten: the participation of soldiers from North Africa, mostly integrated into the 
French army, in the scenarios of the battles that took place in Europe. The 500,000 African 
(and Middle Eastern) soldiers claimed by the Erench authorities, with some characteristics 
of their own (such as their uniform), will make a decisive contribution to battles, notably 
the Battle of Marne. The feeble documentation available, and the comparatively little rec¬ 
ognition of these aspects of the history of World War 1, are analogous to the minimization 
of Portuguese soldiers, the lesser and despised relative of the Allies. 

Einally, in the sixth chapter three works of a different nature are offered. The first one 
by Joaquim de Castro presents an exploratory and qualitative study in which he describes 
his personal experiences of emigration, using the method of phenomenological reduction 
and, therefore, seeking to be descriptive rather than normative and prescriptive. The focus 
is the ethnic identity and the experience of emigration, in comparative reflection with the 
main models of acculturation. 

in commemoration of the centenary of the end of World War 1, Mehdi Jendoubi focuses 
on the path made by Europe that has shifted from imperial rivalries and exacerbated na¬ 
tionalisms to the reconciliation and unification of the continent, in a century. However, 
this was not achieved without problems, and World War 11, even more devastating than 
WWl, is the evident proof of this. The author also focuses on the reasons for the wars in 
the Middle East, addressing the civilizational crossroads of this meeting point for Europe, 
Africa and Asia, also reflecting about the crises of this this area of instability that is the 
epicenter of international current tensions. 

Aurore Rouffelaers focuses her attention on the analysis of a private correspondence 
fund exchanged between the soldiers of the Portuguese Expeditionary Corps (CEP) and 


20 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


their families, friends and lovers. The author states that between 1914 and 1918 over 20 hil- 
lion letters were exchanged between soldiers of hfty-two nationalities and their families. 
Touching exchanges where tragedy and anxiety, daily life and hope, intimacy and doubt 
are presented. This practice affects all social strata, constituting the only link of these sol¬ 
diers with the world away from the trenches. On the other hand, the analysis of these 
letters reveals the intensity and capacity of observing, far beyond the stage of the battle to 
express the deepest feelings of pain, love, longing, doubt, and hope. 

in a complementary contribution, the same author. Exhibition Commissioner presents 
the exhibition Bataille de LaLys - Exposition Racine: 19 memoire vivantes, and proposes 
clues to continue the work of collecting of data, on a topic which is simultaneously public 
and private. 


Judite Gonsalves de Freitas 
Joao Casqueira Cardoso 
Pedro Reis 


PRESENTATION 


21 



Avant-propos 


L’ouvrage que nous lan^ons maintenant, et qui par option des editeurs est au format 
e-Book, constitue le resultat des travaux presentes et developpes dans le cadre du Congres 
international sur la premiere guerre mondiale - Centenaire de la bataille de la Lys, qui 
s’est tenu dans les locaux de la Faculte des Sciences humaines et sociales de I’Universite 
Fernando Pessoa, entre les 9 et 11 avril 2018. Ce congres a reuni plus de deux douzaines de 
chercheurs nationaux et etrangers, en vue d’entreprendre une analyse pluridisciplinaire 
des motifs, des implications, des occurrences et des analyses de Fun des plus importants 
evenements militaires de la premiere moitie du XX'= siecle. Le congres a ete developpe en 
association avec I'lnstitut de la defense nationale (IDN) du Portugal. 

La structure de presentation des textes obeit a une subdivision organique et thematique, 
en partant d’unites thematiques de nature generale et de grandes problematiques vers 
des unites thematiques specibques qui leurs sont liees. Celles-ci ont trait en particulier 
au contexte, a la portee et aux dimensions de la participation portugaise a la bataille de la 
Lys, et etablissent une relation de complementarite entre les deux. Ainsi, I’ordonnance- 
ment des textes commence avec un chapitre 1 intitule “la problematisation de la premiere 
grande guerre” et un chapitre 11, specibquement consacre a la participation du “Portugal 
dans la guerre - la bataille de la Lys”. Dans le meme ordre d’idee, dans le troisieme cha¬ 
pitre, intitule “La guerre dans les colonies”, toutes les contributions traitent, entre autres, 
de la projection du conflit au sein des territoires portugais en Afrique, en soulignant la 
question esclavagiste, les menaces anglaises et allemandes, le role de defense joue par la 
Marine portugaise au Cap-Vert, et enbn I’intimidation de la part de I’AUemagne dans la 
region de Tanganyika (Afrique de Test). 

Le chapitre IV est entierement consacre a Thistoriographic de la guerre, ce qui com- 
prend des etudes sur la construction des images de la participation du Portugal au conflit, 
les changements conceptuels et les orientations de la recherche sur le discours historio- 
graphique, tout au long du XX^ siecle et, enfm, les images de la guerre a travers la presse 
portugaise. 

Le chapitre V renferme quant a lui les afhches presentees au congres, qui traitent respec- 
tivement du role de la Croix-Rouge et de I’impact de la Premiere guerre mondiale dans la 
ville de Porto. Le dernier chapitre (Vl) cloture cet ouvrage, rassemblant toutes les contri¬ 
butions abordant divers problemes lies au phenomene de guerre, mais qui sont en quelque 
sorte plus distantes du noyau thematique principal du livre. 

S’agissant des actes d’une rencontre scientihque de caractere international, les editeurs 
ont voulu accorder une pleine liberte aux auteurs, ceux-ci pouvant rediger les textes deh- 
nitifs de leurs contributions dans Tune des trois langues ofhcielles du congres (portugais, 
fran^ais ou anglais). 


Le premier texte de Helmut Bley est une approche problematisant la guerre en tant que 
phenomene mondial, et le phenomene de la croissance de la ferveur nationaliste et de la 
crise de I’imperialisme europeen sur les continents africain et asiatique. 11 se poursuit par 


I’analyse des repercussions de la revolution russe de 1917 dans le monde, I’inipact de la 
guerre sur les populations civiles europeennes et extra-europeennes, ainsi que de I’ins- 
tabilite causee dans le Proche-Orient. Luis Alves de Fraga consigne quant a lui dans son 
texte revaluation des contraintes internes et externes liees a I’entree du Portugal dans la 
guerre, et en particulier I’analyse des dependances politiques du Portugal, principalement 
vis-a-vis de la Grande-Bretagne, ainsi que Pechec de la politique de belligerance nationale 
par suite du manque de soutien britannique. Claudia Ramos souligne les contradictions du 
premier quart du XX'= siecle, entre Fheritage du conflit, avec les rivalites croissantes entre 
les principales puissances europeennes, et la delimitation d’un nouvel ordre international 
dans I’apres-guerre, guide par un cadre de dialogue, de comprehension et de coopera¬ 
tion. Antonio Fernando de Caseais aborde la question de Putilisation pionniere des armes 
chimiques et biologiques et de ses aspects traumatisants et durables, a la fois d’un point de 
vue humain immediat, en tant que precedent, ainsi que son incidence posterieure. De son 
cote, Gon 9 alo Paixao aborde I’analyse des services veterinaires de I’armee portugaise, en 
utilisant les sources des archives militaires. Decrivant les quotas et les conditions d’envoi 
de chevaux dans les zones de guerre en Europe et en Afrique portugaise, il expose les rai¬ 
sons du manque d’organisation des services veterinaires. Pour terminer le premier cha- 
pitre, Teresa Toldy, en hommage a son grand-pere, combattant et survivant de la bataille 
de La Lys, reflechit sur les questions ethiques soulevees par la modibcation des pratiques 
militaires, par le recours a des mecanismes de «mort a distance», avec les bombardements 
aeriens et Putilisation d’armes chimiques (gazages), qui permettait une destruction mas¬ 
sive de soldats devenu des «personnes sans visage». 

Dans son article sur Phistoire de la bataille de la Lys, Guilhermina Mota souligne les as¬ 
pects de P historiographic portugaise a propos de cet evenement. L’auteur contextualise 
ces aspects, et les compare avec des elements objectifs, des documents et des elements 
connus maintenant. La bataille de la Lys, qui a eu lieu le 9 avril 1918, et les jours suivants, 
s’insere dans un ensemble d'offensives allemandes de la bn de la premiere guerre mon¬ 
diale en Flandre, notamment dans P operation allemande dite «Georgette». L'obensive a 
ete caracterisee par de tres dures attaques sur les fronts, par Penvoi de troupes allemandes 
aguerries et bien equipees, contrairement a la situation de I'armee portugaise. Les circons- 
tances precedant la bataille de la Lys, ainsi que les changements tactiques concernant le 
Gontingent expeditionnaire portugais, sont decrits et analyses dans le detail. La durete de 
la bataille de la Lys est bien patente, en particulier si on la rapporte a d'autres operations 
similaires. Luis Alves de Fraga, se concentrant sur les conditions politiques, economiques 
et sociales de Pintervention portugaise dans la premiere guerre mondiale, demontre quant 
a lui la fa^on dont le Gontingent expeditionnaire portugais a ete, en fait, abandonne par le 
pouvoir politique, precisement au moment ou la bataille de la Lys a eu lieu. Au Portugal, 
une reserve presque generalisee existait quant a Pentree du pays dans la guerre. En depit 
de la propagande en faveur de Pentree dans la guerre, la situation n’a pas change subs- 
tantiellement durant Pannee qui s’etendit d’avril 1917 a avril 1918. Le contexte meme de 
la bataille de la Lys, dans laquelle Parmee allemande connaissait dans le detail les points 
faibles du Gontingent expeditionnaire portugais, et qui visait specibquement celui-ci, ex- 
plique le nombre extremement eleve de prisonniers portugais. Get aspect explique aussi 
le fait que, dans la phase de Papres-conflit, les soldats portugais aient ete integres dans 
les unites britanniques, faisant en sorte que se poursuive Pabandon politique des soldats 


24 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


portugais et, en somme, de la guerre elle-meme comme objeetif national. Joao Casqueira 
Cardoso souligne quant a lui certains aspects de la participation portugaise a la premiere 
guerre mondiale. L’armee portugaise n’a nullement ete preparee pour ce type de conflit, 
en particulier dans un contexte europeen, en raison du retard dans sa formation et dans 
son equipement. Dans ce qui constituera un exemple des premieres guerres modernes, le 
Contingent expeditionnaire portugais se trouvait rapidement depasse. En outre, la moti¬ 
vation des soldats, ainsi que de leurs officiers, etait la plupart du temps fort diminuee. Les 
objectifs et I’interet de la guerre n’ont pas ete assimiles, mais sa cruaute a en revanche ete 
severement ressentie, dans des situations oil la vie dans les tranchees etait marquee par 
I’attente et I’epuisement nerveux. Apres la bataille de la Lys, de nombreux soldats portu¬ 
gais ont ete obliges de travailler, alors meme qu’ils n’auraient pas du accomplir ces taches 
s’ils avaient re^u le soutien des autorites politiques nationales. 11 souligne par ailleurs la 
fa^on dont le Portugal a entame ce qui se sont revelees etre de mauvaises negociations de 
paix, incapable qu’il a ete de faire valoir ses revendications. Manuel do Nascimento indique 
les details de I’arrivee du Contingent expeditionnaire portugais au port de Brest, en France, 
et la fa^on dont la population fran^aise a accueilli ces soldats venus d’un pays relativement 
peu connu, et qui foulaient le sol fran^ais pour la premiere fois. Des details sont egalement 
donnes sur les aspects logistiques du transport du Contingent expeditionnaire portugais, 
et sur les difftcultes rencontrees par celui-ci dans ses relations avec I’armee anglaise qui, 
par exemple, refusa de fournir les navires pour le deplacement des troupes portugaises. En 
Flandre, sur le site futur de la bataille de la Lys, I’armee portugaise, sous commandement 
anglais, fut submergee par les taches, et placee dans un secteur marecageux, ou la vie dans 
les tranchees etait particulierement complexe. Aujourd’hui encore, on discute du nombre 
exact de victimes au sein du Contingent expeditionnaire portugais lors de la bataille de la 
Lys, ou a la suite de celle-ci - comme c’est le cas des soldats portugais faits prisonniers. 

Certains articles se concentrent sur la guerre dans les colonies portugaises. Helmut 
Bley aborde la situation de la main-d’ceuvre et meme de la poursuite de I’esclavage en 
Angola et au Mozambique, dans le contexte des debats internationaux sur les scandales 
coloniaux. L’accord anglo-allemand de 1898 est discute, dans le contexte de la defaite at- 
tendue du Portugal et de la guerre des Boers, en suivant deux lignes de pensee existant 
en Grande-Bretagne et en Allemagne : le controle territorial, d’un cote; I’influence eco- 
nomique informelle, de I’autre. L’impact de la guerre en Afrique de Test est egalement 
note, en soulignant les souffrances de la population africaine, tant en Tanzanie qu’au Mo¬ 
zambique. Enftn, les plans allemands sur I’Afrique centrale sont evoques, considerant le 
postulat selon lequel, apres la defaite de la Russie, les militaires allemands voulaient une 
expansion vers I’Europe de Test et non vers les territoires d’outre-mer. 

Jose Antonio Rodrigues Pereira mentionne le role de la Marine portugaise, qui a du 
prendre immediatement des mesures pour defendre les interets nationaux sur la mer et 
dans les colonies, en appuyant que c’etait le Cap-Vert qui a re^u la plus grande attention 
de la part de la Marine portugaise. Dans ce contexte, 11 souligne Fimportance strategique 
du port de Mindelo (Cap-Vert), ou des mesures importantes de defense maritime du port 
ont ete prises. 

Jose Soares Martins debat la presence du Portugal dans les colonies, et plus speciftque- 
ment au Mozambique, en vue de la defense de ses interets coloniaux face a la presence 
mena^ante de FAllemagne sur le territoire du Tanganyika. 11 defend que cette presence 


AVANT-PROPOSA 


25 


viendrait a se solder par Tune de plus grandes defaites militaires de I’liistoire recente, etant 
donne I’absence de preparation de Farmee portugaise. A eet egard, Fauteur rappelle le sort 
des malheureux qui, par milliers, furent enterres dans des fosses communes et des cime- 
tieres improvises dans le nord du Mozambique. 

De son cote, Antonio Paulo Duarte aborde la question de la construction d’images autour 
de la participation portugaise a la premiere guerre mondiale, et en particulier celles que 
la memoire collective conserve, et la construction historique qui s’ensuit. L’explication et 
I’analyse de cette construction est realisee en trois moments; I’analyse des protagonistes, 
la phase de resultat ftnal et de consequences et, enbn, la reconstruction actuelle du conflit. 
Judite Gon9alves de Freitas questionne les modeles d’analyse historiographique de la par¬ 
ticipation portugaise au premier conflit mondial, identibant les principaux changements 
de I’historiographie portugaise tout au long du XX‘= siecle, et I’evolution de I’auto-concep- 
tion du Portugal qui y est associee. A ce propos, I’auteur explique les caracteristiques du 
discours historiographique des syntheses du Portugal sur le probleme de la guerre et I’al- 
teration des perspectives historiographique dominantes, durant les differentes etapes de 
I’historiographie du Estado Novo et durant la periode democratique. Jorge Pedro Sousa 
traite quant a lui des images de la guerre dans la presse, abordant les questions liees au 
discours iconographique du magazine Portugal na Guerra (Le Portugal dans la guerre ), en 
s’appuyant sur une approche qualitative et quantitative. 11 met en perspective I’utilisation 
de I’iconographie pour projeter et souligner les significations du conflit. Dans ce contexte, 
I’auteur considere que I’iconographie du magazine en question reflete I’effort de propa- 
gande du Portugal en vue de la promotion d’une justification de I’idee de participation 
au conflit, visant par la meme le controle de I’opinion publique - ce que I’auteur qualifie 
d’ «lnfo-propagande». 

Joao Casqueira Cardoso et Isabel Silva soulignent, a travers leur afhche sur le role de la 
Croix-Rouge pendant la Premiere guerre mondiale, certains aspects generaux, tels que le 
role que la Croix-Rouge acquiere durant cette periode, ou encore les services d’assistance 
aux prisonniers qui commencent a emerger et a fournir un service essentiel - comme le 
transfert du courrier vers les camps de prisonniers. Les regies internationales sur la guerre 
changeront alors, grace a ces efforts, avec la reglementation sur I’utilisation d’armes qui, 
en bonne rigueur, n’auraient pas du etre utilisees pendant ce conflit mondial, lls sou¬ 
lignent les consequences tres graves, pour les combattants, en termes physiques et psy- 
chologiques, de la guerre de tranchees et de I’utilisation de gaz. Catarina Nogueira Pereira 
et Diogo Guedes Vidal, dans un travail original mene a partir de plusieurs sources d’infor- 
mations locales, presentent pour leur part une afhche sur I’impact indirect de la Premiere 
guerre mondiale dans la ville de Porto. La pression demographique pour I’emigration por¬ 
tugaise hors d’Europe - en particulier vers le Bresil - augmente de fa^on drastique; les prix 
locaux et les difhcultes liees au manque de main-d’ceuvre sont egalement ressenties; et 
I’admission accrue de soldats ayant des problemes de sante au sein de la population hospi- 
taliere est visible. Mehdi Jendoubi montre un autre aspect de la Premiere guerre mondiale, 
souvent neglige : la participation des soldats provenant de I’Afrique du Nord, surtout inte- 
gre dans I’armee fran^aise, sur les champs des batailles en Europe. Les soldats africains (et 
du Moyen-Orient), dont le nombre se rapproche du nombre de soldats africains revendi- 
ques par les autorites fran^aises, soit 500.000, avaient certaines caracteristiques propres 
(comme leurs uniformes), et auront une contribution decisive dans les batailles, et no- 


26 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


tamment dans la bataille de la Marne. La documentation sur ce theme est plutot pauvre, et 
la reconnaissance de cet aspect de I’histoire de la Premiere guerre mondiale relativement 
rare, s’agissant d’une dimension qui presente des analogies avec la marginalisation dont 
furent I’objet les soldats portugais, parents pauvres et meprises des Allies. 

Enbn, dans le sixieme chapitre, trois contributions de nature differente sont presentees. 
La premiere est celle de Joaquim de Castro, qui presente une etude exploratoire et quali¬ 
tative dans laquelle il decrit ses experiences personnelles d’emigration, employant la me- 
thode de la reduction phenomenologique et, par consequent, cherchant a etre descriptif 
plutot que normatif. L’accent est mis sur I’identite ethnique et I’experience de I’emigra- 
tion, dans une reflexion comparative avec les principaux modeles d’acculturation. 

S’encadrant dans la commemoration du centenaire de la fln de la Premiere guerre mon¬ 
diale, Mehdi Jendoubi concentre une autre contribution sur la fa^on dont I’Europe, en un 
siecle, est passee des rivalites imperialistes et nationalistes exacerbees a la reconciliation et 
a I’uniflcation du continent. Cette realisation n’a cependant pas ete faite sans problemes, 
et la Seconde guerre mondiale, encore plus devastatrice que la Premiere, est la preuve evi- 
dente de ces difflcultes. L’auteur met I’accent sur les raisons des guerres qui ont eu lieu 
au Proche-Orient, un carrefour de civilisations et un point de rencontre pour I’Europe, 
I’Afrique et I’Asie, en reflechissant egalement sur les crises qui secouent cette zone d’ins- 
tabilite qui est I’epicentre des tensions internationales actuelles. 

Aurore Rouffelaers focalise quant a elle son attention sur I’analyse d’un fonds de cor- 
respondance privee echange entre les soldats du Corps expeditionnaire portugais et leurs 
families, amis et ou etres aimes. L’auteur mentionne qu’entre 1914 et 1918, plus de 20 mil¬ 
liards lettres ont ete echangees entre les soldats de 52 nationalites et leurs families. Des 
echanges poignants, ou la tragedie et I’anxiete, la vie quotidienne et I’esperance, I’intimite 
et le doute sont projetes. Cette pratique a concerne toutes les strates sociales, constituant 
le seul lien de ces soldats avec le monde loin des tranchees. D’autre part, I’analyse de ces 
lettres revele I’intensite et la capacite d’observer, se depla^ant bien au-dela du champ de 
bataille pour exprimer les sentiments de la plus profonde douleur, de I’amour, de la nos¬ 
talgic, du doute et de I’esperance. 

Dans le cadre d’une contribution complementaire, la meme auteure, Commissaire d’ex- 
position, presente Bataille de La Lys - Exposition Racine: 19 memoires vivantes, et pro¬ 
pose des pistes pour continuer le travail de collecte de donnees, sur un theme qui est a la 
fois public et prive. 


Judite Gonsalves de Ereitas 
Joao Casqueira Cardoso 
Pedro Reis 


AVANT-PROPOSA 


27 



1. A probleinatiza 9 ao da 
Primeira Grande Guerra 




The Impact of World War I on the 
non-European World^ 

Helmut Bley 

Leibniz University Hannover 


Abstract: The paper starts with discussing the various meanings and aspects of the notion 
Impact. It then touches the more general impact on the late stage of imperialism. It remem- 
hers the six to seven million soldiers, carries and workers from Africa, India and China who 
were recruited. It continues with the impact of the Russian Revolution in Africa, the Near- 
and Middle East, Latin America and Asia. In this context the longer-term social and political 
developments of the 19* century are taken into consideration. The major human catastro¬ 
phes in the Near- and Middle East are discussed. A special section is given to the impact 
on the Near East, the successful defense of Anatolia hy the troops of Kamal Atatilrk against 
partition. The expectations of Arah Nationalism and the partition of Syria, Irak and Palestine 
is reflected on. In a last section the Chinese hfth of May 1919 movement, which radicalized 
the revolution is mentioned and other Asian themes connected to our issue, including Japan, 
Australia and India. Some questions are asked with regard to Portugal in this context. 

Keywords: notion impact, imperialism, Russian Revolution in Africa, human catastro¬ 
phes, Arab Nationalism, Asian political movements. 

Resume: Cette contribution commence par discuter des differentes signihcations et as¬ 
pects de la notion dTmpact. Il aborde ensuite Timpact plus general sur le stade tardif de 
Timperialisme. Il rememore les six a sept millions de soldats, des transporteurs et des ou- 
vriers d’Afrique, dTnde et de Chine qui ont ete requisitionnes. Il evoque aussi Timpact de 
la Revolution russe en Afrique, au Proche et Moyen-Orient, en Amerique Latine et en Asie. 
Dans ce contexte, les developpements sociaux et politiques a plus long terme du XIXe siecle 
sont pris en consideration. Les principals catastrophes humaines du Proche et du Moyen- 
Orient sont discutees. Une section speciale est dediee a Timpact sur le Proche-Orient, la 
defense reussie de TAnatolie par les troupes de Kamal Atatilrk contre la partition. Les at- 
tentes du nationalisme arabe et de la partition de la Syrie, de Tirak et de la Palestine sont 
aussi analysees. Dans une derniere section, il mentionne le mouvement chinois du 5 mai 
1919, qui a radicalise la revolution, et d’autres themes sur TAsie lies a la question centrale, 
y compris le Japon, TAustralie et Tinde. Certains points concernent et posent des ques¬ 
tions au sujet du Portugal, dans ce contexte. 

Mots-cles: notion dTmpact, imperialisme. Revolution russe en Afrique, catastrophes hu¬ 
maines, nationalisme arabe, mouvements politiques asiatiques. 


’ The paper is mainly based on Bley and Kremers (2014), eds. articles in that book are quoted under “World”, au¬ 
thor and title. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


31 



The dimensions of “Impact” 


The term “impact” has many dimensions: direct and indirect ones, short and long-term 
influences changed the political systems or the social fabric in the societies which could be 
related to the war. There are demographic consequences. Not only the deaths in battles or 
in camps of war prisoners caused millions of deaths, but the war caused in many regions 
hunger and diseases, the worst the Influenza pandemic. Targe scale forced population 
movements happened, caused by the war or through the peace treaties. 

A rather indirect impact was caused by structural changes in the World-Economy and 
the new role in this respect of the United States. The sea-blockade against Germany and 
the Ottoman Empire changed the economic map of Europe. A temporary retreat of the 
major European economies from their export activities was influential for many countries 
who had imports from and exports to the continent. 


The War Period 1911-1923 

With regard to the beginning of the War it can be argued that the Italian conquest of Tibya 
which started 1911 was the beginning of the war by an attack on the Ottoman Empire, 
which also suffered a heavy defeat after the second Balkan War with hundred of thousands 
of Muslim refugees mainly from Bulgaria. 

Germanys nationalist reaction on the second Morocco-Crisis of 1911 prepared the ground 
to risk a preemptive strike against Erance and Russia, partly. The leadership of the Army 
thought it would lose the potentials to win an European war after the intensified Erench 
and Russian increase of military power decided atl912/13 would be established at least 
1917. But no War was planned or prepared before the Sarajevo crisis 1914. 

The aftermath of the War as part of the long end of the war should be included - at least 
up to 1923, but even up to the Great Depression of 1929. 


First Crisis of the imperialist Period 

The broadest impact worldwide in the long run was that the heydays of European Im¬ 
perialism were fading. Targe cracks in the Image of Europe as a champion of civilization 
were observed worldwide, although the Western Powers did not realize this. They even 
increased their colonial possessions. US President Wilson’s 14 points declaration was by¬ 
passed in the treaties of Paris. This applied also for the Mandate-System as a type of dis¬ 
guised colonial annexations. But the promises of the 14 points were heard in China. The 
Chinese students and the nationalist business community were listening. Despite of the 
defeat of Wilson in Paris and later in the US Senate on the signing of the peace treaty, the 
United States were the only winning state politically, economically and even culturally. It 
was regarded in many parts of the wider world as the main example for modernization. 
But because of the growing isolationist tendencies, this was not realized in Europe until to 
the financial crisis in the twenties. 


32 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Impact of the Russian revolution 


A strong echo had the socialist Revolution of 1917/18. The revolution seemed to offer a 
political and social alternative. It proposed the end of Imperialism and Colonialism. The 
Soviet Russian foreign policy skipped many unequal treaties and left northern Iran. There 
was an upswing of socialist ideas and movements. Since 1923 this was supported hy the 
communist international all over the world. But also anarchist influences were strength¬ 
ened. The movements often were transformed hy the Bolshevik example of the socialist 
“One Party State”. The Komintern supported the developments of communist parties in 
China, South East Asia, Tatin America and South Africa. However, it failed to radicalize the 
Revolution in Germany and lost the war against Poland 1920. 


The impact on civilian populations^ 

Six to seven millions of men were recruited from overseas for the war (Helmut, 2014, pp. 
9-2l). They were mainly African men under French, British, German and Belgian Rule. 
They were forced to participate often under cruel conditions in the War. They worked 
mainly as porters and workers hut also as soldiers in Europe. 

In the East African Campaign about two millions of African worked as porters, several 
ten thousand as hghters. They were recruited hy the “African Rifles” under British com¬ 
mand, the German “Askari” and through the “Force Puhlique” of the Belgian Congo. The 
South African Army and Indian troops were hghting in East Africa. The campaign of von 
Tettow Vorheck cost about 300 000 lives of the peasant population, because their harvests 
were taken by force. 

The main burden of the British operations in the Near East lay on the Indian contingent, 
and mainly in the Near East. Australia and New Zealand were sending several tenths of 
thousands conscripts. Many lost their lives in the battles of Gallipoli, during their attempt 
to take Constantinople or Istanbul. 

China sent about 90 000, some information says 50 000, contract workers in order to 
improve its international standing for revising its semi colonial status (Xu Guoqi, 2014, pp. 
59-78). This failed, because of Japans successful expansionist claims to advance in China 
getting the German colony there and other privileges in Versailles. 

So, the hrst-time workers von China experienced Europe. Some later famous political 
leaders were in-between them, Chu en Tai for instance or for Vietnam Ho chi min. Those 
who went back home came with the experience of the European slaughter-house in their 
mind and the continuity of European Colonialism. 


2 See Hoerder (2014), pp. 119-132 and Fall (2014), pp. 113-118. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


33 



The impact on the World Economy in Latin America 

Latin America (Rinke 2014, pp.79-88 and Geli 2014, pp.201-214) during the War concen¬ 
trated more on its own destiny, losing the markets in Europe, enduring inflation and loss¬ 
es in wages of the working class. The hrst attempt towards industrialization was started. 
The workers in Latin-America acted during their protest movements in the war period 
mainly in the framework of anarcho-syndicalist traditions of Iherian background. After 
1917 the influence of the Russian Revolution was felt, and it was attempted to merge an¬ 
archism with socialism. A centre of this development was Argentina. The Komintern has 
influenced the political biographies of more than 10 000 leftists (Meschkat and Buckmiller, 
2007). However, it is important to note, that the Mexican Revolution from 1910 to 1924 was 
not connected with World War One despite of growing US activities in Central America in 
the postwar period. 


Colonial Crisis in Africa short before the First World War 

Large colonial wars had been fought by the Germans in East Africa and Namibia between 
1904 and 1906 (Ealola and Ezekwem, pp. 89-100). There was a rebellion of the Zulu of 
South Africa in 1906. The international public was informed about colonial scandals in the 
Congo. As a result, the King of Belgium was forced to give his private colony to the Belgian 
state. Severe abuse of the workers was the reason, who were employed to collect wild 
rubber for the car-tires and electrical isolation. Portuguese rule in Angola at the same time 
was strongly criticized because Angolan people were sold as slaves to the Islands Sa Tome 
and Principe for the cacao plantations. Both happened 1908/09. 

The Boer War, which ended 1902, changed the perspectives of the educated African 
elites under the pressure of the growing Boer concepts of race-segregation after 1910. 
In 1913 was decided through the Land Act on the future of the South African peasantry. 
Only 15% of the land in South Africa should belong to Africans. This caused the founding 
of the ANG 1912. 

In the aftermath of the World War 1 the African working Glass was put under intensihed 
pressure because organized white labor and even the communist party of South Africa 
strengthened their position against the African labor force. In the bloody Rand Revolt of 
1922 to keep Africans out of skilled labor occupations, the government of President Smuts, 
a former Boer General, used machine guns against the white workers. This event radical¬ 
ized the Boer nationalism even more and was a big step forward to future Apartheid. 

These developments and experiences in the Early twenties century were of greater 
relevance than the War. In Africa the colonial situation was not basically shattered. The 
colonial powers had no problems to continue with the so-called colonial development, 
which however came into a new stage of systematic economic exploitation through 
plantation systems. 

The exception was Italy, who in the early twenties and then 1935 lead very brutal wars in 
Libya and 1935 Ethiopia. Despite of the East African Gampaign in other parts the impact of 
the War was more indirectly felt despite of the many being recruited for the War. 


34 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Economic Impact on African populations 

Large quantities of Afriean agricultural products were shipped to Western Europe and led 
to shortages. In Morocco the grain was taken in such quantities that hunger was hiting 
(Cornwell 2014, pp. 253-260). The French administration imported tea and used sugar, 
creating a new pattern of consumption in order to reduce the feeling of hunger. Agricul¬ 
tural labor was recruited for the war. Inflation hit many. But it was also the heginning of 
efficient colonial rule and economic concepts estahlished in a more systematic scale. This 
happened mainly after 1910. 

The war encouraged also resistance. Especially nomadic people of the Sahara used the 
occasion when French West Africa was depleted of French troops. They started again their 
old resistance against the estahlishment of French rule in large parts of the Sahara up to the 
borders of Egypt. French colonial troops defeated these movements at the end of the war 
(Braukamper, 2015). The Giriama in Kenya were 1915 fighting against the British, because 
they regarded recruitment to the war as continuation of slavery (Bley, 1996, pp. 305-328). 

It is possible that social and religious movements in South Africa, Congo and Ghana 
were influenced by the strains of the war economy. Protest-movements in Western- and 
Southern Africa were influenced by the radical black conscious movement of Markus Gar¬ 
vey. Mass conversion to Christianity reflected the colonial crisis during the 1920s, and the 
movements of independent black churches began to flourish. 

The impact of the World War increased the perception of the educated elites that colonial 
rule broke too many promises and the status of the colonizers was looked at more critical¬ 
ly. However Ex-soldiers did not play a bigger role although they had suffered in France and 
were disappointed, when they did not get the expected salaries and pensions. They most 
probably retreated into their communities without political activity against colonialism. 

More relevant became the black business elite, which for instance in Nigeria gained a lot 
in exploiting the gap, which the German business community had left. This business com¬ 
munity suffered under the British War economy. The British denied them transport facil¬ 
ities to Europe and the USA. They also were hit by the inflation and became disappointed 
and even angry about the restrictions which hurt them. This accelerated in the twenties 
(Bley 1995, pp. 37-58), when big multinational firms first used the postwar depression 
of 1920-22 and again the Great Depression, which started in tropical agriculture already 
1926, to force African firms out of business. So first demands for colonial reforms and even 
for independence were articulated. The future first President of Nigeria, Azikiwe, was the 
most outspoken businessman in the interwar years. 

Major human catastrophes in the Near East. 


The Armenians 

During the War in 1915/16 when loosing battles against the Russian Army in the Caucasus, 
the regime of the Young Turks was forcefully resettling the Armenians (Akcam, 2004). This 
led to genocide of up to 2 millions dyeing. It happened under full knowledge of the German 
military personal in the Ottoman Empire and the Chancellor Bethmann-Hollweg. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


35 


The Lebanon^ 


The Lebanon had relied in peaeetimes to a very large degree on export, based on Silk and 
other manufaetured goods. When the British and French Navy blockaded the coasts of the 
Ottoman Empire this export was cut off as well as the import of food. The result was hun¬ 
ger, increased by traders who were interested in shortages to get higher prices. The local 
production anyway had been reduced because many laboring hands had been recruited 
into the army. The result was that about on third of the Lebanese population died. 


Eastern Anatolia 

A similar fate hit South Eastern Anatolia. Because of the forced recruitment of the labor 
force to the army and massive flights out of service to the informal militias the cotton pro¬ 
duction broke totally down, the irrigated helds turned to swamps again and malaria came 
back on a grand scale, it was increased by a very dangerous form of Malaria, which the 
allied troops brought in during the hnal stage of the war. Not only tens of thousands of 
soldiers died but again about one third of the population, not counted the Armenians, who 
had been the leading organizers of the cotton production before death marshes. 


Northern Iran and Muslim societies of the defunct Russian Empire"^ 

Northern Iran was drawn into a catastrophic and violent period as a consequence of the 
new Bolshevik policy to dismantle the Russian colonial rule in Asia. So, the Russian Army 
left the huge area. The result was a power vacuum where ethnic groups, Christian and 
Muslim populations, armed banditry and informal militias as well as warlords were hght- 
ing for influence and territorial control. 

it is estimated that up to 1923 about one third of the population in those areas died main¬ 
ly through hunger or during their flights. One of these warlords was Rezah Shah, who 
pushed for power in a reunited Iran. The British tolerated his route to power. Their soldiers 
were tired of the new war period in the Near- and Middle East. Britain anyhow was mainly 
interested in the control of the oil-reserves at the Gulf. On his way towards power the fu¬ 
ture Shah crushed the parallel movements of the Iranian Ayatollahs to create a Shiite State, 
so they needed another 60 years to achieve their goal in 1978. 


= Gratien and Pitts (2014), pp. 239-252. 

“ Atabaki and Ehsani (2014), pp. 261-290. See especially, Atabaki and Ehsani (2014), pp. 266-283. See also, Re- 
ichmuth (2014), pp. 47-58. 


36 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 



The expulsion of the Greek population out of Anatolia after Greek attacks 
in Anatolia^ 

Kemal Ataturk, who had defeated the Greek Army, whieh attempted to oceupy parts of 
Anatolia, foreed all Greeks out of the eoastal areas of Anatolia and to Greeee. They had lived 
for many hundreds of years in Asia Minor. 

A similar fate had struek the Muslim population a few years earlier after the second Bal¬ 
kan War of 1912 when several hundreds of thousand refugees were forced to leave northern 
Greece and Bulgaria. Hunger and deaths went with these events. 


Human Catastrophes in Postwar Eastern Europe 

The breakup of Austria and of Hungary caused millions of people suffering under forced 
population movements. This was in large parts of Southeastern Europe due to the nation¬ 
alism in the restructuring of states. New states were created with disputed borders which 
produced many minorities, who were discriminated and longed for revision. 

in Russia*^ especially after the extreme harsh treaty of Brest Litowsk 1917 turmoil devel¬ 
oped. it led to more human lives lost than during the war in Russia itself. A violent mixture 
through merging of the beginnings of the civil war between “Red” and “Whites”, peasant 
rebellions, ethnic conflicts caused that large Regions were poisoned through enormous 
violence without any humanitarian rules. 

Especially the Ukraine was in permanent crisis i. Widespread Banditry added to it and 
massacres against Jews happened. Actions of a Gzechoslovakian Army of former prisoners 
of war operated. Bolshevik armies lost and re-conquered provinces. The military activities 
of the Germans to defend their extended lines up to the Caucasus and Baku were an ad¬ 
ditional factor. By taking enormous quantities of food to Berclaudilin and Vienna this led 
together with the insecurity for the peasants to widespread deadly hunger. 


The Influence Pandemic 1918/ 9^ 

it is difficult to include the influence pandemic to our theme, because it really worked 
worldwide, it is discussed that it might have been the biggest pandemic in the known his¬ 
tory of man-kind. There are two connections with the War. it is argued that the pandemic 
originated in northern China and that Chinese contract worker brought it to Erance. The 
alternative explanation is, without knowing its origins, that it broke out in USA-military 
camps, especially in Kansas were Soldiers for the fronts in Erance were concentrated and 
brought it to the front. 

So, it is possible to argue that the chain of worldwide communications during the war 
increased and hastened the spread decisively. The biggest figures which are given are 50 to 


® Fromkin (1989), pp. 540-557. 

"■ Leonhard (2014), pp. 811-826. 

’ Johnson and Muller (2002), pp. 110-124. See also Phillips (2014), pp. 303-314. 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


37 



100 Million deaths or about 5% of the world population. Older estimates eame to about 20 
million, equaling the deaths eaused by the war itself. In India about 17 million were killed. 
In Iran aceording to estimates died 2 Million or 8% of the population. Howard Philips from 
South Afriea believes that the last German attempt to wage a decisive offensive in France 
1918 was weakened by hundreds of thousands of flus infections. Jurgen Muller from Han¬ 
nover in our research group claimed for the African coastal population almost 10% of 
victims. The long-term impact as far as we know for Africa is that people were confused 
because of so many unexplained deaths of the young people. The impact of the pandemic 
therefore strengthened prophetic movements in Ghana, Congo and South Africa. 


The impact on the Near- and Middle East since 1916® 

The most intensive impact of the War outside Europe affected the Near East which lasted 
to 1923. Targe scale hghting happSened between the Ottoman and German Troops and the 
British in order to control the Suez Canal and the oil reserves. A huge contingent of Indian 
soldiers was one aspect. Military activities of Arab militias in the context of the 1916 upris¬ 
ing were the other. They were encouraged by the Hashemite dynasty in Mecca along with 
British encouragement of Arab nationalism. 

This had long term consequences. The Sykes-Picot agreement of a colonial partition of 
Syria, Palestine and Mesopotamian was a cynical break with all promises to Arab national¬ 
ism, including that of a Jewish Homeland by the Balfour Declaration 1917. This intensihed 
the conflict between Zionist and Palestinians. The Tandlords sold their land to the Zionist 
settlers with 80 times higher prices of the real value. The conflict between Palestinians 
and Zionist settlers became violent. But the large Palestinian absentee Tandowners were 
blocking any attempt of the British in Tondon to achieve a compromise. Palestine became 
a British Protectorate. 

The main trouble developed immediately after the War. The partition of Anatolia was 
decided at the peace conference in Sevres. The two powers in alliance with the Greek and 
Italian armies tried to implement it. The Greek army came close to Ankara; the Italian oc¬ 
cupied a Section of the Southeast. This increased the chaos in South East Anatolia. How¬ 
ever, Kemal Ataturk created an army independent from Istanbul with soldiers who had 
deserted the Ottoman army during the war and lived of banditry. Hr integrated also ethnic 
based Anatolian militias. This army crushed the Greek Army and forced the British and 
French troops out of Anatolia. The war tired British soldiers were not any longer ready to 
hght. Britain’s withdrawal left the turmoil to its fate, also in Iran Iran. The Kurds missed 
their chance of a Kurdish united state, because of hghts between various Groups. Only 
in northern Mesopotamia, later Irak the British conceded autonomy, which is still func¬ 
tioning. The new Turkish Regime got a new peace treaty in Tausanne 1923 recognizing the 
borders of modern Turkey. 

The Hashemite used the opportunity of the War to establish their rule in Syria. They had 
used the military power of the Arab militias in order to establish its Dynasty in Damascus. 


Fromkin (1989). 


38 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 



But the French chased them later away. The British Government was divided how to deal 
with Arah nationalism. The Oriental experts of the Foreign Ofhce wanted elements of Arah 
independence. Other groups in the British Government simply wanted the Oil. The em¬ 
powerment of the fiashemite was the compromise. With Mesopotamia the British secured 
her second oil-region after controlling the oil recourses in Southern Iran together with 
the US Companies. A Fiashemite became King. This monarchy was toppled as late as 1958 
by Saddam Fiussein. Another member of the Fiashemite was established as King of Jordan. 
The dynasty is still in power. Ibn Saud the king of Saudi Arabia conquered Mecca 1926 and 
forced the Fiashemite to give up their old center but recognized their new positions. 


Asian Dimensions 
China'’ 

The Chinese Delegation was not allowed participating in the Conference despite of its co¬ 
operation with sending a huge workforce to Europe. The handling of the Eastern Asian 
Affairs by the big powers was regarded as a against expectations in China. The unequal 
treaties, which gave Europeans and Americans enormous privileges, were not cancelled as 
Soviet Russia had done. Japan got the former German Colony of Tsingtao.This the Chinese 
regarded as a real scandal it was obvious that Japan aimed at full colonial control of China. 
The 21 demands published 1915 formulated the long term aims of Japan. 

The fourth of May Movement in 1919 was the Chinese reaction on those decisions. Stu¬ 
dents and nationalist bourgeoisie protested and gave to the Chinese revolution, which had 
started 1912, a more radical dimension. The Chinese Communist Party was founded. A co¬ 
alition with Chiang-kai-shek’s Kuomintang was formed. After he had defeated the war 
Lord in China, he tried to destroy the Communist Party using massacres as methods since 
1926 which forced the communists under Mao to risk the long March in 1934. 

A similar incident with long-term repercussions was that fJo Chi Ming from Vietnam 
had hoped to move the French towards colonial reform but was blocked from attending 
the Peace Conference and went back. 1930 the Communist Party of Vietnam was founded. 


Japan’® 

Japan got and got an equal naval standard with the USA in 1922. The militaristic aggres¬ 
sive tendencies toward China were a permanent issue, it was in line with the imperialistic 
tendencies in Japan since the 1880s. The Firdst World War was a good occasion for further 
expansion. But social tensions related to the war, in order to achieve general suffrage in¬ 
creased. After 1923 proto-fascist tendencies developed, it is likely that they were related to 
the social crisis which was accelerated by the catastrophe of the huge Earthquake in Tokyo 


’ Guoqi (2014). p. 59. See also, Tse Tsung (i960). 
“&gt; Weber (2014). 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


39 



and not related to the consequences of the War. But the parallels to European fascism and 
authoritarian rule of the decades after the War are striking. 


Indian anti-colonial movements after the War^^ 

The leadership of the Indian Congress Party supported Britain in its War against Germany. 
They were hoping that their loyalty would lead to substantial reforms toward self-gov¬ 
ernment. This was fully disappointed. Indian nationalist sentiments, especially those of 
Muslims were radicalized when the British organized a massacre in Amritsar 1919. These 
Indian demonstrations were a reaction that Britain had supported the abolishment of the 
Caliphate in Constantinople. So here we can see a direct impact caused by unfulhlled ex¬ 
pectations, especially because the new reform act of 1919 was producing deep anger in the 
Indian congress Movement. Ghandi who had left South Africa 1915 started his nonviolence 
campaigns in India 1921. 


Australia and New Zealand^^ 

The disaster of Gallipoli, where the Australian and New Zealand troops were heavily deci¬ 
mated by Ottoman and German troops who successfully defended the path towards Con¬ 
stantinople was celebrated as Anzas Day in 26* April up to today, it and accelerated the 
process of national Identity in both States. 


Conclusion 

The Asian, Tatin American and African developments were not decisively influenced by 
the war, except that a general change of climate for self-determination had increased. The 
mainstream of world economic hegemony was not changed and shifted towards a US he¬ 
gemony. The world economic crisis had a worldwide impact in 1920-1922 and again since 
1926 in the tropical economies, it cumulated partly caused be the hnancial turmoil through 
war debts and reparations in 1928 and lasteted in many regions upt to 1938. Germany was 
denied the right to export goods up to 1925. up 1938. So, a rather long term impact can be 
discussed. 

The period of the First World War still was part of the classical imperialist period of the 
19* and early 20* Gentury. The period of Decolonization was mainly a result of the Second 
World War, perhaps with the exception of the Arabic World and India. 

Saying this, 1 would like to underline that we should avoid exaggerating the impact of 
World War 1 in the World outside Europe. We have to observe the long dure of social, po¬ 
litical and economic developments in the large societies of the non-European world. They 


“ Keith(2014,pp. 329-352). 
Beaumont (2014, pp. 185-196). 


40 


A PRIMEIRA GUERRA MONDIAL. NA BATALHA DE LA LYS 



were off more heavily influeneed through Colonialism and Imperialism before the war hut 
heading for social crisis’s. Tenth of thousands of punitive military actions of all colonial 
powers still happened. The military and economic impact of Europe’s successful indus¬ 
trialization and imperialist onslaught was answered hy many Reform- movements, in the 
Ottoman Empire, in Japan and China and in. Some started in the hrst decades of the 19th 
Century so Latin America, others around the middle of the century so the Ottoman Em¬ 
pire. Others began at the End of the century so in China, Japan and India. But it is possible 
to say that the War accelerated by the war and perhaps even more by the decisions which 
were taken in the peace treaties. During the interwar period after 1919 this process contin¬ 
ued, and the end of another World War 1945 brought this process to a new height. 

Einally, a word about the millions of victims in the hrst mechanized War. Stig Eorster 
(2014, pp. 29-46) in his overview in “The World during the Eirst World War” has argued 
that especially the German Elites and most probably the Elites of other powers as well were 
not caring that their offensives were killing in each large battle around Verdun, the Somme 
and others hundreds of thousand soldiers oftrenin a day. Their enormous class distance to 
the normal soldier produced this uncultured neglect, in Germany the elites hoped for vic¬ 
tory in order to avoid revolution and democracy after the war. Others needed War aims to 
appease the masses, it was risk taking and killing of many millions including workers from 
many countries and within the Near East without caring. 


References 

Akcam, T. (2004). Armenien und der Volkermord. ffamburg. 

Atabaki, T. and Ehsani, K. (2014). “Oil and Beyond”, in: The world during the First World 
War, pp. 261-290. 

Beaumont; J. (2014, pp. 185-196). “The impact of the Gallipoli Campaign”, in: The world 
during the First World War, pp.185-196. 

Bley, H. &amp; Kremers, A. (eds.) (2014). The world during the First World War. Essen: Klar- 
text Verlag. 

Bley, H. (1996). Migration und Ethnizitat im sozialen, politischen und okologischen Kon- 
text: Die Mijikenda in Kenya, in: Klaus Bade (ed.). Migration, Ethnizitat, Konflikt. Osna- 
brilck, pp. 305-328. 

Bley, H. (1995). Die koloniale Dauerkrise in Westafrika: Das Beispiel Nigeria, in: Rother- 
mund, D. (ed.). Our laws, their lands. Land use and land laws in modern colonies socie¬ 
ties, Munster: Lit Verlag, pp. 37-58. 

Braukamper, U. (2015). Afrika 1914-1918. Antikolonialer Wider stand jenseits der Welt- 
kriergsfronten. Berlin. 

Cornwell, G. H. (2014). The Great War on the Moroccan Eront. in: Bley. H. and Kremers, 
A. (eds.). The world during the First World War. Essen: Klartext Verlag, The world during 
the Eirst World War, pp. 253-260. 

Fall, B. (2014). EorcedLaborinErench-West Africal900-1919. in: Bley. H. and Kremers, A. 
(eds.). The world during the First World War. Essen: Klartext Verlag, pp. 113-118. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


41 


Falola, T. Ogchukwu, E. (2014). Consequences of the First World war for Africa”, In: Bley. 
H. and Kremers, A. (eds.). The world during the First World War. Essen: Klartext Verlag, 
pp.89-100. 

Forster, S. (2014). Not the end of the world. The Great War in Global Perspective. In In: 
Bley. H. and Kremers, A. (eds.). The world during the First World War. Essen: Klartext 
Verlag, pp. 29-46. 

Fromkin, D. (1989). A Peace to End all Peace. The Fall of the Ottoman Empire and the Cre¬ 
ation of the Modern Middle East. New York. 

Geli, P. (2014). Representations of the Great War in the South American Left. The Socialist 
Party of Argeninia. In: Bley. H. and Kremers, A. (eds.). The world during the First World 
War. Essen: Klartext Verlag, pp. 201-214. 

Gratien, Ch. and Pitts, G. (2014). Towards an Environmental History of World War I. In: 
Bley, H. and Kremers, A. (eds.). The world during the First World War. Essen: Klartext 
Verlag, pp. 239-252. 

Guoqi, X. (2014). China’s Great War. In: Bley. H. and Kremers, A. (eds.). The world during 
the First World War. Essen: Klartext Verlag. 

Johnson, N.PA.S. and Muller, J. (2002). “Updating Gohal Mortality of the 1918-1920 Span¬ 
ish Influenca Pandemix”, Bulletin of the History of Medicine, 76,1, pp. 110-124. 
Leonhard, J. (2014). Von der Eront zum Gewaltraum: Diktatfrieden und Bilrgerkrieg im 
Osten Europas. In: Bley. H. and Kremers, A. (eds.). The world during the First World War. 
Essen: Klartext Verlag, pp.811-826. 

Mesehkat, K. and Buekmiller, M. (2007). Biographisches Handbuch zur Geschichte der 
Kommunistischen Internationale. E-Book, University of Hannover: DeGruyter. 

Nelson, K. (2014). Several Aspects of the British Empire in the hrst World War. In: Bley. 
H. and Kremers, A. (eds.). The world during the First World War. Essen: Klartext, pp. 
329-352. 

Phillips, H. (2014). Nogreat War, noGreatElu - no German Defeat? In: Bley. H. andKremers, 
A. (eds.). The world during the First World War. Essen: Klartext Verlag, pp.303-314. 
Reiehmuth, S. (2014). The Transformation of Muslim Societies and the Reorganization of 
Muslim Statehood during and after the Eirst World War. In: In: Bley. H. and Kremers, A. 
(eds.). The world during the First World War. Essen: Klartext Verlag, pp. 47-58. 

Rinke, S. (2014). Thunderstorm a Lightening. A hrst look at Latin America the Eirst World 
War. In: Bley. H. and Kremers, A. (eds.). The world during the First World War. Essen: 
Klartext Verlag, pp.79-88. 

Rothermund, D. (1983). Die Peripherie in der Weltwirtschaftskrise: Afrika, Asien und 
Lateinamerika. Schoningh: Paderborn. 

Tse-tung, Ch. (i960). The Map 4th Movement: Intellectual Revolution in Modern China 
(Harvard East Asian). Harvard University Press. 

Weber, T. (2014). Der lange Schatten des kurzen 20. lahrhunderts: lapan und der Erste 
Weltkrieg in Ostasien. Web Max Weber Stiftung. 


42 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Portugal e a beligerancia na Grande Guerra: 
Razoes de uma interven9ao^^ 


Luis Alves de Fraga 

Universidade Autonoma de Lisboa (UAL) 


Resumo: Breve explica^ao das dependencias externas de Portugal, tanto no final da Mo- 
narquia Constitucional como nos primeiros anos da Primeira Repiiblica, para permitir 
perceber a revolugao republicana ate, praticamente, ao final da Grande Guerra. Depois, 
evidenciar a dependencia politica de Portugal da Gra-Bretanha e de como foi objectivo, 
atraves da interven^ao de Portugal na guerra, quebra-la, impedindo a possibilidade de o 
Governo britanico poder usar Portugal ou as suas colonias como processo de alcan^ar uma 
paz negociada. Demonstrar que a beligerancia portuguesa constituiu um objectivo nacio- 
nal e que a oposi^ao britanica a beligerancia era tambem um objectivo nacional assumido 
por Londres, para, sub-repticiamente, poder ter mao livre aquando das negocia^oes da 
paz. 

Palavras-chave: Portugal; Grande Guerra; Beligerancia; 1.“ Repiiblica; Gra-Bretanha. 

Abstract: Brief explanation of the external dependencies of Portugal, hoth at the end of the 
constitutional monarchy and in the hrst years of the First Republic, to allow to perceive 
the republican revolution until, practically, the end of the Great War. In addition to that, 
highlight the political dependence from Great Britain and how it was an objective, through 
the intervention of Portugal in the war, to break it, preventing the possibility of the British 
Government being able to use Portugal or its colonies as a process of achieving a negotiated 
peace. To demonstrate that the Portuguese belligerence was a national objective and that 
the British opposition to belligerence was also a national objective defended by London in 
order to surreptitiously, be able to have free hand during the peace negotiations. 

Keywords: Portugal; Great War; Belligerence; First Republic; Great Britain. 


O autor nao escreve segundo o Acordo Ortograhco. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


43 



Antecedentes 


Na viragem do seculo XIX, Portugal foi um pals que nao soube e nao conseguiu acompanhar 
a Revolu^ao Industrial, essencialmente por dois motivos: um, o subsolo e pobre em carvao 
e ferro - materias-primas essenciais ao desenvolvimento de entao - e, outro, se continuava 
a privilegiar a agricultura como forma de manifesta^ao de riqueza, encaminhando para a 
aquisi^ao de propriedades agrleolas o eapital que poderia ser utilizado na industrializa^ao 
posslvel (Fraga, 2014a). 

Necessariamente, estamos a falar de um tipo especlfteo de mentalidade, que nem tern 
semelhan^as com a generalidade da postura dos povos de Espanha, embora possa asse- 
melhar-se a algumas regioes do Estado vizinho. Se quisessemos ir buscar uma posslvel 
justibca^ao para esta ausencia de empreendedorismo industrial, talvez a encontrassemos 
numa certa voca^ao maritima comercial a grande distancia, que tera debnido para o Estado 
portugues um modo de se posicionar no mundo, condicionando, desta maneira, a forma de 
pensar das gentes nacionais. 

Reflectindo sobre o que acabamos de dizer, e posslvel, tambem, justibcar o apego ao 
imperio colonial restante, no bnal do seculo XIX, como elemento Salvador da pobreza 
endemica do subsolo nacionab especialmente, os grandes territorios de Angola e de Mo¬ 
zambique constituiam a reserva estrategica de um desenvolvimento futuro e que nunca 
chegou a efectivar-se. Deste modo, tanto para a Monarquia constitucional como para os 
republicanos da 1.“ Republica, as colonias e a sua defesa eram imprescindiveis, pois o futu¬ 
ro de Portugal estava essencialmente ligado a elas, como havia estado ligado ao Brasil ate 
quase ao bnal do primeiro quartel do seculo XIX. Compreender este facto ajudar-nos-a a 
compreender nao so a Republica como regime politico, em Portugal, como, ate, de modo 
indirecto, a perceber a necessidade de o pals ser beligerante na Grande Guerra. 

Foi, em grande parte, porque a Monarquia liberal nao soube acautelar a integridade colo¬ 
nial - tenha-se em aten^ao o ultimato britanico, em 1890 - que os republicanos ganharam 
preponderancia e visibilidade. Mas nao foi so essa incapacidade que caracterizou negativa- 
mente a Monarquia liberal; ela foi marcada pela ma governa^ao bnanceira do pals, donde 
resultaram continuados saldos orzamentais debcitarios. Era imposslvel governar sem re- 
curso ao favor orzamental, gerando empregos pagos pelo Estado. Ampliou-se, num tempo 
que teria de ser de conten^ao, o numero de empregados publicos, entre civis e militares. Os 
impostos, tal como estavam estruturados, nao chegavam para pagar a quern servia o Estado 
nem para pagar os juros da dlvida gerada pelos debcits constantes (Eraga, 2014b). 

Outra caracterlstica negativa da Monarquia constitucional - identibcada e assim classib- 
cada, naturalmente, pelos republicanos - consistiu no facto de existir como que um pacto 
entre o clero catolico e o trono. Pacto que, na pratica, resultava no apoio mutuo das duas 
instituizoes: a Monarquia segurava e assegurava o estatuto do clero e este mantinha docil o 
Povo para aceitar aquela. Isto era posslvel por dois motivos: o clero era conservador, numa 
Igreja que abominava as modernidades do seu tempo, e o Povo era ignaro e paciente. 

Em suma, Portugal, na viragem do seculo XIX, era um pals atrasado em relazao a mo- 
dernidade que se vivia para alem dos Pireneus; era um pals a pedir uma revoluzao capaz de 
quebrar atavismos e conservadorismos ja entao inaceitaveis. Na ausencia de outro, o motor 
da revoluzao era o Partido Republicano Portugues (PRP), com um programa, que, na epo- 
ca, se apresentava radical em determinados aspectos. 


44 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


A Repiiblica 


Nao vamos aqui relatar o que foi a Revolugao de 5 de Outubro de 1910, porque ela nao 
e aquilo que vulgarmente julgamos. Com efeito, a palavra revolu^ao tern varios signifi- 
cados e, sendo um «conjunto de altera^oes bruseas e profundas nas estruturas politieas, 
econo micas, sociais e institucionais de uma comunidade, geralmente imposto de forma 
violenta» (Dicionario Infopedia da Lingua Portuguesa sem Acordo Ortografico), pode ser 
tambem um motim ou revolta. E neste ultimo sentido que a palavra revolugao esta asso- 
ciada a data de 5 de Outubro de 1910, porque, no outro sentido - no do conjunto de altera- 
goes - ela so aconteceu depois de formado o Governo Provisorio, que nao foi estavel desde 
o primeiro dia (5 de Outubro) ate a sua dissolu^ao, por vontade propria, cm 4 de Setembro 
de 1911. Na verdade, o conjunto de legisla^ao aprovado pelo Governo Provisorio, porque 
constituido por membros do comite central do PRP, foi revolucionario, introduzindo as- 
pectos modernos na vida portuguesa e criando condi^oes para profundas altera^oes no 
comportamento e na mentalidade dos Portugueses. 

Temos de destacar um pormenor muito importante, o Governo Provisorio nao represen- 
tava, realmente, todas as tendencias republicanas dentro do PRP; assim, quando se elegeu o 
primeiro Presidente da Repiiblica e se formou o primeiro Governo Gonstitucional surgiram 
os primeiros desentendimentos que, menos de dois anos depois, deram origem a cisao e ao 
aparecimento de dois outros partidos republicanos: o Evolucionista e o Unionista; aquele, 
era moderado - de centro, se se preferir - e este era conservador. Liderava o primeiro o Dr. 
Antonio Jose de Almeida e o segundo o Dr. Brito Gamacho. No PRP ficou a ala mais radical 
chefiada por Afonso Costa, que assumiu chamar ao seu grupo Partido Democrdtico. 

Percebe-se que a autoria de uma boa parte da legisla^ao mais revolucionaria do Governo 
Provisorio se ficou a dever a vontade de Afonso Costa, que, afmal, a havia anunciado muitos 
anos antes, na tese de doutoramento, em Coimbra (SOUSA, s. d. 13-19), na qual evidencia 
a sua tendencia para ideais socializantes. Esta perspectiva da Repiiblica ia, de certo modo, 
ao arrepio do pensamento moderado e conservador dos outros dois lideres republicanos. 

Apds a vigencia do Governo Provisorio ia ter inicio o confronto entre o radicalismo de 
Afonso Costa - com uma visao alargada do que poderia vir a ser a Repiiblica Portuguesa - e 
uma pequena reviravolta politica na politica monarquica que se havia herdado depois da 
queda da Monarquia. Julgamos que o peso da tradi^ao tera configurado um tipo de Re- 
piiblica aceite por Antonio Jose de Almeida e Brito Camacho, que pouco ou quase nada se 
identificava com a Repiiblica de Afonso Costa. Por isso, parece-nos - e os factos dao-nos 
manifesta razao - de cada vez que Afonso Costa formou Governo viveu-se, durante esses 
periodos (Janeiro de 1913 a Eevereiro de 1914; Novembro de 1915 a Dezembro de 1917), a 
continua^ao da revolu^ao iniciada no Governo Provisorio, embora com outros objectivos. 
Realmente, no Governo de 1913-1914, Afonso Gosta revolucionou os impostos de tal forma 
que conseguiu, pela primeira vez, desde 1834, superavit ornamental, deixando preparado 
um ornamento com previsao de novo saldo positivo. No Governo iniciado em Novembro 
de 1915, Afonso Gosta pos em pratica uma estrategia diplomatica que visava conseguir a 
beligerancia nacional na Grande Guerra, dando continuidade a revolugao que o animava 
desde os tempos de estudante. E havia razoes para tal. Vejamo-las. 


I. A PROBLEMATIZAnAO DA PRIMEIRA GRANDE GUERRA 


45 


Mai eclodiu a guerra, na Europa, em Agosto de 1914, come^ou a circular o desejo de apoio 
a Gra-Bretanha, aliada de Portugal, para defesa das coldnias. Vamos tentar descodificar 
esta formula, aparentemente simples, de demonstrar o desejo de ser beligerante. 

- Defender as eolonias do ataque de quern? 

Quase evidente, aparece hoje a resposta: 

- Do ataque dos Alemaes, vizinhos no sul de Angola e no norte de Mozambique! 

- Mas Portugal so tinha estas eolonias? 

- Nao. Portugal tinha os arquipelagos de S. Tome e Principe (na epoca, o maior produtor 
mundial de caeau, sofrendo ataques na Camara dos Comuns, em Londres, provocados pela 
fiiria do maior choeolateiro mundial, o senhor William A. Cadbury) (Afonso; Vladimiro, 
1982, 711-713), Cabo Verde (arquipelago estrategieo para a navega^ao do Atlantico Sulpelas 
embareazoes movidas a vapor) e mais outras no Oriente. 

Esta nova perspectiva de eompreender a ideia de defesa das coldnias leva-nos a perceber 
que havia, na abrmazao, uma subtileza so eompreenslvel por parte de quern a soubesse ler. 

- E o que era preciso para saber ler essa subtileza? 

- Saber que, em 1898, Londres e Berlim haviam assinado um tratado de apoio financeiro, 
a Portugal, com um anexo secreto, no qual se previa a partilha de quase todas as eolonias 
naeionais entre a Gra-Bretanha e a Alemanha, a troeo de os Alemaes reduzirem o seu in- 
vestimento na construzao da frota naval. 

Curiosamente, em Lisboa, em 1912, eomezou a suspeitar-se que novo acordo estava a ser 
negoeiado, em Londres, entre os mesmos dois Governos (Lichnowsky, 1918) o qual so foi 
interrompido com a eclosao da guerra, em Agosto de 1914. Entao, para os mais esclarecidos 
politieamente, nao se tratava de defender as coldnias do perigo alemao, mas da Perfida Al¬ 
bion (Ximenes, 1820,160). Havia receios e desconbanzas da polltica britanica, cujas raizes 
se entranhavam no passado, porque, pelo menos desde as Invasdes Erancesas, Portugal 
vivia sob uma feroz tutela britaniea. 

A Revoluzao Liberal, em 1820, com a imposizao da salda da obcialidade inglesa do pals, 
levando a frente o detestado Beresford, nao foi suftciente para reduzir a intervenzao brita¬ 
nica nem para colocar Portugal num patamar de igualdade soberana com a Gra-Bretanha. 
Os constantes desentendimentos politicos, mesmo no perlodo do constitucionalismo libe¬ 
ral, deram sempre oportunidade a que a Inglaterra tivesse uma palavra a dizer na corte e na 
soeiedade portuguesas. Essa oportunidade ampliava-se de cada vez que, para eolmatar os 
deftcits orzamentais, se tinha de contrair emprestimos em Londres. Portugal, mesmo no 
final do seeulo XIX, continuou a ser um Estado tutelado pela Gra-Bretanha. E, eom des- 
gosto da ala mais radieal dos republieanos, o mesmo aeonteeeu, mesmo depois de Outubro 
de 1910. De certo modo, a situazao agravara-se, pois, a Repiiblica, numa Europa essencial- 
mente monarquica, coneitara sobre Portugal o desejo de anexazao por parte de Espanha 
(Torre-Gdmez, 1978), sendo a diplomaeia inglesa quern eontinha Madrid em estado letar- 
gieo. Por outro lado, a Repiibliea prosseguiu a pratiea de uma polltica ftnanceira debcitaria, 
contando com as boas vontades da Gra-Bretanha. Assim, parece, nao havia como fugir a 
tutela. Contudo, expliquemos melhor como poderia ser redireeeionada essa polltica e des- 
feita essa tutela. 


46 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Antes do mats, era necessario que em Portugal se deixasse de acreditar na bondade bri- 
tanica como Velha Aliada; era preciso que se entendesse que a politica tern varias caras e 
que nem tudo o que se passa no seu ambito obedece aos mesmos principios eticos da vida 
dos homens em sociedade. 

Depois, era preciso que se percebesse - que toda a gente, em Portugal, percebesse - que, 
no piano externo e das rela^oes entre Estados soberanos, nao ha uns que tutelam outros, 
ou seja, nao ha Estados mais importantes do que os restantes, ainda que, alguns, sejam 
militarmente mais poderosos ou economicamente mais ricos. 

Em seguida, tornava-se imperioso fazer compreender que o Estado credor, porque em- 
presta ou e bador de emprestimos, nao passa a ter sobre o Estado devedor qualquer tipo de 
superioridade ou de autoridade. 

Por ftm, impunha-se a percep^ao, em Portugal, por toda a gente com alguma ilustra^ao, 
que, tanto ou mais importante do que ser, no piano internacional, e parecer, isto e, o que 
se julga de um Estado e mais relevante do que aquilo que ele e na realidade. 

Ora, estes principios resultantes da pratica das relaqoes internacionais eram atropelados 
pelas praticas politicas internas herdadas do passado, cheias de manhas, de preconceit os e 
de irregularidades. De tudo isto nao estava isento, tambem, o Partido Democrdtico, sim- 
plesmente nele pugnava-se por um novo tipo de projecgao internacional hem diferente do 
que se havia her dado da Monarquia. Era nisto que se consubstanciava uma parte do sentido 
revoluciondrio de Afonso Costa e dos sens seguidores. 

Para se perceber a forma como o lider mais radical dos politicos republicanos pretendia 
executar a sua estrategia internacional, temos de passar a analise da forma como foi vivida 
a guerra em Portugal, desde a primeira bora. 


A Guerra 

Nos primeiros dias de Agosto de 1914, Lisboa, atraves do seu ministro plenipotenciario em 
Londres, Manuel Teixeira-Gomes, interrogou o Foreign Office sobre o que a Gra-Bretanha 
esperava do Governo portugues. A resposta nao demorou: o Governo de Sua Majestade 
desejava que Portugal se declarasse nao neutral e nao beligerante. 

Vejamos quais eram os beneficios para ambos os Estados pela aceita^ao desta postura 
portuguesa perante o conflito que acabava de estalar e que todas as chancelarias julgavam 
iria ser de curta dura^ao. 

Para a Gra-Bretanha era absolutamente necessario que Portugal nao fosse neutral, pois, 
sendo-o, perderia certas vantagens que benebciariam tanto os navios da Armada britanica 
como o seu Exercito, ja que havia normas muito precisas para a permanencia de embar- 
ca^oes de guerra em portos neutrals tal como as havia para a travessia de territorios. Ora, 
a Gra-Bretanha carecia de desembarcar em portos ultramarinos portugueses tropas para 
refor^o das suas colonias africanas sem acesso ao mar e de abastecer os sens navios com 
carvao nos portos atlanticos nacionais. Objectivamente, a nao neutralidade portuguesa 
servia objectivos politicos e militares britanicos. 

Para Portugal a situa^ao de nao neutral poderia representar uma excelente vantagem, 
pois colocava-o, sem o desgaste da beligerancia, na mesa das conversaqoes bnais do con¬ 
flito, se fossem os Aliados os vencedores da guerra. Mas essa vantagem desfazia-se, de 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


47 


imediato, com a segunda parte do desejo britanico: que nao fosse beligerante. Ora, nao 
sendo beligerante, Portugal tinha de ser neutral, mas essa condi^ao estava-lhe vedada pela 
exigencia de Londres e assim la la por agua abaixo a possibilidade de estar na mesa das 
negocia^oes finals. Fieava evidente, agora sem diividas, o estatuto de Estado tutelado pela 
Gra-Bretanha. 

Para o Reino Unido, Portugal nao ser beligerante, trazia vantagens, pois Lisboa nunea 
poderia reivindicar qualquer tipo de beneffcios ou compensa^oes materials no final da 
guerra, exaetamente por nao ter sido beligerante. 

Enfim, a Velha Aliada ganhava vantagens e Portugal perdia, em toda a linha, qualquer 
tipo de migalhas que pudessem eair da mesa da eonfereneia da paz. 

Pior do que as negocia^oes de paz, que lam ainda longe logo no comedo da guerra, foi a 
instabilidade interna que este desejo ingles veio eausar, pois serviu para dividir, de imedia¬ 
to, as opinioes nacionais: por um lado, fiearam os que acreditavam na bondade britaniea e, 
pelo outro, os que duvidavam dela. Mais grave ainda, foi a deelara^ao feita, no Parlamento, 
por Bernardino Machado, pois era tao ambfgua que a Franca - Estado invadido e carente de 
todos os apoios internacionais -, a princfpio, a tomou como uma afirma^ao de beligerancia 
ate que sofreu o terrivel desengano (Fraga, 2012 a). 

Joao Chagas, ministro plenipotenciario em Paris, homem informado quanto a Gra-Bre- 
tanha e ao seu usual meio de proceder, politico e jornalista experiente, percebeu a urgencia 
de uma tomada de posi^ao em Franca e pediu autoriza^ao a Lisboa para se deslocar a Por¬ 
tugal, sem que, todavia, em Bordeus, falasse primeiro com o ministro dos Negocios Estran- 
geiros frances e Ihe desse conhecimento das suas intenqoes, ao mesmo tempo que, com 
subtileza, ihe pediu auxilio para conseguir levar a bom porto a missao que o fazia deslocar- 
se ao seu pais (Eraga, 2012 b). A verdade e que tudo resultou como ele previra: a Eran^a 
pediu auxilio a Portugal, atraves de este ceder, aquela, armamento que ihe fazia falta. Mas 
Portugal tinha uma alian^a com a Gra-Bretanha e nao com a Eran^a. Assim, Paris tinha de 
envolver Londres no pedido de auxilio e, deste modo, o Governo britanico tinha de recuar 
na sua posi^ao inicial. O lago estava a apertar o Foreign Office. 

No entanto, na Gra-Bretanha tinha-se conhecimento perfeito das incapaeidades logis- 
ticas portuguesas e, tambem, dos desentendimentos politicos. Eoi nisto que se apostou. 

Manuel de Arriaga, Presidente da Repiiblica, com o auxilio indirecto de Machado Santos 
e de parte da oficialidade do Exercito da guarni^ao de Lisboa - atraves do ehamado Golpe 
das Espadas - levou a que o Gabinete Azevedo Coutinho - claramente prd-beligerante - 
apresentasse a demissao e fosse, por iniciativa presidencial, nomeado o general Pimenta 
de Castro para formar Governo, com a inten^ao de retardar ou acabar com os preparativos 
beligerantes resultantes do pedido frances. Desta maneira a Gra-Bretanha, sem grande es- 
forqo, viu Portugal bear sem o armamento, que seguiu para Eran^a, e sem concretizar o 
seu desejo de beligerancia. Internamente, os nao-beligerantes encarregaram-se de faci- 
litar os objectivos do Governo de Londres. Voltava-se a situa^ao anterior: nao beligerante 
e nao neutral. Agora, os adeptos da beligerancia - nao por ela propria, mas como meio de 
alcan^ar um objectivo nacional e internacional, como ja explicado - tinham de reverter a 
situa^ao interna e tentar alterar a situaejao externa. Tudo se complicava. 

O Partido Democrdtico teve de recorrer a revolta militar para conseguir alterar a po- 
lltica nacional. Eoi assim que, depois de o general Pimenta de Castro, em Marqo de 1915, 
ter come^ado a governar sem apoio do Parlamento, impedido de reabrir, se iniciou uma 


48 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


conspira^ao que levou, em 14 de Maio, ao derrube do Governo e a resigna^ao do Presidente 
Manuel de Arriaga o qual, deelaradamente, havia entrado em ruptura eom a politica beli- 
gerante. 

Quando Afonso Costa, depois de algumas peripeeias de ordem nao politica, assumiu, b- 
nalmente, a presidencia do Ministerio, em 29 de Novembro de 1915, estavam criadas as 
condi^oes para, novamente, se debnir toda a estrategia condutora a beligerancia e, por 
conseguinte, capaz de levar Portugal a paridade de soberania com a Gra-Bretanha, sonho e 
objectivo do Partido Democrdtico. Todavia, um obstaculo se levantava: a oportunidade de 
o Governo britanico pedir a interven^ao de Portugal na guerra. Havia, por conseguinte, de 
cria-la. Ela surgiu poucos meses apos o inlcio da governa^ao, tendo como base de partida o 
facto de elevado numero de navios mercantes alemaes e austriacos, no comedo da guerra, 
tomando como princfpio a neutralidade portuguesa, se terem recolhido em portos nacio- 
nais. Dada a actividade destruidora dos submarinos germanicos sobre os navios mercantes 
britanicos, a tonelagem de transporte inglesa estava bastante reduzida, facto que afectava 
o comercio aliado. Londres carecia de todos os navios possfveis e mais de setenta estavam 
paralisados nos portos portugueses; impunha-se requisita-los ou toma-los como presa. O 
Governo de Lisboa fez aprovar, na lei que regulava o pre^ario e a distribui^ao dos generos 
alimentares, um artigo no qual se permitia a requisi^ao de todos os meios de transporte 
estrangeiros estacionados em territorio nacional e respectivas mercadorias (MARQUES, 
1974). Estava montada a armadilha. 

Enquanto se preparava o processo de levar Londres a solicitar o auxflio de Portugal, o 
Governo de Lisboa, para proceder ao rearmamento, organiza^ao e equipagem do Exercito 
tinha de pedir um emprestimo de tres milhoes de libras a Gra-Bretanha e o Foreign Of¬ 
fice, pela boca de Sir Edward Grey, resolveu, tambem, armadilhar a resposta, de modo 
a obrigar o Governo portugues a requisitar os navios sem alterar o estatuto imposto pela 
Inglaterra. E fe-lo do seguinte modo: emprestava dois milhoes e o terceiro milhao bcava 
condicionado a Lisboa requisitar os navios alemaes e austriacos (Vicent-Smith, 1975). 

Naturalmente, esta era a contra-armadilhahritanica. Em Portugal sentiu-se o efeito do 
jogo de Londres. Mais uma vez, a Velha Aliada estava a fugir a invocar a alian^a e a impor 
ao Governo da Republica outro favor. Contra isso reclamaram os ministros em reuniao do 
Gabinete (Praga, 1912 a, 286-297). Havia que fazer brago deferro. Mas, na Gra-Bretanha 
sentia-se terrivelmente a falta de navios e cerca de duzentas e quarenta mil toneladas bru- 
tas representava um auxflio extraordinario. Deste modo, por pressao do chamado Gabi¬ 
nete de Guerra, o Foreign Office teve de apelar para a requisi^ao dos navios ao abrigo da 
alian^a luso-britanica. Estava ganha uma parte da partida; havia que garantir a restante. 
Para tal, em Lisboa, no designado quadro do Tejo, os navios alemaes foram tornados por 
destacamentos da Armada, como se de um acto de guerra se tratasse, e, no bnal, um navio 
de guerra portugues disparou vinte e um tiros de salva saudando o feito, com o claro intuito 
de provocar o ministro plenipotenciario alemao em Lisboa, barao von Rosen. E conseguiu- 
se o efeito desejado: a Alemanha declarou guerra a Portugal. Estavam alcan^ados todos os 
objectivos portugueses. 

Restava, para se consumar a politica revoluciondria de restaura^ao da dignidade in- 
ternacional portuguesa, fazer marchar para as trincheiras de Eran^a uma grande unidade 
militar, cujo efeito tactico e estrategico seria quase nulo, mas cujo efeito diplomatico e 
politico era de importancia maxima. Eoi o que aconteceu nos primeiros dias de Eevereiro 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


49 


de 1917. Contudo, em Londres nao se deixou de desenvolver uma politica de obstru^ao aos 
objectives portugueses; agora, se nao se podia ja evitar a beligerancia de Portugal, havia 
que a denegrir, reduzindo-a a uma expressao tactica e estrategica capaz de ser menospre- 
zada aquando da conferencia da paz. E o que vamos ver de seguida. 


O Malogro de uma Politica 

Em Janeiro de 1917 foi, bnalmente, assinada uma conven^ao militar entre Portugal e o Rei- 
no Unido, na qual se previam as obriga^oes aceites por ambas as partes no que concernia 
ao apoio a dar mutuamente. No essencial, o armamento, muni^oes, o transporte e a defesa 
maritima dos navios com tropas ftcava a cargo dos britanicos, sendo que Portugal forne- 
cia homens, pe^as de artilharia (as que ainda existiam em deposito, no pais), fardamento, 
animals de trac^ao, veiculos automoveis e hipomdveis, material sanitario e outro equipa- 
mento. Nao estava prevista nenhuma data para deixar de se fazer o transporte de tropas. A 
organiza^ao da for^a bcava a cargo do Exercito portugues e, na fase inicial, combinou-se 
enviar uma divisao refor^ada, ou seja, cerca de trinta mil homens. Exactamente, porque 
nao era debnitivo o contingente a mandar para Eran^a, deu-se-lhe a designa^ao de Corpo 
Expedicionario Portugues (CEP), embora o ministro da Guerra, Norton de Matos, previsse, 
segundo os sens melhores calculos, colocar nas trincheiras tres divisoes refor^adas (Praga, 
2010, 284-307), formando tres Corpos Expedicionarios. 

Depois de instalado o CEP, e apds varias altera^oes organicas, no mes de Outubro, bcou 
assente, entre Portugal e o Reino Unido, que as formas nacionais passavam a constituir um 
corpo de exercito a duas divisoes sem serviqo de avia^ao e artilharia pesada. Em Novem- 
bro, o general-comandante e toda a tropa assumiram a defesa de um sector de trincheiras, 
com cerca de onze quilometros de extensao, entre as cidades de Armentieres e Bethune, na 
Elandres francesa, quase frente a Lille. Todavia, em Setembro, o Governo britanico tinha 
cortado o serviqo maritimo de transporte de tropas portuguesas para Pran^a (Meneses, 
2018,142-143). Londres, mais uma vez, jogava uma cartada decisiva em rela^ao, agora, a 
presen^a militar portuguesa nas trincheiras. 

Olhando objectivamente, sejam quais forem as explicaqoes arranjadas para justiftcar a 
decisao britanica, para a supressao dos transportes de tropas, sabendo que Portugal nao 
tinha condiqoes para suprir o corte, so se pode concluir que, fosse qual fosse o pre^o, o 
Governo de Londres desejava levar a exaustao o GEP ate que em Lisboa se aceitassem as 
condi^oes impostas pela Inglaterra. 


Quais eram essas condi^oes? 

Temos de recuar aos meses de Agosto e Setembro de 1917. Os comandos militares britanicos 
em Pran^a punham em diivida a possibilidade de as duas divisoes portuguesas formarem 
um corpo de exercito com capacidade de resistir ao desgaste que a simples presen^a nas 
trincheiras imporia, ja que ia deixar de haver ou ja deixara de haver transporte de tropas 
para colmatar as baixas do GEP (Meneses, 2018, 123-124). Assim, Lord Derby, ministro 
da Guerra britanico, em Setembro, escreveu uma carta ao ministro da Guerra portugues. 


50 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Norton de Matos, propondo que as tropas do CEP recuassem para zona menos perigosa, 
se sujeitasse as mesmas a um enquadramento maior por parte de oficiais britanicos e que, 
em caso de empenhamento nas trincheiras, bcasse uma linica divisao em linhas e a outra 
servisse de suporte e reserva de pessoal a retaguarda (Fraga, 2010, 480). Claro que esta pro- 
posta foi violentamente recusada pelo ministro portugues. Mesmo correndo o risco de nao 
haver mais transportes de tropas - sempre se podia fazer pressao sobre o Governo do Reino 
Unido - Lisboa preferiu jogar forte e manter o corpo de exercito em linhas. 

Aparentemente, embora o desgaste se fosse fazendo sentir, dado que o sector portugues 
nao era excessivamente massacrado e nem oferecia condi^oes para sobre ele haver uma 
grande ofensiva, a decisao de Lisboa - pesem embora todas as reclama^oes do coman- 
dante do CEP, general Tamagnini de Abreu e Silva, homem que compreendia mal a verda- 
deira missao da tropa que comandava - tera visado for^ar o Governo de Londres, depois 
do Inverno de 1918 a disponibilizar mais navios para completar os efectivos em falta. No 
entanto, tudo leva a crer, a disposi^ao do War Office ia no sentido oposto: levar o CEP 
ate a exaustao e obrigar Lisboa a reconhecer que tinha de aceitar algumas das condi^oes 
britanicas. Nenhuma das partes, em Novembro de 1917, poderia imaginar que, em Abril 
de 1918, porque no mes de Mar^o nao choveu como de habitual na zona da Flandres fran- 
cesa, o Alto Comando Alemao ia determinar uma grande ofensiva exactamente na frente 
do sector portugues. Muito menos, poderia Norton de Matos e Afonso Costa, imaginar que 
o antigo ministro plenipotenciario em Berlim, major doutor Siddnio Pais ia comandar um 
golpe militar para derrubar o Governo. 

Foi, precisamente, o golpe de Siddnio Pais, nos primeiros dias de Dezembro de 1917, 
que determinou toda a mudan^a operada no mes seguinte. Vejamos com um pouco mais 
de pormenor. 

No nosso livro (Fraga, 2010, 439-523) fazemos uma descri^ao bastante completa das 
consequencias do golpe militar de Siddnio Pais na condu^ao da politica de guerra. 

Ao contrario do que foi propalado e ganhou raizes, Siddnio Pais nao foi germandblo, nem 
praticou uma politica germandbla. Foi, isso sim, angldblo ate ao limite possivel, pois cedeu 
em tudo o que havia a ceder a vontade britanica. Logo apds a vitdria no Parque Eduardo 
Vll, foi visitado, ainda no acampamento, pelo general Barnardiston, acompanhado da es- 
posa e de um outro oftcial. Basta este facto para evidenciar o apoio directo ou indirecto 
do Governo de Londres, tal como o denunciou, na epoca, Bernardino Machado, exilado 
em Paris. Mas o citado general britanico tinha incumbencia mais precisa do que a simples 
manifestacjao de apoio: ele pretendia fazer aceitar por Siddnio Pais o piano de altera^ao or- 
ganica do CEP, empenhando somente uma divisao na frente e bcando a outra na retaguarda 
como depdsito e reforqo de pessoal. Era a machadada ftnal no corpo de exercito portugues 
e no CEP, pois a divisao empenhada na frente ficaria a fazer parte de um corpo de exercito 
britanico e a da retaguarda limitar-se-ia a manter o treino e a instru^ao de todos os mili- 
tares que tivessem de marchar para as primeiras linhas. O CEP seria uma unidade tedrica, 
sem qualquer empenhamento tactico e, muito menos, estrategico. O general-comandante 
seria exclusivamente responsavel pela aplica^ao das penas disciplinares. 

Foi isto que Siddnio Pais aceitou no dia 18 de Janeiro de 1918. Todavia, esta nova organi- 
za^ao nao foi posta em pratica logo de imediato, porque, devido ao empenhamento das di- 
visdes do Reino Unido em toda a frente, faltavam efectivos para guarnecer a pequena frente 
portuguesa. Assim, com base neste exiguo facto, bca provado que a verdadeira intensao 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


51 


britanica nao tinha natureza militar, nao sendo, por conseguinte, nem de ordem tactica 
nem estrategica; era, exclusivamente, de ordem politica, pois, se o nao fosse, tinham-se, 
eom anteeipa^ao, calculado as neeessidades em homens para eobrir a retirada das divisoes 
do CEP! Isso nao foi feito, em tempo algum, quer pelo War Office, em Londres, quer pelo 
estado-maior de Sir Douglas Haig, em Franca. Contudo, absolutamente eerto era o des- 
gaste do eorpo de exercito portugues, pois as primeiras unidades a terem contacto eom o 
inimigo entraram em linhas em Abril de 1917 e por la se mantiveram ate aos primeiros dias 
de Abril de 1918. Nenhuma tropa, sob eomando superior britanico, esteve tanto tempo em 
trincheiras e sujeita ao logo alemao. 

O mes de Mar^o foi altamente desgastante para o CEP, pois, do lado germanieo, estava ja 
a preparar-se uma enorme ofensiva eontra o seetor portugues e os flancos britanieos que 
ladeavam o eorpo de exereito naeional. Nesse mes, o moral das tropas portuguesas caiu a 
vertical. Todas as noites houve bombardeamentos das linhas, dos post os de eomando dos 
batalhoes, das brigadas e, ate das divisoes. O servi^o de informa^oes do estado-maior do 
CEP fez saber ao Alto Comando Britanico as desconftan^as que tinha sobre a eventualidade 
de um ataque alemao, porem os britanieos, porque nao tinham possibilidades de substituir 
por unidades suas as duas divisoes portuguesas, protelaram ate aos primeiros dias de Abril 
a entrada em vigor da nova organiza^ao do CEP. So ja em ultima instancia procederam a 
essa altera^ao. Contudo, deve dizer-se, para que a Historia o registe para sempre, a 2.“ 
divisao - aquela que ftcou em primeiras linhas, nas trincheiras - ocupou o mesmo espa90 
que as duas divisoes do CEP oeupavam anteriormente. Ou seja, enfraqueceu-se, com plena 
consciencia, a defesa do sector e essa ordem foi dada pelo Alto Comando Britanieo . Era ine - 
vitavel que, em caso de ataque germanieo, a 2.“ divisao cedesse e fosse incapaz de oferecer 
uma resistencia credfvel. 

O peso da responsabilidade histdrica tera falado mais alto do que a imprudente vontade 
de esmagar o CEP e tera levado o Alto Comando Britanico a determinar que a 2.“ divisao 
portuguesa safsse da frente no dia 9 de Abril de 1918 e caminhasse para a retaguarda, onde 
iria gozar um merecido repouso. Mas, quando a ordem foi dada, ja vinha tarde, pois, as 
quatro boras e quinze minutos de dia 9, os alemaes iniciaram um ataque feroz sobre toda 
a frente portuguesa, come^ando por um destruidor bombardeamento (Fraga, 2010, 401- 
438) que causou trezentos e noventa e oito mortos e levou ao aprisionamento de seis mil 
quinhentos e oitenta e cinco militares (Oliveira, 1994,123). 

Desapareceu, assim, a expressao da vontade politica dos revoluciondrios republicanos 
que sonharam ser posslvel impor Portugal a Gra-Bretanha com igual peso soberano na or¬ 
dem internacional. O que restou do CEP passou a fazer trabalho de tropa de engenharia, 
abrindo trincheiras e estradas para os combatentes britanieos passarem rumo as primeiras 
linhas. So ja nas ultimas semanas de guerra, em Outubro de 1918, foi posslvel organizar 
tres batalhoes que foram integrados em grandes unidades britanicas e tomaram parte na 
persegui^ao das formas alemas em fuga, para la do rio Escalda. 


Conclusao 

Depois do armistlcio e do bm da guerra, na conferencia da paz, Portugal ainda foi repre- 
sentado por Afonso Costa, que optou por bear a viver em Paris apos a morte de Sidonio 


S2 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Pais, da sua liberta^ao e em seguida a reposi^ao da ordem alterada pela Monarquia do Nor¬ 
te, em 1919. Houve uma tentativa de o trazer, de novo, ao cargo de Presidente do Ministerio 
(designa^ao, entao, usada para o Primeiro-Ministro), mas recusou, por achar nao estarem 
reunidas condi^oes para tal. 

Na conferencia da paz, Afonso Costa, pela ultima vez, ainda deu largas ao seu sentido 
revoluciondrio conseguindo para Portugal, como compensa^ao de guerra, para alem de 
avultada soma de dinheiro, que chegou para pagar a divida de guerra a Gra-Bretanha, a 
entrega de navios germanicos que serviram para, com eles, se fundarem quatro grandes 
companhias de transportes maritimos, modernizando aquilo que, pelo esfor^o nacional, 
nunca seria modernizado. 

E verdade que, apos o regresso das tropas a Portugal, por ca houve muita instabilidade 
politica, alguns atentados a ordem piiblica com crimes hediondos, mas a culpa de tal esta- 
do de coisas, ao contrario do que alguns historiadores procuram concluir, nao e nacional. 
Realmente, e preciso contextualizar a situa^ao interna portuguesa com aquilo que se pas- 
sava um pouco por toda a Europa, desde a Alemanha a Espanha. 

Os circuitos comerciais haviam sido rompidos com a guerra, gerando niveis de infla^ao 
altissimos, desemprego e fome. Este cenario gerou, naturalmente, instabilidade social e 
esta, na esperan^a de encontrar uma solu^ao, gerou instabilidade politica. Ora, Portugal, 
sendo um pais dependente das importa^oes estrangeiras, ao mesmo tempo que importava 
os produtos necessarios ao consumo interno, importava a infla^ao e tudo o mais que vinha 
arrastado por ela. A Histdria da segunda parte da l.“ Repiiblica - de 1918 a 1926 - esta ligada 
a Histdria da Europa do pds-Grande Guerra: ascendencia politica dos militares nas ques- 
tdes nacionais e incapacidade de os partidos politicos encontrarem solu^des apropriadas 
para o equilibrio da vida. 

Associado ao quadro que acabamos de tra^ar estao causas tipicamente portuguesas agra- 
vadas com a mudan^a do regime monarquico para o republicano. Gom efeito, como vimos, 
o atraso nacional era transversal a toda a actividade social e econdmica e o impulso que a 
Repiiblica, atraves do Partido Democrdtico, pretendeu dar a evolu^ao gerou uma reac^ao, 
que elegia a tradi^ao, o obscurantismo e a religiosidade como salvagdo do descalabro re¬ 
publicano. Essa foi a superestrutura que sustentou a Ditadura Militar entre 1926 e 1928 e 
serviu de apoio a deftni^ao do Estado Novo de Antdnio de Oliveira Salazar, que, em nome 
da ordem, do equilibrio ornamental, da paz social e da acalmia da luta partidaria, impds 
uma ditadura retrdgrada, que se prolongou no tempo ate colocar Portugal, cada vez mais, 
fora do tempo da Europa e da modernidade de entao. So ja na segunda metade dos anos 
50 do seculo XX e que houve a percepnao de que se impunha acelerar o passo para chegar 
a certos niveis sociais, econdmicos e educacionais europeus e, nessa altura, abriram-se 
as portas a cultura cinematograftca e a televisao, bem como a uma ligeira modernizanao 
literaria. O verdadeiro salto para um mundo modificado fez-se depois de 25 de Abril de 
1974, com a aceitanao do direito de as colonias ascenderem a independencia e a abertura a 
democracia com integranao no Mercado Gomum. 

Em conclusao, nao se pode dizer que a beligerancia, na Grande Guerra, tenha sido um 
mal ou uma decisao errada; ela foi a decisao que parecia mais acertada para a epoca de 
modo a tentar garantir a salda de uma tutela amarfanhadora dos brios nacionais. A belige¬ 
rancia na Grande Guerra nao se pode opor a neutralidade colaborante - como Ihe chamou 
Salazar - na 2.“ Guerra Mundial, pois, cada uma das conjunturas era distinta e determinan- 


I. A PROBLEMATIZAnAO DA PRIMEIRA GRANDE GUERRA 


53 


te de objectives nacionais diferenciados. Por isso, a Histdria nao deve julgar, mas simples 
mente contar e explicar. 


Referencias 

Afonso, Aniceto; Vladimiro, Victor (1982). A correspondencia oficial da Lega^ao 
de Portugal em Londres — 1900-14. Andlise Social, vol. XVIII (72-73-74), 1982-3.° -4.° 
-5°, pp. 711-739. [Em linha]. Disponivel em http://analisesocial.ics.ul.pt/documentos/ 
1223400239Q9ySK8rnlTf46ZL8.pdf 

Fraga, Luis Alves de (2010). Do Intervencionismo ao Sidonismo: Os dois segmentos da 
politica de guerra na 1.“ Republica: 1916-1918. Coimbra: Imprensa da Universidade. 

Fraga, Luis Alves de (2012a). OFimdaAmbiguidade:AEstrategiaNacionalPortuguesade 
1914 a 1916. 2.“ ed. Lisboa: Universidade Autonoma de Lisboa. 

Fraga, Luis Alves de (2012 b) - When a diplomat goes into politics because of war: The 
case of Joao Chagas (1910-1914). JANUS.NET, e-journal of International Relations Vol. 3, 
n.° 1 (Spring 2012), pp. 123-141. [Em linha]. Disponivel em http://repositorio.ual.pt/bits- 
tream/11144/536/1/ en_ vol3_ nl_ art6 .pdf 

Fraga, Luis Alves de (2014a). Reflexao Sobre Classes Medias e Elites em Portugal nos 
seculos XIX e XX. Lisboa: UAL. [Em linha]. Disponivel em http://repositorio.ual.pt/ 
bitstream/11144/476/1/Classes%20M%C3%A9dias%20e%20Elites%20em%20Portu- 
gal % 20nos%20s % C3 %A 9culos% 20XIX%20e% 20XX.pdf 

Fraga, Luis Alves de (2014b). Reflexao sobre o Capitalismo Portugues no Seculo XIX [Em 
linha]. Lishoa: UAL. Disponivel em http://repositorio.ual.pt/bitstream/11144/475/l/Ca- 
pitalismo%20Portugu%C3%AAs.pdf 

Lichnowsky, Principe (1918). A MinhaMissdo emLondres: 1912-1914. Londres: Cassell. 
Marques, A. H. de Oliveira (org.) (1974). O segundo governo Afonso Costa, 1915-1916: ac- 
tas dos Conselhos de Ministros. Lisboa: Publica^oes Europa-America. 

Meneses, Filipe Ribeiro de (2018). De Lisboa a La Lys: O Corpo Expediciondrio Portugues 
na Primeira Guerra Mundial. Alfragide: Dom Quixote. 

Oliveira, General A. N. Ramires de (Coord.) (1994). Histdria do Exercito Portugues (1910- 
1945). Vol. III. Lisboa: Estado-Maior do Exercito. 

Sousa, Jorge Pais de (s. d.). Afonso Costa: Republicanismo Socialista e Agdo Politica 
(1887-1911) [s. 1.]: [s. n.]. [Em linha]. Disponivel em http://wujuj.intellectus.uerj.br/Tex- 
tos/Anol2nl/JORGE_PAIS_DE_SOUSA.pdf 

Torre-Gomez, Hipolito de la (1978). Conspiragdo Contra Portugal (1910-1912): As Rela- 
goes Politicos entre Portugal e Espanha. Lishoa: Livros Horizonte. 

Vicent-Smith, John (1975). AsRelagdesPoliticosLuso-Britdnicas (1910-1916). Lisboa: Li¬ 
vros Horizonte. 

Ximenes, Augustin Louis Marie de (1820). “L’ere des Eran^ais”. In: Poesies revolution- 
naires et contre-revolutionnaires, ou Recueil des hymnes, chants guerriers, chansons 
re'publicaines. Paris: Libraire Historique, vol. 1,1821. 


54 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Do paradigma da guerra ao paradigma da paz: O 
tratado de Versalhes e a genese de organiza9oes 
internacionais intergovernamentais 

Claudia Toriz Ramos 

FCHS/Universidade Fernando Pessoa 
CEPESE / ECT 


Resumo: O seculo XX soube ser um seculo paradoxal: supera^ao dos limiares tecnicos, eco- 
nomicos e populacionais; destrui^ao maci^a, material e humana. Neste jogo a dois tempos, 
a 1 Guerra Mundial marca o ciclo da destrui^ao; a sua supera^ao a inversao do ciclo, mas 
tambem a tentativa de romper, estruturalmente, o papel da guerra, nas rela^oes entre Es- 
tados. O ‘contrato’, eivado de circunstancias embora, foram os tratados decorrentes da 
Conferencia de Paris, o Tratado de Versalhes, nomeadamente. Os resultados, ainda que 
parcos e porventura contraditorios, abriram timidamente o palco de uma nova ordem in- 
ternacional: a da coopera^ao entre Estados e a do entendimento entre povos. No concerto 
das na^oes, introduzia-se a dissonancia, o paradigma da paz duradoura. O presente artigo 
analisa, neste contexto, o surgimento, decorrente do Tratado, e brevemente a sequencia de 
duas organiza^oes internacionais em particular: a Liga das Na^oes e a Organiza^ao Inter- 
nacional do Trabalho. No palco classico das rela^oes internacionais de entao, nenhuma das 
duas seria facil de levar a cena. Todavia, quase cem anos depois, o balance sob re a guerra e 
a paz, no ‘seculo do povo’ sera porventura diferente. 

Palavras-chave: Tratado de Versalhes; Organiza^ao Internacional do Trabalho; Sociedade 
das Na^oes; paradigma da paz 

Abstract: The twentieth century was a paradoxical century: on one hand, it overcame te¬ 
chnical, economic and populational limits; on the other, there was material and human 
mass destruction, in this twofold game. World War 1 marks the cycle of destruction; its re¬ 
solution represents the inversion of this cycle, but also the attempt to break down, struc¬ 
turally, the role of war, in the relations between States. The ‘contract’, although riddled 
of circumstances, was the Treaty of Versailles. The results, alheit scarce and perhaps con¬ 
tradictory, have timidly opened the stage of a new international order: that of cooperation 
between States and that of understanding between peoples, in the concert of Nations, the 
dissonance was introduced: the paradigm of lasting peace. This article analyses, in this 
context, the emergence, arising from the Treaty, and the sequence of two particular inter¬ 
national organisations: the League of Nations and the International Labour Organisation. 
On the classic stage of international relations of that time neither of them would he easy to 
bring to the scene. However, almost a hundred years later, the balance on war and peace 
in the ‘People’s century’ will be eventually different. 

Keywords: Treaty of Versailles; International Labor Organization; Society of Nations; pa¬ 
radigm of peace. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


5S 


Introdu^ao 

A historia contemporanea beneficia de um tipo de fontes que os estudos sobre outras 
epocas histdricas nao podem ja utilizar - rebro-me a memdria oral dos protagonistas, 
transmitida as gera^des prdximas e recuperavel, como fonte, por meio da metodologia da 
histdria oral (Oral History Society, 2018). Por ter deeorrido no inlcio do seculo XX, assim e 
o caso da 1 Guerra Mundial e mult os de nds teremos ainda reeolhido, na famllia, memdrias 
dos acontecimentos infaustos que tal guerra produziu. Ao invoearmos a batalha de La Lys, 
assim me oeorre tambem invoear as memdrias familiares - eontribuindo para a memd¬ 
ria ‘demoeratiea’ do seeulo XX, pleno de contradi^des, de luzes e de sombras, de feitos e 
de malogros, de paz e de guerra, enbm. Aos olhos do historiador, todavia, uma memdria 
que chega ao presente pela boca dos vivos sera tambem uma memdria que evidencia elos 
eausais entre um passado recente e o presente que ora vivemos. Na minha prdpria easa, 
posicionando-me no ponto de observa^ao do mais jovem membro, vejo bisavds, trisavds, 
eujo fado os carreou pelos meandros da guerra, euriosamente dos dois lados do eonflito: 
um bisavd, mobilizado em Portugal, prestes a partir, quando os desaires da participa^ao 
portuguesa o deixam aftnal no aconchego do lar (preciosa oferta da sorte, dado o triste 
destino de tantos que partiram); um tio trisavd, na Alemanha, mobilizado pelo Imperio 
Alemao e sobrevivente tambem dos horrores da guerra, leone familiar de uma cidadania 
cumprida no amago dos valores nacionais (‘pro patria mori’), todavia, famllia mais tarde 
devolvida ao horror da histdria, quando a bdelidade cidada ja nao bastou para expurgar o 
sangue, alegado marcador etnico da inscri^ao judaica. Aos olhos do historiador, uma banal 
e singular histdria como esta convoca questdes maiores sobre o como e o porque da his¬ 
tdria. Aos olhos da demais ciencia social, convoca interroga^des prementes sobre os elos 
passado-presente, ja que nao se trata de um passado remoto, desligado de nds, do nos- 
so contexto e dos nossos problemas actuals, pelo contrario. Por isso, estudarmos hoje a 1 
Guerra Mundial signibca tambem perscrutarmos o passado, para entendermos o presente 
e preparamos o futuro. 

Por outro lado, o interesse academico que me move leva-me a observar o peso do para- 
digma da guerra, no inlcio do seculo passado, mas tambem o parto do paradigma da paz, 
que emerge dessa guerra destrutiva, tlmido e incipiente, porventura inconsequente, mas 
abrmando ja os princlpios que viriam a ditar, no longo seculo XX, a emergencia do para¬ 
digma da paz. Reftro-me aqui, em especial, ao ftnar do eonflito, aos acordos que selam a 
paz, transitdria, incompleta e ferida ainda dos odios da guerra, mas que contem tambem 
as raizes de um discurso de coopera^ao, que nao de competi^ao belica entre os Estados. No 
texto que se segue, aludirei, pois, aos textos constitutivos da Liga das Naqoes e da Organi- 
za^ao Internacional do Trabalho (OIT), vertidos nos tratados de paz do bm da 1 Guerra, o 
de Versalhes e os demais (vide infra). Se aquele tratado bcou tristemente conhecido pela 
dureza das condiqoes impostas aos vencidos e pelo impacto que tal tera tido na sequencia 
de acontecimentos que viriam a levar a 11 Guerra Mundial, e eerto, tambem, que os pactos 
do bm da 1 Guerra continham o germen de um novo sistema internacional, albergado a 
sombra da coopera^ao entre Estados, da rela^ao internacional paclbca, em ultima analise 
de uma humanidade una emergente, ainda que longlnqua e tedrica. 


56 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Na triste alvorada do seculo XX, por entre os destro^os da alegada ‘civiliza^ao’, auto-a- 
niquilada, soavam, pois, acordes dissonantes, como em alguma da miisica da epoca“, rom- 
pendo conven^oes e irritando a ortodoxia da guerra, cantando em desarmonia as pautas da 
paz possivel e duradoura. 


Conferencias de Paris e o Tratado de Versalhes 

O processo de paz, no final da I Grande Guerra decorreu a sombra da conferencia de Paris 
(1919-20), como e consabido. Nela se fez a negocia^ao da ordem do pos-guerra, almejando 
superar as rupturas que a guerra introduzira. Nesta conferencia, participaram os Estados 
parte no conflito, tendo, no entanto, os grandes vencedores, isto e, os EUA, o Reino Unido, 
a Eran^a e a Italia, predominado, na decisao do cenario do pds-guerra. A Russia assinara 
previamente, em 1918, o tratado de paz de Brest-Litovsk com a Alemanha, o que a afastara 
deste processo. Os vencidos foram por sua vez excluldos da negocia^ao (Encyclopaedia 
Britannica, 2018; Holsti, 1991). 

No bm da mesma, foram assinados tratados entre vencedores e vencidos: o de Versailles, 
com a Alemanha (em 28 de Junho de 1919); o de Trianon, com a Hungria (em 4 de Junho 
de 1920); o de Saint-Germain-en-Laye, com a Austria (em 10 de Setembro de 2019), o de 
Neuilly com a Bulgaria (em 27 de Novembro de 2019), o de Sevres, com o Imperio Otomano 
(em 10 de Agosto de 1920) e que viria a ser sucedido pelo tratado de Lausanne, em 1923 (En¬ 
cyclopaedia Britannica, 2018; Holsti, 1991). Nestes tratados, redesenhava-se o mapa poli¬ 
tico europeu, euroasiatico e colonial, defmiam-se compensa^oes de guerra, mas tambem 
se buscavam as bases de uma futura sociedade internacional assente na coopera^ao entre 
estados. E este ultimo aspecto o loco do presente artigo, pelo que interessa aqui convocar 
a analise das partes 1 e Xlll do Tratado de Versalhes (replicadas nos demais tratados supra- 
mencionados) que contem, respectivamente, a Carta da Sociedade das Na^oes e a Consti- 
tuicjao da Organiza^ao Internacional do Trabalho. 


A Carta da Sociedade das Na9des 

O equillbrio de formas entre os Estados nao e uma inven^ao da conferencia de Paris. O do- 
brar da guerra e o negociar da paz haviam estabelecido reequillbrios, ainda que tempora- 
rios, imimeras vezes, nos seculos antecedentes. Ressaltam desse cenario a paz da Vestefalia 
(1648), arcana fundadora da ordem internacional classica, no longlnquo seculo XVll; a paz 
de Utreque (1713); a paz de Viena (1815), fundadora do ‘concerto das Na^oes’ e sua balan^a 
de poder, equilibrador das rela^oes entre Estados e garante da paz na Europa, nas decadas 
subsequentes (Campos et al., 2011; Holsti, 1991). Todavia, nenhum desses tratados alguma 
vez se deslocara do paradigma da guerra, isto e, da considera^ao da guerra como ‘voca^ao’ 
do Estado, actividade normal e ate necessaria, inscrita no amago da sua propria existencia. 


Como em Arnold Schoenberg, por exemplo, fundador da Escola de Viena (cf. Schoenberg, 2010). 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


57 



Desse ponto de vista, a paz era um intervalo necessario, mas nao um estado permanente a 
garantir (Holsti, 1991). 

Por outro lado, o seculo XIX contribulra ainda para o enquadramento normative da 
guerra, atraves das conven^oes de Haia (de 1899 e 1907) - sem que a guerra fosse abomi- 
nada, era por essa via enquadrada e restrita, nos sens meios e nos sens alvos (Armstrong, 
Lloyd e Redmond, 2004). 

Ja a politica de alian^as entre Estados, que podera ter refor^ado o efeito equilibrador 
durante algum tempo, veio abnal a revelar-se perversa, no quadro da I Guerra Mundial, 
arrastando para o conflito toda uma rede de partes assim associadas (Armstrong, Lloyd 
e Redmond, 2004; Campos et al., 2011). A paz armada, estrategia preventiva classica dos 
Estados, deflagrava num conflito ‘total’, extensivamente devastador e, por isso mesmo, 
assustador para a humanidade. 

Na prepara^ao das negocia^oes de paz, destaca-se o papel do presidente americano, 
Woodrow Wilson, cuja lista de prioridades para a negocia^ao ftcou sobejamente conhecida 
como os ‘catorze pontos’ (Armstrong, Lloyd e Redmond, 2004; Campos et al., 2011). Sen- 
do heterogenea (da redefmi^ao territorial aos caboucos dum novo sistema internacional) 
cabe, no ambito deste texto, salientar os pontos 1 e 14, em que o Presidente enunciava a 
necessidade, respectivamente, de: 

(l) Pactos de paz de livre adesao dos estados, apds os quais nao havera en- 
tendimentos internacionais privados, progredindo a diplomacia de forma 
aberta e piiblica; (...) 

(14) uma associa^ao das naqoes sob pactos especificos, com o proposito de 
assegurar garantias miituas de independencia politica e integridade territo¬ 
rial a estados grandes e pequenos'^. 

Wilson ftcava assim conhecido por ter trazido as negocia^oes uma perspectiva do sistema 
internacional de ralz idealista (por oposi^ao a realista) e por ter por isso convidado a re- 
constru^ao com base na tecitura da coopera^ao internacional entre os Estados. A sequencia 
do processo nao permitiu ao presidente americano consolidar integralmente o seu projec- 
to, nomeadamente pela oposi^ao interna, expressa na rejei^ao da ratibca^ao do tratado de 
Versalhes no Senado dos EUA. No entanto, as ideias lan^adas a debate haviam ainda assim 
sido incorporadas na minuta da proposta anglo-saxdnica (proposta Hurst-Miller) a con- 
ferencia de Paris, pelo que o piano da cria^ao da Sociedade das Na^oes acabou por veneer e 
por ser incorporado no tratado de Versalhes, logo a parte I do mesmo (Armstrong, Lloyd e 
Redmond, 2004; Campos et al., 2011). 

Ha, pois, que salientar, para la do facto singular da introdu^ao do Pacto no Tratado, uma 
serie de conteiidos de suma importancia que apontam a renova^ao e restrutura^ao do sis¬ 
tema internacional, conforme se detalha abaixo, a partir do texto da Carta (Treaty of Ver¬ 
sailles, 1919). 


Traduzido de: 1 . Open covenants of peace, openly arrived at, after which there shall be no private international 
understandings of any kind, but diplomacy shall proceed always frankly and in the public view. / A general 
association of nations must be formed under specific covenants for the purpose of affording mutual guarantees of 
political independence and territorial integrity to great and small states alike (Yale Law School, 2008a: s.p.). 


58 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



A Liga inicia-se com quarenta e dois Estados membros, mas o seu preambulo consagra- 
-a como organiza^ao aberta a aceita^ao de futures membros, desde que estejam estes na 
disposi^ao de se eonformarem as regras instituidas no Facto e que sejam aceites por uma 
maioria de dois ter^os dos membros da Assembleia (Campos et al., 2011). Os orgaos de go- 
verno da Organiza^ao sao justamente a Assembleia dos representantes de todos os Estados 
membros; o Conselho, um orgao mais restrito que comporta representantes permanentes 
das principals potencias aliadas e representantes rotativos, escolhidos na Assembleia; e um 
Secretariado permanente que eoadjuva os anteriores (vide artigos 2°, 3“ e 4°). 

Desde logo, o Paeto prebgura a constitui^ao de uma ‘estrutura de seguran^a coleetiva’ 
(a expressao nao e da epoca) sendo responsabilidade miitua e eomum a manuten^ao da 
soberania e da integridade territorial de eada Estado membro e sendo a agressao a qual- 
quer um deles entendida eomo um ato de beligerancia eontra todos (vide artigos 10°, 11°, 
16° e 17°). Desenha-se uma estrutura multilateral de dissuasao de conflitos, procurando 
prevenir poteneiais agressoes entre Estados. Giza-se assim uma estrutura de gestao de cri¬ 
ses, que almeja prevenir esealadas e que desse modo institui um mecanismo moratorio 
da deflagra^ao dos conflitos, abrindo-se um processo diplomatico e negocial, de inque- 
rito e media^ao, que envolve o Conselho e, possivelmente, a Assembleia (vide artigos 12° 
e 15°). Prescreve-se tambem a judieializa^ao das disputas, por recurso a uma estrutura de 
arbitragem e resolu^ao legal de conflitos, no piano internacional (vide artigos 13° e 14°). 
O Pacto nao exclui o uso ultimo da for^a belica, mas procura retarda-lo e enquadra-lo 
normativamente, num modelo que visa conter a guerra, privilegiando as rela^oes paci- 
ftcas. Preconiza-se ainda o desarmamento, limitando a produ^ao de armas dentro de um 
quadro normativo comum e contrariando, desse modo, esealadas belicas (vide artigo 8°). 
Deftnem-se, para alem disso, os termos da constitui^ao de protectorados, sob re territo- 
rios de ascendente colonial (vide artigo 22°), com vista a progressiva autodetermina^ao 
dos mesmos. Einalmente (vide artigo 23°), abordam-se de forma lata assuntos de caracter 
socio-economieo, que aqui interpreto como a considera^ao de condieionantes estruturais 
da eonstru^ao da paz, a saber: 

Art.23. Sob reserva e em conformidade com as disposi^oes das Convenejoes 
internacionais atualmente exist entes ou que serao ulteriormente concluidas, 
os membros da Sociedade: 

(1) esfor^ar-se-ao por assegurar e manter condi^oes de trabalho equitativas 
e humanas para homens, mulheres e crian^as, quer nos sens prdprios ter- 
ritdrios, quer em todos os paises aos quais se estendam as suas relaqoes co- 
merciais e industriais e, com esse ftm, fundarao e manterao as organiza^oes 
internacionais necessarias; 

( 2 ) comprometem-se a garantir o tratamento equitativo das populaqoes in- 
digenas dos territorios submetidos a sua administra^ao; 

( 3 ) conftam a Sociedade a fisealiza^ao geral dos acordos relativos ao trabco de 
mulheres e crian^as, ao comercio de opio e de outras drogas nocivas; 

( 4 ) conbam a Sociedade a ftscaliza^ao geral do comercio de armas e muni^oes 
com aqueles paises nos quais a fiscaliza^ao desse comercio e indispensavel ao 
interesse comum; 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


59 


(s) tomarao as medidas necessarias para assegurar a garantia e manuten^ao 
da liberdade de comunica^oes, transito e equitativo tratamento comercial a 
todos os membros da Sociedade. Neste contexto, as necessidades especiais 
das regioes devastadas durante a guerra de 1914 a 1918 serao tomadas em 
considera^ao; 

(6) esfor^ar-se-ao por tomar medidas de ordem internacional para a pre- 
ven^ao e o controle de doen^as.^ 

Ou seja, o trabalho, a condi^ao indigena, as mulheres e crian^as, a saiide, mas tambem o 
trafico de pessoas, drogas e armas e a liberdade de comunica^ao, transito e comercial sao 
entendidos como factores a considerar para a estabiliza^ao das sociedades e, por isso, fun- 
^oes concertadas dos Estados membros da Sociedade das Na^oes. 

E certo que a histdria subsequente da Liga das Na^oes nao endossa o seu sucesso, ei- 
vada como foi de insubciencias, desde logo pela nao adesao dos EUA, mas tambem pelos 
miiltiplos conflitos que nao pode prevenir ou medlar de forma capaz (Armstrong, Lloyd e 
Redmond, 2004; Graebner, 2014; Neiberg, 2017). Algunsautores apresentam mesmopers- 
pectivas francamente negativas do desenrolar desse processo (Graebner, 2014), o que, por 
si, nao invalida o argumento de que o paradigma da coopera^ao entre Estados esteve pre- 
sente, na negocia^ao e resultados da conferencia de Paris. 

A preftgura^ao das Na^oes Unidas estava, de facto, ja na estrutura da Sociedade das Na¬ 
nces, como bem se evidencia pelas caracteristicas acima salientadas. Se o futuro proximo 
reservava o retorno a logica egocentrica dos Estados que o realismo teorico viria a con- 
sagrar, a evolu^ao secular do seculo XX mostraria mais tarde que a Sociedade das Naqoes 
aflorara um caminho que importava palmilhar - o que viria a ser feito, apds a 11 Guerra 
Mundial, no ambito, tambem imperfeito, das Naqoes Unidas (Armstrong, Lloyd e Red¬ 
mond, 2004; Campos etal., 1999; Stirk e Weigall, 1995). 


A Constitui9ao da Organiza9ao Internacional do Trabalho 

A Organiza^ao Internacional do Trabalho nascia tambem com o bm da guerra e com os 
tratados de paz, dada a inclusao da sua Constitui^ao nos mesmos (Yale Law School, 2018b). 
No aparelho institucional entao desenhado, a OIT conbgurava-se como uma agenda da 
Sociedade das Na^oes. Sera porventura interessante salientar que Ihe sobreviveu, tendo 


ARTICLE 23: Subject to and in accordance with the provisions of international conventions existing or here¬ 
after to be agreed upon, the Members of the League: a) will endeavour to secure and maintain fair and humane 
conditions of labour for men, women, and children, both in their own countries and in all countries to which their 
commercial and industrial relations extend, and for that purpose will establish and maintain the necessary inter¬ 
national organisations; b) undertake to secure just treatment of the native inhabitants of territories under their 
control; c) will entrust the League with the general supervision over the execution of agreements with regard to 
the traffic in women and children, and the traffic in opium and other dangerous drugs; d) will entrust the League 
with the general supervision of the trade in arms and ammunition with the countries in which the control of this 
traffic is necessary in the common interest; e) will make provision to secure and maintain freedom of communi¬ 
cations and of transit and equitable treatment for the commerce of all Members of the League. In this connection, 
the special necessities of the regions devastated during the war of 1914-1918 shall be borne in mind;f) will endeav¬ 
our to take steps in matters of international concernfor the prevention and control of disease. (Treaty of Versailles, 
1919: s.p.). 


60 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



chegado ao presente como a agenda internacional global cujo foco sao as questoes laborais 
(Campos et al., 2011; Hurd, 2014). 

As questoes do trabalho, ou mais latamente a ‘questao social’, percorrem a segunda me- 
tade do seculo XIX, na turbulencia dos radicalismos socialistas, mas tambem na sua pro¬ 
gressiva entrada nos sistemas politicos da epoca, por via eleitoral, a sombra das versoes 
reformistas do socialismo e, por via legal, a custa da progressiva incorpora^ao das reivin- 
dica^oes laborais, fruto das campanhas operarias e da ‘advocacia’ de algumas ftguras de 
proa, como Robert Owen, Daniel Legrand, o casal Webb, ou J. laures, entre outros (Free- 
den, 1996; ILO, 2018). 

Tambem neste ambito as conferencias intergovernamentais de 1890,1897 e 1900 haviam 
aberto caminho para a cria^ao da Organiza^ao e delas resultara a Associa^ao Internacional 
para a Legisla^ao Laboral, sediada em Basileia (Campos et al., 2011; ILO, 2018). A cons- 
ciencia da dimensao internacional dos problemas laborais e da necessidade de coopera^ao 
internacional, num quadro institucional e legal proprio, estavam pois ja presentes. 

Na ralz do processo de cria^ao da OIT tera estado esse historico do ftnal do seculo XIX e 
inlcio do seculo XX, interrompido apenas pelo hiato da guerra, mas presente na mente dos 
estadistas (Winter, 2014). Tambem Ihe nao foi alheia a revoluqao russa de outubro de 1917, 
cujo radicalismo convidava a atitudes preventivas, por parte dos Estados europeus (Arms¬ 
trong, Lloyd e Redmond, 2004; Campos et al., 2011). 

A Constitui^ao da OIT fez-se assim um documento de monta da reconstru^ao do pos- 
guerra, na concep^ao que venceu nas negociaqoes de Paris. Por essa razao, chama a aten^ao 
dos analistas para a consciencia de entao do quanto as questoes socio-econdmicas se en- 
trosavam com as politico-securitarias. Tal leitura refor^a o meu argumento inicial de que, 
tlmida embora, despontava a consciencia de que a paz se constroi a partir de dentro e de 
modo estrutural. Mais acresce que, num mundo ja de extensa rela^ao comercial interna¬ 
cional, o quadro legislativo do trabalho se impunha como algo de transnacional, embora a 
maquina conservadora dos sistemas politicos de entao reservasse a essa dimensao apenas 
um pequeno poder de orienta^ao, ainda assim persistente, como a OIT o viria a revelar. 

Desse modo, no preambulo da sua Constitui^ao (Rodgers et al., 2009: 249), aftrma-se 
que a paz so podera ser atingida por meio da justi^a social; e que as condi^oes de trabalho 
injustas produzem turbulencia social - pelo que os dois males terao que ser expurgados, no 
caminho para a paz duradoura'L As solu^oes a encontrar foram sucessivamente enuncia- 
das no preambulo da Constitui^ao da OIT, de acordo com o seguinte elenco'®: regula^ao dos 
horarios de trabalho (tecto maximo); regula^ao dos fluxos de trabalhadores e preven^ao 
do desemprego; provisao de um salario mlnimo adequado e protec^ao dos trabalhadores 


’’ Whereas the League of Nations has for its object the establishment of universal peace and such a peace can be 
established only if it is based upon socialjustice / And whereas conditions of labour exist involving such injustice, 
hardship and privation to large numbers of people as to produce unrest so great that the peace and harmony of the 
world are imperilled (....) (Rodgers et al., 2009: p.249). 

(...) as, for example, by the regulation of the hours of work, including the establishment of a maximum working 
day and week, the regulation of the labour supply, the prevention of unemployment, the provision of an adequate 
living wage, the protection of the worker against sickness, disease and injury arising out of his employment, the 
protection of children, young persons and women, provision for old age and injury, protection of the interests of 
loorkers when employed in countries other than their own, recognition of the principle of equal remuneration for 
work of equal value, recognition of the principle of freedom of association, the organization of vocational and 
technical education and other measures; (...) (Rodgers et al, 2009: p.249). 


I. A PR0BLEMATIZA(;A0 da PRIMEIRA grande GUERRA 


61 



na doen^a e em caso de acidente; protec^ao das crian^as, trabalhadores jovens e mulheres; 
protec^ao na velhice; protec^ao dos trabalhadores migrantes; protec^ao da liberdade as¬ 
sociativa; refor^o da educa^ao vocacional e tecnica. 

No artigo 427° do Tratado, verdadeiramente programatico, enunciam-se ainda os nove 
principios e metodos fundamentals que as comunidades industrials deveriam procurar 
implementar, a saber: que o trabalho nao fosse um mero produto comercial; que o direito 
de associa^ao, de empregados e de empregadores, fosse respeitado; que o salario fosse tal 
que permitisse manter um mvel de vida razoavel; que a jornada de trabalho deveria ser de 
oito boras ou quarenta e oito semanais; que deveria haver um descanso semanal de pelo 
menos vinte e quatro boras; que o trabalho infantil deveria ser extinto; que o pagamento a 
homens e mulheres deveria ser igual para trabalho igual; que os trabalhadores, dentro de 
um mesmo Estado, deveriam ser tratados da mesma forma perante a lei; que deveriam ser 
implementados sistemas de inspec^ao do trabalho, neles sendo inclmdas mulheres'’’. 

Convem ainda salientar que esta Organiza^ao foi fundada com base num modelo de re- 
presenta^ao nacional que extravasa o governamental - e tripartida, isto e, cada Estado se 
faz representar pelos seus governantes, mas tambem por representantes da sociedade ci¬ 
vil, no seu tecido empresarial e laboral, fugindo assim a convencionalidade da democracia 
representativa e trazendo para o amago da organiza^ao a propria tensao social que almeja 
mediar. Segundo a Constitui^ao da OIT, cada Estado tern quatro representantes, dois go- 
vernamentais e dois nao-governamentais (vd. artigo 389°). Cada delegado tern direito a 
um voto individual, na Conferencia de Representantes (vide artigo 390°). Esta e a Reparti- 
9 ao Internacional do Trabalho (normalmente conhecida como o Bureau, da versao france- 
sa Bureau International du Travail-BIT) constituem os orgaos de governo da Organiza^ao 
(vide artigo 388°). O Bureau, composto por uma especie de Conselho Executivo e tambem 
tripartido. Comp6em-no vinte e quatro membros, doze governamentais (de entre os quais 
oito provenientes dos Estados de “importancia industrial fundamental”, nos termos do 
Tratado), seis representantes dos empregadores e seis representantes dos trabalhadores, 
estes dois liltimos grupos eleitos na Conferencia de Representantes (vide artigo 393°). 

Do ponto de vista dos seus instrumentos legislativos, a Organiza^ao actua por duas for¬ 
mas fundamentals: emitindo recomenda^oes, ou propondo aos Estados, para ratiftca^ao, 
conven^oes (vide artigo 405°). Pelas suas caracteristicas intrinsecas, as recomenda^oes 
nao tinham, nem tern, um poder vinculativo. Ja as convenqoes so o teriam, caso a caso, na 
sequencia da op^ao por cada Estado de as ratiftcar. O mecanismo ‘brando’ de convergencia 
dos Estados assim desenhado nao conferia, nem confere hoje, a OIT uma verdadeira capa- 


ARTICLE 427: First. The guiding principle above enunciated that labour should not be regarded merely as a 
commodity or article of commerce. / Second. The right of association for all lawful purposes by the employed 
as well as by the employers. / Third. The payment to the employed of a wage adequate to maintain a reasonable 
standard of life as this is understood in their time and country./ Fourth. The adoption of an eight hours day or a 
forty-eight hours week as the standard to be aimed at where it has not already been attained. / Fifth. The adoption 
of a weekly rest of at least twenty-four hours, which should include Sunday wherever practicable. / Sixth. The 
abolition of child labour and the imposition of such limitations on the labour of young persons as shall permit the 
continuation of their education and assure their proper physical development. / Seventh. The principle that men 
and women should receive equal remuneration for work of equal value./ Eighth. The standard set by law in each 
country with respect to the conditions of labour should have due regard to the equitable economic treatment of all 
workers lawfully resident therein./ Ninth. Each State should make provision for a system of inspection in which 
women should take part, in order to ensure the enforcement of the laws and regulations for the protection of the 
employed. (Yale Law School, 2008b: s.p.). 


62 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



cidade de coagir. Todavia, o historial da OIT tem comprovado que este mecanismo colocou 
nas maos da Organiza^ao uma capacidade branda e lenta, mas persistente, de propagar um 
padrao internacional de boas praticas, em materias laborais (Campos et al, 2011; Hurd, 
2013). 

Para se aquilatar do caminho debnido pela Organiza^ao note-se que, logo em 1919, 
aquando da sua primeira reuniao, se adoptaram seis conven^oes internacionais do traba- 
Iho, a saber sob re: horarios de trabalho; desemprego; protec^ao na maternidade e trabalho 
feminino nocturno; desemprego; idade minima no trabalho e trabalho nocturno de meno- 
res (ILO, 2018). O facto de ela ter tido continuidade ate ao presente duplamente prova a sua 
oportunidade, a epoca, e o caracter incompleto da regula^ao global das rela^oes laborais, 
ate aos nossos dias. A sua inser^ao no Tratado e a abrma^ao expressa da necessidade de 
prover ao bem-estar dos assalariados para a promo^ao da harmonia internacional provam 
a consciencia, por parte dos negociadores, do quanto rupturas socio-econdmicas pode- 
riam afectar a paz. 


Conclusao 

A naturaliza^ao da guerra, isto e, a sua aceita^ao como um estado normal da rela^ao entre 
Estados, foi um suporte classico do sistema internacional dos Estados. A Grande Guerra 
representou, nesse contexto, o abismo, o paroxismo da violencia e da destrui^ao. A in- 
versao dessa tendencia, ou, no piano normativo, a aceita^ao de novas pautas insinuou-se 
timidamente, desde o ftnal da Guerra. 

Assim, ja em 1919 se preconizavam: a preven^ao dos conflitos; a moratoria dos conflitos 
em deflagra^ao; a restri^ao a produ^ao de armamento; mas, sobretudo, a ac^ao no piano 
das causas estruturais dos conflitos, nomeadamente as econdmico-socials capazes de cau- 
sar instabilidade. Acresce que se advogava que estas ac^des ocorressem num quadro in¬ 
ternacional e multilateral institucionalizado, dando, pois, consistencia acrescida a rela^ao 
cooperativa entre os Estados. 

Todavia, essa mudan^a era dissonante, face ao paradigma vigente. Com na arte, na mii- 
sica, ou na pintura coeva, onde uma nova estetica despontava, irritante, chocante, desa- 
linhada do senso comum, tambem a cantata da paz nao tinha ainda lugar nas conven^des 
maiores dos Estados. O seculo XX gastou-se, no seu desenvolvimento, por tentativa-erro, 
tendo assim chegado aos nossos dias um novo padrao, um emergente paradigma da paz 
que desloca a guerra para a condi^ao de excep^ao e que normaliza as rela^des pacfficas. Tal 
transito nao e alheio a multiplica^ao das democracias, como o aftrma a teoria da paz liberal 
(Ramos, 2014), logo ao papel dos povos, no decurso da sua histdria. Este recuo do discurso 
e do paradigma da guerra nao pode, todavia, ser confundido com pacibsmo generalizado 
ou institucionalizado. Os Estados nao abdicaram da guerra, mas procuram, regra geral, 
evita-la. Desse modo, o numero de conflitos entre Estados diminuiu substancialmente, 
embora tenha emergido uma nova conflitualidade intra-Estado, que corresponde sobre¬ 
tudo a situaqdes de ruptura interna da capacidade dos aparelhos de Estado de proverem 
ordem polftica (Kaldor, 2007). 

Sobre a agenda de 1919 podera o presente perguntar-se acerca do que foi adquirido e 
do que falta ainda concluir. Eacilmente se veribca que algumas das questoes prementes 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


63 


da epoca continuam em aberto, ou que ganharam novos contornos, nos novos contextos 
historicos, como e o caso das questoes laborais no ambito da globaliza^ao. 

Em suma: cem anos volvidos da batalha, a paz nao e, ainda, o estado normal das socieda- 
des, mas a paciftca^ao e um objectivo politico assumido por muitos e o discurso da guerra 
teve que recuar. Como a evolu^ao historica nao e linear, esse proeesso por si nao garante a 
paz futura, apenas a anuneia eomo um horizonte para o qual eonvergem vontades. Os visio- 
narios da paz duradoura nao a puderam institueionalizar, no prinelpio do seculo passado, 
mas eonseguiram, pelo menos, que hoje se fale dela eomo um horizonte que e licito desejar. 


Referencias 

Armstrong, D., Lloyd, L. e Redmond, J. (2004). International Organisation in World Pol¬ 
itics. London: Palgrave. 

Campos, J. et al. (2011). Organiza^oes Internacionais. 4“ ed. Coimbra: Coimbra Editora. 
Encyclopaedia Britannica (2018). Paris Peaee Conferenee. Disponivel em https://iuiuw. 
britannica.com/event/Paris-Peace-Conference (aeedido em 13.04.2018) 

Graebner, N. (2014). The Versailles Treaty and its Legacy: The Failure Of The Wilsonian 
Vision. Cambridge: Cambridge University Press. 

Holsti, K. (l99l). Peace and War: Armed Conflicts and International Order 1648-1989. 
Cambridge: Cambridge U.P 

Hurd, 1. (2013). International Organizations: Politics, Law, Practice. Cambridge: Cam¬ 
bridge University Press. 

ILO - International Labour Organization (2018). Origins and History. Disponivel em: 
http://www.ilo.org/global/about-the-ilo/history/lang--en/index.htm (aeedido em 
13.04.2018) 

Kaldor, M. (2007). New &amp; Old Wars. 2nd ed., Stanford: Stanford University Press. 

Neiberg, M. (2017). The Treaty of Versailles: A Concise History. New York: Oxford Uni¬ 
versity Press. 

Oral History Soeiety (2018). Oral History Society. Disponivel em http://www.ohs.org.uk/ 
(aeedido em 13.04.2018) 

Ramos, C. (2014). O jogo da paz e a democracia. InPatim, 1. etal. (eds.). Literatura &amp; logo 
- narrativas, discursos, representagoes e mitos. Lisboa: Esfera do Caos, pp. 223-229. 
Rodgers, G., Lee, E., Swepston, L. e VanDaele, J. (2009). The International Labour Organ¬ 
ization and the quest for social justice, 1919-2009. Geneva: International Labour Office. 
Schoenberg, A. (2010). Theory of Harmony (lO0th Anniversary edition). Oakland: Uni¬ 
versity of California Press. 

Treaty of Versailles (1919). Treaty of Versailles [fac-simile]. Disponivel em https://www. 
loc.gov/law/help/us-treaties/bevans/m-ust000002-0043.pdf {aeedido em 13.04.2018) 
Winter, J. (2014). Socialism and the Challenge of War. Ideas and Politics in Britain, 1912- 
18. Abingdon: Routledge. 

Yale Law School (2008a). President Woodrow Wilson’s Fourteen Points. Disponivel em 
http://avalon.law.yale.edu/20th_century/wilsonM.asp (aeedido em 13.04.2018) 

Yale Law School (2008b). The Versailles Treaty Tune 28, 1919: Part XIII. Disponivel em 
http://avalon.law.yale.edu/imt/partxiii.asp (aeedido em 13.04.2018). 


64 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


O gaseamento na imagem medica e na memoria 
historica da Primeira Guerra Mundial 


Antonio Fernando Cascais 

FCSH/Universidade Nova de Lisboa 
CECL /UNL 


Resumo: Constituindo uma das mais terriveis inova^oes da moderna tecnociencia aplicada 
a fins belicos, o recurso ao gaseamento maei^o na Primeira Guerra Mundial nao so se reve- 
lou de imediato uma poderosa e efieaz arma de destrui^ao de vidas humanas que influen- 
ciaria toda a futura pesquisa e utiliza^ao de armamento quimico e biologico em posteriores 
conflitos, e em ultima analise como instrumento de exterminio no mundo coneentraeio- 
nario do Holoeausto, eomo teve duradouro impaeto na cultura e no pensamento europeu 
do seeulo XX e inspirou uma lenda negra profusamente ilustrada na memorialistica, na 
literatura e nas artes no piano internaeional. Esta comunica^ao analisa a experiencia par- 
tieularmente traumatiea que representou o gaseamento no seio do trauma existeneial e 
historieo global da Primeira Guerra Mundial, e a sua rece^ao em Portugal. Em partieular, 
cotejar eritieamente a tematiza^ao das narrativas ofieiais com os raros documentos litera- 
rios e biograficos que abordam o gaseamento, como a cole^ao de memorias de combatentes 
e os registos ofieiais, mas sujeitos a confidencialidade, dos hospitais militares. 

Palavras-Chave: Gaseamento, armas quimicas e biologicas, historia da medicina militar, 
medicina portuguesa, 1 Guerra Mundial. 

Abstract: Constituting one of the most terrifying innovations of modern techno-science 
applied to warfare, the use of massive gasing in World War 1 immediately revealed a pow¬ 
erful and effective weapon of destruction of human lives, that would influence the whole 
future research and use of chemical and biological weaponry in later conflicts. It was also, 
later, an instrument of extermination in the concentrated camps of the ffolocaust, as has 
had a lasting impact on the European culture and thought of the twentieth century, in¬ 
spiring a hlack legend profusely illustrated in memoir literature and arts at international 
level. This communication analyzes the particularly traumatic experience that represented 
the gassing within the existential and historical global trauma of World War 1, and its re¬ 
ception in Portugal. In particular, to critically looks at the way this topic was referred in 
the official narratives, namely in the rare literary and biographical documents mentioning 
gasing, such as the collection of comhatants ‘ memories and in the official records, always 
subject to confidentiality, of Military hospitals. 

Keywords: Gassing, chemical or biological weapons, Portuguese militar history, history of 
medicine. First World War. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


65 


“Mas sabei que mais nos doi o vosso esquecimento do que o muito que pe- 
namos por ca”^° 


Constituindo uma das mais terriveis inova^oes da moderna tecnociencia aplicada a fins 
belicos, o recurso ao gaseamento maci^o na Primeira Guerra Mundial nao so se revelou de 
imediato uma poderosa e ebcaz arma de destrui^ao de vidas humanas que influeneiaria 
toda a futura pesquisa e utiliza^ao de armamento quimieo e biologico em posteriores con- 
flitos, e em ultima analise eomo instrumento de exterminio no mundo eoncentracionario 
do Holocausto, eomo teve duradouro impacto na cultura e no pensamento europeu do se- 
eulo XX e inspirou uma lenda negra profusamente ilustrada na memorialistica, na litera- 
tura e nas artes no piano internaeional. Experiencia partieularmente traumatica no seio do 
trauma existencial e histdrico global da Primeira Guerra Mundial, a sua rece^ao em Portu¬ 
gal foi, no entanto escassa, no campo da narrativa, das artes e da biografta, e abafada por 
um arquivo consideravelmente maior de expressoes (literarias, plasticas, politicas e histo- 
riograbcas) coevas centradas na propaganda do esfor^o militar, ou apologeticas do patrio- 
tismo e das virtudes castrenses dos elementos do Corpo Expedicionario Portugues, nao so 
em fontes institucionais, eomo inclusive por parte de intervenientes que testemunharam 
os factos no proprio teatro de guerra. Potenciada pelo facto de o Pais se encontrar no lado 
vitorioso, a insistencia da narrativa predominante no adquirido absoluto da preserva^ao 
de Portugal eomo potencia colonial no concerto das na^oes serviu eomo ftltro de rece- 
9 ao dos fenomenos traumaticos (inadapta^ao as sub-humanas condiqoes do quotidiano 
nas trincheiras, elevada morbilidade de lesoes fisicas e psiquicas, stress post-traumatico, 
experiencia dos prisioneiros de guerra em campos de concentra^ao alemaes, mortalidade 
maci^a das tropas nacionais impreparadas), entre os quais o gaseamento. A sua importan- 
cia devera, porem, ter sido determinante no impacto que tiveram os horrores da guerra na 
popula^ao portuguesa, veiculado pelos relatos dos veteranos que os testemunharam em 
primeira mao e patente nas devasta^oes causadas aos proprios sobreviventes dos ataques, 
cujo comportamento consagrou o generalizado uso popular de “esgaseado”. Em que terao 
consistido uns e outras, podemos deduzi-lo das narrativas memorialisticas e dos textos 
medicos tao raros eomo eloquentissimos, que as comemora^oes do centenario do conflito 
mundial despertam agora da sua dormencia de um seculo. 


Gaso exemplarmente ilustrativo da experiencia do gaseamento narrado na primeira pes- 
soa e o das Memdrias da Grande Guerra, de Jaime Gortesao (1919), incluido na cole^ao de 
memdrias de combatentes publicada pela Renascen^a Portuguesa entre 1916 e 1924, en¬ 
tre os quais se contam Nas trincheiras da Flandres e Galvdrios da Flandres, de Augusto 
Gasimiro, A Malta das Trincheiras, de Andre Brun e Ao Parapeito e O Soldado-Saudade 
na Guerra-Grande, de Joao Pina de Morals, Gartas da Guerra (com o Exercito Ingles), 
de Adelino Mendes, A Ferro e Fogo na Grande Guerra (1917-1918), de Eduardo Pimenta, 


Extrato de carta de um soldado recolhida por Jaime Cortesao, Memdrias da Grande Guerra, p. 144. 


66 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



Tropa d’Africa, de Carlos Selvagem (pseudonimo de Carlos Tavares de Andrade Afonso 
dos Santos), para alem de relatos de altas patentes militares, como O Corpo de Exercito 
Portugues na Grande Guerra. A Batalha do Lys, 9 de Abril de 1918, de Gomes da Costa, on 
de prisioneiros na Alemanha, como Da Flandres ao Hanover e Mecklenburg (notas dum 
prisioneiro), de Alexandre Malheiro. A atividade editorial do movimento da Renascen^a 
Portuguesa era guiada por um programa de promo^ao da literacia cultural consentaneo 
com os ideals reformistas, e correspondentes pollticas piiblicas, do regime republicano re- 
cem implantado, e, para alem disso, refletia de algum modo a posi^ao intervencionista e 
beligerante do movimento de que faziam parte destacados apoiantes do Partido democra- 
tico de Afonso Costa, feroz crftico da neutralidade portuguesa na contenda internacional 
em que parecia evidente que se jogava o future do Imperio Colonial. O posicionamento 
militantemente belicista e nacionalista, e, nessa medida, panegfrico do conflito, nao foi de 
molde a comprometer o realismo das narrativas, nem parecia aftgurar-se que a minuciosa 
e barroca descri^ao de toda a especie de horrores e atrocidades pudesse comprometer a de- 
liberada mitifica^ao do soldado portugu&amp;, sugerindo, ao inves, que contribuisse inclusive 
para a alimentar. Com efeito, ela 

"transportava uma visao profetica redentora, ancorada numa dupla justi- 
ftca^ao de sentido patridtico: a justifica^ao polftica de aliados naturals do 
bloco demoliberal anglo-frances contra o expansionismo cesarista alemao; 
a justifica^ao etica de uma proposta de revigoramento moral das energias 
nacionais que o soldado encarnava” (Leal, 2000: p. 445). 

Caso exemplar disto mesmo sao as Memdrias da Grande Guerra, de Jaime Cortesao (1919), 
cujo recorte literario em nada esteticiza a experiencia da guerra das trincheiras: 

“Os vivos teem de viver em promiscuidade com os mortos, - mais do que 
isso, com as mutilaqoes dos cadaveres. All, ao pe da trincheira, a meio duma 
dessas paredes dum po^o de explosao, emergem os dois ossos duma perna 
com farrapos de podridao suspensos e uma bota ainda cal^ada. As vezes o 
odor a came putrefacta e tao intenso que e necessario mandar tapar; outras 
o cheiro nauseabundo erra no ar, vindo nao se sabe donde. Todo este chao 
exala carnagem, loucura, nevoeiros de morte. Em certos pontos dir-se-ia 
que a terra inda esta ensopada de sangue negro” (Cortesao, 1919, pp. 87-88). 

Tambem a experiencia pessoal do gaseamento e narrada sem qualquer autocomplacencia 
por Jaime Cortesao, ele prdprio medico e conhecedor da sintomatologia e efeitos do gas. 
Cortesao come^a a sentir os primeiros sintomas a altas boras da noite no quarto alugado 
onde se aloja. Socorrido pela dona da casa, e atendido por um colega portugues que o faz 
transportar para um primeiro hospital, que e bombardeado, e de onde e enviado para um 
segundo, destinado ao tratamento especlfico de gaseados, mas a ambulancia onde segue e 
alvo de um ataque e a equipa que o acompanha deixa de dar sinal de si, abatida ou em fuga. 
Socorrido fmalmente por ingleses, e transportado para um hospital de campanha britanico 
e so depois para o seu destino ftnal. A batalha de La Lys surpreende-o em plena convales- 


I. A PROBLEMATIZAQAO DA PRIMEIRA GRANDE GUERRA 


67 


cen^a, ainda demasiado combalido para poder ser de alguma utilidade aos outros clinicos 
que no hospital atendem os evacuados: 

“Ha algumas horas que sinto um mal horrivel. Tomou-me uma tosse violen- 
ta, ao passo que me ganha o peito uma opressao e um ardor horrivel, como 
se me houvessem despejado algum liquido eorrosivo ca dentro. Os olhos 
doem-me agudamente. Vejo-me a um pequeno espelho metalico de algihei- 
ra. Diabo! Tenho a impressao de que uma nevoa me nao deixa ver bem. E que 
estao irritados e laivados de sangue. Espregui^o-me. Sinto juntamente uma 
fadiga imensa e uma neeessidade inquieta de me agitar. Disponho papeis; 
abro a mala; tomo e largo coisas a toa, ate que enftm comedo a despir-me. 

Mas eis que anseio numa nova afli^ao. Arquejo, saeudido de haustos e v6- 
mitos hediondos, e, longamente, corre-me da boca uma espuma branea e 
viscosa laivada de sangue. Agora uma atonia funda prostra-me o eorpo. Urge 
que me deite. E, quando vou a meter-me na eama, sinto um ardor violento 
e erueiante nos olhos que entram de chorar a grandes bagadas. De siibito 
eerram-se e quando tento de novo abri-los, sinto que as palpebras estao vio- 
lentamente eoladas uma a outra. Entao as maos ambas afasto-as um poueo 
para logo as deixar eerrar, tao doloroso e esse esfor^o. Mas, — eoisa horrivel! 

— eu nao vi. Uma suspeita terrivel me laneeia a alma: estarei eego? I Afasto 
de novo as palpebras. Horror! Nao vejo! Nao vejo! Estou eego! O cora^ao bate 
marteladas doidas. Sento-me na eama e proeuro dominar-me. Digo a mim 
mesmo que e naturalmente inflama^ao passageira. Mas eomo nao vejo e so 
o tacto agora me guia, na tontura da afli^ao e da fadiga extrema, camba- 
leio e trope^o em tudo. As apalpadelas eonsigo deitar-me. Tento desean^ar. 

Mas nao ha maneira: o eora^ao aeieatado da emo^ao horrivel, exaustinado 
pelo veneno, galopa, galopa ea dentro. No quarto, por eima de mim, os meus 
dois pobres companheiros gemem, deeerto alanceados pelas mesmas dores. 

Agora nao gemem, uivam espantosamente. E o meu cora^ao nao descan^a. 
Sufoco” (pp. 191-193). 

Ao ereseendo de sofrimento fisico vem agora juntar-se o eonflito psiquico que sempre aea- 
ba por acompanha-lo: 

“Perdi a no^ao do espaqo e do tempo. Cai num abismo, donde a custo arraneo 
para voltar a realidade. Por vezes a vida para-me e depois tenho a impressao 
de que ressuscitei: e o cora^ao que desfaleee. Na memoria poucas sensa^oes 
persistem. So isto: a cada arraneo de tosse enche-se-me a boea de sangue. 
Dao-me kite, champagne e drogas, e crivam-me o tronco de ventosas. A 
tosse nao me deixa descan^ar. Adormeqo e acordo a cada passo com pesa- 
delos horriveis. Outras vezes passo horas sozinho. Quero falar e chamar por 
alguem, mas enrouqueci de tal maneira que perdi a voz. E certo tambem que 
nem formas teria para falar. Nem as tenho tambem para sentir” (p. 198). 


68 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


O uso de gases toxicos para fins belicos deve-se a iniciativa alema, que principia a utili- 
za-los em 22 de Abril de 1915 sobre formas aliadas situadas nas imedia^oes de Ypres, na 
Belgica, com o propdsito muito concreto de desalojar elementos inimigos abrigados em 
trincheiras e os transformar em alvo facil para fogo de artilharia. Como bem o faz notar 
Nuno Santa Clara Gomes (2013, pp. 210-211), a aposta da Alemanha na nova arma, en- 
quanto potencia beligerante, e demonstrada pelo facto de nada menos que quatro premios 
Nobel das suas bleiras cientibcas terem contribuido com a sua expertise para a cria^ao de 
um instrumento tecnocientibco cujos riscos e reals danos causados superaram de longe os 
beneflcios esperados. A polltica beligerante de rea^ao em escalada simetrica, prevalecente 
em ambas as partes em conflito, levava a que cada contendor respondesse com qualidade 
e intensidade equivalente ao adversario, de tal maneira que a incontestavel superioridade 
alema no gaseamento, que se registou sensivelmente de Abril a Setembro de 1915, rapida- 
mente foi suplantada, por sua vez, pela resposta inglesa, levando a uma especie de equili- 
brio do terror - e dos danos realmente infligidos - que dessa maneira prolongava o conflito 
com sucessivos impasses sem saida aparente e a inestancavel mortandade dai resultante: 

“Aconteceu com os gases o mesmo que com as metralhadoras ou com a ar¬ 
tilharia pesada de campanha: passaram a ser mais uma ferramenta de mor- 
te em grande escala, sem que o seu emprego levasse a uma decisao. Apenas 
aumentou o grau de horror ja existente, mas o pior estava para vir” (Gomes, 

2013, p. 212). 

O primeiro gas a ser escolhido foi o cloro, abundante na indiistria quimica, mas apenas 
esteve na origem de uma corrida a experimenta^ao de gases cada vez mais letais em doses 
cada vez menores e capazes de penetrar as mascaras de prote^ao ate ai utilizadas ou en- 
tao gases irritantes que levavam os soldados a retira-las, bcando expostos a gases letais. 
Foram assim utilizados gases sufocantes, como o fosgenio, a cloropicrina, alem do cloro 
original, gases vesicantes, como a yperite ou gas mostarda, gases irritantes, com efeitos 
esternutatdrios ou lacrimogeneos, e gases toxicos, efetivos no sangue, como o mondxido 
de carbono, e nos nervos, como o acido cianidrico. De todos, o gas mostarda notabilizou- 
se tristemente pelo facto de atuar insidiosamente, passando despercebido por ser incolor 
e inodoro, quando puro, mas com um terrivel efeito corrosivo sobre as vias respiratorios 
que levavam a uma sufoca^ao estertorante a prazo, alem de permanecer no solo sob a for¬ 
ma de goticulas contaminantes, o que acrescentava as mais comuns vias de exposi^ao, a 
dermica e a inala^ao, a via oral, o mais das vezes por ingestao de agua contaminada. Gom 
efeito, o recurso ao gas mostarda elevou a guerra quimica a um novo patamar de letalidade 
com inicio nos ataques de Julho de 2017, conseguindo piorar, se tal era possivel, o pavoroso 
espetaculo das consequencias do fosgenio ja anteriormente observadas, que literalmente 
derretia os orgaos das suas vitimas num caudal de fluido sanguinolento. O grau de eftcacia 
do gas mostarda era tambem redobrado com o aperfei^oamento da sua fabrica^ao e ma- 
nuseamento e do armamento utilizado para disparar os projeteis de gas, nomeadamente o 
projetor Livens e o morteiro Stokes. Os sintomas iniciais, uma ligeira irrita^ao ocular e na 
garganta, apareciam de maneira insidiosa, ate se irem progressivamente transformando 
em dor insuportavel que obrigava a manter as palpebras cerradas, o que explica as levas 
de evacuados de olhos vendados por pensos agarrados uns aos outros em bla Indiana - isto 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


69 


nos casos ligeiros - que surgem na iconografia da guerra. A desfigura^ao provocada por 
feridas semelhantes a bolhas de queimadura que se agigantavam em algumas boras acrescia 
a generalizada devasta^ao dos drgaos internos - laringe, cordas vocais, cora^ao, pulmoes, 
inchados e encharcados em sangue, cerebro eom imimeras bolhas de gas - observada na 
neeropsia dos cadaveres dos gaseados. Era terrivel o efeito psicologico deste espetaeulo nos 
soldados sobrevivos que testemunhavam o sofrimento martirizante dos seus camaradas e 
estava na origem de uma smdrome de medo tao paralisante eomo os proprios efeitos fisicos 
dos ataques: 

“O gas era - e continuou a ser - tanto uma arma psicologiea eomo flsica. (...) 

Uma e outra vez no deeurso da Grande Guerra, os ataques com gas causavam 
panico e os esfor^os dos militares para conceber contra-medidas tinham 
apenas limitado sucesso” (Shepard 2002: p. 63). 

As mascaras anti-gas so tardiamente utilizadas na guerra, e que as chebas garantiam ser 
seguras, nao se abguravam aos soldados enlouquecidos de medo a simples men^ao do gas 
mais baveis do que os panos embebidos em urina a que inicialmente se recorria eomo meio 
de prote^ao. Do que poderiam ser os treinos de uso da mascara, proporciona-nos o foto- 
grafo Arnaldo Garcez rarissimas imagens nacionais, eomo a intitulada “Soldados portu- 
guezes exercitando-se no uso da mascara contra gazes asphyxiantes” (Anonimo, 1917, p. 
14). O tom do artigo onde se insere essa imagem fotograbca pretende ser tranquilizador e 
infundir o sentimento que a terrivel amea^a do gas alemao nada pode contra as mascaras 
aliadas, mas o que ele nao diz e que os treinos do seu uso, em exposi^ao simulada ao gas 
para reconhecimento dos respetivos sintomas, apenas contribuiam para elevar os mveis 
de ansiedade dos soldados e fazer inclusivamente surgir rea^oes de stress traumatico por 
antecipa^ao. Foram descritos comportamentos de panico coletivo a mera audi^ao de sire- 
nes de alarme de ataque iminente com desenvolvimento de sintomatologia histrionica dos 
efeitos do gaseamento sem que este sequer tivesse ocorrido. Tanto tera contribmdo, igual- 
mente, para distorcer a avalia^ao da gravidade dos efeitos dos gaseamentos reals pelos me¬ 
dicos que tendiam a percebe-los eomo predominantemente histericos, quando, em Mar^o 
de 1918, os alemaes tentaram por bm a guerra com ataques incessantes e maciqos eomo 
forma de prevenir a interven^ao iminente das formas norte-americanas. E este o contexto 
da participa^ao do Gorpo Expedicionario Portugues a partir do imcio de 1917 e durante o 
ano de 1918, e destacadamente na batalha de La Lys, no qual os ataques com gas atingiram 
um auge. 

Isto mesmo e assinalado no Relatdrio sobre As intoxicagoes pelos gases de guerra - 
1915-1918 (doravante abreviadamente referido eomo Relatdrio) apresentado em 1919 ao 
Ministro da Guerra pelo medico militar David de Mornes Sarmento, encarregado do estudo 
clmico dos gases de guerra no Gorpo Expedicionario Portugues e que constitui um docu- 
mento absolutamente singular e precioso na literatura medica portuguesa respeitante a 
Primeira Guerra Mundial: 

Gontudo no Gorpo Expedicionario Portugues (...) ate as vesperas de terminar 
a guerra, os clfnicos nao possmam o mais rudimentar conhecimento sobre 
os ‘gaseamentos’, cujos problemas, inteiramente novos em medicina cas- 


70 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


trense, foram pela primeira vez observados, durante a grande conflagra^ao 
europeia, algumas dezenas de meses antes das nossas tropas terem parti- 
do desfraldar a bandeira de Portugal sobre as trineheiras da Flandres. E que 
eram secretas todas as publica^oes dos aliados sobre o que se ia investigan- 
do acerca destas intoxica^oes e jamais a Cheba dos nossos Servi^os de Saiide 
curara em averiguar e difundir os conhecimentos que elas faeultavam a todos 
os exercitos aliados (p. 3). 

Pouco lisonjeiro para os nossos aliados britanicos, o estudo dos efeitos do gaseamen- 
to viu-se, porem, na contingencia de depender da experiencia pratica acumulada, das 
observances clinicas, e dos meios tecnicos ingleses para ter acesso a um conhecimento 
minimamente satisfatorio acerca do gaseamento que afetava por igual as tropas lusas ex- 
postas aos ataques e todas as demais. Com efeito, o Relatorio esclarece que o estudo dos 
gaseamentos, iniciado a 2 de junho de 1918 por uma equipa composta pelo Coronel Sinel 
de Cordes e os Tenentes-coroneis Ferreira Martins e Pires Monteiro, e apoiado depois 
em Portugal pelos medicos Azevedo Neves, Geraldino Brites e Magalhaes Ramalho, se fez 
sobre os liltimos gaseados ingleses, visto quase totalidade das tropas portuguesas ja esta- 
rem afastadas das linhas da frente. O facto de os primeiros ataques alemaes sobre aliados 
terem ocorrido muito antes de as tropas portuguesas terem chegado a pode explicar que 
a hierarquia das formas portuguesas tenha depositado uma pouco justibcavel conbanna 
na protenao aliada, tendo em conta que ela em nada compensou o real desinteresse e 
despreocupanao das autoridades militares nacionais, tao impreparadas para os ataques 
como os soldados que deles foram alvo. 

O Relatorio informa que o Serviqo de Saude portugues nao organizou desde o inicio um 
imprescindivel arquivo clinico, que permitisse, nomeadamente, proceder a uma frutifera e 
correta comparanao estatistica entre as baixas nas tropas nacionais, aparentemente menos 
afetadas que outros contingentes estrangeiros, devido terem ocupado setores do terreno 
menos expostos a ataques do que estes. Assim, das 843 mortes em combate, alem de 57 
por acidente e 252 por doen^as varias, o Corpo Expedicionario Portugues viria a regis- 
tar 54 mortes por gaseamento, o que corresponde a 4,6 por cento do total de 1186 obitos, 
em franco contraste com o calculo estimado de 17 por cento na generalidade dos exerci¬ 
tos combatentes em Franca desde o inicio do conflito. Quanto as 6938 baixas sem mor¬ 
tes, houve 2649 feridos, 2058 desaparecidos ou aprisionados - o que lan^a uma incognita 
quanto ao seu estado de saude e respetivas causas - e 1848 gaseados nao fatais que cor- 
respondem a 26,6 por cento do total, superior a percentagem de falecimentos, mas ainda 
assim abaixo dos cerca de 34 por cento ocorridos nas restantes formas militares. Por outro 
lado, bcou por determinar o peso das sequelas do gaseamento nas baixas de que resultaram 
5549 incapacitados, entre os quais se contam 4277 permanentes, 321 parciais incapacitados 
para o serviqo, com 116 do Corpo Expedicionario e 823 que transitaram para os serviqos 
auxiliares. Desta estatistica diz o Relatorio que nao podem ser deduzidas conclusoes certas 
acerca das mortes diretamente decorrentes do gaseamento: 

“o numero representative das perdas em combate apresenta-se indiferente 
ao genero de morte que as ocasionou. Podem ter caido nesta designa^ao geral, 
nao sdmente os gaseados graves, que nem vida tivessem tido para alcan^ar os 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


71 


postos sanitarios (o que, conhecida a ingenua e irresponsavel inciiria que a 
tal respeito lavrou nos nossos combatentes e a ignorancia do pessoal medico, 
sem instru^oes algumas sob re a clinica dos gases, nao e para conceber como 
excepcional raridade), mas ainda muitos dos feridos que, tambem gaseados, 
tivessem vindo a sucumbir, mais cedo ou mais tarde, aos efeitos associados 
da metralha e do veneno, facto que se constatou com uma frequencia, que 
adquiriu foros de generalidade, pela dificil situa^ao do ferido na atmosfera 
sempre gaseada, que pairava sobre os campos metralhados” (p. 22). 

Nao deixa de ser sintomatico, e muito interessante desse ponto de vista, que um Relatdrio, 
escrito por um clinico a quern seria exigivel rigor e fria objetividade cientibca, mas abnal 
tao franco na conbssao das limita^oes da prepara^ao de militares e na ignorancia impo- 
tente dos medicos como na demincia da inciiria das hierarquias, atinja, em compensa^ao, 
cumes de eloquencia gongorica na descri^ao do apocaliptico desenrolar de um ataque com 
gas que espalha nuvens toxicas sobre um acampamento militar, apanhando de surpresa as 
tropas de apoio a boras de desprevenido repouso: 

“Numa alvorotada zoada de violentos acessos de tosse convulsiva e de la- 
mentos clamorosos, gritados por desesperada angiistia, despertaram milha- 
res de soldados e, em rapidos segundos, foram os que mais se esfor^aram, 
bravejando pela vida, os que, primeiro, se sentiram estrangulados e atirados 
a agonia furiosa da mais aflitiva asbxia. E os que, a tossir, sequiosos de ar 
tambem, anhelantes, tresloucados, buscavam fugir, a curtas passadas para- 
vam esbofados. Debaixo da fumarada espessa, que Ihes atabafava a tosse e a 
voz, uns a arroxearem-se progressivamente, cansado o cora^ao no trabalho 
estrenuo de pulsa^oes vibrantes, cheias e compassadas; outros a tingir-se- 
Ihes a pele com uma palidez pliimbea, entre esvaimentos, com que a aritmia 
desvairada do pulso pequeno e fugidio Ihes fazia ver a morte; todos com o 
peito descoberto e arquejante a avultar entre os rasgoes, abertos nas roupas 
pelas maos crispadas, iam caindo por terra desfalecidos, mas sempre bxados 
a toalha do veneno, que os embebia. (...) De repente, claroes deslumbrantes 
de relampagos sucessivos, jorrados das linhas alemas surgiram a dilacerar 
a escuridao profunda e, ao mesmo tempo, um furioso furacao de metralha 
desencadeou-se sobre a nevoa densa a tentar remove-la, dissipa-la com os 
cortantes sibilos das estilhas e com o estrondoso reboar dos obuses, que ca- 
vavam no solo juncado de cadaveres, bocas informes, onde o veneno corria a 
esconder-se com os farrapos retalhados pela saraivada dos estilha^os, e, tin- 
tas de sangue, permaneciam abertas, como que em avido desejo de deglutir 
maisvidas” (pp. 27-28). 

Segue-se a debandada geral e desordenada dos diretamente atingidos e bem assim de 
quantos, tornados pelo panico a vista do estertor dos caidos, acorriam de toda a parte onde 
a nuvem de gas tinha chegado para invadirem aos milhares os postos de socorro: 


72 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


“Entre os doentes que se conservavam de pe, uns agitavam-se como loucos, 
com as faces vultuosas, os olhos injectados, procidentes a contrairem-lhes as 
pupilas, com as narinas dilatadas, escumantes e os labios roxos a distende- 
rem a boca entreaberta donde pingava a escassa expectora^ao amarela, go- 
mosa e arejada. Retorciam-se em luta desesperada, ora em crises de vdmitos 
sucessivos que surgiam a quebra-los pela cintura, ora contra extenuantes 
paroxismos de tosse que mal Ihes deixava as gargantas doloridas coar o s6- 
pro esganado da respira^ao curta e dificil. E com os ombros soerguidos e o 
pesco^o atado por engrossadas veias tiirgidas, latejantes, rasgavam furiosa- 
mente as roupas que sentiam como aneis de a^o a apertarem-lhes as goelas, 
a esmagarem-lhes o arcaboi^o distendido um amplo bojo imdvel. Erios, com 
o pulso vagaroso, cheio e vibrante, mal sofriam que se Ihes escutassem os 
pequenos fervores de congestao discreta que, sem mais sinais, se difundiam 
pelo tdrax ressonante de cima a baixo. Aqui e alem outros sentados ou ja dei- 
tados em macas, sobre os bancos, ou por terra, numa fadiga extrema, iam-se 
entorpecendo a pouco e pouco e cobrindo-se de uma leve tinta azulada que 
escurecia nos labios e nas orelhas; humedecidos por suor viscoso e frio, com 
as maos tremulas e regeladas, com o pulso ainda ritmico, mas muito fra- 
co a apressar-se, recurvados e anhelantes, abandonavam-se sonolentos ao 
edema, que Ihes inundava os pulmoes, e frouxamente debatiam-se contra as 
angustiosas tasias de ar, com que a tosse convulsiva Ihes trazia a abundante 
expectora^ao glutinosa e espumante, que ftcava a babar-lhes as faces, as ves- 
tes, a escorrer ate o solo” (p. 29). 

Do ambiente vivido na imediata sequencia de ataque com gas da-nos uma eloquente ima- 
gem o quadro “Depois de um ataque de gas”, do pintor Adriano Sousa Lopes, que, a sos, 
tomou a sua conta o Serviqo Artistico do CEP, apos se ter voluntariado para o integrar como 
obcial. A vista desta realidade, realmente digna de um triptico de Bosch, a impotencia dos 
medicos revela-se em toda a sua terrivel brutalidade: 

“Perante tarn extravagantes e mortiferos efeitos desta intoxica^ao desconhe- 
cida, nao ocorria outro recurso de tratamento, que suavizasse o sofrimento 
de tanta gente, que nao viesse da terapeutica sintomatica” (p. 30). 

A sintomatologia variava consoante o tipo de gas utilizado. A este respeito, o Relatorio 
adianta uma tipologia propria, claramente oposta ao modelo germanico, rejeitado por clas- 
sibcar os gases em fun^ao de um criterio exclusivamente militar, dividindo-os basicamen- 
te em dois grupos, o dos gases irritantes que rapidamente neutralizavam os combatentes 
feridos, e o dos gases como o gas mostarda, que eliminava de forma retardada os comba¬ 
tentes que por ele eram atingidos. A isto, o Relatorio prefere uma classiftca^ao pautada 
por criterios clinicos, proxima das tipologias de gases adoptadas pelos exercitos frances e 
britanico. Os franceses distinguiam entre gases sufocantes (do tipo cloro), nos quais se in- 
cluiam o fosgenio, a cloropicrina, a palite e as cetonas bromadas, gases vesicantes, caso da 
iperite e gases irritantes, estes liltimos divididos entre lacrimogeneos, do tipo do brometo 
de benzil, e esternutatdrios, do tipo do cloreto de difenil-arina. Os ingleses distribuiam os 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


73 


gases pelas categorias de irritantes pulmonares (os vapores nitrosos, o cloro, o fosgenio, o 
oxicloreto de carbono ou cloreto de carbonilo, a palite ou cloro-metil-cloroformato, o dis- 
fogenio ou tricloro-metil-cloroformato, a cloropicrina ou nitro-clorofdrmio, o cloreto e a 
fenil-carbilamina), irritantes nasals (o cloreto de difenil-arsina), irritantes lacrimogeneos 
(o brometo de benzil, o brometo de xilil, a bromacetona, o brometo de metil-etil-ceto- 
na e o dibrometo de metil-etil-cetona), vesicantes (a iperite), um intoxicante do sangue 
(o oxido de carbono) e um veneno do sistema nervoso (o acido cianldrico). O Relatdrio 
portugues adopta uma versao simplibcada, que classiftca os gases de guerra em gases al- 
terantes da composi^ao qulmica do sangue, distinguindo estes em nocivos por anoxemia 
(como o dioxido de carbono) e nocivos ao sistema nervoso ou nevrostenicos (como o acido 
cianidrico), e gases irritantes celulares dos tecidos epiteliais de revestimento (tais como o 
cloro, os brometos, o dibrometo, o fosgenio, a palite, o difosgenio, a cloropicrina, os sul- 
furetos e os cloretos, etc.), e descreve a sintomatologia e as lesoes decorrentes de cada um 
destes tipos de gases, a partir de uma revisao do estado da arte da literatura medica ingle- 
sa. Alem desta, o Relatdrio cita igualmente o documento Defence Against Gas das British 
Expeditionary Forces, no intuito de realgar que a precariedade das soluqoes terapeuticas 
obrigava a escrupuloso cumprimento das medidas preventivas, o que as formas britanicas 
cometiam as responsabilidades formais da oftcialidade militar. Com efeito, o exito variava 
com as diferen^as de toxicidade dos gases, e, se era menor na carboxemia, agravava-se no 
caso do acido cianidrico que atuava no sistema nervoso, e mais ainda, se possivel, com os 
gases irritantes e corrosivos que destroem os element os celulares. A corrosao da arvore 
respiratoria era invariavelmente fatal e atingia foros de horror, o que era patente na au- 
topsia aos cadaveres de gaseados, que mostravam extensas e mortiferas lesoes, sobretudo 
nos gaseados que morriam com rapidez subciente para nao bcarem afetados outros drgaos, 
que, quando o eram, apareciam entao completamente destruidos tambem. No conjunto 
das impressionantes imagens fotograbcas inseridas no bnal do Relatdrio, disponibiliza- 
das por fontes inglesas, alem de algumas que mostram as lesoes tecidulares em pulmoes e 
hemorragias no cerebro de gaseados, inclui-se uma que re vela o revestimento interno de 
uma traqueia expelido na expetora^ao de um soldado, com uma grande parte da mucosa 
vascularizada (Figura f). 


74 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 



Figura 1. (Fonte: Centro de Estudos de Comunicagao e Linguagens) 


Com efeito, a sintomatologia do gaseamento nao so variava consoante o tipo de gas uti- 
lizado, como em fun^ao do grau de exposi^ao das vitimas dos ataques, que podiam dear 
afetadas de forma ligeira (Figura 2), moderada (Figura 3) ou grave (Figura 4), e sobreviver 
com equivalentes graus de incapacidade ou entao a morte a prazo mais ou menos curto, 
variavel entre umas poucas boras e alguns dias. 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


75 




Soldado S. D. — Artilharia de costa, 2° grupo do C. A. R, 3.* Bataria 
(Fotografia tirada no 4“ dia de intoxicagdo) 

Gaseamento ligeiro de inlcio. Queimaduras do 1." e 2° grau. Rash escarlatiniforme. Intensa 
conjuntivite pumlenta. Moderadas laringite, faringite, traqueite e bronquite. Complicaqoes 
bronco-pneumonicas ao 8.° dia. Albumina, cilindros hialiiios na urina. Hospitalizaqao durante 
dois meses. Ulterior situaqao militar: incapaz do serviqo activo. 


Figura 2. (Fonte: Centro de Estudos de Comunicagdo e Linguagens) 


76 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 





Figura 3. (Fonte: Centro de Estudos de 
Comunicagdo e Linguagens) 


Figura 4. (Fonte: Centro de Estudos de 
Comunicagdo e Linguagens) 


For seu lado, o grau de eficacia do tratamento, mesmo assim apenas paliativo dos sintomas, 
era inversamente proporcional a gravidade dos efeitos: 

“De uma maneira geral, pode-se coneluir que o tratamento fundamental das 
intoxiea^oes de guerra so dispunha de reeursos para obviar as ulteriores al- 
tera^oes, que vinham a prejudicar em globo a eeonomia organica pelas va- 
riadas formas de asfixia. Todas as disposi^oes terapeutieas mais importantes 
esealonavam-se, entao, eom o eapital intuito de soeorrer os feridos asffxicos, 
cuja sintomatologia ora se estabelecia de siibito, ora evolueionava vagarosa- 
mente. E, era em torno deste prinefpio essencial, que a restante mediea^ao 
seeundaria garantia a efieaeia da terapeutica prineipal, evitando e moderan- 
do quaisquer dibeuldades e complica^oes intereorrentes” (p. 131). 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


77 






Para todos os gaseamentos de guerra o tratamento geral assentava nas seguintes quatro 
condi^oes essenciais: 

“l) Ao retirar os feridos das atmosferas envenenadas, deviam ser mantidos 
em meios, onde permanentemente circulasse ar livre e puro; 

2) A fim de Ihes serem evitadas combustoes organicas escusaveis e sempre 
perniciosas pela carencia de oxigena^ao do sangue, que em tais easos era 
sempre difieil, todos os feridos gaseados deviam ser mantidos no mais rigo- 
roso repouso; 

3) E eomo ao entrarem enregelados, o frio os obrigava a noeivos movimentos 
musculares e era agravado pelo despir das roupas, que traziam sempre mais 
on menos contaminadas pelo toxieo, deviam ser todos mantidos em meios 
moderadamente aquecidos e ser rodeados das preeau^oes neeessarias, que 
Ihes evitassem perdas de ealor; 

4) Finalmente, a instabilidade da sintomatologia inicial destes feridos, que 
pioravam de repente e gravemente, sem eausas facilmente reconheeiveis e 
portanto inevitaveis, aeonselhava os elinicos a reservar sempre os seus prog- 
ndsticos nas primeiras fases, mostrando-lhes que se deviam manter na mais 
rigorosa vigilancia, ate que o perigo das surpresas fosse removido” (p. 132). 

Deste modo, os gaseados em sincope eram imediatamente socorridos nos postos de socor- 
ros (Batallion Aid Post), eventualmente ventilados com respira^ao artificial nos easos que 
dela necessitassem, depois encaminhados para as Advanced-Dressing Station(s), ja muni- 
dos de um diagnostico provisdrio, e enbm para os Centros Avan^ados de Gaseados que po¬ 
dia ser uma Main-Dressing-Station ou uma Casualty-Clearing-Station. Ai eram despidos e 
esfregados com soluto aquecido de hipoclorito de calcio, os olhos e as narinas copiosamen- 
te irrigados com bicarbonato de sodio em solu^ao aquecida, podendo ser sangrados ligeira 
ou abundantemente em caso de dispneia agitada e vultuosa, mas so se a pressao sanguinea 
e as pulsa^oes estivessem normais, e ftnalmente vestidos com pijamas e obrigados a deitar- 
se em absoluto repouso ate a evacua^ao final. 

Em Portugal, apos o tratamento na fase aguda da doen^a, os soldados eram recebidos, 
para acompanhamento prolongado, em Esta^oes de Convalescen^a, em regra instala^oes 
adaptadas especialmente para esse efeito em edificios ja existentes, eomo foi o caso do an- 
tigo Lazareto de Lisboa, um vasto edificio situado em Porto Brandao, na margem sul do rio 
Tejo, atualmente ainda de pe, mas devoluto e em adiantado estado de degrada^ao, apos a 
sua derradeira serventia eomo local de acolhimento de retornados das ex-col6nias africa- 
nas. Do ambiente vivido nesses locals, pode dar-nos uma palida imagem uma pouco co- 
nhecida obra do medico Bissaia Barreto, O sol em cirurgia (1915), que exalta os beneficios 
da exposi^ao aos raios solares, profusamente ilustrada com imagens, entre as quais uma 
que mostra soldados a apanhar banhos de sol (p. 340). 


78 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



Figura 5. (Fonte: Centro de Estudos de Comunicagdo e Linguagens) 


Efetivamente, a literatura medica que chegou ate nos, sugere que pouco mats era dado 
fazer ao acompanhamento elmieo dos eonvaleseentes, e menos ainda quanto aos efeitos 
psiquiatrieos respeitantes aquilo que so muitas decadas - e muitas guerras passadas - apos 
o termo da Primeira Guerra Mundial foi deserito eomo stress de guerra e stress pos-trau- 
matico. No estado atual da investiga^ao, apenas se pode aventar que era pratica exeeeional 
o eneaminhamento de evaeuados para hospitals psiquiatrieos, eomo foi o easo do Manuei 
Joaquim da Cunha, oriundo da Murtosa, que, vindo de Franca, onde ja tinha estado in- 
ternado num hospital de eampanha entre 7 de Janeiro e 24 de Ahril de 1918, deu entrada 
no entao Manieomio Bomharda a 17 de Maio (1918). Diagnostieado eom eonfusao mental e 
dellrio (aponta uma pequena cicatriz que diz ser de um golpe no arame farpado, num dos 
bravos, e pede ao medico que Iho corte, para nao Ihe envenenar o sangue), a sua historia 
cllnica assinala o choro facil. Para alem da tuberculose pulmonar subjacente, encontrava- 
-se emaciado (ganha quatro quilos e meio nas duas semanas de internamento) e era porta- 
dor de uma infe^ao urinaria. Nada mais tendo hcado registado quanto ao seu seguimento 
psiquiatrico, foi-lhe dada alta a 3 de junho seguinte. 

Identico desconhecimento se aplica aos proprios efeitos neurologicos, e respetivas 
complica^oes de ordem psiquiatrica, resultantes dos gaseamentos, embora as suas mani- 


1. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


79 









festa^oes fossem evidentes no comportamento dos “esgaseados”. Na sua extensa obra A 
neurologia na guerra (1917), na qual se debru^a sobre as altera^oes motoras provocadas 
por lesoes medulares, Egas Moniz ignora na pratica os efeitos neurotdxicos dos gaseamen- 
tos. Imp6e-se, porem, ressalvar que tal estudo teria decerto de limitar-se a algumas obser¬ 
vances superftciais, como acontecia nas necropsias que registavam alteranoes patologicas 
nos cerebros cadavericos dos gaseados, na impossibilidade de as visualizar in vivo nos 
sobreviventes a longo prazo que apresentavam desordens comportamentais, o que so foi 
possivel com o surgimento da angiografta cerebral, em epoca muito posterior. Na verdade, 
o mais que conhecia a medicina castrense anterior a Primeira Grande Guerra era a cha- 
mada “embriaguez pela polvora” libertada pelas explosoes, mas nada que se assemelhasse 
aos efeitos do gaseamento, de que se limitava a veribcar no teatro de guerra as consequen- 
cias mais impressionantes, de resto ja conhecidas atraves de experimentanao laboratorial. 
A sintomatologia da intoxicanao por dioxido de carbono levava a falencia das faculdades 
psiquicas com confusao mental que simulava a sintomatologia da embriaguez alcodlica, 
por outro lado reconhecidamente prevalecente entre as tropas. Tanto acarretava que, no 
teatro de guerra, as hierarquias reagissem com a imposinao de puninoes previstas para a 
intemperanna, como bem recorda o Relatorio (p. 59), que informa igualmente que as com- 
plicanoes nervosas, com irritabilidade e depressao neurastenicas, devidas nao so a gravi- 
dade dos sintomas como a inadapta^ao das vftimas ao seu estado de inferioridade ffsica e 
de total dependencia, se vinham ainda sobrepor a debilidade e a fadiga ffsica permanentes 
e irrecuperaveis que persistiam rebeldemente durante os longos meses de convalescenna: 

“foram aos milhares de jovens, que, escolhidos na mocidade mais robusta e 
vigorosa, esgotaram na tortura dos gaseamentos de guerra, durante algumas 
semanas apenas, quasi toda a a energia vibrante, que pujantemente Ihes ofe- 
recia longos anos da mais saudavel e solida resistencia” (p. 87). 

Notafinal: As bguras 1, 2, 3, 4 e 5 pertencemao domfniopublico e foramrecolhidas no am- 
bito do Projeto de l&amp;D daFundanao Para a Ciencia e Tecnologia HG/0110/2009: Historia da 
Cultura Visual da Medicina em Portugal, de que o autor do texto e Investigador Responsa- 
vel, acolhido pelo Gentro de Estudos de Gomunicanao e Linguagens da Universidade Nova 
de Lisboa / GEGL, cujo logotipo foi impresso em cada imagem reproduzida. 


80 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Referencias 


Anonimo (1917). “As Mascaras contra gazes asphyxiantes”, Portugal na Guerra. Revista 
Quinzenal Illustrada. 1 (2), 15 de junho de 1917,14. 

Araujo, F. M. (2014). Reminiscencias nacionais da Grande Guerra: as edi^oes literarias da 
“Renascen^a Portuguesa” (1916-1924). Cadernos de Literatura Comparada, 31, 83-110. 
Barreto, B. (1915). O sol em cirurgia. Coimbra, Imprensa da Universidade. 

Cortesao, J. (1919). Memorias da Grande Guerra. Porto, Renascen^a Portuguesa. 

Gomes, N. S. C. (2013), Guerra Quimica. Os gases de combate. In Afonso A. &amp; Gomes C. M. 
(orgs.) Portugal e a Grande Guerra. Vila do Conde, Verso da Historia, pp. 210-212. 

Leal, E. C. (2000). Narrativas e imaginarios da 1“ Grande Guerra: “O Soldado-Saudade” 
portugues nos “nevoeiros de morte”. Revista de Historia das Ideias. 21, 441-460. 
Manieomio Bombarda (1918) Gaderno de Admissao n° 1204, Livro 13, P Sec^ao, Ano de 
1918, Enfermaria n“ 3, Classe 4“. Manuel Joaquim da Cunha. 

Moniz, E. (1917). A neurologia na guerra. Lisboa, Livraria Ferreira. 

Sarmento, D. P. M. (1919) As intoxicagoes pelos gases de guerra - 1915-1918. Relatorio 
Apresentado a S.Ex.a o Ministro da Guerra, pelo Dr. David P. de M. Sarmento, Encarregado 
do Estudo Cllnico dos Gases de Guerra no Corpo Expedicionario Portugues. Lisboa, Im¬ 
prensa Nacional. 

Shepard, B. (2002) A War of Nerves. Soldiers and Psychiatrists 1914-1994. London, 
Pimlico. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


81 



O Service Veterinario Militar na 
Primeira Guerra Mundial 

Gon9alo Paixao 

Exercito Portugues 


Resumo: No imcio da Primeira Guerra Mundial, a posi^ao de Portugal encontrava-se condi- 
cionada pelas dificuldades politicas e economicas internas e, no piano externo, pela amea- 
9a da expansao das potencias europeias em Africa. A partir de agosto de 1914 o Governo 
Portugues sentiu a necessidade de refor^ar a presen^a nos territorios de Angola e Mozam¬ 
bique em virtude da amea^a Alema que ali se fazia sentir. A a^ao do Servi^o Veterinario Mi¬ 
litar (SVM), a data, era em grande parte de profilaxia, diagnostico e tratamento de cavalos, 
estando a area de saiide piiblica e epidemiologia no seu inicio. Tambem em Angola o SVM 
foi chamado a apoiar o esfor^o de guerra, sobretudo no que diz respeito ao apoio sanitario 
aos solipedes mobilizados. Durante o periodo da Grande Guerra foram mobilizados para 
Angola um total de 2321 solipedes. O apoio veterinario estava dividido em servi^os veteri- 
narios de 1“ linha ou 2“ linha consoante fosse efetuado na linha da frente ou na retaguarda. 
A guarni^ao das colonias de Angola e Mozambique, e a constituizao e organizazao do Gor- 
po Expedicionario Portugues (GEP), motivaram uma profunda reorganizazao do Exercito. 
Nesse periodo, o SVM revelou-se uma estrutura de apoio imprescindivel a manutenzao 
do efetivo animal. A criazao do Hospital Veterinario Militar de Lisboa e do Deposito Geral 
de Material Veterinario data de julho de 1916. Em 9 de Marqo de 1916 a Alemanha declara 
guerra a Portugal. Entre maio e agosto de 1916 foram reunidos em Tancos cerca de 19.000 
homens e cerca de 4.000 solipedes. O primeiro contingente do GEP parte para Franza a 30 
de Janeiro de 1917. Sao mobilizados 7.783 solipedes, enviados de Portugal via maritima. A 
estes juntam-se mais 1476 fornecidos pelos depdsitos de remonta Ingleses. Durante este 
periodo o Servizo de Veterinaria Militar revelou-se uma estrutura de apoio imprescindivel 
a manutenzao do efetivo animal, pelo que constitui o objeto de analise deste artigo. 

Palavras-chave: Servizo veterinario militar, organizazao, solipedes. Hospital Veterinario 
Militar, guerra na Europa e guerra nas colonias 

Abstract: At the beginning of the First World War, Portugal’s position was conditioned by 
internal political and economic difficulties and, at the external level, by the threat of the 
expansion of European powers in Africa. Since August 1914 the Portuguese Government 
felt the need to strengthen the presence in the territories of Angola and Mozambique be¬ 
cause of the German threat. In Mozambique tensions grew along the border of the Rovu- 
ma, where the Germans attacked the post of Maziiia. The action of the Military Veterinary 
Service (SVM), then, was mostly of prophylaxis, diagnosis and treatment of horses, being 
the area of public health and epidemiology at an early stage. The SVM was called to support 
the war effort, also in Angola, especially with regard to health support for the solipeds 
mobilized. During the period of the Great War, a total of 2321 solipeds was mobilized to 
Angola. Veterinary support was divided into hrst-line or second-line veterinary services 


I. A PROBLEMATIZAZAO DA PRIMEIRA GRANDE GUERRA 


83 


depending on the front or rear line. The garrison of the colonies of Angola and Mozam¬ 
bique, and the constitution and organization of the Portuguese Expeditionary Corps (CEP), 
motivated a profound reorganization of the army. During this period, the SVM proved to 
he an indispensahle support structure for the maintenance of the operational animals. The 
creation of the Military Veterinary Hospital of Lisbon and the General Deposit of Veterinary 
Material, date of July 1916. On March 9,1916 Germany declares war to Portugal. Between 
May and August 1916 about 19,000 men and about 4,000 solipeds were gathered in Tan- 
cos. The hrst contingent of the CEP leaves to Prance on January 30,1917. 7,783 solipeds are 
mobilized, shipped from Portugal by sea. These are joined by 1476 more provided by the 
English deposits. During this period the SVM (Military Veterinary Service) proved to be an 
essential support structure for the maintenance of the effective herd and is therefore the 
object of analysis of this article. 

Keywords: Veterinary services, organization, contingents, solipedes. Military Veterinary 
Hospital, war in Europe, war in Portuguese colonies. 


84 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


1911-1916: Prepara 9 ao e Mobiliza 9 ao do Servi 90 Veterinario do 
Exercito Portugues para as campanhas da Grande Guerra 

Atraves da reorganiza^ao do Exercito de 25 de maio de 1911 (Secretaria da Guerra, 1911)^' 
o Servi^o Veterinario do Exercito (SVE) foi desanexado do Servi^o de Saiide (SS), tendo 
sido criadas a 6“ Reparti^ao (Servi^o Veterinario) da T Dire^ao Geral (DG) da Secretaria de 
Estado dos Negocios da Guerra, chefiada por um Coronel Veterinario (a que foi atribuida a 
fun^ao de inspetor do SVE), e a 6“ Reparti^ao da T DG do EME, chefiada por um Tenente 
Coronel Veterinario e tendo por subchefe um Major Veterinario. 

Ate entao, a administra^ao do SVE estabelecia-se atraves da T sec^ao da 6“ Reparti^ao 
(SS) da T DG da Secretaria de Estado dos Negocios da Guerra. A reorganiza^ao de 1911 re- 
sultou na completa autonomia do SVE enquanto Servi^o do Exercito. Por ser justo e meri- 
torio, cumpre-nos referenciar os nomes de Almeida Beja, Mota de Almeida, Simoes Alves, 
Chaves de Lemos, Concei^ao e Almeida, Evangelista de Sousa e Alves Simoes, que foram os 
principais obciais obreiros da nova organica emancipadora do SVE. 

Nesta reorganiza^ao do Exercito de 1911, foi tambem estatuida a cria^ao do Hospital Ve¬ 
terinario Militar (HVM) e do Deposito Geral de Material Veterinario (DGMV) (Secretaria 
da Guerra, 1911)^^ mas somente 5 anos mais tarde se viria a materializar aquela que tinha 
constituido, ate entao, uma aspira^ao de toda a classe medico-veterinaria do nosso pais 
(Secretaria da Guerra, 1916a)^^. Com a cria^ao do HVM procura-se ultrapassar as insub- 
ciencias nos sistemas de recrutamento e mobiliza^ao de pessoal veterinario (medicos e 
enfermeiros) e de pessoal siderotecnico. Juntamente com o HVM foram estabelecidos o 
Deposito Geral de Material Veterinario (DGMV), uma escola de enfermagem hipica e uma 
escola de siderotecnia com uma oftcina anexa para fabrico de ferragem. 

As missoes atribuidas ao HVM dividiam-se por 2 areas: (i) forma^ao academica/tecnica 
do pessoal e investiga^ao cientibca e (ii) apoio sanitario ao efetivo das unidades do Exercito 
e das Guardas Republicana e Eiscal. Assim, o HVM destinava-se a especializa^ao e aperfei- 
9 oamento tecnico do pessoal superior do servi^o veterinario, a instru^ao da escola pre- 
paratdria de obciais milicianos, a instru^ao do pessoal de enfermagem e ao tratamento e 
hospitaliza^ao de solipedes. O HVM possuia na sua organica um laboratorio de bacteriolo- 
gia e analises clinicas e bromatoldgicas, que possibilitavam a realiza^ao de analises clinicas 
e bacteriologicas, o estudo de enzootias e epizootias (como o violento surto de mormo que 
eclodiu nos solipedes do CEP em 1917), hem como a realiza^ao de estudos de anatomia pa- 
tologica, parasitologia e microbiologia. 

O DGMV destinava-se a adquirir, fabricar, guardar, reparar e fornecer as unidades e for- 
maqoes veterinarias de campanha todo o material veterinario de que necessitassem para o 
servi^o. Por sua vez, os cursos de enfermeiro hipico e de ferrador nas escolas do HVM so 
seriam efetivamente criados em 1917 (Secretaria da Guerra, 1917)^'*. 

A escola de siderotecnia destinava-se a aperfei^oar os conhecimentos dos medicos ve- 
terinarios nesta especialidade e a ministrar instru^ao tecnica as pranas que se habilitassem 


Publicado na Ordemdo Exercito n.“ 11, de 26/5/1911. 

Artigos 151“ e 157”. 

Publicado na Ordem do Exercito n.“ 16, de 15 de julho de 1916. 
Publicado na Ordem do Exercito n° 6 de 28/4/1917. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


85 



para ferradores militares. O pessoal instrutor e instruendo da escola de siderotecnia cons- 
tituiam um esquadrao de tropas do service veterinario. 

A escola de enfermagem era destinada a ministrar a instru^ao tecnica aos enfermeiros 
hipicos. O pessoal instrutor e instruendo da escola de enfermagem hlpica constitulam ou- 
tro esquadrao de tropas do servi^o veterinario. Os enfermeiros hipicos e ferradores cm 
servi^o nas diferentes unidades tinham a matrfcula aberta nos respetivos esquadroes das 
escolas de enfermagem e siderotecnia, e consideravam-se adidos fazendo servi^o naquelas 
unidades. 

Os esquadroes de tropas do servi^o veterinario cram constitufdos por comandante ca- 
pitao veterinario, 2 subalternos veterinarios, primeiros sargentos (enfermeiros hipicos ou 
ferradores, consoante o esquadrao), segundos sargentos (enfermeiros hipicos ou ferrado¬ 
res) , primeiros cabos (enfermeiros hipicos ou ferradores), soldados (enfermeiros hipicos 
ou ferradores) e soldados recrutas (todos em niimero a hxar, consoante o or^amento atri- 
buido). O tempo de dura^ao da escola de recrutas era hxado em 4 meses, hndo o qual as 
pranas passavam a classe de aprendizes. A tftulo de curiosidade, rehra-se que uma pra^a 
recruta do servi^o veterinario tinha um vencimento (pre) de 2 escudos, um aprendiz de 
12 escudos, um soldado de 16 escudos; um primeiro sargento tinha direito a um pre de 47 
escudos (mais gratihea^ao de 20 escudos). 

As pranas do servi^o veterinario militar tinham uniforme analogo ao da cavalaria, subs- 
tituindo a cor vermelha por carmesim e tendo no barrete as letras “SV” em metal. Os ferra¬ 
dores usavam o distintivo de ferrador. Os enfermeiros hipicos usavam “na gola do dolman 
de pano uma estrela vermelha de cinco pontas“ assente sob re uma carcela de pano preto. 
No dolman de cotim, a estrela assentara sobre uma carcela de mescla cinzenta ” (Secretaria 
da Guerra, 1916a). 

01° Diretor do HVM foi o Tenente-coronel Aniceto Rodrigues, tendo o HVM sido levan- 
tado nas instala^oes da antiga “Real Fabrica de Lanificios de Alenquer” no Campo Grande. 
Em 1928 o HVM foi transferido para o Rio Seco (Lisboa). Pelas antigas instala^oes do HVM 
no Campo Grande passariam mais tarde o Batalhao de Sapadores Mineiros do Regimento de 
Engenharia 2 (Exercito Portugues, 2013a) e o Batalhao de Servi^o de Transportes (Exercito 
Portugues, 2013b). Atualmente, naquele espa^o esta o polo do Campo Grande da Univer- 
sidade Lusofona. 

De 1911 a 1928 o quadro permanente foi hxado em 41 oheiais veterinarios, mas no periodo 
da Grande Guerra houve uma marcante e necessaria remodela^ao do quadro dos oheiais 
veterinarios milicianos e, em Portugal, quase todos os elementos da classe medico-veteri- 
naria foram chamados a cumprir o seu dever. A constitui^ao de um quadro de oheiais ve¬ 
terinarios milicianos iniciou-se pela promo^ao de todas as pranas que possuissem o curso 
de medico-veterinario (Secretaria da Guerra, 1916b)“. De seguida, foi publicada legisla^ao 
no sentido de estabelecer um regime especiheo de incorpora^ao para os alunos da Escola de 
Medicina Veterinaria (Secretaria Geral do Ministerio de Instru^ao Piiblica, 1916)^^. Os alu¬ 
nos do 5“ ano cram promovidos a alferes veterinarios milicianos (desde que aprovados nos 
exames desse ano); os alunos dos outros anos que tivessem mais de 20 e menos de 30 anos 


Simbolo da Alian^a Internacional da Estrela Vermelha. 
Publicado na Ordemdo Exercito n“ 6, de 20/04/1916. 
Publicado na Ordem do Exercito n° 9,11/05/1916. 


86 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



de idade ficaram ob rig ados a apresentar-se no HVM no prazo de 5 dias apds a conclusao dos 
sens exames, para assentarem pra^a em Cavalaria 4. 

Em Cavalaria 4, os alunos do 3“ e do 4° ano, apds receberem instru^ao, seriam promovi- 
dos a aspirantes veterinarios, sendo ulteriormente promovidos a alferes quando terminas- 
sem o curso sem necessidade de mais instru^ao. Os alunos do 1° e do 2° ano frequentavam 
uma escola de sargentos enfermeiros hipicos (a data da publica^ao deste Deereto nao estava 
ainda regulamentado o curso de enfermeiro hipico na escola de enfermagem do HVM). 

Finalmente, a 30 de novembro de 1916, e para vigorar durante o estado de guerra, foi 
publicado o Deereto n“ 2:874, de 30/11/1916 da 6“ Reparti^ao - 2“ DG da Secretaria da 
Guerra^®, que estabeleceu o regime especibco de mobiliza^ao para todos os cidadaos con- 
siderados aptos que exercessem a probssao de medico-veterinario ou que possuissem o 
diploma de curso de medicina veterinaria. Nele se debnem 3 escaloes etarios, sendo que os 
cidadaos com mais de 20 e menos de 30 anos de idade que possuissem o curso de medici¬ 
na veterinaria foram nomeados alferes veterinarios milicianos, e bcaram obrigados a fazer 
parte das tropas ativas ate completarem os 30 anos de idade. Os medicos veterinarios com 
idades compreendidas entre os 30 e os 40 anos de idade foram nomeados alferes veteri¬ 
narios de reserva. Os obciais veterinarios milicianos no ativo e na reserva deviam receber 
a sua instru^ao no Hospital Veterinario Militar, nos termos do Deereto 2:367 de 4 de maio 
de 1916, devendo apresentar-se ai diretamente sem necessidade de transitarem por outras 
unidades (Nota n“ 3:222 da Reparti^ao do Gabinete, de 6/12/1916, reproduzida na Gircular 
n° 22, 8“ Reparti^ao, 2“ DG da Secretaria da Guerra)^'*. Naturalmente, a chamada de obciais 
veterinarios milicianos para o servi^o de campanha fez-se principiando pelos mais moder- 
nos, sendo tambem estabelecidos neste deereto os criterios para o estabelecimento da sua 
antiguidade. Neste deereto foi ainda estabelecido um 3“ escalao etario que compreendia os 
alferes veterinarios da reserva territorial (ate perfazerem 60 anos de idade). Guriosamente, 
os diplomados com o curso de medicina veterinaria, mas que nunca tivessem exercido a 
probssao, seriam tambem nomeados alferes veterinarios milicianos e poderiam ser coloca- 
dos no Hospital ou no DGMV para ai desempenharem servi^o burocratico. 

O regime especibco de mobiliza^ao e de incorpora^ao de medicos veterinarios milicia¬ 
nos, conbgurava o reconhecimento por parte do Exercito de que a existencia de servi^os 
veterinarios especializados era indispensavel as suas necessidades. Neste periodo, o SVE 
conheceu um desenvolvimento impar na sua historia, tendo consolidado os projetos e as- 
pira^oes de gera^oes de uma classe probssional que sempre procurou dignibcar o seu Ser- 
vi^o dentro do Exercito, bem como as institui^oes militares na Sociedade. 


Alian^a Internacional da Estrela Vermelha 

No inicio do seculo XX, o entao Secretario da Guerra Americano, Newton Baker compreen- 
dendo que o sofrimento dos animals no campo de batalha deveria ter a mesma aten^ao que 
o sofrimento de um soldado, solicitou colabora^ao a William Stillman, presidente da As- 


29 


Publicado na Ordem do Exercito n° 23,1° serie. 
Publicada na Ordem do Exercito n° 24,serie. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


87 



socia^ao Humana Americana, no sentido de ser prestada assistencia aos animais vitimas 
da guerra, a semelhan^a do que a Cruz Vermelha fazia com as pessoas. Assim, fruto desta 
necessidade, cm dezembro de 1914, fundou-se a Alian^a Internacional da Estrela Verme¬ 
lha. Esta Alian^a resultou de uma assembleia realizada em Genebra, na mesma data, onde 
se reuniram representantes de varios paises com o proposito de debater na senda interna¬ 
cional a problematica dos animais feridos na guerra. Constituiu ainda um outro objetivo 
desta assembleia a coordena^ao dos trabalhos de varias institui 96 es^°, entretanto criadas, 
que prestavam assistencia aos animais nos campos de batalha. 

Na assembleia de Genebra estabeleceu-se igualmente a necessidade de cria^ao de dele- 
gardes nacionais, sendo que de Portugal foi a Sociedade Protetora dos Animais de Lisboa 
que aceitou o apelo. O Governo Portugues atraves do Ministerio da Guerra e por intermedio 
desta Sociedade integrou a Alian^a Internacional da Estrela Vermelha, em 14 de outubro de 
1915, por considerar de elevada vantagem para o Exercito Portugues os serviros prestados 
por aquela Alianra, no tratamento dos solipedes doentes e feridos em campanha. 

Assim, atraves do Decreto n.° 2:363, de 2 de maio de 1916 (Secretaria da Guerra, 1916d)^\ 
no seu artigo 1°, a Alian^a Internacional da Estrela Vermelha e reconhecida como institui- 
rao de utilidade piiblica e auxiliar do SVE. 

O Decreto n.° 2:571, de 15 de agosto de 1916 (Secretaria da Guerra, 1916e)“, estabelece os 
distintivos que eram usados pelas tropas do serviro veterinario e da Alian^a Internacional. 
Assim, o artigo 1° deste Decreto determina que os obciais e as pranas dos servi^os veterina- 
rios militares, deveriam usar, quando em serviro, no bra^o esquerdo, um bra^al de flanela 
branca tendo no centro uma estrela vermelha de seis centimetros. 

O artigo 3° do Decreto n.° 2:391, de 15 de maio de 1916 (citado em Secretaria da Guerra, 
1916f)^^ determina que o pessoal da Alian^a seja equiparado ao do servi^o veterinario mi- 
liciano, ficando sujeito as mesmas leis e regulamentos militares. 


1914-1919: Servi 90 Veterinario Militar durante a Grande Guerra. 

Africa 

No inicio da Grande Guerra o governo portugues manteve-se neutral, no entanto em 18 de 
agosto de 1914 entendeu ser necessario guarnecer e refor^ar o territdrio do sul de Ango¬ 
la, em virtude da amea^a Alema que ai se fazia sentir (Arrifes, 2004). Eoram organizados 
dois destacamentos mistos para Angola, o primeiro foi enviado a 11 de setembro de 1914, 
comandado pelo Tenente-Coronel Alves Ro^adas, e o segundo foi comandado pelo Gene¬ 
ral Pereira de E^a que chegou a dia 23 de mar^o de 1915. (Rita, 2013) Estes destacamentos 
eram constituidos por varias subunidades de diferentes armas e servi^os, tendo sido re- 
for^adas ao longo das campanhas (Afonso, sd). O primeiro destacamento comandado pelo 


Estas instituiqoes, reconhecidas pelas autoridades militares, organizaram, nos campos de batalha, hospitals, en- 
fermarias e postos de socorro para o tratamento de animais feridos. 

Publicado na Ordeni do Exercito n.° 7,1^ serie. 

Publicado na Ordem do Exercito n.° 17,1’* serie. 

Publicado na Ordeni do Exercito n.° 16,1^ serie. 


88 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



TCor Aves Ro^adas era um contingente quase simbolico e nao se encontrava preparado 
para combater contra formas europeias. (Arrifes, 2004) 

Durante o periodo da Grande guerra foram mobilizados para Angola um total de 2321 
sollpedes (cavalos e muares), 335 na primeira expedi^ao, 1708 na segunda expedi^ao e 278 
nas rendi^oes efetuadas entre 1917 e 1918 (Oliveira, 1994). Para alem deste efetivo de soll¬ 
pedes mobilizado, tambem existiam em Angola um mimero signiftcativo de bovinos, que 
eram utilizados como animals de tra^ao e como efetivo dos rebanhos e parques de reses, e 
alguns camelos (Ro^adas, 1919) (E^a, 1921). 

O apoio Veterinario estava dividido em servi^os veterinarios de P linha ou T linha, con- 
soante fosse efetuado na linha da frente ou na retaguarda. (Lemos, 1915) 


Service Veterinario de 1“ linha 

Este servi^o era constituldo pelos Obciais Veterinarios e pelos ferradores das unidades, 
utilizando o material veterinario e siderotecnico das proprias unidades. O servi^o era dis- 
tribuldo entre eles, sendo que uns bcavam responsaveis pelos sollpedes da coluna de com- 
bate, outros pelos do trem de combate e outros pelos do comboio. Cada Obcial Veterinario 
disponha de uma bolsa Machifall, uma ambulancia veterinaria M/902 completa e duas ou 
mais caixas de medicamentos de reserva. A cada ferrador estava distribuldo um par de 
bolsas de ferrador, existindo nas unidades montadas uma oftcina de ferrador completa 
(Lemos, 1915). 

Os Obciais Veterinarios eram responsaveis por todos os assuntos relacionados com o es- 
tado de saiide dos animais, o arra^oamento e abeberamento dos sollpedes e tambem de 
aconselhamento acerca do seu trabalho. Quando uma unidade nao disponha de um Obcial 
Medico Veterinario para garantir esse apoio veterinario, era-lhe atribuldo um pelo chefe 
do Servi^o Veterinario, ou entao era executado pelo Obcial Veterinario da Unidade mais 
proxima (Lemos, 1915). 

Sempre que no local nao era posslvel efetuar os tratamentos indispensaveis, os sollpedes 
eram evacuados para os servi^os de 2“ linha. No caso de estarem inutilizados para o servi^o 
ou de terem contraldo algum tipo de doen^a infecto contagiosa, eram abatidos no local. 
Os sollpedes que nao podiam acompanhar as marchas da unidade eram evacuados para os 
servi^os de 2“ linha onde eram tratados, sendo para isso reunidos em locals determinados 
pelos servi^os para posteriormente serem organizadas as colunas de evacua^ao (Lemos, 
1915). 

Este apoio era executado durante os estacionamentos, nas marchas, nos combates e apds 
os combates, em que os Obciais Veterinarios, auxiliados pelos ferradores, passavam revista 
aos sollpedes, garantindo que estes estavam em condiqoes de continuar o seu servi^o (Le¬ 
mos, 1915). 

O trabalho dos ferradores quase se resumia a executar os tratamentos prescritos pelos 
Obciais Veterinarios, visto que o servi^o de ferra^ao era muito reduzido devido a natureza 
muito macia e mole do terreno (Lemos, 1915). 

Lunejoes dos Obciais Veterinarios nas marchas e estacionamentos: (Lemos, 1915) 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


89 


a. Dirigir o service clmico e de ferra^ao dos solipedes das suas unidades e 
das que estejam a seu cargo, por forma a garantir o seu bom estado de saiide. 

b. Propor ao Comandante da Unidade medidas acerca da alimenta^ao do tra- 
balho dos solipedes a seu eargo, para garantir a sua operaeionalidade. 

c. Propor ao comandante da unidade a ado^ao de medidas problaticas e de 
desinfe^ao em casos de doen^as infeeto-contagiosas, de carater enzodtieo ou 
epizootieo, fazendo a devida eomunica^ao ao ehefe do Servi^o Veterinario. 

d. Propor ao chefe do Servi^o Veterinario, o destino a dar aos solipedes da 
sua ou Unidade ou de outras a seu eargo, que por motivos de doen^a, de lesao 
grave ou de eura ineerta ou muito demorada, nao possam acompanhar ou ser 
tratados junto das suas unidades de origem. 

e. Propor quer ao Comando da sua Unidade, quer ao Chefe do Servi^o, todas 
as medidas que aehe convenientes para assegurar o bom funeionamento do 
seu servi^o. 

f. Requisitar em tempo oportuno ao Chefe do Servi^o todo o material veteri¬ 
nario e siderotecnieo necessario para se reabastecerem, por forma a nao ser 
necessario realizar novas requisi^oes a curto prazo e para que nao haja faltas. 

g. Enviar ao Chefe do Serviqo relatorios sucintos de fatos dignos de referen- 
cia, ocorridos nas marchas e nos estacionamentos, bem como os mapas no- 
sologicos mensais. 

Funqoes dos Oficiais Veterinarios durante o Combate: (Lemos, 1915) 

a. Estabeleeer junto do trem de combate da sua unidade ou noutro local ade- 
quado e o mais proximo possivel da linha de logo, um posto de socorros onde 
fosse possivel efetuar todos os tratamentos neeessarios aos solipedes da sua 
Unidade, bem eomo aos de outras Unidades que ali estivessem apresentados. 

b. Enviar um relatdrio sueinto ao Chefe de Servi^o Veterinario, no qual eons- 
te todos os fatos relevantes, a forma como decorreu o servi^o e a bora a que 
foi montado e eneerrado o posto de socorros. 


Eunices dos Obciais Veterinarios depois do eombate: (Lemos, 1915) 

a. Eazer a explora^ao do campo de batalha, reeolhendo todos os feridos que 
sejam posslveis de tratar e mandar abater todo os outros, que deverao ser 
devidamente enterrados com os mortos. 

b. Elaborar um relatdrio que devera ser enviado ao chefe do servi^o veterina¬ 
rio, no qual eonste o mimero de animais mortos, feridos e abatidos. 

c. Dirigir o tratamento clinico aos e apresentar ao Chefe do Serviqo todas as 
propostas sobre o reabastecimento de material veterinario, sobre a classift- 
eaejao e destino dos doentes, feridos e extenuados. 

d. Dirigir os tratamentos e a mareha das colunas de evaeuaejao dos solipedes 
doentes, feridos e extenuados de 1“ linha para 2“ linha. 


90 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Service Veterinario de 2“ linha 


O service veterinario de T linha era prestado pelo Service Veterinario das Etapas e era 
constituido por estabelecimentos veterinarios de hospitaliza^ao e evacua^ao de solipedes e 
pelos depositos de material sanitario veterinario e siderotecnico (Costa, Relatdrio do Chefe 
dos Servi^os Veterinarios de Etapes, 1915). 

Este servi^o servia para dar apoio aos destacamentos em opera^oes, nos sens desloca- 
mentos, devido a grande extensao do territorio, e apos as opera^oes. Era constituido por 
Oheiais Medicos Veterinarios, ferradores e soldados tratadores de solipedes. Os locais de 
hospitaliza^ao e evacua^ao de solipedes encontravam-se divididos em Enfermarias Vete- 
rinarias e Postos de socorros veterinarios, consoante a sua capacidade (Costa, Indica^oes 
Tecnicas para o Apoio Veterinario de T linha, 1915). 

O funcionamento deste servi^o era muito semelhante ao funcionamento das enfermarias 
veterinarias em tempo de paz. Os animais doentes, feridos ou extenuados, provenientes 
das unidades de 1“ linha, eram recebidos nestas enfermarias veterinarias para serem tra- 
tados e recuperarem. Eram classificados em tres grandes grupos: curaveis, incuraveis e 
incapazes para todo o servi^o. Aos primeiros eram feitos os tratamentos necessarios e per- 
maneciam nas enfermarias o tempo necessario ate a sua cura. Aos outros, por nao estarem 
aptos para o servi^o, era-lhe dado um destino que nao passaria por trata-los e mante-los 
(Costa, Indicaqoes Tecnicas para o Apoio Veterinario de T linha, 1915). 

Os Oheiais veterinarios eram tambem responsaveis pelo apoio veterinario e inspe^ao sa¬ 
nitaria das rezes existentes (E^a, 1915). 


Fun^oes dos Oheiais Medicos Veterinarios 

Como chefes das enfermarias veterinarias: (Costa, Indica^oes Tecnicas para o Apoio Vete¬ 
rinario de T linha, 1915). 

a. Receber e alojar convenientemente e fazer o respetivo registo dos solipedes 
provenientes das unidades de 1“ linha, da tropa de etapas ou de outras Enfer¬ 
marias Veterinarias. 

b. Eazer o devido isolamento ou sequestro em instala^oes apropriadas de 
animais suspeitos de doen^a contagiosa, enzodtica ou epizootica. 

c. Dirigir todo o servi^o de uma Enfermaria Veterinaria, nomeadamente no 
que diz respeito aos tratamentos medicos, medidas prohlaticas e relaciona- 
das com o regime alimentar dos solipedes. 

d. Reportar ao chefe do Servi^o Veterinario de Etapas o mimero de altas e de 
solipedes prontos para voltar ao servi^o. 

e. Enviar para as Unidades de 1“ linha todos os solipedes que se encontrem 
em condi^oes que sejam requisitados. 

f. Propor destino para os solipedes dados como incapazes para o servi^o e 
para aqueles considerados de “cura incerta”. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


91 


g. Elaborar e enviar ao Chefe de service Veterinario de Etapas, o mapa noso- 
logico da sua enfermaria, assim como qualquer outro relatorio que Ihe seja 
solicitado. 

Como diretores do Deposito de Material Sanitario Veterinario: (Costa, Indiea^oes Teenieas 
para o Apoio Veterinario de 2“ linha, 1915). 

a. Eazer o inventario de todo o material existente no deposito, medieamen- 
tos, material sanitario e material sideroteenico. 

b. Garantir que todas as requisi^oes efetuadas ao deposito sao satisfeitas em 
tempo oportuno. 

c. Eazer as requisi^oes neeessarias para garantir a eapacidade de respostas as 
requisi^oes efetuadas ao deposito. 

d. No easo de ser testa de etapas de estrada enviar uma rela^ao do material 
existente em deposito para regular o abastecimento as unidades de 1“ linha. 

Como Veterinarios responsaveis pelo parque de reses: (Costa, Relatorio do Chefe dos Ser- 
vi^os Veterinarios de Etapes, 1915) 

a. Vigiar o estado sanitario dos animals e efetuar todos os tratamentos elini- 
eos neeessarios. 

b. Aplicar todas as medidas problaticas neeessarias para evitar doen^as in- 
fecto - eontagiosas. 

c. Executar o exame ante e pos mortem aos animals destinados ao eonsumo 

d. Seleeionar quais os animals que devem ser abatidos primeiro de acordo 
eom a sua eapacidade de se deslocarem para junto das unidades de 1“ linha. 

e. Dar indicaqoes teenieas no que diz respeito ao abate dos animals, esquar- 
tejamento e armazenamento. 

Tambem em agosto de 1914 foi decretado o envio de uma expedi^ao a Mozambique coman- 
dada pelo Tenente-Coronel Pedro Erancisco Massano de Amorim. Partiram o Batalhao de 
Infantaria 15, Esquadrao de Cavalaria 10, Bateria de Artilharia de Montanha, Seezoes de 
Engenharia, Saiide e Administrazao Militar (Demony, 1936). 

Pela Bateria de artilharia de montanha marchou o Alferes veterinario do batalhao de te- 
legraftstas de campanha, Adriao Jose Afonso de Castro (Portuguesa, 1914) e pelo Regimento 
de cavalaria n.lO o Tenente Veterinario em servizo na Guarda Nacional Republicana (GNR), 
Macario Evangelista de Sousa (Portuguesa, 1914). Qualquer um deles viria tambem a servir 
no teatro de guerra da Elandres. 

A azao do Serviqo Veterinario a data era em grande parte de problaxia, diagndstico e 
tratamento de cavalos (Mendes, 2003), estando a area de saiide piiblica e epidemiologia no 
seu inicio. 

A 2“ - Expedizao a provincia de Mozambique decorreu no ano de 1915, por decreto de 
11 de Setembro. O micleo desta expedizao foi o 3“ Batalhao de infantaria 21, de guarnizao 
em Penamacor, mobilizado “como as restantes forzas de forma improvisada e por isso 
sem coesao nem apurada instruzao” (Martins, 1938). Gonstituiam-na tambem o 1“ Ba- 


92 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


talhao de Artilharia de Montanha, 1 Esquadra de cavalaria, 1 bateria de metralhadoras e 
services de Engenharia, Saiide e Administrativos, num total de 41 obciais e 1502 pranas 
(Portuguesa, 1915a). 

Pelo Regimento de artilharia de montanha 5“ Bataria, marchou o Capitao Veterinario do 
regimento de artilharia n.°5, Joao Coelho de Castro Villas Boas Junior (Portuguesa, 1915b) 
e pelo Regimento de Cavalaria N.3, 4.° esquadrao, o Tenente Veterinario do regimento de 
Cavalaria n.°5, Bernardino da Cruz. 

Atraves do testemunho do Capitao Veterinario Joao Coelho de Castro Villas Boas Junior 
podemos perceber que um dos principals problemas com que se deparou a for^a foi o apa- 
recimento de uma doen^a que afetava os solipedes conhecida por “peste equina Africana”: 
“Nao conhecendo nem sabendo ate ao presente de medica^ao dominante da peste do ca- 
vallo (horse sickness), mas sabendo que a aplica^ao terapeutica, dos sob tanto requi- 
sitados, tern dado resultados satisfatorios em Trypanossomiases, impondo-se a requisi^ao 
para a enfermaria, visto que na HORSE SICKNESS se trata de uma doen^a cujo agente e 
infectante de sangue”. 

Numa nota de 28 de Maio de 1916 o Capitao Veterinario relata medidas de conten^ao de 
epizootias, nomeadamente o isolamento e afastamento do loco epidemico para fazer face 
ao surto de peste equina africana: “Atendendo que nesta localidade o gado tern sido de- 
veras atingido pela infe^ao de HORSE SICKNESS que desima (sic) as nossas montadas e 
sendo da opiniao de que afastando-o daqui enquanto nao entra em a^ao, estara mais fora 
do alcance de qualquer contagio, propondo a vossa excelencia que todo o gado montado va 
para (...) localidade que observe! estar em boas condi^oes de salubridade. O gado pode alii, 
e dever, o devido trato e assistencia clmica”. 

Pelo mimero elevado de relatorios de obito arquivados, 28 no total (AHM, 1916), emi- 
tidos pelo Capitao Veterinario Villas Boas, entre os meses de Maio e Agosto de 1916, e evi- 
dente que surtos de peste equina e outras doen^as endemicas frequentemente assolavam 
os contingentes portugueses. Destes 28 relatorios doze reportavam como causa do obito a 
tripanossomfase e dezassete a peste equina africana. 

A entrada oftcial de Portugal na guerra a 9 de Mar^o de 1916, colocou o pals sob pressao, o 
que levou a que Lisboa, a 28 de Maio 1916, enviasse a 3“ - Expedi^ao, a mais forte ate entao, 
para a provfncia de Mozambique comandada pelo General Eerreira Gil. O servizo de vete- 
rinaria foi guarnecido com dois veterinarios que embarcaram nos navios Amarante e Ma- 
chico (Cor. E. A. Azambuja Martins, 1916a). Um deles o Tenente Veterinario do regimento 
de cavalaria n.8 Erancisco Gervasio Elores (Portuguesa, 1916). 

Quer a higiene do soldado, quer o maneio problatico e sanitario dos eqmdeos, eram deftci- 
tarios, havendo varios relatos de afetazao do gado pelas doenzas classicas recomendado me- 
Ihor maneio para incrementar a qualidade e quantidade da alimentazao (Villas-Boas, 1916). 

Apesar da falta de comunicazao, da falta de recursos e da falta de condizoes para o trans- 
porte de cavalos e burros, acrescentando a comum desorganizazao dos militares portu¬ 
gueses, os veterinarios militares mantinham a sua competencia para minimizar as per das 
por epizootias, como o Mormo, reportados pelo Coronel Azambuja Martins: “No dia 2 de 
Agosto chegaram no vapor “Inhambane” duzentos cavalos e 300 muares remontados na 
Africa do Sul. Mas em que lamentaveis condizoes desembarcaram! A Comissao encarrega- 
da do seu desembarque dizia no relatorio que apresentou: - Nao trazerem os solipedes nem 
cabezadas nem qualquer prisao tendo sido desembarcados em manada, parte deles fugindo 


I. A PROBLEMATIZAZAO DA PRIMEIRA GRANDE GUERRA 


93 


para o mato por nao serem animais domesticados, havendo enorme dificuldade em os apa- 
nhar e perdendo-se trinta e seis cabe^as de gado. Depois manifestou-se nos solipedes uma 
molestia suspeita, que os veterinarios classificaram de mormo, pelo que, alguns tiveram 
de ser abatidos e os restantes isolados, apesar da debeieneia de recursos para efetuar esta 
precau^ao.” (Cor. E. A. Azambuja Martins, 1916c, Cor. E. A. Azambuja Martins, 1916d). 

A deftciente prepara^ao e os estudos insuftcientes quanto ao material a utilizar e qual 
o melhor meio de transporte para este tipo de expedi^oes, bca patente em mais uma 
declara^ao do Coronel Azambuja Martins quando reporta que “O material de montanha 
nas coldnias e demasiado pesado para o transporte a dorso porque as muares tern menos 
capacidades de resistencia e o solo arenoso as fatiga muitissimo” (Cor. E. A. Azambuja 
Martins, 1916c). 


Service de Veterinaria do Exercito no Corpo Expedicionario Portugues 

Apos a declara^ao obcial de Guerra feita pela Alemanha em Mar^o de 1916, sao organizados 
os exercicios de prepara^ao do CEP em Tancos. O Servi^o Veterinario Militar e mandado 
apresentar-se em Tancos no dia 5 de Maio de 1916. O relatorio elaborado pelo chefe dos 
servi^os veterinarios. Major Augusto Barradas, e elucidativo das diftculdades com que o 
servi^o veterinario se debatia a altura da prepara^ao do CEP, tanto em termos de material 
como de pessoal, come^ando por dizer logo no inicio “senti-me embara^ado, embora es- 
tivesse animado dos melhores desejos, pondo ao lado do servi^o de que era encarregado, 
toda a minha boa vontade e todo o meu esfor^o.” Nesta altura foi constatado pelo Chefe 
do Servi^o Veterinario que nada estava pronto, a maior parte do material de campanha 
tinha sido enviado para Africa e nada tinha voltado! Nao sabia quern eram os veterinarios 
nomeados para fazer parte da Divisao de instru^ao, tendo chegado a Tancos “sem um lini- 
co instrumento ciriirgico, sem um linico medicamento”! Tambem o servi^o siderotecni- 
co, que dependia do Servi^o Veterinario, se apresentou completamente desorganizado. As 
unidades apresentavam-se sem “ferragens” e o pessoal siderotecnico sem equipamento. 
Neste periodo foi reunido um efetivo de 4553 solipedes tendo sido observado o seguinte 
movimento clinico: 

-1.475 baixas, dos quais 1115 foram curados, 64 morreram e 7 foram abatidos. 289 solipe¬ 
des ainda se encontravam de baixa no acto da desconcentra^ao. 

O primeiro contingente do CEP parte para Eran^a a 30 de Janeiro de 1917. Sao mobilizados 
7.783 solipedes, enviados de Portugal via maritima. A estes juntam-se mais 1476 solipedes 
(188 cavalos e 1284 muares) fornecidos pelos depdsitos de remonta Ingleses. 

Ate ao armistlcio sao numerosos os elementos do Servi^o de Veterinaria que passam pelo 
teatro de guerra na Elandres. De 1917 a 1919 sao destacados 49 Obciais Veterinarios, 33 En- 
fermeiros Hlpicos, 25 Sargentos Eerradores, 115 Primeiros Cabos Eerradores, 162 Soldados 
Eerradores.^^ 


Jornal do Exercito n"656 - Mar^o de 2016 
Arquivo Histdrico Militar, 1/35/220 


94 


A PRIMEIRA GUERRA MONDIAL. NA BATALHA DE LA LYS 



Organiza 9 ao do apoio Veterinario na Frente Ocidental 


A organiza^ao do apoio veterinario e efetuada essencialmente em 3 niveis ou esealoes. 

O apoio mais proximo e realizado pelos element os do Servi^o de Veterinaria que estao colo- 
cados junto das varias unidades em que o mimero de solipedes existentes justifica este apoio. 

Tabela 1. Pessoal tecnico e auxiliar do Servigo Veterinario nas diferentes unidades 


Quartel General 

1 Major Veterinario; 1 Capitao Veterinario; 1 1° Sarg. Enf. Hfpico; 2 l^s 
Gabos-Ferradores e 1 Soldado-Ferrador 

1% 2"^ e 3* Companhias de Sapadores 
Mineiros 

11® Cabo-Ferrador e 1 Soldado-Ferrador/ Companhia 

4^ Companhia Sapadores Mineiros 

2 l°s Gabos-Ferradores e 1 Soldado-Ferrador 

1° Grupo de Bataria de Artilharia 

1 Tenente Veterinario; 1 1“ Sarg. Enf. Hipico; 1 2° Sarg, Ferrador; 3 l“s 
Gabos-Ferradores; 7 Soldados-Ferradores 

2° Grupo de Bataria de Artilharia 

1 Tenente Veterinario; 1 Alferes Veterinario; 11° Sarg. Enf. Hipico; 1 2° 
Sarg, Ferrador; 5 l°s Gabos-Ferradores; 7 Soldados-Ferradores 

3° Grupo de Bataria de Artilharia 

1 Tenente Veterinario; 1 Alferes Veterinario; 11° Sarg. Enf. Hipico; 1 2° 
Sarg, Ferrador; 3 l°s Gabos-Ferradores; 9 Soldados-Ferradores 

1° Grupo de Metralhas 

11° Sarg. Enf. Hfpico; 1 2° Sarg, Ferrador; 2 l^s Gabos-Ferradores; 1 Sol¬ 
dado-Ferrador 

2° Grupo de Metralhas 

11° Sarg. Enf. Hfpico; 12° Sarg, Ferrador e 2 Soldados-Ferradores 

Brigada de Infantaria 

1 Tenente Veterinario; 1 Alferes Veterinario; 11° Sarg. Enf. Hipico; 3 l^s 
Gabos-Ferradores; 5 Soldados-Ferradores 

2® Brigada de Infantaria 

2 Tenentes Veterinarios; 11® Sarg. Enf. Hipico; 4 l°s Cabos-Ferradores; 9 
Soldados - Ferradores 

3* Brigada de Infantaria 

1 Tenente Veterinario; 11“ Sarg. Enf. Hfpico; 11° Cabo-Ferrador; 6 Sol- 
dados - Ferradores 

4^ Brigada de Infantaria 

1 Tenente Veterinario; 1 Alferes Veterinario; 11® Sarg. Enf. Hipico; 1 1° 
Cabo-Ferrador; 9 Soldados-Ferradores 

Ambulancias 1 e 5 

11" Cabo-Ferrador 

Ambulancia 3 

1 Soldado-Ferrador 

Coluna de Transporte de Feridos 1 

1 1° Cabo-Ferrador e 1 Soldado-Ferrador 

Trem Divisionario 

1 Tenente Veterinario; 11*^ Sarg. Enf. Hipico; 2 2^ Sarg, Ferrador; 11° Ca¬ 
bo-Ferrador; 3 Soldados-Ferradores 

4° Grupo de Bataria de Artilharia 

1 Tenente Veterinario; 1 Alferes Veterinario; 11° Sarg. Enf. Hipico; 1 2° 
Sarg, Ferrador; 10 l°s Cabos-Ferradores; 5 Soldados-Ferradores 


(Fonte: Arquivo Histdrico Militar, 1/35/210) 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


95 




















Os Grupos de Bataria de Artilharia usavam um grande niimero de solipedes devido a ne- 
cessidade da for^a de tra^ao para a desloca^ao das pesadas pe^as de artilharia. Esta situa^ao 
justihca o elevado quantitative de pessoal do Service de Veterinaria neste apoio de proxi- 
midade a estas unidades. 

Ao nivel de apoio intermedio, ou de 2° escalao, foram organizadas 2 secedes moveis (Sec- 
(;ao Movel Veterinaria n“l e Sec^ao Movel Veterinaria n‘’2) que prestavam apoio as unidades 
do CEP. Estas secedes recebiam os animais portadores de lesdes mais graves e que nao po- 
diam acompanhar as unidades. 


Tabela 2. Sec^ao movel veterinaria (Quadro n° 26B) 


1 Chefe (Capitao Veterinario) + 1 impedido + 1 animal de sela 
11° Sargento Enfermeiro Hipico + 1 animal de sela 
11° Cabo Ferrador + 1 animal de sela 

1 Soldado Ferrador + animal de sela 

2 Cabos de Cavalaria + 2 animais de sela 

20 Soldados de Cavalaria + 20 animais de sela 
2 Carros de Esquadrao de 4 rodas + 4 Soldados + 8 animais de tiro 

1 Carro para transporte de animais feridos e doentes de 2 rodas + 2 soldados + 2 animais tiro 
TOTAL: 34 Flomens; 37 Animais; 3 Viaturas 


(Fonte: Arquivo Histdrico Militar, 1/35/810) 


As Secqoes Moveis Veterinarias n° 1 e n“ 2 eram, por sua vez, apoiadas pelos Elospitais Ve- 
terinarios Ingleses, para onde eram evacuados os solipedes em caso dos cuidados veteri- 
narios necessarios ultrapassarem as suas capacidades. Os Hospitals Veterinarios Ingleses 
funcionavam entao como um 3“ escalao de apoio que, uma vez recuperados os solipedes, 
os devolviam ao Depdsito de Remonta Portugues em Calais. 

O apoio logistico as unidades do CEP e as Secqoes Moveis Veterinarias, ao nivel dos me¬ 
dicament os, material veterinario e siderotecnico, era prestado pelo Depdsito do Serviqo 
Veterinario da base, situado em Calais. 

Inicialmente o Depdsito do Serviqo Veterinario da base apresentava o seguinte quadro 
organico (n°43 A): 


36 Arquivo Histdrico Militar, 1/35/209 


96 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 




Tabela 3. Deposito do Servigo Veterinario da base (n° 43 A) 


I Dire^ao 

1 - Chefe (Capitao Veterinario do quadro permanente) 

I - adjunto (Subalterno Veterinario) 

1-2° Sargento de Cavalaria 

II Enfermaria de SoUpedes 

1 - Subalterno Veterinario Miliciano 
1 - Sargento Enfermeiro Hipico 
6 - Ferradores 
4 - 1°® Cabos 
1 - Clarim 

25 - Soldados de arma montada 

III Oficina Siderotecnica 

1 - Sargento Ferrador 
6 - 1°® Cabos Ferradores 

6 - Soldados Ferradores 

12 - Malhadores (soldados de qualquer arma) 

IV Deposito de Material 

1 - Subalterno de Veterinaria Miliciano 
1 - Praticante de Farmacia 
1 - Ferrador 

1 - Soldado servente 

1-1° Sargento Enfermeiro Hipico 

V Reserva de Pessoal 

2 - Subalternos Veterinarios Milicianos 
2 - 1°® Sargentos Enfermeiros Hipicos 

7 - Ferradores 

1-2° Sargento Ferrador 

TOTAL: 83 (6 O&amp;ciais + 77 Praqas) 


(Fonte: Arquivo Histdrico Militar, 1/35/770) 


A 19 de Junho de 1917 foi mandado suprimir a enfermaria veterinaria do Deposito do 
Serviqo Veterinario da base de operaqoes do CEP. A 11 de Agosto de 1917 o Deposito do 
Serviqo Veterinario passa a estar adstrito ao Deposito de Remonta. A18 de Agosto de 1917 
e extinta a oficina siderotecnica do Deposito do Serviqo de Veterinaria que passa para o 
Deposito de Remonta. O Deposito do Serviqo de Veterinaria passa a apresentar um novo 
quadro organico. 


I. A PROBLEMATIZAqAO DA PRIMEIRA GRANDE GUERRA 


97 



Tabela 4: Deposito do Servigo de Veterinaria (n° 43 B) 


I Dire^ao 

1 - Chefe (Capitao Veterinario do quadro permanente) 

1 - Chefe (Capitao Veterinario do Quadro Permanente) 

1 - Adjunto (Subalterno Veterinario do Quadro Permanente) 

I - Amanuense (Sargento de Cavalaria) 

II Deposito de Material 

1 - Kiel do Deposito (Cabo de Cavalaria) 

1 - Praticante de Farmacia (l“ Cabo) 

4 - Soldados serventes 

III Reservas 

4 - Subalternos (Veterinarios) 

2 - Sargentos Enfermeiros Hipicos 
2 - Sargentos Ferradores 

6 - Cabos Ferradores 
14 - Soldados Ferradores 

TOTAL: 37 (6 Oficiais + 31 Pragas) 


(Fonte: Arquivo Histdrico Militar, 1/35/770) 


O dispositive de tropas do SV do CEP nao atingiu a dimensao em efetivos e em meios dos 
dispositivos estabelecidos pelos restantes paises Aliados. 

Segundo o relatorio do Chefe do SV do CEP, Tenente Coronel Augusto Barradas, entre 
junho e dezembro de 1917 o niimero de solipedes doentes foi de 5.230, tendo no decurso 
deste ano oeorrido um surto de mormo, zoonose muito infeciosa e grave, que implicava o 
isolamento e abate do animal afetado, mas que tinha diagnostico relativamente facil e mui¬ 
to sensivel (metodo intradermico da maleina), tendo a epidemia sido considerada extinta 
em 3 meses, apos a maleinizaqao de todo o efetivo e o abate de 110 cavalos e 54 muares (cada 
animal foi testado 3 vezes, tendo sido realizadas 18.757 maleinizaqoes, o que pressupoe um 
efetivo de cerca de 6.000 animals) (Junior, 1958). Durante este periodo estabeleceram-se 
zonas de quarentena em Calais (onde estava o Hospital Veterinario Australiano) e Thiem- 
brane, estando o SV do CEP, neste periodo, impedido de evaeuar animals para os Hospitals 
Britanicos. Para alem do mormo, as doenqas infeciosas incluiam a linfangite epizootiea e 
a estomatite vesicular. As entidades morbidas mais frequentes eram as colicas, a sarna, os 
ferimentos provocados por projeteis, as infeqoes provocadas por corpo perfurante, as as- 
sentaduras, a exaustao por exposiqao ao frio e a fome, e as lesoes provocadas pela exposi^ao 
a agentes quimicos de guerra. 


98 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



Referencias 


1. Legisla^ao 

Secretaria da Guerra. (l91l). Decreto do Governo Provisorio da Republica, de 25l5ll911. 
Lisboa: Diario do Governo. 

Secretaria da Guerra. (l916a). Decreto n.° 2:515-H de 1510711916 da 6“ Repartigao - 2“ Di- 
regdo Geral. Lisboa: Diario do Governo. 

Secretaria da Guerra. (l916b). Decreto n.° 2345 de 20/411916. Lisboa: Diario do Governo. 
Secretaria da Guerra. (l916d). Decreto n.° 2:363, de 2 de maio de 1916. Lisboa: Diario do 
Governo. 

Secretaria da Guerra. (l916e). Decreto n.° 2:571, de 15 de agosto de 1916. Lisboa: Diario do 
Governo. 

Secretaria da Guerra. (1917). Decreto n” 3093, de 18/4/1917, da 6“ Repartigao - 2^ Diregao 
Geral. Lisboa: Diario do Governo. 

Secretaria Geral do Ministerio de Instru9ao Publica. (1916). Decreto n° 2:384, de 11/5/1916. 
Lisboa: Diario do Governo. 


2. Arquivo Historico Militar 

Arquivo Historico Militar. (s.d.). 1/35/209. 

Arquivo Historico Militar. (s.d.). 1/35/210. 

Arquivo Historico Militar. (s.d.). 1/35/211. 

Arquivo Historico Militar. (s.d.). 1/35/212. 

Arquivo Historico Militar. (s.d.). 1/35/220. 

Arquivo Historico Militar. (s.d.). 1/35/770. 

Arquivo Historico Militar. (s.d.). 1/35/810. 

Arquivo Historico Militar. (1916-1918). Ordens do Exercito. 


3. Bibliografia 

Afonso, A. (sd). Portugal Grande Guerra. Lisboa: Diario de Noticias. 

Army Medical Services Museum, (s.d.). History of the Royal Army Veterinary Gorps. Ob- 
tido em fevereiro de 2013, de http://wiviv.ams-museum.org.uk/museum/ravc-history/ 
Arrifes, M. F. (2004). A Primeira Grande Gueera na Africa Portuguesa. Lisboa: Edi^oes 
Cosmos. 

Coelho, S. V. (2001). O Exercito Portugues na I Guerra Mundial 1914-1918. Lisboa: Plus 
Ultra, Lda. 

Costa, A. R. (1915). Indicagoes Tecnicas para o Apoio Veterindrio de 2“ linha. 

Costa, A. R. (1915). Relatdrio do Ghefe dos Servigos Veterindrios de Etapes. 

E9a, G. P. (1921). Gampanha do sul de Angola em 1915. Lisboa: Inprensa Nacional. 

Exercito Portugufe. (2013a). Site Oficial do Exercito. Obtido em fevereiro de 2013, de 
http://www.exercito.pt/sites/REl/Actividades/Paginas/2420.aspx 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


99 


Exercito Portugues. (2013b). Site Oficial do Exercito. Obtido em fevereiro de 2013, de 

http://wiviv.exercito.pt/sites/RTransp/Historial/Paginas/default.aspx 

Junior, J. (1958). A Influencia das Institui^oes Militares na Evolu^ao da Veterinaria Portu- 

guesa. Revista de Ciencias Veterindrias, 53°, 145-220. 

Krenzelok, G. (s.d.). The History of US Veterinary Services, AEF, During WWl. Obtido em 
fevereiro de 2013, de http://freepages.genealogy.rootsweb.ancestry.coni/~gregkrenze- 
lok/veterinary %20corp% 20in % 20wwl / usvetcorpshistorywwl .html 
Lemos, A. A. (1915). Relatdrio do Chefe do Servigo Veterindrio. 

Ministerio da Defesa Nacional. (1993). Os Potugueses naPlandres 1917-1918 (excertoshis- 
toricos). Lisboa: Direc^ao do Servigo Historico-Militar. 

Oliveira, A. d. (1994). istdria do Exercito Portugues (1910 -1945). Lisboa: Estado Maior do 
Exereito. 

Pereira Junior, J. M. (1958). A influencia das instituigoes militares na evolugdo da vete- 
rindria portuguesa, separata da revista de ciencias veterindrias vol III Ease 365-366. 
Rita, E. (2013). Na Sombra do Expediciondrio - A vidaem Combate de Soldados Portugue- 
ses na Primeira Guerra Mundial. Porto: Fronteira do Caos Editores. 

Ro9adas, J. (1919). Operagoes no Sul de Angola em 1914. Lisboa: Inprensa Nacional. 


100 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Ethical challenges posed hy faceless wars. 
In memory of a combatant in La Lys^^ 

Teresa Toldy 

FCHS / University Fernando Pessoa 


In memory of my grandfather, Lms Leal, 
a survivor of the Battle of La Lys. 

Abstract: According to Robert O’Connell (1989), the First World War, by introducing weap¬ 
ons and ways of using them, for example, air bombardments or chemical weapons, which 
allowed massive destruction, contributed decisively to turn the alive and dead soldiers 
into statistics, and consequently, into “faceless human beings”. The increasing sophisti¬ 
cation of the war, throughout the twentieth century and at the beginning of this century, 
as a result of the use of weapons of “death from a distance” and of technologies used by 
military trained in simulators poses relevant ethical issues, such as the strengthening of 
the separation between the act of using a weapon and its consequences on the real victims 
(considering the current issue of remote drone activation). The separation between the 
weapon that one uses and its consequences seems to make death “virtual”, thus contrib¬ 
uting to the lack of responsibility and to blur the decisive question of the confrontation 
with the real victims’ face. This paper intends to address some of the ethical issues raised 
by these “death from a distance” mechanisms. For this purpose, based on the thought of 
Emmanuel Levinas and Judith Butler, a reflection will be developed around the relevance 
of the “face of the other” as an ethical appeal and challenge in wartime circumstances. This 
reflection also intends to be a simple tribute to my grandfather, Luis Leal, combatant and 
survivor of the Battle of La Lys. 

Keywords: Weapons, Ethics, Technology, 1 World War. 

Resumo: Segundo Robert O’Connell (1989), a Primeira Guerra Mundial, ao introduzir ar- 
mamento e formas de utiliza^ao do mesmo como sejam, por exemplo, os bombardeamentos 
aereos ou as armas quimicas, que permitiam a destrui^ao massiva, contribuiu decisiva- 
mente para transformar os soldados vivos e mortos em estatisticas, e, consequentemente, 
em “seres humanos sem rosto”. A sohstica^ao crescente da guerra, ao longo do seculo XX 
e no inicio deste seculo, em consequencia do recurso a armamento de “morte a distancia” 
e de tecnologias utilizadas por militares treinados em simuladores coloca questoes eticas 
relevantes, sendo uma delas a do reforqo da separa^ao entre o ato de acionar uma arma e as 
consequencias sobre as vitimas concretas (pense-se no recurso atual a ativa^ao de drones 
a distancia). A separa^ao entre a arma que se aciona e as consequencias desse acionamento 


I am very grateful to Francisco Queiroga for sharing bibliography and knowledge that was decisive to enrich 
this text. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


101 



parece “tornar virtual” a morte, contribuindo, assim, para a desresponsabiliza^ao e para o 
esbatimento da questao decisiva do enfrentamento com as vltimas concretas, com rosto. A 
comunica^ao que aqui se propoe pretende debru^ar-se sobre algumas das questoes eticas 
levantadas por estes mecanismos de “morte a distancia”. Para tal, baseando-se no pensa- 
mento de Emmanuel Levinas e de Judith Butler, desenvolver-se-a uma reflexao em torno 
da relevancia do “rosto do outro” como apelo e desabo etico em circunstancias de guerra. 
Esta reflexao pretende tambem constituir uma singela homenagem ao meu avo, Luis Leal, 
combatente e sobrevivente da batalha de La Lys. 

Palavras-Chave: Armamento, Etica, Tecnologia, 1 Guerra Mundial. 


102 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Introduction 


According to O’Connell (1989), the First World War, by introducing new kinds of weap¬ 
ons and procedures (for example, air bombardments or chemical weapons which enabled 
massive destruction) contributed decisively to turn both survivor and dead soldiers into 
statistics. O’Connell gives Verdun as a relevant example of the massibcation of war origi¬ 
nated by the Great World War. But the same could be said about La Lys: 

Combatants were gassed, torpedoed, bombarded by invisible artillery, or 
mowed down randomly by puny-looking machine guns; there was hardly a 
heroic death to be had. (...) For the extreme lethality of the weaponry drove 
participants underground, huddled in trenches; so it was rare that either 
side caught a glimpse of a recognizable adversary. Life in the trenches re¬ 
duced soldiers from warriors to statistics, casualty hgures, so that the west¬ 
ern quintessential action, Verdun, was fought not to gain a superior position 
or to drive the enemy front the held but simply to kill (O’Connell 1989: 243). 

The massibcation of wars throughout the 20*'’ century (the Great World War and the Sec¬ 
ond World War took millions and millions of military and civilian lives) turned Europe into 
what O’Connell calls “an automated corpse factory” (1989: 242) and Bartov considers an 
inglorious situation, since the “human degradation and extermination” (Bartov 2000:12) 
of the Great World War “ensured that our century’s belds of glory would be sown with the 
corpses of innocent victims and the distorted fragments of shattered ideals” (idem). Ac¬ 
cording to him, this war become the moment in which “innocence was forever shattered” 
(Bartov 2000: 13), since the western front “has come to epitomize the notion of war as a 
vast arena of victimhood” (idem). Bartov considers the monuments and commemoration 
of the “Unknown Soldiers” as an attempt to glorify the fallen soldiers in the context of a 
narrative that tried to give meaning to what seemed to be meaningless: 

The bgure of the unknown soldier thus made possible a shift from the inflat¬ 
ed and largely discredited rhetoric of the abstract nation to the individual, 
yet presented the individual as a soldier who by debnition had no specif¬ 
ic traits and features, and who consequently embodied the nation after all. 
(Bartov 2000:16) 

However, this “embodying” of a nation erases once more the real face of the soldier, since 
the only thing that is known about him is that he was a soldier, a man and a citizen from a 
nation. He remains anonymous, faceless. 

The faceless feature both of soldiers and of civil victims in wars is not something totally 
new. The use of weapons that did not confront its user with his enemy already existed in 
the late Paleolithic (see Queiroga 2008: 2201-2202). Nevertheless, the increased sophisti¬ 
cation of war weapons developed throughout the past and the present centuries add new 
forms of “anonymity” to war. Perhaps the diberence between the “bght at a distance” 
already existing for many centuries ago and the weapons used nowadays with the same 
purpose lies in the level of precision of the late ones. The weapons used to “bght at a dis- 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


103 


tance” (for example, remote controlled drones), as well as technologies used by military 
after training in simulators may reduce the number of casualties in current wars, but do 
not solve the ethical challenge posed by the reinforcement of the separation between the 
activation of a weapon and the consequences of that act for human targets. Therefore, even 
if it seems a paradox, both massive wars and “targeted attacks” put the same ethical prob¬ 
lem: the separation between the use of a weapon and the awareness of its consequences 
seem to turn death into something “virtual”, thus contributing to the lack of responsibili¬ 
ty and to blur the decisive question of the confrontation with the face of the other. 

This text, inspired by the thought of Emmanuel Levinas and of Judith Butler, aims to 
address in a very briefly way some of the ethical issues raised by “faceless wars”, that is, 
wars in which combatants are not directly confronted with the face of the enemies. This 
reflection also intends to be a modest tribute to my grandfather - Lms Leal - a face in the 
middle of the cruelty of a massive war. The narrative of some of his routines and reactions 
is also an attempt to build an approach to the ethical challenge already mentioned: to give 
faces to faceless wars. 


1. A war with a face 

Lor me and my family, the Battle of Lys has a face: the face of my grandfather, Luis Leal, a 
young combatant and survivor of the Battle. He was 23 years old at that time, and he re¬ 
mained a member of the Association of the Great War Combatants until his death in 1977. 

My grandfather was a very peaceful and quiet person. 1 must say that until today 1 feel 
sad for not being old enough to ask him more questions about what happened at La Lys (l 
was not old enough to put this question before he died), but 1 remember some of his almost 
“telegraphic” words about the war and some of his routines. When 1 think of some of them 
now, it seems to me that they had a link with his experience at war. So 1 will try to make 
a connection between his memories and behaviours especially in the hrst part of this text. 

My grandfather kept and cherished: the “Golden Book of Infantry”, published in 1922. 
This book does not only present pictures, it also includes testimonies of Portuguese com¬ 
batants and an appalling list with the names of the Portuguese soldiers who died in the 
Great War. According to Marques (2008: 390), alone in the military operation between the 
21®' of March and the 29"’ of April 1918, “143 German divisions suffered 340 000 casualties; 
58 British and 2 Portuguese divisions suffered 341 000 casualties and 58 Trench divisions 
231 000.” This means that we are talking of 912 000 deaths in 6 weeks. 

According to the already mentioned book of O’Connell (1989), the Lirst World War sets 
the beginning of a new form of warfare: a massive war (lO million men died in combat), an 
“impersonal slaughter” (O’Connell 1989: 243) where because of “the extreme lethality of 
the weaponry” combatants “drove (...) underground, huddled in trenches” (idem). 

Living in trenches, like rats, had a very negative psychological impact upon soldiers. The 
conditions were terrible. Marques (2008) quotes a testimony of Pedro Lreitas, a soldier in 
the Lirst World War, about what he was living in the trenches: 

There is an abundance of putrefying cadavers of soldiers, mules, horses, and 
other macabre fragments. Spread at the surface of the ground they disturb 


104 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


US, they horrify us. The lack of preparation to face that situation is the cause 
for our moral annihilation. (...) The appetite vanishes. (...) Cleaning or carry¬ 
ing something, wandering around we are on the verge of vomiting whenever 
we hnd one more leg or a head while we were using a shovel or a pick. (cit. 
in Marques 2008: 94-95) 

My grandfather never spoke about these terrible conditions. But some of his routines could 
have to do with that. He appreciated a good and paused meal, for instance. He was able 
to sleep standing (maybe because of the experience of having to sleep in muddy trenches 
or because of the fear of falling asleep or of hypothermia while on sentinel duty). And he 
had what the family considered to be a “strange ritual”: he always began the day washing 
his head under running cold water. It is common to hear former soldiers saying that one 
of the worst things at war is not having water to wash yourself, because it makes you feel 
subhuman. 

Though I heard no word from our grandfather describing life in trenches, there was 
something he talked about: the fear of chemical weapons and the panic attacks of some of 
his colleagues when they were forced to use them. The reality of being exposed to gas even 
introduced a new expression in Portuguese language: the word “gaseado” (gassed) a word 
that was used to talk about someone that had been exposed to a toxic gas that caused a 
brain damage. After the war, whenever someone seemed to have a mental disorder, people 
used to say that that person was “gaseado”. 

The triumph of the Allies, as we know, was celebrated in their own countries. My grand¬ 
father was very proud of his participation in the Portuguese troops march on the Victory 
Parade of the Allied Troops in Paris celebrating the official end of the First World War. But 
what really brought silent tears to his eyes was the reference to the “Battle of Lys” on the 
9* of April. He never said a word about what happened. My grandmother knew some de¬ 
tails (for instance, about the chaos Portuguese soldiers tried to survive to, including my 
grandfather), but she only spoke a little about that after his death, when we found the 
military register of his acts of courage. 

Now, hundred years after the battle and forty years after my grandfather’s death, I un¬ 
derstand a little bit the inhumane situation he was put in, and I praise the fact that he had 
no place for hate or bitterness in his heart. 


2. Ethical challenges of a faceless war 

Wars are always inhumane, and the existence of mechanisms of “unaccountability” is 
common as a way to suffocate guilt. We can see this in the breaking of the chain of respon¬ 
sibility in wars or in genocides when, for instance, one of the weapons of a bring squad 
has no bullets (so that individuals participating in shootings keep the hope of having the 
weapon with no bullets), when the person that threw Zyklon B through the chimney of 
gas chambers was not the same person who made the list of people for Nazi concentra¬ 
tion camps, the person that forced people into the death chambers, and the person that 
removed their bodies and turned them into ashes. Breaking the chain of responsibility in 
those situations appeases guilt and transposes it to the “person in charge”. 


I. A PROBLEMATIZACJAO DA PRIMEIRA GRANDE GUERRA 


105 


However, as O’Connell (1989) wrote, the use of weapons hred from the distance (whether 
aircraft, the feared “Katiuska” [also known as “Staline’s organs”], or homhs activated at 
distance) also introduced new forms of inhumanity, not only because of the massive de¬ 
struction and slaughter they caused and still cause, hut also because they dehnitely con¬ 
tributed to turn both alive and dead soldiers into nothing but numbers on a chart and alive 
or dead civilians into “collateral damages”, that is, “faceless human beings”. 

The already mentioned increasing sophistication of wars emphasizes “unaccountability” 
and indifference, since it “virtualizes” war, and virtualizes its victims, in fact, the separa¬ 
tion between the weapon that is used and the consequences it has, seem to turn death into 
something “virtual”, thus contributing to the lack of responsibility and to blurring the de¬ 
cisive question of the confrontation with the real victims’ faces and with the consequences 
of a decision to kill (or not to kill) someone that is standing in front of us. Marina (1996) 
recalls the answer given by an aircraft pilot during the hrst Gulf war (in the nineties) when 
a journalist asked him what he felt when he pressed the button to release the hrst bomb. 
He just answered that it was not very different from the exercises they practiced with sim¬ 
ulators. Krishnan, in a book with the signihcant title “War as Business” (2008: 8l) says that 
“more and more soldiers now belong to the ‘video game generation’ and are not only fa¬ 
miliar with computer simulations, but also enjoy training on them. ” The abyssal difference 
between war games or simulators, and real use of technology in war is that in the second 
one victims are real (there is no “reload/play again” in real bombings). The use of drones, 
for instance seem to reinforce this “blindness”. Manikkalingan (2014: x) considers them 
“both more humane and more inhumane”, since they “humanize the enemy - he becomes 
a living, breathing feeling person” (idem). Drones are targeted) but, at the same time, they 
create a situation where “in order to kill, you must dehumanize him” (idem). And even if 
it seems to be true that the process of dehumanizing the enemy is a common mechanism in 
all wars and genocides, what the authors consider to be different with drones is that “the 
warhghter is completely insulated from any interaction with those he is targeting ” (idem). 
He is not confronted with the face of the person that died because of a machine sent from 
the other side of the world, nor with “collateral damages” if the drone fails its target. 

This is why it seems important to put these “faceless human beings” at the core of an 
ethical reflection on the dehumanization present in “modern” technologies of war. in fact, 
contemporary wars not only dehumanize the victims. They also dehumanize those who 
activate these technologies. They are also faceless for the victims who only see an artefact, 
a death-machine. To know or at least to see the face of the enemy calls for an ethical deci¬ 
sion: “shall 1 kill him or not”? And it also wakes a feeling of vulnerability: “is he going to 
kill me or not? ” 

That is why Levinas (1979) considered the face both as a menace and a responsibility. He 
says: 


To manifest oneself as a face is to impose oneself above and beyond the 
manifested and purely phenomenal form, to present oneself in a mode ir¬ 
reducible to manifestation, the very straightforwardness of the face to face, 
without the intermediary of any image, in one’s nudity, that is, in one’s des¬ 
titution and hunger. (...) To manifest oneself in attending one’s own mani¬ 
festation is to invoke the interlocutor and expose oneself to his response and 


106 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


his questioning. (...) The being that expresses itseif imposes itseif, but does so 
preciseiy by appeaiing to me with its destitution and nudity (...) without my 
being abie to be deaf to that appeai. Thus in expression the being that impos¬ 
es itseif does not iimit but promotes my freedom, by arousing my goodness. 
(Levinas 1979: 200). 

Butier in a diaiogue with Athanasiou (20i3), on the other hand, uses the word “dispos¬ 
session” to taik about what Levinas caiis “destitution”. Like Levinas, she aiso sees “dis¬ 
possession” as a positive and a negative notion: dispossession can “mark the iimits of 
seif-sufficiency” (Butier &amp; Athanasiou 20i3: 3), and so it means the recognition of human 
beings as “reiationai and interdependent”. But it can aiso express the situation of peopie 
that iose everything and “become subject to miiitary and iegai vioience.” (idem) The first 
notion of dispossession emphasizes encounter with others, “being moved and even sur¬ 
prised or disconcerted by that encounter with aiterity” (idem). And by saying this Butier 
(Butier &amp; Athanasiou 20i3: 3) formuiates the key for an ethicai, private and pubiic, poiiticai 
standpoint. This key is “reiationaiity”. She says: “we do not simpiy move ourseives, but 
are ourseives moved by what is outside of us, by others, but aiso by whatever ‘outside’ 
resides in us. (...) we are moved by others in ways that disconcert, dispiace and dispossess 
us”, (idem) 

Summarizing: the recognition of human beings as being dispossessed, deprived, pre¬ 
carious, “grievabie” (as Butier emphasizes in a book written after 9/ii: 2004) instead of 
speeding up a civiiization with a “paranoid” refusai of the “others”, couid iead to a civi- 
iization of diaiogue, of recognition of aii human beings as beings with a face. If not, then 
maybe a new Battle of La Lys could happen, especially if the memory of such atrocities will 
be forgotten and the voices of those who know that “war is hell” will be silenced. 


References 

Bartov, O. (2000). Mirrors of Destruction. War, Genocide, and Modern Identitp. Oxford: 
Oxford University Press. 

Butler, J. (2004). Precarious Life. The Powers of Mourning and Violence. London: Verso. 
Butler, J. and Athanasiou, A. (2013). Dispossession: The Performative in the Political. 
Cambridge: Polity Press. 

Comissao Teeniea da Infantaria (1922). Livro de ouro da Infantaria. MCMXIV-MCMXVIII. 
S.l. 

Krishnan, A. (2008). War as Business. Technological Change and Military Service Con¬ 
tracting. Hampshire: Ashgate Publishing. 

Levinas, E. (1979). Totality andlnfmity. The Hague: Martinus Nijhoff Publishers. 
Manikkalingan, R. (2014), “Foreword”. In: Opposing Perspectives on the Drone Debate, 
ed. By Bradley Jay Strawser, lisa Hajjar, Steven Levine, Feisal H. Naqvi and John Fabian 
Witt. New York: Palgrave MacMillan, ix-xi. 

Marina, J. A. (1996). Etica para Naufragos. Lisboa: Caminho. 


I. A PROBLEMATIZAgAO DA PRIMEIRA GRANDE GUERRA 


107 


Marques, I. P. (2008). Das Trincheiras com Saudade. A vida quotidiana dos militarespor- 
tugueses na Primeira Guerra Mundial. Lisboa: A Esfera dos Livros. 

O’Connell, R. L. (1989). Of Arms and Men. A History of War, Weapons, and Aggression. 
Oxford: Oxford University Press. 

Queiroga, F. M. V. R. (2008), “Weapons and Warfare”. In: Encyclopedia of Archaeology, 
ed. by Deborah M. Pearsall. Vol. 3, 2197-2212. New York: Academic Press. 


108 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


11. Portugal na Guerra 
- A Batalha de La Lys 




A Batalha do Lys de 1918 e a sua historia 

Guilhermina Mota 

Faculdade de Letras da Universidade de Coimbra 


Resumo: Esta comunica^ao propoe uma leitura da batalha que se travou na planicie do Lys 
em 9 de abril de 1918, leitura que incide nao so na atua^ao do exercito portugues, mas que 
leva em conta tambem, e sobretudo, a integra^ao do combate na Ofensiva alema da Prima- 
vera desse ano que se desenrolou entre mar^o e julho e, de forma mais especihca, na Ope- 
ra^ao Georgette. Atendera as formas beligerantes (portuguesas, britanicas e alemas), as suas 
caracteristicas, as estrategias e taticas militares (a relevancia da tatica alema das tropas de 
assalto), as condi^oes de terreno e climatericas, as dinamicas do combate. 

Palavras-chave: Batalha de La Lys, Ofensiva alema da Primavera de 1918, Opera^ao Geor¬ 
gette, estrategia e tatica militar. 

Abstract: This communication proposes a reading of the battle that took place in the plain 
of Lys on April 9, 1918, focuses not only on the performance of the Portuguese army, hut 
above all, on the integration of combat in the German Spring offensive of that year that 
developed between March and July and, more specihcally, in Operation Georgette. It will 
focus on the belligerent forces (Portuguese, British and German), namely their character¬ 
istics, military strategies and tactics (the relevance of the German assault troops tactic), 
and also to the held of operations and climatic conditions, the dynamics of combat. 

Keywords: Battle of La Lys, Spring German offensive of 1918, Operation Georgette, strategy 
and military tactics. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


111 


Quando, ha anos, publiquei um relate da Batalha de La Lys (Mota, 2006), escrito por um 
alferes de artilharia que nela eombateu, ao compulsar a bibliograba relativa ao tema, aper- 
eebi-me que muitos dos textos que sobre a batalha se escreveram em Portugal tendiam a 
analisa-la em si mesma, como se nao tivesse contexto, levavam em conta apenas a a^ao das 
formas portuguesas e nao a das formas britanicas, e, na analise da derrota, conferiam um 
grande peso a altera^ao da polltica de beligeraneia, introduzida com a chegada ao poder de 
Sidonio Pais, em dezembro de 1917, que conduzira a um desinvestimento do pals na guerra, 
e pouco a atua^ao do comando aliado. 

Entre as razoes apontadas para o fracasso, salientava-se o estado calamitoso em que o 
exercito portugues se encontrava na Flandres, com as tropas ha muito na frente sem serem 
rendidas, com acentuada carencia de obciais e com o moral muito baixo. 

Por outro lado, minimizavam a magnitude da Ofensiva alema da Primavera de 1918, a 
importancia fulcral que a zona do Lys tinha nessa ofensiva e a intensidade esmagadora do 
ataque alemao no dia 9 de abril. A batalha era olhada como mais um combate rotineiro 
da guerra. 

Uma outra perspetiva, porem, apresentava em texto publicado logo em 1920 o major de 
infantaria Joao Maria Ferreira do Amaral. Escrevia: «A 2.“ Divisao portuguesa, estava nesse 
dia guarnecendo a frente que cortava o caminho que Ludendorff necessitava ter livre, para 
se aproximar de Calais e Boulogne. [...] Para isso entendeu e muito bem, que tendo pela 
frente uma divisao, devia lan^ar contra ela oito divisoes, sem se importar se era de portu- 
gueses, se era forte ou fraca, se estava nas linhas com ou sem vontade, se estava cansada 
ou nao. [...] Fez o que manda a cartilha da guerra moderna, que preceitua a quern ataca, 
faze-lo na propor^ao de oito contra um». E acrescentava: «Que a ninguem bque duvidas 
sobre o destino que uma Divisao Francesa, Inglesa ou Americana teria no dia 9 de Abril se 
estivesse onde esteve a 2.“ Divisao Portuguesa. Quern la estivesse seria esmagado, atrope- 
lado e ... varrido» (Amaral, 1923, pp.8 e 45). 

Este excerto pode ser encarado como uma mera justibca^ao do ocorrido, mas levou-me 
a refletir sobre as reals condiqoes em que se bateram os soldados portugueses e a concen- 
trar, assim, a minha aten^ao na dinamica da guerra, tentando perceber a interdependencia 
de todas as formas em presen^a, sendo as do Corpo Expedicionario Portugues (conhecido 
como o cep) apenas parte delas. 

E certo que a grande diferen^a numerica entre as unidades lusas e germanicas era sub- 
linhada em muitos relatos, assim como a superioridade do armamento alemao, quer em 
qualidade, quer em quantidade, mas nem sempre avaliando subcientemente o impacto que 
isso teve no desenrolar dos acontecimentos. 

Como atenuante, apresentava-se o facto do ataque se ter dado no dia em que as unida¬ 
des portuguesas tinham recebido ordens para, bnalmente, se deslocarem para posi^oes a 
retaguarda, momento de especial vulnerabilidade. Nao considerando, porem, que isso nao 
se devia a um golpe tragico do destino, mas a decisao tardia da cheba militar inglesa que as 
havia mantido na linha da frente, por delas ter necessidade, uma vez que a investida alema 
do mes de mar^o no Somme al retivera as britanicas. 

Para tentar conferir algum sentido a uma derrota penosa, defende-se por vezes que o 
confronto desse dia teve, pelo menos, o merito de retardar o inimigo e assim permitir que 
os aliados se recompusessem e pudessem resistir. Nada menos exato. Ate aos bnais de abril, 
o exercito ingles viveu as boras mais desesperadas da guerra. 


112 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Em 1917, a guerra chegara a um impasse, com os soldados enterrados nas trincheiras, 
travando escaramu^as constantes, mas sem lucrarem ou cederem ganhos ou recuos signi- 
ficativos. Nao se descortinava a possibilidade de uma solu^ao que pusesse fim ao conflito. 
Os paises iam-se exaurindo por anos de luta e de priva^oes, com as popula^oes esgotadas 
numa pemiria extrema. 

E entao que a Alemanha, recorrendo a novas taticas de guerra, inflige duas grandes der- 
rotas aos Aliados. Em setembro esmaga o exercito russo em Riga e em novembro o italiano 
em Caporetto. A conban^a na sua propria capacidade militar, estribada nos triunfos obti- 
dos, leva o comando alemao a planear uma grande ofensiva nos inicios do ano seguinte com 
a bnalidade de acabar com a guerra e alcan^ar a vitoria. O momento e propicio. Em 3 de 
mar^o, brma a paz com a Russia, pelo Tratado de Brest-Litovsk, e isso permite-lhe dispor 
de mais 50 divisoes, ftcando com uma vantagem temporaria. E o ataque e inadiavel, pois 
convinha ser desencadeado antes de um maior envolvimento norte-americano, quer em 
homens quer em recursos. 

A Ofensiva alema da Primavera de 1918, apelidada pelos alemaes de Batalha do Kaiser, 
consta de uma serie de ataques - “Michael” (21 de mar^o a 5 de abril), “Georgette” (9 a 29 
de abril), “Blilcher” (27 de maio a 5 de junho), “Gneisenau” (9 a 15 de junho) e “Marnes- 
chutz” (15 de julho a 3 de agosto) - desferidos ao longo da Erente Ocidental, iniciados a 
21 de mar^o e que se prolongaram ate 18 de julho, dia em que se iniciou a contra-ofensiva 
aliada. Eoi seu estratega o general Erich Ludendorff que optou por arriscar uma manobra 
de “tudo ou nada”, contra a opiniao de outros generals mais experientes que teriam prefe- 
rido progredir menos no terreno, mas de forma mais consolidada. Esta investida foi assim 
a mais violenta desde que a conflagra^ao come^ara e foi a que atingiu os avanqos mais pro- 
fundos desde 1914. E por isso mesmo acabou por precipitar o termo da guerra. 

A “Opera^ao Michael” decorreu na zona do Somme e com ela pretendia a Alemanha to- 
mar a cidade de Amiens, um no ferroviario vital, separar as linhas aliadas, empurrar o 
exercito britanico para la da Mancha, contando que a Eran^a, nessa eventualidade, se dis- 
pusesse a pedir o armisticio. 

Simultaneamente, os alemaes gizaram a “Opera^ao Georgette”, na zona do Lys, com o 
ftm de conquistar a cidade de Ipres, dominar os portos de Dunquerque, Calais e Boulogne- 
sur-Mer, para cortar o abastecimento aliado por mar, e tomar Hazebrouck, um centro de 
caminho de ferro onde os britanicos tinham um grande deposito de mantimentos. Esta 
cidade bcava a cerca de 24 km, para la do Setor Portugues, a noroeste. 

Esta opera^ao nao foi, como se tern dito, um ataque subsidiario cujo linico proposito era 
coagir o comando aliado a desviar as formas do Somme. Absorveu no total praticamente 
tantos homens como os que estiveram envolvidos em todas as batalhas de 1915 e 1916 juntas 
(williams, 2008, p.l74). E certo que teve uma escala menor que “Michael” - devido, em 
parte, a pressao que a manuten^ao da area entao conquistada exercia -, mas e o proprio 
Ludendorff que abrma que ela nao visava apenas objetivos limitados e que se tornou uma 
«opera 9 ao principal». Depois da rapida progressao alema, que tomou aos ingleses Armen- 
tieres, Messines e Merville, ja Winston Churchill, em 12 de abril, a descrevia como «pro- 
vavelmente, depois do Marne, o climax da guerra» (Stevenson, 2013, p.68). A11 de abril, 
a situacjao aliada era critica. Eoi nesse dia que Douglas Haig, o comandante da Eor^a Expe- 
dicionaria Brittaica, reconhecendo a falta de solu^Qes, emitiu a famosa palavra de ordem 
“Backs to the wall” - «With our backs to the wall and believing in the justice of our cause 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


113 


each one of us must fight on to the end»^® (Stevenson, 20f3, p.73) - , pela qual impunha a 
ohriga^ao de lutar ate ao hm, sem recuo nem retirada. 

A ofensiva do Lys durou ate 29 de ahril. Nesta data, o exercito alemao suspendeua. Tinha 
sofrido duras haixas e estava incapaz de conservar o terreno de que se apossara por escassez 
de provisoes e de refor^os. As rapidas tropas de assalto que lideravam os ataques nao foram 
acompanhadas por falencia da mohilidade da retaguarda. E assim os impressivos ganhos 
taticos acaharam por nao levar a concretiza^ao de resultados operacionais. 

A “Opera^ao Georgette” envolveu, da parte hritanica, do is exercitos, o i.°, comandado 
pelo general Henry Horne, e o 2.°, pelo general Herbert Plumer, que ocupavam as zonas a 
sul e a norte do rio Lys respetivamente. E envolveu, da parte alema, o 6.° Exercito, do ge¬ 
neral Eerdinand von Quast, a sul, e o 4.°, do general Sixt von Armin, entre o rio e a cidade 
de Ipres. 

Na manha de 9 de ahril, o primeiro dia da ofensiva, a hatalha colocou em confronto o 
poderoso 6.° Exercito alemao e dois corpos do l.° Exercito hritanico, o XI, soh o comando 
do general Richard Haking, com duas divisoes na frente, a 2.“ portuguesa e a 55.“ hritanica, 
e o XV onde se integrava a 40.“ Divisao hritanica. 

O CEP estava nas trincheiras da Elandres francesa desde ahril de 1917, operando com 
duas divisoes, soh a dire^ao do general Tamagnini de Ahreu e Silva. O Setor Portugues lo- 
calizava-se no vale do rio Lys, entre Armentieres e La Bassee, Merville e Bethune, com uma 
area que chegou a ter 14 km de frente e quatro setores de brigada (Perme du Bois a sul, Neu- 
ve Chapelle e Eauquissart ao centro e Eleurbaix a norte). Como os ingleses admitiram que a 
frente era muito extensa, esta foi reduzida para 11 km e em dezembro o setor de Eleurbaix 
foi atribuido a referida 40.“ Divisao. 

Ao longo desse ano, os portugueses mantiveram com determina^ao a defesa do seu setor. 
E, quando, nas primeiras semanas de mar^o de 1918, os alemaes intensihcaram as hostili- 
dades, os militares lusos nao so repeliram varios ataques inimigos, como ainda lan^aram 
com exito ataques proprios. O seu desempenho nao sofreu reparos e em 7 de ahril foi ate 
elogiado por Haking (Costa, 1920, p.l25). 

Por Conven^ao de 21 de Janeiro de 1918, hrmada entre os comandos portugues e britani- 
co, o CEP deixou de funcionar como Corpo de Exercito. Nesse acordo, estabeleceu-se que a 
l.“ Divisao passava para a retaguarda e a 2.“ se incorporava no XI Corpo hritanico, deixando 
de estar subordinada ao CEP para efeitos taticos. Em 3 de ahril foi emitida a ordem que 
mandava efetivar essa resolu^ao e tres dias depois, nas vesperas da hatalha, a 2.“ Divisao, 
a frente da qual estava o general Gomes da Costa, passou a tomar conta sozinha de todo 
o Setor Portugues, responsabilidade antes atribuida as duas divisoes (Costa, 1920, p.ll6). 

Para agravar a questao, a divisao nao se achava completa, contando apenas com pouco 
mais de dois ter^os dos sens efetivos, e estava muito desfalcada de ohciais (Praga, 2013, 
p.407). Poi refor^ada por isso com uma brigada da l.“ Divisao, mas que se destinava a guar- 
necer a Linha das Aldeias e, portanto, nao era uma for^a a disposi^ao do comando (Costa, 
1920, p.36). 


38 “Encostados a parede, e acreditando na justi(;a da nossa causa, cada um de nos tern de continuar a lutar ate ao hm” 
(tradu(;ao minha). 


114 


A PRIMEIRA GUERRA MUNDIAL. NA HATALHA DE LA LYS 



A frente portuguesa, ate aos imcios de abril guarnecida por 16 batalhoes, ftcou entao 
apenas com 12 e a Reserva, ja insubciente, foi ainda reduzida para metade (Costa, 1920, 
p.34). Ao todo, cerca de 16 mil homens, os efetivos realmente combatentes (Fraga, 2013, 
p.407), para defender uma frente de 11 km - veja-se que o exercito americano tinha uma 
frente de 14 km e 60 mil homens (Henriques; Leitao, 2001, pp.67). A extensao da frente 
portuguesa e um dado a reter para a compreensao do sucedido. Estava claramente acima 
da media que existia na frente ocidental e era excecionalmente longa para os padroes da 
Primeira Guerra. De notar que a 40.“ britanica a sua esquerda defendia 6,858 km e a 55.“ a 
sua direita apenas 3,657 km. 

A ofensiva alema estendeu-se ao longo de toda a frente lusa e de metade das frentes das 
divisoes britanicas que com ela conftnavam - duas milhas (3,218 km) da 40.“ e 1 milha 
(1,609 km) da 55.“ (Pyles, 2012, p.l). O piano germanico selecionou assim o Setor Portu- 
gues como zona preferencial de ataque. Este constituia com certeza o elo mais fraco das 
formas britanicas, mas nao foi esse facto que determinou a escolha. Esta foi ditada pelo pro- 
prio vale do Lys. Por um lado, o setor ficava no caminho do trajeto mais curto e direto que 
o exercito alemao tinha que percorrer para cumprir o objetivo tra^ado: atravessar o rio 
no primeiro dia e seguir depois para Hazebrouck. Ficava tambem numa zona da planicie 
extremamente rasa, o que permitia uma progressao mais celere. Estes dois considerandos 
tambem se aplicavam em parte a 40.“ Divisao, mas nao a 55.“, situada num terreno mais 
afastado e favoravel a defesa. Alem disso, por ser uma zona lamacenta, cortada por mult os 
drenos, tornava inoperacional a utiliza^ao de tanques, linica arma em que o exercito aliado 
tinha a primazia. Por bm, a localiza^ao do setor, entalado entre dois canals (Merville-Es- 
taires e La Bassee), comprometia seriamente os movimentos em caso de retirada (Costa, 
1920, p.55) e proporcionava aos alemaes uma posi^ao dominante, pois os portugueses es- 
tavam numa cova com a ribeira La Lawe a retaguarda (Amaral, 1923, p.l2). 

Os alemaes atacaram com uma imensa vantagem numerica. Movimentaram oito divisoes 
em primeira linha e quatro em apoio (Eraga, 2013, pp.405-406), o que so por si sugere que 
nao consideravam inaptas as formas do outro lado. Mas mais importante do que esse enor- 
me recurso em homens, que Ihes permitia avan^ar em ondas sempre refeitas, era o facto 
de grande parte dessas divisoes serem constituidas pelo escol das tropas germanicas, as 
eximias Stosstruppen, as tropas de assalto. 

Os militares alemaes tinham percebido que os ataques frontais por vagas massivas contra 
as posi^oes de atiradores entrincheirados geravam a perda de muitas vidas e eram pouco 
ebcazes. Em vez de confrontar as linhas inimigas, experimentaram uma outra tatica, a da 
inbltracjao. Algumas unidades, compostas por infantaria e sapadores jovens e motivados, 
criaram pelotoes de ataque bem armados que se deslocavam em bla e conseguiam mais 
facilmente cruzar a chamada “Terra de Ninguem”, sem se tornarem alvos tao expostos, 
e aparecer nas trincheiras inimigas ou mesmo mais atras. Alem das espingardas, levavam 
consign tambem granadas de mao e lan^a-chamas que se revelaram poderosas armas ofen- 
sivas, e dispunham de metralhadoras ligeiras, morteiros e canhoes leves para apoio de fogo 
(Pyles, 2012, p.89). Estas tropas de assalto eram a elite do exercito germanico, tinham o 
moral muito elevado e estavam abastecidas com o ultimo armamento de alta tecnologia. 
Quando as taticas de assalto num ataque atuavam juntamente com bombardeamentos pe- 
sados e de gas provaram ser inexoraveis. Eoram elas as responsaveis pelo arrasamento dos 
exercitos russo e italiano, assim como do britanico em 21 de mar^o no Somme, na abertura 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


115 


da “Opera^ao Michael”. E foram elas tambem, e ainda mais aperfei^oadas, com as li^oes 
colhidas nesta ultima ofensiva, que enfrentaram a exausta e enfraquecida divisao portu- 
guesa e a 40.“ Divisao britanica igualmente cansada de combate e que para o Lys tinha sido 
deslocada depois de tomar parte na Batalha de Cambrai, travada nos bnais de 1917. 

A a^ao da cheba britanica revela-se, em vesperas da batalha, confusa e inconsequente. 
Douglas Haig, encravado na ofensiva do Somme, em retirada com grandes cedencias, ten- 
do o seu comando amea^ado pelos mans resultados, nao percebeu a importancia da pla- 
nicie do Lys para a estrategia alema e nao teve a cautela, ou os instrumentos ao seu dispor, 
para dotar a zona a cargo dos portugueses com capacidade militar. 

Em contraponto, a prepara^ao da batalha foi muito bem conduzida por parte dos gene¬ 
rals alemaes do 6.° Exercito. A precisao com que certos alvos foram atingidos logo apds o 
inicio do bombardeamento evidencia que tinham feito os reconhecimentos e localizado os 
objetivos em devido tempo (Eraga, 2013, p.409). Os ingleses sabiam disso, pois os locals 
estavam referenciados em mapas capturados pelos alemaes, e procederam a altera^oes no 
seu dispositivo, mas os portugueses nao foram avisados e mantiveram as mesmas posiqoes 
(Henriques; Leitao, 2001, p.64). 

Em 25 de marqo, Haking, comandante do XI Corpo, tinha feito saber que, em caso de um 
ataque forte, a 55.“ Divisao nao estava preparada para defender as linhas da frente. Por isso, 
deveria bear a assegurar a Linha das Aldeias, um set or mais recuado, e era aos portugueses 
que deveria caber a defesa na Linha B, a linha principal de resistencia dos postos avan^a- 
dos (Pyles, 2012, p.79). Esta sugestao bcou materializada dias depois na ordem do general 
Horne que intimava a 2.“ Divisao a morrer na Linha B. Os ingleses, ao mesmo tempo que 
pediam o sacrificio maximo aos soldados portugueses, nao Ihes facultavam os meios neces- 
sarios para combaterem. De facto, tinham-lhes tirado grande parte da artilharia pesada, 
para socorrer Amiens, e o poder de fogo que deixaram era muito insubciente para respon¬ 
der a uma investida em grande escala (Henriques; Leitao, 2001, p.7). 

Em 7 de abril, o general Haking fez saber a Gomes da Costa que a 2.“ Divisao ia continuar 
na frente e que a principal linha de defesa que devia guarnecer era a Linha B. Neste dia, os 
britanicos estavam cientes da iminencia de um ataque, por informa^oes obtidas atraves dos 
prisioneiros e da observa^ao aerea e do terreno que mostrava uma grande movimenta^ao 
em frente do Setor Portugues, pois Ludendorff, desejando adiantar o inicio da ofensiva, 
e cheio de pressa, movia as suas unidades e a sua artilharia a luz do dia. O general Horne 
comunicou a Douglas Haig no dia 8 que contava ser atacado no dia seguinte, mas este en- 
tendia que a “Opera^ao Georgette” era provavelmente so uma diversao (Stevenson, 2013, 
p.7l). Assim, nao se preveniram os portugueses. 

A ultima bora, em contradi^ao com o que estava estipulado, e no momento menos in- 
dicado, os ingleses decidiram bnalmente ordenar a retirada da frente das unidades de in- 
fantaria lusas, mas nao as de artilharia que se deviam manter nas posiqoes (Costa, 1920, 
pp.125-126). 

Os estrategas da “Opera^ao Georgette” debniram que no dia de abertura, o nosso aziago 
dia 9 de abril, se devia fazer uma exibi^ao de for^a implacavel. O ataque seguiu as regras 
das taticas das formas de assalto, exatamente como ja o bzera em «Michael» dias antes. 
Existe, alias, uma grande similitude entre as duas operaqoes militares. Pelas 4,15 h da ma- 
nha, come^ou um bombardeamento muito intenso de artilharia e morteiros pesados e de 
fosgenio e gas mostarda, que alvejou as zonas mais recuadas, onde estavam os postos de 


116 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


comando, depois as comunica^oes, a artilharia, as segundas linhas de infantaria e a linha 
da frente. Assim conseguiram romper todas as comunica^oes e lan^ar uma grande deso- 
rienta^ao no campo de batalha, desorienta^ao agravada por um nevoeiro eerrado que im- 
pedia a visao. Este bombardeamento durou perto de quatro boras sem dar deseanso. Foi 
neste poder avassalador da artilharia que assentou o eerne da investida. Quando diminuiu, 
e os efeitos nocivos do envenenamento por gas se dissiparam, avan^ou entao a sua infan¬ 
taria de assalto bem equipada e treinada, inbltrando-se no sistema defensivo e isolando as 
tropas das diferentes divisoes. Os britanicos recuaram nas suas posi^oes para formar flaneo 
defensivo, deixando aberturas por onde os alemaes penetraram com mais desenvoltura, 
vindo asurpreender as unidadesportuguesas pelaretaguarda (Costa, 1920, p.130). Depois, 
os alemaes, em vagas sucessivas, foram dizimando os rest os dos batalhoes de infantaria. As 
baterias de artilharia portuguesas, por sua vez, como a barragem inimiga nao deixava fazer 
o remuniciamento, foram-se gradualmente reduzindo ao silencio. De notar, no entanto, 
que a artilharia existente era apenas de campanha e nunca teria podido contrabater a ar¬ 
tilharia pesada inimiga, o que so estaria ao alcance da inglesa que havia sido em boa parte 
retirada (Costa, 1920, p.151). 

Para Gomes da Costa, o problema fundamental do piano de defesa foi empregar dema- 
siadas formas na primeira linha, que ftcaram destro^adas pelo bombardeamento inicial, 
nao havendo depois mais atras outras unidades para as refor^ar e fazer os contra-ataques 
(Costa, 1920, p.7l). Nao houve o cuidado de colocar na retaguarda da posi^ao portuguesa 
reservas que pudessem apoiar as linhas da frente. 

Para melhor se percecionar o que foi a violencia da investida do Lys nada como compa- 
ra-la com a do dia de abertura da “Opera^ao Michael”, a 21 de mar^o. A este dia chamou o 
historiador Joseph Gies «a abertura do Armagedao» (Gies, 1974, p.8l), tal a dimensao da 
catastrofe. Analisemos o poder de logo em ambos os ataques, em propor^ao com a exten- 
sao da frente do campo de batalha. No primeiro dia de “Michael”, o exercito alemao utili- 
zou 6.200 armas de logo (Gies, 1974, p.8l) que dispararam 3,2 milhoes de tiros, um terqo 
dos quais quimicos (Stevenson, 2013, p.53), numa linha de frente de 50 milhas (80 km). 
A frente de ataque da “Opera^ao Georgette” era muito mais estreita, 10 milhas (16 km: 11 
da frente portuguesa e 5 das frentes britanicas) e, no dia de abertura desta ofensiva, o dia 
9 de abril, o 6.“ Exercito alemao usou 1.686 armas de logo que dispararam um total de 1,4 
milhoes de tiros (Zabecki, 2006, p.l40). Feitas as contas, se conclui que em 21 de marqo se 
usaram 78 armas por quildmetro e em 9 de abril 105, e que os projeteis caidos no campo de 
batalha sao no primeiro caso 40 mil e no segundo 87 mil em rela^ao ao mesmo comprimen- 
to, mais do dobro portanto. 

Mas nao so os mimeros contam, tambem o tipo e o poder destrutivo das armas de logo 
importam para o resultado. Praticamente metade das armas de logo do 6.“ Exercito per- 
tenciam as categorias de pesadas ou super pesadas, muitas delas liltimos modelos. 

Esta batalha, a ultima do exercito portugues, como a classiftca Alves de Fraga (Fraga, 
1993), causou um traumatismo profundo em quern a viveu e no pais em geral, e que perdu- 
rou no tempo. Tal nao se podera atribuir somente a derrota, mas sim ao aspeto humilhante 
de que ela se revestiu. Tudo se resolveu em oito boras. Come^ado o assalto por volta das 
quatro da manha, ao meio dia tudo estava acabado, subsistindo apenas algumas bolsas de 
resistencia dispersas. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


117 


A incapacidade militar no combate, juntou-se o desconcerto na retirada, com muitas 
tropas vagueando desordenadas. E os ho mens nao morreram na Linha B, como a cheba 
tinha prescrito. Nao se conhecem com rigor as perdas humanas sofridas na batalha, mas 
aceita-se que perbzessem no total metade dos efetivos da divisao: 614 mortos (Fraga, 2013, 
p.418), pelo menos 1500 feridos (Henriques; Leitao, 2001, p.79), 6585 prisioneiros e um 
mimero indeterminado de desaparecidos. O que signibca que grande parte dos ho mens 
mantiveram as suas posi^oes ate serem mortos, feridos, presos ou ultrapassados. 

A elevada propor^ao entre os que foram eapturados e os que foram mortos (dez vezes 
mais) ainda hoje causa estranheza e mal-estar em quern acredita que, como na antiga Es- 
parta, de uma peleja so se volta ou com o escudo ou sobre o escudo (vencedor ou morto). 
A dita propor^ao, que leva alguns a interpretar que os soldados eapitularam e nao se bate- 
ram, nao se expliea apenas eom a desmoraliza^ao das tropas. Em primeiro lugar, o brutal 
fogo de barragem ter-se-a destinado mais a romper as eadeias de eomando - sabendo-se 
a importaneia que a hierarquia tern nos meios militares e os riscos que envolvem decisoes 
particulares - e a criar a desorganiza^ao no teatro de opera^oes do que a aniquilar os ho- 
mens fisieamente. Depois, o facto de os alemaes envolverem os portugueses - uma vez que 
romperam pelos flancos ingleses e os surpreenderam pela retaguarda, de onde nao vieram 
reforcjos - nao Ihes deixava a minima hipdtese de vencerem. Assim, imperou o bom senso 
da rendi^ao, para evitar um banho de sangue numa rea^ao imitil. 

Os ingleses tiveram uma derrota igualmente esmagadora em 21 de mar^o. Na frente La 
Fere-Vermelles, com condi^oes muito superiores de resistencia, cederam 60 km de frente 
e outros tantos de profundidade (Costa, 1920, p.l73) e em toda a opera^ao os alemaes ft- 
zeram cerca de 90 mil prisioneiros sem ferimentos, segundo aftrma Ludendorff (Amaral, 
1923, p.l3). Mas os britanicos que lutaram em “Michael” foram vistos com indulgencia. O 
seu comportamento foi justiftcado. Nao poderiam ter feito melhor, com frio e sem comer, 
muitos deles gaseados, vendo os seus camaradas desfeitos em peda^os ou enterrados vivos, 
entorpecidos pelo embate, impossibilitados de pensar ou agir, como narra David Steven¬ 
son (Stevenson, 2013, p.54). Esta descri^ao aplica-se por inteiro as condi^oes dos portu¬ 
gueses em 9 de abril. Para eles, contudo, nao houve qualquer benevolencia. A imagem que 
cedo bcou estampada e que deitaram as armas ao chao e deram aos tacoes logo no inicio 
do ataque. 

O CEP e o l.“ Exercito britanico, de que fazia parte, sofreram um doloroso reves na pla- 
nicie do Lys naquela manha de abril, tendo os alemaes logrado o seu objetivo, atravessar o 
rio. 

Os portugueses da 2.“ Divisao e os ingleses da 40.“, sobre as quais caiu o furor da a^ao 
ofensiva, viveram entao as suas boras mais amargas. Apenas puderam lutar sem esperan^a 
em a^oes de pequenas unidades contra ondas sucessivas de atacantes. Mesmo formas vigo- 
rosas, o que nao era o caso de nenhuma delas, teriam tido dibculdade em responder. Alias, 
nos dias seguintes, Ludendorff continuou a galgar terreno e a tomar cidades. 

A 55.“ Divisao, pelo contrario, tinha tropas frescas, suportou um ataque de baixa prio- 
ridade, confrontou-se com unidades alemas constituidas por homens de familia de meia 
idade e que costumavam ser utilizadas em papeis defensivos, as chamadas tropas para de- 
fesa de trincheiras (Pyles, 2006, p.98). E, mesmo assim, nao foi capaz de repelir o embate, 
lamentando Gomes da Costa que nao tivesse feito grande esforqo quando «devia e podia ter 


118 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


contra - atacado, em vez de formar flanco defensive », desguarneeendo a divisao portugue - 
sa (Costa, 1920, p.l75). 

No entanto, na aprecia^ao desigual que Douglas Haig resolveu fazer das formas sob o seu 
comando, foi a 55.“ Divisao aleandorada a gloria, enquanto calou o procedimento da 40.“ 
e aviltou a 2.“ Divisao portuguesa, reportando que a maior parte das suas tropas abando- 
naram as posi^oes assim que a investida alema quebrou a frente, desertaram do eampo de 
batalha, nao detiveram o avan^o alemao e expuseram as formas britanicas, fazendo dela o 
bode expiatorio do desaire. A visao negativa da atua^ao dos portugueses foi assim eons- 
truida de imediato. 

Os historiadores reproduziram depois os argumentos: as formas portuguesas, indubita- 
velmente as piores de todas as na^oes do Oeidente, sempre olhadas como praticamen- 
te imiteis, deixaram eair as armas e fugiram em panieo, tendo os alemaes um inesperado 
golpe de sorte por a linha ser defendida apenas por uma divisao portuguesa, cansada, de- 
primida, que estava para ser rendida, e que quebrou na primeira investida. David Steven¬ 
son afirma mesmo que dois terqos dos portugueses fugiram para a retaguarda (Stevenson, 
2013, pp.71-72), oque manifestamente estaemcontradi^aoeomo mimero de prisioneiros. 

Houve quern nao seguisse a tendeneia, como Arthur Conan Doyle, para quern o ataque 
alemao foi de tal ordem que nenhuma eensura podia ser assacada as tropas portuguesas, 
aereseentando ainda que os artilheiros, homens valentes, permaneeeram no seu posto e 
eontinuaram a fazer logo mesmo depois da infantaria os ter deixado desprotegidos (Pyles, 
2012 , p.ll). 

E David Lloyd George, primeiro-ministro do Reino Unido em 1918, recorre a palavras 
bem eontundentes para qualiftcar a injusti^a do discurso ofteial que se foi impondo para 
eneobrir a inepeia da eiipula militar: «[...] it is rather hard on a small nation, which has a 
long and honoured reeord for valour and intrepidity on sea and land [...] that they should 
have to bear the stain of reproach for a defeat which was entirely attributable to the crass 
stupidity of a General from another race» (George, 1937, p.29)^‘’. 

Pode afirmar-se que o GEP nao se eneontrava nas melhores condi^oes em 1918, que nao 
tinha desde o inicio as competencias bastantes para participar num eonflito que suseitou 
uma verdadeira revolu^ao na arte da guerra, que o poder instalado em Lisboa nao era favo- 
ravel a sua estada na Elandres, mas tudo isso afmal acabou por ser menos determinante na 
derrota de 9 de abril do que as suas eireunstancias, o impeto da ofensiva alema e a incerta 
condu^ao da guerra por parte do comando ingles. 

O caraeter extraordinario da batalha - a veemencia inaudita do bombardeamento, a for- 
9 a da infantaria de assalto, o nevoeiro que tornava irreais os movimentos do inimigo, a 
quebra das eomuniea^oes isolando as unidades, a destrui^ao do sistema defensivo, a ine- 
xistencia de reservas e de artilharia pesada - basta para compreender a derrota e a confu- 
sao da retirada. A ideia de inoperaneia nao quadra a uma for^a de exercito que cumprira 
o dever de manuten^ao do seu setor, durante um ano de guerra, eom lutas constantes em 
que perderam a vida muitos homens. 


«[...] e muito duro, para uma pequena naqao que tern um longo e honroso historial de valor e coragem no mar e 
em terra [...] ter de suportar o estigma da reprovaqao por uma derrota que se deve atribuir inteiramente a estupidez 
crassa de um general de outra raqsL» (traduqao minha). 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


119 



Douglas Haig estaria mesmo convencido que os alemaes nao iam lan^ar um grande ata- 
que na regiao do Lys e que o movimento militar observado na area se destinava a dissimular 
a verdadeira ofensiva que eontinuava a ser a do Somme. Essa a linica razao que pode expli- 
ear o abandono a que votou uma divisao que tinha uma frente muito longa para defender e 
um reduzido mimero de efetivos mal equipados para o fazer. 

For ftm, ha que frisar que no dia 9 de abril nao foram so os portugueses que sairam der- 
rotados da Batalha de La Lys. Todas as formas aliadas que nela combateram tiveram a mesma 
sorte. Em ultima analise, quern perdeu a batalha foi o l.° Exercito britanieo, em eujo XI 
Corpo se integrava uma divisao portuguesa. 


120 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Referencias 


1. Fontes impressas 

Amaral, J. M. F. do. (1923). A Batalha do Lys, a Batalha d’Armentieres ou o 9 de Abril. 
Lisboa: Tipograba do Comercio. 

Costa, General M. G. da. (1920). O Corpo de Exercito Portugues na Grande Guerra: a Ba¬ 
talha do Lys - 9 de Abril del918. Porto: Renascen^a Portuguesa; Rio de Janeiro: Luso-Bra- 
siliana. 

2. Estudos 

Afonso, A. e Matos Gomes, C. de (Coords). (2013). Portugal e a Grande Guerra 19141918. 
(2.“ ed). Vila do Conde: Verso da Historia. 

Fraga, L. M. A. de. (1993). La Lys: a ultima batalha do exereito portugues. In: IV Goldquio. 
A Historia Militar de Portugal no se'culo XIX. Actas. Lisboa: Comissao Portuguesa de His¬ 
toria Militar. 

Fraga, L. M. A. de. (2013). La Lys, a batalha portuguesa. In: Afonso, A. e Matos Gomes, C. 
de (Coords). (2013). Portugal e a Grande Guerra 1914-1918. (2.“ ed). Vila do Conde: Verso 
da Historia, pp. 404-418. 

George, D. L. (1937). War memoirs of David Lloyd George. 6.° vol. 1918. Boston: Little, 
Brown. 

Gies, J. (1974). Grisis 1918: the leading actors, strategies and events in the German gamble 
for total victory on the Western Front. New York: Norton. 

Henriques, M. C. &amp; Leitao, A. R. (2001). La Lys 1918: os soldados desconhecidos. Lisboa: 
Prefacio. 

Marshall, S. L. A. (2001). World War I. Boston: Houghton Mifflin. 

Mota, G. (2006). Batalha de La Lys: um relato pessoal. Revista Portuguesa de Historia 38: 
77-107. 

Pyles, J. (2012). The Portuguese Expedicionary Gorps in World War I: from inception to 
combat destruction, 1914-1918. MA diss. Denton: University of North Texas. 

Stevenson, D. (2013). With our backs to the wall: victory and defeat in 1918. Cambridge 
Ma: The Belknap Press of Harvard University Press. 

Williams, J. F. (2008). Modernity, the media and the military: the creation of national 
mythologies on the Western Front 1914-1918. New York: Routledge. 

Zabeeki, D. T. (2006). The German 1918 offensives: a case study in the operational level of 
war. New York: Routledge. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


121 



O CEP na Batalha de La Lys: O fim de 
um objective politico nacionaP® 

Luis Alves de Fraga 

UniversidadeAutonoma de Lisboa/UAL 


Resumo: Faz-se uma analise das condi^oes sociais, politicas e culturais de Portugal nos 
anos anteriores a proclama^ao da 1.“ Repiiblica para justificar o estado cultural do Corpo 
Expedicionario Portugues (CEP) em Franca, da sua desmoraliza^ao, desinteresse militar 
e pouca combatividade, de modo a tentar compreender a batalha de La Lys, ou, mais em 
particular o dia 9 de Abril de 1918. Em seguida, procura-se descobrir as projec^oes cultu¬ 
rais, sociais e politicas da presen^a do CEP em Franca e da batalha na vida nacional portu- 
guesa e os efeitos que teve ao longos dos anos da ditadura. 

Palavras-chave: 1.“ Repiihlica Portuguesa, Grande Guerra, La Lys, Estado Novo, Salazar. 

Abstract: An analysis of the social, political and cultural conditions of Portugal in the years 
prior to the proclamation of the 1st Republic is carried out to justify the cultural state of the 
Portuguese Expeditionary Gorps (GEP) in France, its demoralization, military disinterest 
and lack of comhativeness, in order to try to understand the Battle of La Lys, or, more 
particularly, on April 9, 1918. Next, we seek to discover the cultural, social and political 
projections of the presence of the GEP in France and the battle in Portuguese national life 
and the effects that it had during the years of the dictatorship. 

Keywords: First Portuguese Republic, Great War, La Lys, Estado Novo, Salazar. 


O autor escreve segundo o antigo Acordo Ortografico. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


123 



Introdu^ao 

Ja fizemos, ao longo de mais de tres decadas, varios trabalhos onde descrevemos a batalha 
de La Lys, por isso, julgamos, esgotamos o tema, pois, pouco mais se pode dizer que seja 
novo. Contudo, parece-nos, a batalha pode, e deve ser, analisada sob prismas novos: efei- 
tos sociais, consequencias politicas - de natureza interna e de natureza externa - sanitarias 
e, ate, culturais. 

E uma abordagem desse genero que pretendemos fazer aqui, muito embora tenha de 
ser, naturalmente, sintetiea. Tratar-se-a menos de um relato faetual do que de uma re- 
flexao transversal aquilo que a batalha representou, efectivamente, no tecido social, na 
ambiencia politica e no imaginario nacional portugues. Sera esse o escopo do trabalho que 
se segue. Mas, para que tal seja possivel dentro de poucas paginas, e imperioso tra^ar uma 
contextualiza^ao epocal para se compreender quern eram e como eram os actores envolvi- 
dos em tudo o que se relaciona com a batalha. E o que vamos fazer de seguida. 

Nao vale a pena perder muito tempo a dizer que na decada de dez do seculo XX, em 
Portugal, cerca de 75% da popula^ao era analfabeta, mas este facto tern de ser chamado 
a cola^ao, porque justiftca o panorama social do pais. Assim, a esmagadora maioria dos 
portugueses nao estava em condi^oes de perceber as razoes que levavam Portugal a ser 
beligerante e, muito menos a de ter de morrer, la longe, numa batalha, em Eran^a. 

Do ponto de vista economico, e necessario dizer que a industria que mais mao-de-obra 
empregava era a da constru^ao civil centrada nas maiores cidades de entao: Lisboa e Porto 
e, a uma grande distancia, Coimbra e Setiibal. Depois, como emprego de mao-de-obra, 
vinha a agricultura, pois, Portugal era essencialmente agricola; dai resultava que uma parte 
dos proprietarios das terras gozavam, nas cidades, dos rendimentos provenientes da labu- 
ta do amanho do solo. Havia, em especial nas grandes cidades, uma classe media, vivendo, 
quase toda ela, de empregos no Estado ou no comercio por grosso e por retalho; era gente 
de poucas posses ftnanceiras, mas com um grau cultural ligeiramente acima das popula- 
^Qes rurais. Mas as cidades estavam cheias de servi^ais, vindos das areas rurais, analfabetos 
e dispostos a vender a sua capacidade de trabalho por baixo pre^o, que, de qualquer forma, 
sempre era melhor do que o recebido no trabalho da terra. 

As elites culturais e politicas concentravam-se nas tres cidades mais importantes - 
Lisboa, Porto e Coimbra - onde liam e escreviam ou trabalhavam em areas especibcas 
e bem remuneradas: advocacia, medicina, engenharia, docencia no ensino superior, 
juizes, altos funcionarios do Estado e, a uma certa distancia dos anteriores, na carreira 
militar, como obciais. As origens desta elite estavam, quase sempre, ligadas a posse de 
propriedades agricolas. 

O clero catolico teve, ao longo dos tempos, em Portugal, sempre uma importancia rele- 
vante e excessiva - recordemos o papel censorio e, de certa maneira, limitador da evolu^ao 
do pensamento cientifico e cultural devido ao Tribunal do Santo Oficio - quer junto das 
populaqoes rurais - onde passava por ser a voz mais escutada por todos em todo o tipo 
de assuntos - quer junto das popula^oes urbanas, fossem burguesas ou aristocraticas. De 
certa forma, a Igreja, por ac^ao do clero, foi um elemento condicionador da mentalidade 
dos Portugueses, ja que limitou, atraves de um culto obscurantista, a abertura a uma com- 
preensao da vida e dos fenomenos naturals e sociais. A este proposito, deve recordar-se 
que os Estados onde a pratica religiosa foi protestante estiveram sempre mais prontos para 


124 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


encarar a modernidade e as evolu^oes cientificas e isso devia bastar para compreender que 
Portugal foi vitima da religiao oficial. 

E dentro destas linhas que vamos, entao, olhar a batalha de La Lys e os soldados por- 
tugueses integrantes do Corpo Expedicionario Portugues (CEP), passando pela sociedade 
nacional, tanto na versao politica como cultural. 


O CEP de Abril de 1917 a Abril de 1918 

Portugal mandou para Eran^a, constituindo o CEP, 55165 indivlduos, dos quais 3366 eram 
obciais, incluindo 2 elementos da Cruz Vermelha, 1 oficial da Armada e, para alem destes, 
mais 54 enfermeiras. 

Neste conjunto as diferen^as eram extremas, pois estiveram desde deputados e sena- 
dores, entre os oficiais, e soldados completamente analfabetos; estiveram desde ricos a 
pobres; escritores, professores e simples agricultores; gente das grandes cidades e gente 
de aldeias perdidas nas serranias deste pals. A diversidade social era imensa. De comum, 
havia um aspecto: a grande maioria estava contrariada na guerra. Era minima a por^ao de 
militares concordantes com a entrada de Portugal no conflito. Isso levanta uma questao, 
que carece de resposta: 

— Qual o motivo dessa discordancia ou dessa contrariedade? 

Julgamos, de acordo com o muito que temos investigado, escrito e defendido (PRAGA, 
2012), que a razao capital se centra na falta de informa^ao sobre os reals motivos para es- 
tarmos na guerra e, em especial, cm Eran^a. 

Essa carencia de informa^ao resultou de varios elementos fundamentals: a excessiva taxa 
de analfabetismo, a dificuldade de circula^ao da informa^ao no pals, o comodismo de al- 
guns, em especial da obcialidade pouco esclarecida, o apego ao lugarejo de nascimento e a 
relutancia em alterar ritmos de vida, a debciente instru^ao militar (esta nao se deve limitar 
a ensinar o manejo e utiliza^ao das armas, mas a explicar a obriga^ao de servir a patria 
onde e quando necessario), o medo do desconhecido e, acima de tudo, a quase total falta 
de instru^ao politica. 

Como se percebe, algumas das justificaqoes assentam numa mesma explica^ao: a bo^ali- 
dade da popula^ao portuguesa, por um lado, e, por outro, ignorancia clvica dessa mesma 
popula^ao. E isto mesmo que se encontra na correspondencia postal apreendida pela cen- 
sura e oriunda de soldados, de sargentos e, ate, de oficiais. Cem anos depots, continua a 
explorar-se estes dois aspectos, sem os explicar ou, ao menos, os contextualizar. Tentemos 
esclarecer. 

A razao profunda da decisao de passar de uma neutralidade amblgua, pedida pelo Rei- 
no Unido, para uma beligerancia activa, imposta pelo Governo portugues, nao podia ser 
explicitada (ERAGA, 2012), mas, podia fazer-se propaganda. Todavia, mesmo que tivesse 
sido feita - e temos de levar em conta que se estava ainda bastante longe dos metodos 
de convencimento colectivo usados cerca de duas dezenas de anos mais tarde, com gran¬ 
de resultado, na Alemanha nazi - seria quase iniitil dada a dificuldade de comunica^ao 
provocada pelo analfabetismo, pois a forma mais vulgar de difundir ideias, nessa epoca. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


125 


era a expressao oral e escrita. No entanto, houve uma tentativa de efectivar a propaganda 
possivel, encarregando-se desse trabalho um jovem medico e politico - Jaime Cortesao - 
consciente das razoes da necessidade de Portugal entrar na guerra (CORTESAO, 1916). Foi 
um livrinho de facil leitura, concebido para gente do povo, escrito sob a forma de dialogo. 
Contudo, mesmo tendo sido publicados largos milhares de exemplares, o certo e que pouco 
efeito pratico teve nas aldeias. 

E verdade que houve outro tipo de propaganda apoiando a beligerancia virada para as 
elites politizadas, especialmente feita na Revista Aguia, editada pela Sociedade Renascen^a 
Portuguesa. Todavia, essa propaganda nao fazia mais do que repisar os argumentos conhe- 
cidos de todos os que ja estavam convencidos. 

Houve, e em grande quantidade, propaganda contra a beligerancia. Essa tambem tinha 
como alvo as elites cultas e as massas trabalhadoras. Expliquemos melhor esta divisao e as 
respectivas origens. 

A propaganda destinada as elites cultas fazia-se em dois tipos de jornais: os republicanos 
de direita, conservadores, e os monarquicos. A campanha contra a entrada de Portugal na 
guerra foi intensa e brutal. Procurou-se desacreditar o Partido Democrdtico, aquele que, 
declaradamente, defendia a ruptura do estatuto imposto pela Gra-Bretanha a Portugal. 
De alguma maneira, esta campanha contra a beligerancia, chegou, tambem, a popula^ao 
comum, fosse republicana ou monarquica. A incompreensao das razoes de ir combater 
na Europa nao se limitavam aos pobres e ignaros camponeses. Ja depois do CEP estar em 
Franca, surgiu, em Portugal, uma publica^ao, cuja origem esteve num jornal monarquico, 
chamada Role de Desonra, onde se dava conta de nao estarem na frente de combate, mas, 
na retaguarda, no recato dos estados-maiores, alguns oftciais politicos e os Mhos de politi¬ 
cos republicanos. Criticavam-se, tambem, todos os que nao estavam em primeiras linhas. 
Tratava-se, sem diivida, de uma igndbil publica^ao, visando desmoralizar os combatentes 
e por a opiniao piiblica contra os politicos republicanos beligerantes, pois distorcia a ver¬ 
dade, ja que, nas primeiras linhas ou nos estados-maiores, se corria risco de vida, ainda 
que este fosse maior nas primeiras linhas, como e evidente. 

A propaganda destinada aos grupos sociais mais desfavorecidos era feita atraves dos sin- 
dicatos de trabalhadores, os quais, maioritariamente, Miavam os sens principios ideold- 
gicos nas teorias anarquistas com caracter revolucionario, muito em voga na Peninsula 
Iberica, de entao. Note-se, que esta propaganda contra a guerra nao atingia a maior parte 
dos militares, porque o grosso do contingente era proveniente do ambiente rural e o anar- 
quismo estava enraizado nas cidades, especialmente na de Lisboa. 

Do exposto, compreende-se a clara falta de vontade animica para combater, para estar 
na frente de batalha, para estar em Franca e, ate, para estar nas bleiras militares. Na sua 
esmagadora maioria, a tropa nacional, a partida de Portugal, ja ia moralmente derrotada. 
Esta conclusao conduz a uma diivida: 

- Como foi possivel, entao, criar algum espirito de corpo, neste exercito de descontentes? 

A explica^ao so a podemos encontrar nas condi^oes especificas caracterizadoras dos ho- 
mens ignorantes e brutos que constituiam a maioria do CEP. Expliquemo-nos. 

Come^amos por dizer que a taxa de analfabetismo, em Portugal, era, pelos anos de 1911- 
1914, da ordem dos 75% e que a influencia do clero catolico junto das popula^oes rurais 


126 


A PRiMEIRA GUERRA MUNDIAL. NA BATALEIA DE LA LYS 


era absoluta; acrescentamos que essa popula^ao era dominada pelo obscurantismo de uma 
sociedade fechada ao desenvolvimento. Entao, e facil perceber que, na ausencia do padre, 
do cacique, do pai e da mae, integrados num sistema hierarquico e exigente em termos de 
disciplina, estes homens viam-se ob rig ados, quase como se fossem dependentes de auxilio 
ou orfaos, a acreditar nos graduados militares e a cumprir as ordens que recebessem. Era 
gente simples, paciente, sofredora, mas espontanea na sua revolta quando se achava vitima 
de injusti^a, dentro dos sens limitados principios de justi^a. 

Entao, temos, por um lado, uma minoria de graduados militares esclarecidos e renitentes 
a beligerancia, fazendo tudo para se escapar da morte e dos perigos das primeiras linhas, 
que nao tinha relutancia em abandonar os seus subordinados ao destino que o proprio des- 
tino Ihes havia proposto e, por outro, esta massa enorme de ignaros soldados disposto, se 
necessario, a morrer desde que Ihe dessem ordem para tab E, assim retratado, compreen- 
de-se o que foi o CEP entre Abril de 1917 e Abril de 1918. Assim, tambem se compreendem 
as desconftan^as do Alto Comando Britanico em Eran^a, mas tambem se compreendem as 
lutas, que os obciais com consciencia da necessidade de entrar na guerra travaram a todos 
os niveis para manter de pe o CEP como representa^ao de Portugal e de uma politica que 
se pretendia dignibcante e digna. Mas tambem se compreende a trai^ao que foi o golpe e a 
politica de Sidonio Pais ao abandonar esta massa de soldados ao seu destino, quase sem ter 
a comanda-los oficiais e sargentos crentes na razao de se estar em Eran^a, nas trincheiras. 

Eoi esta gente, desmoralizada por ter vivido um mes de Mar 90 esgotante de combates e 
bombar deament os, que enfrentou, na madrugada de 9 de Abril de 1918, um terrivel ataque 
germanico efectuado para rebentar a frente aliada e poder chegar a costa maritima e dividir 
a frente em duas. 

Chegamos ao ponto de poder explicar, numa outra perspectiva, o dia 9 de Abril e aquilo 
que foi o primeiro momento da longa batalha de La Lys. 


A batalha de La Lys 

Se nos perguntarmos qual a justiftca^ao para a decisao alema de atacar, na madrugada de 
9 de Abril de 1918, o sector defendido pelos soldados portugueses, percebemos que o Alto 
Comando germanico, del ao seu principio de estudar o caracter e os habitos do general 
inimigo, tera estudado muito bem, nao so quern era Tamagnini de Abreu e Silva, como os 
outros dois generals que comandavam as divisoes portuguesas e, por arrasto, o moral e o 
espirito combativo dos soldados. 

Desse estudo deve ter concluido que o CEP estava, como sempre esteve, desmoralizado, 
incapaz de se bater por uma causa que nao percebia e nao advogava. Alias, foi atraves de 
panfletos lan^ados por aeroplanos germanicos, que os militares portugueses, nas trinchei¬ 
ras, tomaram conhecimento previo do golpe de Sidonio Pais, em Lisboa e contra a guerra. 

Aquilo que nos dissemos paginas atras, sob re o desanimo e falta de combatividade da 
maioria dos militares do CEP, saberia, muito bem, o Comando alemao. Entao, nao havia 
que hesitar: o ataque seria sobre os Portugueses e eles render-se-iam com facilidade e com 
a menor quantidade de perdas humanas nas tropas de assalto germanicas. Esta constata- 
(jao nao foi feita no mes de Marqo anterior ao comedo da batalha; ja vinha de meses antes. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


127 


quando se percebeu que os soldados portugueses se rendiam facilmente ao serem atacados 
nas trincheiras por formas alemas de certa envergadura. 

— Mas, sendo assim, qual a razao motivadora da decisao de Portugal entrar na guerra? 

E aqui que temos de perceber a diferen^a entre a decisao politica e a incapacidade de os 
mecanismos estatais seguirem essa decisao. A decisao politica estava certa e era correcta; 
no entanto, nao era susceptivel de ser posta em marcha pelo conjunto de razoes ja identi- 
bcado. 

— Haveria, entao, que se desistir do objectivo politico? 

Julgamos que nao. O que se impunha era for^ar todos a cumprir a decisao politica, porque 
esta visava umbem maior. Naturalmente, passar o Bojador exigiu sacriflcios, mas nem por 
isso o Infante D. Henrique deixou de continuar a mandar que os seus navegadores o bzes- 
sem! E ftzeram-no. E desvendaram mares e terras desconhecidos. E isso foi progresso, foi 
modernidade. Qual o motivo por que nao deveria o Povo, os soldados, cumprir o objectivo 
de quern os mandava para a guerra para se conquistar o respeito da Europa? A resposta so 
pode ser uma: o orgulho nacional estava perdido, porque pesavam mais os comodismos e 
os medos, as ideologias e as diferen^as. 

Quando, do lado alemao, come^ou o bombardeamento do sector portugues, o moral dos 
soldados caiu a vertical ate bear de rastos. E verdade que as trincheiras e o terreno nao 
ofereciam condi^oes para uma resistencia condigna; e verdade que os efectivos estavam 
desfalcados; e verdade que as liga^oes com os escaloes superiores estavam destruidas; e 
verdade, que o bombardeamento foi assustador; e verdade, que estava nevoeiro e os gases 
mortiferos foram determinantes (ERAGA, 2010, 397-438). Tudo e verdade, mas tambem e 
verdade que os soldados e grande parte dos obciais quis sob reviver ao holocausto no mo- 
mento do ataque. Eugiram desordenadamente para a retaguarda sem oferecerem resisten- 
cia digna desse nome; deixaram-se fazer prisioneiros por julgarem ser melhor o cativeiro 
do que a guerra. E porque tudo foi verdade, as divisoes alemas, sabedoras da pouca resis- 
tencia que iam ter na frente portuguesa, atacaram nos flancos da 2.“ divisao, obrigando 
portugueses e britanicos a abrirem/endas por onde a infantaria germanica avanqou, cer- 
cando uma grande parte das tropas nacionais que estava em primeiras linhas, nas trinchei¬ 
ras desfeitas entre a terra de ninguem e os quarteis-generals das brigadas. Esta manobra 
tactica ja havia resultado anteriormente em ataques feitos nas jun^oes dos exercitos fran- 
ceses e britanicos e, com maioria de razao, resultaria, tal como resultou, nos dois flancos 
que ligavam a desgastada e desmoralizada divisao portuguesa as divisoes britanicas. 

Neste dia tenebroso para o CEP, morreram quase quatro centenas de militares - o que, para 
a for^a do ataque, foi ridiculo - e bcaram prisioneiros mais de seis mil homens. Os que 
escaparam, por ja estarem na retaguarda - 1. “ divisao do CEP - ou por terem fugido, acaba - 
ram formando unidades de infantaria, que os britanicos desprezaram e mandaram que fos- 
sem utilizadas como trabalhadores de enxada, picareta e pa. Nao mais iriam ser chamados 
a defender ou a atacar qualquer tipo de inimigo. Sujeitavam-se a sofrer os resultados dos 
bombardeamentos mais longos das pe^as da artilharia germanica ou dos aeroplanos que os 


128 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


sobrevoavam. Dadas as caracteristicas sociais e culturais destes militares e, acima de tudo, 
a falta de brio da maioria dos obciais que os enquadravam, os soldados conformaram-se 
e, de certo modo, agradeceram os trabalhos que Ihes foram atribuidos. Tratava-se, abnal 
do retorno a um conforto quase semelhante ao da ausencia do estado de guerra. Ao mes- 
mo tempo, o Alto Comando Britanico via conseguido o mais profundo desejo da politica 
prosseguida, em Londres, relativamente a Portugal. E, com a devida cautela, a Historia iria 
atribuir todas as culpas do destino do CEP aos Portugueses, enquanto militares e enquanto 
politicos. E, neste particular, o Alto Comando nao se enganou, pois, ate agora, cem anos 
passados, e em Portugal, por parte de certos historiadores portugueses, continua a apon- 
tar-se como errada a beligerancia nacional na Grande Guerra - gente que, se tivesse vivido 
nos primeiros anos da Expansao, no seculo XV, condenaria, quase pela certa, a passagem 
do Cabo Bojador. 

Sabemos, olhando os factos, que apos Agosto de 1918, quando tomou posse do comando 
do CEP o general Tomas Garcia Rosado, um monarquico convicto, mandado por Sidonio 
Pais - o novo senhor da politica portuguesa - a situa^ao come^ou a mudar, em Eran^a. O 
novo comandante quis, com os soldados que restavam, apoiado nos obciais crentes nos 
fundamentos da beligerancia portuguesa, formar unidades combat entes e faze-las integrar 
em grandes unidades britanicas para que, ao menos, ao calarem-se as armas no campo de 
batalha, la estivessem soldados de Portugal. 

Tentemos perceber e explicar este fenomeno militar dentro de todo o contexto ja antes 
abordado (PRAGA, 2010, 535-585). 

Tomas Garcia Rosado, era, em 1918, Chefe do Estado-Maior do Exercito Portugues e co- 
nhecido monarquico, mas, fundamentalmente, militar por excelencia, com a inteligen- 
cia necessaria para perceber e distinguir o importante do acessorio. Era um obcial com as 
habilita^oes de estado-maior, ou seja, uma elasticidade intelectual superior a do anterior 
comandante do GEP, general Tamagnini de Abreu e Silva; era um militar que sabia viver 
bem e comodamente nos circulos de intriga da alta roda politica e castrense; era um ho- 
mem com larga experiencia de comando e, ao mesmo tempo, sem o deslumbramento da 
importancia dos cargos para que havia sido chamado a cumprir. Como monarquico, sabia 
perfeitamente quanto o ex-rei D. Manuel 11 tinha defendido a beligerancia portuguesa na 
Grande Guerra. 

O general Garcia Rosado so precisava de encontrar entre a obcialidade, ainda em Eran^a, 
aqueles em quern poderia apoiar-se para levar a cabo o arranque bnal, ou seja, a recupe- 
ragdo possivel dos efeitos nefastos do comeqo da batalha de La Lys. Havia-os, escapados 
a fiiria repressiva de Sidonio Pais, e foram esses que se ofereceram para enquadrarem as 
tropas de quatro batalhoes de infantaria. Eram poucos, mas bons e empenhados nas razoes 
profundas da beligerancia. 

O general Garcia Rosado ja havia dado provas de nao aceitar qualquer subordina^ao a 
vontade militar e, ate, politica da Gra-Bretanha, uma vez que, antes de assumir o comando 
do GEP, esteve em negocia^oes no War Office, impondo a sua determinada vontade - as 
vezes, em oposi^ao a titubeante vontade de Sidonio Pais - sob re o que e como devia ser a 
colabora^ao militar do CEP renovado somente com as tropas exist entes em Eran^a. Era de 
prever que, uma vez assumido o comando, fossem bem debnidas as suas ordens para se 
alcan^ar o objectivo pretendido. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


129 


Quando os soldados souberam das inten^oes do novo comandante, tal como era de espe- 
rar, revoltaram-se; ao principio, nao foram alem de uma resistencia passiva eom pequena 
dose de agressividade, mas, ja em Setembro, quando veribearam que era inabalavel a von- 
tade de os mandar para a frente de eombate, nao tiveram qualquer relutancia em pegar em 
armas e oferecer resisteneia as ordens. Face a esta atitude bem clara e debnida, a reac^ao, 
com conhecimento e autoriza^ao do general Rosado, foi tambem violenta, levando a que 
fossem metralhados os insubmissos soldados. Todos se conformaram a cumprir as ordens, 
pois, veribearam que tambem nao seriam poupados as balas dos sens camaradas (FRAGA, 
2003, 39-65). 

Estava vencido o entorpecimento das tropas operacionais em Franca. Continuavam a 
achar-se abandonadas, desoladas com a Patria e com os politicos, mas acharam quern os 
enquadrasse e bzesse deles o minimo que deles se poderia esperar. 

Exposto deste modo menos vulgar o que foi e porque foi o dia 9 de Abril de 1918, resta- 
nos tecer as considera^oes bnais, para chegarmos as conclusoes a que nos propusemos no 
inicio deste trabalho. 


Corolario da batalha de La Lys 

O imediato pos-batalha gerou - como, alias, tinha de e deveria gerar - um clima de anti- 
derrota em Portugal. La Lys tornou-se na razao de exalta^ao dos herois. Durante anos, a 
9 de Abril, faziam-se dois minutos de silencio em todo o pais, recordando o sacribcio dos 
soldados portugueses. Houve como que uma sacralizagao desse dia doloroso. Ele passou 
a adornar o panteao das glorias nacionais de tal modo que, de uma derrota, se transmutou 
numa quase vitoria. Os combatentes apegaram-se a data e reviram nela todos os seus sa- 
crificios. Esqueceu-se a relutancia de ir para a guerra, porque a guerra fez esquecer como 
o soldado andnimo nela se tinha comportado. Importante era ter estado na guerra. E isto 
entrou no imaginario da epoca - anos vinte e trinta do seculo XX - e por la bcou. Mas, ao 
mesmo tempo, cresceu, nas familias, um outro culto: o do padecimento dos seus soldados. 
Soldados mortos, estropiados, antigos prisioneiros, gaseados ou simplesmente sacribca- 
dos. E desse relicario de memorias que, neste centenario, se estao a arrancar depoimentos 
para, qui^a, relatar uma outra Historia bem diferente da Historia politica e diplomatica, 
que temos vindo a explorar ao longo de anos, gramas a estudos cuidados e apurados. Mas, 
ao menos, que uma nao deturpe a outra! 

Este imaginario pos-batalha acabou por ir morrendo nos anos da 2.“ Guerra Mundial 
e nos que se Ihe seguiram ate ser suplantado pela guerra colonial. E nao se tratou de um 
acaso, mas de uma manobra bem feita pelo Estado Novo. Salazar nunca poderia ser o heroi 
da suposta neutralidade por ele alcan^ada para defesa e tranquilidade dos Portugueses se 
subsistisse a saga da beligerancia na Grande Guerra. Foi este fendmeno que se nao conse- 
guiu resolver e evidenciar nas comemoraqoes do primeiro centenario desse acontecimento 
mundial. Era preciso por de lado as lamurias dos combatentes e desmascarar a politica 
que abafou a beligerancia na Grande Guerra. Havia que a explicar, evidenciando o quanto 
foi distorcida a Historia da l.“ Republica pela Historia do Estado Novo, porque uma e con- 
sequencia da outra e, cada uma, procura a gloria da sua Historia, quando, abnal, nao tern 


130 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


que haver gloria, mas sim realidade e verdade explicada melhor do que ela foi apreendida 
por aqueles que viveram os factos. 

Mas a Grande Guerra teve consequencias efectivamente profundas no estado sanitario 
dos Portugueses. Teve, porque de Franca - e tambem de Mozambique - vieram estropia- 
dos, invalidos - 5738 homens incapazes de angariar meios de subsistencia -, doentes com 
males cronicos e, um dos mais notaveis - que foi erradamente identibcado como efeito dos 
gases - desconhecido, entao, o stress pos-traumatico. Quase todas as familias em Portugal 
tiveram um combatente que veio a sofrer, em termos de saiide, de alguma forma, os efeitos 
da ida a Franca. 

Culturalmente, teremos varias vertentes a explorar. Comecemos pelas mais populares e 
menos eruditas. 

Entraram no falar portugues algumas palavras trazidas pelos soldados: estamine (esta- 
minet), ir aos arames e, mais vulgares, sem altera^ao, madame, mademoiselle, fiance e 
algumas outras que ja se perderam. 

Fizeram-se canzoes, pezas de teato de revista e, ate, alguns dramas, mas foi no ambito 
memorialista que a literatura mais recolheu informazoes da guerra e da batalha de La Lys. 
Nao foram so obciais a escrever; tambem bzeram diarios de campanha alguns sargentos 
e prazas. No fundo, cada qual descreveu o que sentiu da forma como encarou a campa¬ 
nha. Os casos de exaltazao vieram de homens com experiencia literaria, tais como Jaime 
Cortesao e Augusto Casimiro; mas, em termos de pratica diarista, sobressai, agora - por 
ter vindo a publico neste centenario -, o Diario de Campanha do general Tamagnini de 
Abreu e Silva (BORGES; MARQUES; DIAS, 2018) onde se podem ler as considerazoes do 
comandante do CEP e, com imparcialidade, perceber o quanto Ihe faltava de capacidade de 
manobra para lidar com a obcialidade sem ser na base da disciplina cega e da obediencia 
sem reticencias; era um militar de linha e jamais um obcial de alta graduazao. No coman- 
do, tinha a postura do capitao ou, no maximo, do major, que determina como quer, mas 
nao e capaz de discernir para alem da ordem. Tambem ele se lamenta da pouca sorte que 
teve e de incompreensoes dos responsaveis de Lisboa, mostrando toda a sua incapacidade 
para lidar com situazoes burocraticas e humanas complexas quando ha em jogo interesses 
contraditdrios. O mvel de lamentos e a qualidade de escrita e que o distingue dos diarios de 
alguns soldados mais letrados. 

Se e certo que este tipo de influencia cultural afectou ou teve efeitos sobre quern sabia ler 
e compreender o que lia, nao e menos verdade que a simples convivencia com estrangeiros 
- militares britanicos e civis franceses - tambem exerceu modibcazoes nos nossos solda¬ 
dos, sargentos e obciais, safdos de uma cultura fechada, obscurantista, clerical e avessa a 
modernidade, pois, projectados para uma sociedade habituada a comportamentos liberals, 
onde o indivfduo se sentia mais livre e menos culpado dos sens actos, isso tera aberto novas 
perspectivas aos nossos expedicionarios. Perspectivas, e certo, que nao terao frutibcado 
nos ambientes rurais onde voltaram, mas que terao criado algumas raizes entre os que re- 
tornaram ou se bxaram nas cidades mais evolmdas. 

Politicamente, La Lys e a campanha militar em Franza foram de importancia capital na 
evoluzao dos acontecimentos em Portugal. Os obciais e sargentos perceberam que traziam 
uma mais-valia extraordinaria, pois haviam corrido riscos para satisfazer um objectivo 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


131 


politico e, assim, pensavam, era-lhes permitido intervir na gestao do pals. E intervieram, 
por mais de uma vez. A derradeira foi comandada pelo mesmo general que eomandou a 2.“ 
divisao portuguesa em La Lys e deu origem a uma ditadura ultraconservadora com dura^ao 
de mais de quarenta anos. Contudo, para se perceber melhor essa tendencia para a inter- 
ven^ao politica, e necessario ir mais alem do que o golpe de estado de 28 de Maio de 1926. 
Com efeito, Salazar, no eome^o da deeada de trinta do seculo passado, soube manobrar 
eonvenientemente eom os obeiais militares que sobraram da Grande Guerra, afastando 
uns, os mais irredutiveis defensores da 1.“ Repiiblica, ou absorvendo outros para a causa 
do Estado Novo, atraves de prebendas ou altos eargos em empresas onde o Estado tinha 
ae^oes ou era mesmo o linico proprietario. Esta habilidade em lidar eom tal tipo de oft- 
eiais eonstituiu, ao cabo e ao resto, um exercicio politieo para ambas as partes: Salazar e os 
benebciados, pois, houve sempre uma cedencia que foi jogada de parte a parte: os antigos 
eombatentes nao interferiam na condu^ao politiea e o Presidente do Gonselho de Ministros 
nao reduzia os benebcios nem incomodava as memorias dos militares. Pode dizer-se que 
esta influencia durou ate ao comeqo da 2.“ Guerra Mundial, data em que ou ja tinha mor- 
rido a maior parte destes obeiais antigos eombatentes ou ja estavam velhos demais para 
fazerem correr qualquer risco a nova situaqao politica. 

Como acabamos de mostrar, a sombra de La Lys e dos eombatentes da Elandres esten- 
deu-se, temporalmente e em termos politicos, por mais de duas deeadas. E tudo sempre 
a custa da pratica de uma politica de defesa de princlpios monollticos e obscurantistas. 
Eoi este Exercito que serviu de pilar e deu apoio a ditadura. Curiosamente, o Exercito e a 
obcialidade nascidos do Estado Novo, quando Salazar entendeu que a questao colonial se 
resolveria pela for^a das armas, em Abril de 1961, come^ou por tentar afastar o ditador, 
procurando uma solu^ao diferente daquela que Ihe era imposta (VALENQA, 1976); treze 
anos mais tarde, o Exercito nascido da guerra colonial, ja sem compromissos com qualquer 
outra guerra, derrubou a ditadura intitula Estado Novo e fez surgir o reencontro historieo 
com os ideais de democracia, liberdade e modernidade da l.“ Republiea. 

Concluimos, assim, que o ciclo iniciado com o Governo Provisdrio da 1.“ Republiea e in- 
centivado pela beligerancia, na Grande Guerra, so se fechou eom um golpe militar efecti- 
vado muitos anos mais tarde, tendo tido um largo interregno - em quase tudo, semelhante 
a cultura monarquica - entre 1926 e 1974. 


132 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Referencias 


Cortesao, Jaime (1916). Cartilha do Povo: l.°Encontro. Portugal e a Guerra. Porto: Renas- 
cen^a Portuguesa. 

Fraga, Luis Alves de (2003). Guerra &amp; Marginalidade: Os comportamentos das tropas 
portuguesas em Franga: 1917 - 1918. Lisboa: Prefacio. 

Fraga, Luis Alves de (2010). Do Intervencionismo ao Sidonismo: Os dois segmentos da 
poUtica de guerra na 1.“ Republica: 1916-1918. Coimbra: Imprensa da Universidade. 

Fraga, Luis Alves de (2012). O Fim da Ambiguidade: A Estrategia Nacional Portuguesa de 
1914 a 1916. Lisboa: Universidade Autonoma de Lisboa. 

Borges, Joao Vieira; Marques, Isabel Pestana; Dias, Eurieo Gomes (orgs.) (2018). Didrio de 
Gampanha: General Fernando Tamagnini: Gomandante do GEP. Lisboa: Comissao Portu¬ 
guesa de Histdria Militar. 

Valen 9 a, Fernando (1976). As Forgas Armadas e as Grises Nacionais: A Abrilada de 1961. 
Lisboa: Publica^oes Europa-America. 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


133 



The Battle of La Lys: legal, political 
and social questions 

Joao Casqueira Cardoso 

FCHS/University Fernando Pessoa 
CEPESE / ECT 


Abstract: The Battle of La Lys, together with the whole First World War in Europe, is a 
moment that gathers the major Western powers in a conflict that will split Europe into 
two opposing sides, increasing the pre-existing divisions and creating additional ones. The 
war showed the limits of the existing rules, especially with respect to the use of weapons 
prohibited by the customary rules of the alleged civilized world. New weapons are creat¬ 
ed, clearly more harming than the previous ones, on both sides. The Portuguese Army, a 
newcomer on the European military scene, is not prepared for this type of conflict. The 
war also has a political dimension, and the Battle of La Lys is inserted into an adverse inter¬ 
national context for the Portuguese Army. Despite the extensive work done on this issue 
in the last years, several aspects remain unclear as concerns the participation of the Portu¬ 
guese Army the hrst World War. An example of this is the lack of support that the soldiers 
received from the military hierarchy, both Portuguese and British, and the many problems 
concerning the post-conflict situation (in particular the ill-treatment of the Portuguese 
prisoners of war). 

Keywords: Battle of La Lys; Portuguese Army; Humanitarian Issues, First World War. 

Resumo: A batalha de La Lys, juntamente com toda a Primeira guerra mundial na Europa, 
e um momento que reiine as grandes potencias ocidentais num conflito que ira dividir a 
Europa em dois lados opostos, aumentando as divisoes pre-existentes e criando outras. A 
guerra mostrou os limites das regras existentes, especialmente no que diz respeito ao uso 
de armas proibidas pelas regras costumeiras do suposto mundo civilizado. Novas armas sao 
criadas, claramente mais prejudiciais do que as anteriores, em ambos os lados. O Exercito 
Portugues, um recem-chegado no cenario militar europeu, nao esta preparado para este 
tipo de conflito. A guerra tern igualmente uma dimensao politica, e a batalha de La Lys in¬ 
sere-se num contexto internacional adverse para o Exercito Portugues. Apesar do extenso 
trabalho realizado sobre esta questao nos ultimos anos, varies aspetos permanecem incer- 
tos quanto a participa^ao do Exercito Portugues na Primeira guerra mundial. Um exemplo 
disso e a falta de apoio que os soldados receberam da hierarquia militar, tanto portuguesa 
como britanica, e os muitos problemas relatives a situa^ao pos-conflito (em particular o 
mau tratamento dos prisioneiros de guerra portugueses). 

Palavras-chave: Batalha de La Lys; Exercito Portugues; Questoes humanitarias, Primeira 
Guerra mundial. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


13S 


Forewords 


As a health statement, it is important to indicate that this contribution owes enormous¬ 
ly to the research done hy my father, Victor Cardoso, who gathered hrst-hand materials 
from the lihraries in Paris, on the precise issue of this article - including the Library of the 
Foundation Calouste Gulbenkian, and many others, in this congress, we can witness an 
impressive number of exchanges between generations, on an issue that is a collective issue 
for all, but also a very peculiar and interiorized issue for each family, each of them looking 
at this issue for many different reasons too. 

This paper focuses on the factual context of the Portuguese Army involvement in the 
First World War 1, or Great War. The hrst and preliminary point deals with the presence in 
France of the Portuguese Expeditionary Corps (to simplify, the paper refers to the Portu¬ 
guese Army) since the hrst day, and up to April 2018, when the so-called Battle of La Lys 
took place. The second point concerns the international political context, internationally 
relevant, of the Portuguese Army involvement in World War 1. The third point deals with 
three pending issues, both social and legal, that still today, are not completely solved: the 
organization of the Portuguese Army during World War 1; actual number of casualties on 
the Portuguese side; and the question of the Portuguese soldiers in Germany during and 
after the war. 


1. Actual presence in France of the Portuguese Army 

The hrst contingents of the Portuguese Army left Lisbon at the end of February 1917 and 
arrived in Brest. Soldiers, equipment and horses, were transported by train to the north 
of France, where the British sector was located. Before being assigned to an operational 
area, the soldiers would undergo a specihc training in trench hghting in English camps, 
near Aire-sur-la-Lys, between Lille and Bethune, 30 km from the front, it was a quick 
training to teach soldiers how to handle weapons (provided), move through trenches, get 
out of trenches, build shelters and repair the trenches, to cross obstacles, and to watch the 
soldiers on the enemy side, among other aspects. The soldiers were housed among the local 
inhabitants, in the houses, farms and the barns scattered in the countryside. 

The arrival of the Portuguese Army will take place between February 1917 and November 
1917. in November 1917 there were in France 55,600 officers and soldiers of the Portuguese 
Army (infantry and light artillery) integrated into the British Army and 1,350 soldiers of 
the CAPl (abbreviation of Corpo de Artilharia Pesada Independente - Heavy Artillery in¬ 
dependent Corps) integrated into the French Army, under direct French command, in ten 
months, have been transported by sea, from Lisbon, 55,600 men, 8,000 horses, 1,500 cars 
and 300 trucks (Castro Henriques &amp; Rosas Leitao, 2001). After the training that they re¬ 
ceived, the hrst units of the Portuguese Army settled in the trenches in April 1917, and the 
Portuguese infantry quickly participated in military actions. There were serious clashes 
with the German Army in the month of June and August 1917. Portuguese units also partic¬ 
ipated in attacks, launch raids and made prisoners. From April 1917 to 11 November 1918: 
on 55,600 men mobilized, and there will be about 2,100 dead, 5,600 wounded and 7,500 
prisoners. 


136 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


On 5 November 1917, the Portuguese Army took possession of the operational sector as¬ 
signed by the British Command, of which it was fully responsible for. It is an area south 
of the River La Lys, between Armentieres and the canal of La Bassee, with a trench front 
of about 11 km stretching from Fauquissart (to the north) to Festubert (to the south). The 
front line formed a kind of belly on Laventie from the north, went round Neuve Chapelle, 
in the east and then returned to Festubert (see Figure 1.). There were three lines of trench¬ 
es: A, B and C. The instructions received were to resist, in case of attack. “Die on line B”, as 
the British Command used to say. 



Figure 1. Portuguese Expeditionary Corps sector, April 1918 
(Source: Pestana Marques, 2008, p. 377) 


This triangular-shaped area had already been dug and fortihed by the British troops during 
the 1914 and 1915 battles, with thousands of losses of lives on both sides. In particular, the 
Canadian Army was there on that position in the months. The area was a flat, humid and 
swampy area, and militarily not “hot”, because the nature of the soil made the attacks 
difficult during the winter months. The Portuguese soldiers settled in the trenches, the 
underground shelters and the ruins of the villages. Senior officers stayed at the manoir of 
La Peylouse, at Saint-Venant, 15 km behind the front. 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


137 






2. International Context of the Portuguese 
Army involvement in World War I 

At the end of the year 1917, three internationally relevant events will have signiheant con- 
sequenees on the aftermath of the war situation, and they are important to understand the 
eontext in which the Portuguese Army will get involved in the conflict in Europe. 

The hrst is the entry into the war of the United States of America. Exceeded hy the fact 
that Germany was sinking its ships, based on the argument that the United States were 
providing armament to its enemies (it was the argument used to sink the liner Lusita¬ 
nia, for instance, on 7 May 1915, a disaster that killed almost all of the 139 US citizens on 
hoard), the United States eventually declared war on Germany on 6 April 1917. The hrst 
troops arrived in Erance in summer of 1917. General John Pershing, who commanded the 
American Expeditionary Corps, had chosen Saint-Nazaire as the landing hase, and it was 
in that port that the hrst buildings of a convoy from New York were built on 26 June 1917, 
and brought at hrst 14,750 soldiers. The US Army will progressively install large logistical 
bases, allowing troop landings to accelerate (78,000 US soldiers present in Erance in early 
November, and nearly 150,000 soldiers at 31 December 1917) (Bonnefoi, 2007) (we must 
bear in mind that the sector allocated to the American Army is not in the north of Erance). 
At that point, the German government knows that it has little time, just a few months, to 
try to change the course of the war before the growing arrival of the American soldiers on 
the battlehelds. 

The second event is the separate peace that was made between Germany and Russia. 
After the October Revolution, Lenin wanted to get out of the war quickly. The Congress of 
the Soviets voted for peace on 26 October 1917. Russia and Germany signed an armistice 
on 15 December 1917. The Treaty of Brest-Litovsk, which will regularize this situation, was 
signed on 3 March 1918. Without enemy to the east, Germany will be able to transfer to the 
West, to Erance, the about 50 divisions now freed, and the armament mobilized until then 
against Russia. 

The third event is the Portuguese coup d’Etat of 5 December 1917, in Lisbon, when Sido- 
nio Pais took over the power. A former ambassador to Berlin between August 1912 and 
March 1916, he was sincerely fascinated by the German culture, its ordered society and 
its level of development. In fact, he was a true germanophile (cf. Nunes Ramalho, 1998). 
It can be legitimately said that he was an admirer of Germany, but that he also regretted 
bitterly the declaration of war against Portugal. It can be said that he did not shared the 
overall wave of animosity as regards “les boches”, as the term (originating from Prance) 
was used in Portugal. It is also for that reasons that he accepted the reasons of the officers 
that (for more interested or even for other political reasons) were not enthusiastic about 
the participation of Portugal to the war. At the same time, Sidonio Pais did not however 
questioned Portugal’s participation in the British side. But from the month of December 
1917, only two ships will leave from Lisbon to Prance, with 500 soldiers. The replacement 
of the Portuguese soldiers of Planders is no longer ensured, at least not by the Portuguese 
Army. As there are no more staff renewals, permissions are eliminated, except for the 
officers. Several officers were also called to Lisbon by the new Portuguese government. 
Others also went away under different reasons (leave, sickness) and no longer return, be- 


138 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


ing sent to other units. Sidonio Pais halted the war machine set up hy its predecessors'". 
Spain, a monarchy that has remained neutral (and which does not see the establishment of 
the Republic in Portugal with sympathy), ad prohibited the passage of Portuguese troops 
through its territory. Add to this that the United Kingdom uses its Navy to transport by 
priority the American troops. 


3. Three pending issues 

The hrst issue is the organization of the Portuguese Army in the context of the Great War, 
and in particular the capacity of the Portuguese Army to cope with the conditions of life 
and combat, in the helds of Flanders. As underlined, the German Army had been rein¬ 
forced in early 2018. The battle of La Lys is part of the 1918 so-called Kaiserschlacht (or 
simply Ludendorff Offensive), that started on 21 March 1918 and included the offensives 
Michael, Georgette, Gneisenau, and Bliicher-Yorck (Zabecki, 2006). Such offensives were 
massive ones. To face this, the Portuguese Army, which was exhausted and to be replaced 
by British troops, was especially weak. Yves Leonard, in his Histoire du Portugal contem- 
porain [History of Contemporary Portugal], evocates the fact that the Portuguese Army 
was sent the front under British command with no real possibility of organizing the re¬ 
placement, that it was subject to some sixty assaults and twenty bombardments, until the 
“great battle of La Lys” (Leonard, 2016, p. 56). 

The Portuguese soldiers were also desperate, revolted by the lack of command; some 
deserted, others will go as far as suicide; and only a few under officers dare to report the 
situation, by fear of being sanctioned. Some practical details were completely inappropri¬ 
ate to the war. In her book, Maria Joao Oliveira voluntarily silenced which was the Portu¬ 
guese battalion that used sheep wool vests, and was welcomed by ridiculous louds of “baa 
baa” by the German Army on the other side of the trenched (Oliveira, 2017, p. 29). The 
clothes of the soldiers, namely jackets collars, were not adequate to the gas masks. Gen¬ 
erally speaking the Portuguese soldiers were little prepared for the new weapons that the 
German Army used - namely the new Bergmann MP18 (or Machinenpistol 18 /l), a concept 
weapon made for trench fighting (Figure 2). 


The predecessors were as follows: President of the Republic Machado de Assis, the President of the Council Afonso 
Costa and the War Minister, General Norton of Matos. After 5 December, Machado de Assis and Norton de Matos 
went into exile and Afonso Costa was imprisoned in Porto, before being released. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


139 




Figure 2. World War I German soldier with a MP18 in Northern Franee. 

(Source: Public domain) 

In general, both on the British and the Portuguese sides, there were problems of organiza¬ 
tion. The British eommand did not manage well the replacement of the Portuguese troops, 
in April 1918, leaving the Portuguese Army 2nd division isolated on an open held, with 11 
kms to control. The senior officers, with a few exceptions (among them. Colonel [and then 
General] Manuel Gomes da Costa, who commanded the 2"'* division), lived in the back and 
had virtually no contact with the troops. The officers went on permissions in the towns of 
the region or on the beaches located 60 km from the front, made trips of a week in Paris 
or went to Portugal with a medical certificate and did not return. The soldiers experienced 
this as a great injustice, as the officer Andre Brun reported after the war (Brun, 1919). in 
the meantime, questions about the organisation of the Portuguese Expeditionary Corp and 
its eventual integration in the English army were being discussed in Tisbon. in general, 
and as far as the organization of the Portuguese Army is concerned, most of the sources 
agree on the unpreparedness, and a perfect cocktail for a disaster (Cf. for instance. Gomes 
da Gosta, 1920). 

The second pending issue is the actual number of casualties on the Portuguese side along 
the conflict. 

The figures concerning the Portuguese Army losses of this first day of the Battle of the Tys 
are controversial, because it is easy to mix the number of deaths, wounded, prisoners and 


140 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 





missing soldiers. Manuel do Nascimento speaks of 1,400 dead, more than 4,600 wounded, 
about 2,000 missing and 7,700 prisoners (Do Nascimento, 2014). These hgures seem a little 
exaggerated. Henrique Manuel Gomes da Cruz speaks of 423 dead, “thousands” of wound¬ 
ed and 6,000 prisoners (Gomes da Cruz, 2014). As for Isabel Pestana Marques, she esti¬ 
mates that there were only 300 dead and 6,000 prisoners that morning (Pestana Marques, 
2008). Maria Jose Monteiro de Oliveira talks about 398 dead and 6585 prisoners (2011). 
Martins Castro Henrique and Antonio Rosas Leitao consider that there were 398 dead and 
6,585 prisoners on 9 April 1918 (Castro Henrique &amp; Rosas Leitao, 2001). These authors es¬ 
timate that the number of wounded should not be less than 1,500, but consider it diffi¬ 
cult to establish the exact number, as there were also wounded among the prisoners. Yves 
Leonard estimates the losses of the Battle of the Lys to 1,500 dead, 5,000 wounded, 2,000 
missing and 8,000 prisoners (Leonard, 2016). What is certain is that on 9 April 1918, in a 
few hours, in the plain of the Lys, nearly 15,000 men of the 2nd Portuguese Division were 
put hors-de-combat, either dead, wounded, prisoners or missing. As for Filipe Ribeiro de 
Meneses, he indicates the loss of 916 men (25 officers, 35 sergeants and 856 soldiers), and 
adds two sergeants 2 and 58 soldiers killed by gas weapons (Ribeiro de Meneses, 2000, p. 
268). Finally, Aniceto Afonso and Carlos de Matos Gomes refer 398 dead (29 officers and 
369 soldiers), and 6.585 prisoners (270 officers and 6.315 soldiers) (Afonso &amp; Matos Gomes, 
2010, p. 418). As we can observe, the overall number is extremely difficult to determine, 
and it only shows that no complete records have been established during of after the war, 
on this specific aspect. 

The Battle of La Lys is not only the 9 April 1918, of course. It is set of battles that lasted 
three weeks, involving the German and the British Army (where have been reintegrat¬ 
ed the Portuguese Army after 9 April 1918), on a territory located between Bethune, Ha- 
zebrouck and Ypres, to the north and south of the Riviere La Lys. The German Imperial 
Army had move forward from 8 to 13 km, crossed the river La Lys, but from the first day 
it had not achieved its objectives and did not succeed in taking Bethune and Hazebrouck. 
It launched attacks, with new reinforcements, at north of the Lys, but again it will not be 
able to take Ypres, defended by the British Army and reinforcements sent by the French 
Army. It took Messines and Bailleul, occupied a few strategic hills after very hard fights 
and bombardments that made many deaths on both sides, but could not go further, espe¬ 
cially because the troops were exhausted. On 29 April, the Battle of the Lys (the Georgette 
Offensive) was over, and it lost by the German Army, as it did not break the line where it 
had planned to do so. 

The British troops had lost 236,000 soldiers, the French 92,000 soldiers and the Germans 
348,000 soldiers. The structure of the casualties for this set of battles is quite peculiar: rela¬ 
tively few deaths (10-15%), a large number of missing soldiers (buried under terrible bom¬ 
bardments) and many prisoners. It was no longer a trench war, in fact, but it was rather a 
war of movement, with raids or surprise attacks, ground attacks which were preceded by 
intensive bombardments. This explains the large number of prisoners on both sides during 
these three weeks of fighting. When the whole Offensive Georgette was over, the Portu¬ 
guese contingent would have about 2,000 dead, 6,000 wounded, 7,000 prisoners, as well 
as hundreds of missing soldiers, a little over 15,000 men (Do Nascimento, 2008). Despite 
the lack of data, one thing is for sure: most of the dead, wounded and Portuguese prisoners 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


141 


of the Great War come from the Battle of the Lys, and especially from this terrihle day of 9 
April 1918. 


4. The question the Portuguese soldiers in 
Germany during and after the war. 

There is a part of history that is almost forgotten: the fate of the Portuguese soldiers that 
remained in Germany. During the war, the German Army captured nearly 7,500 Portu¬ 
guese soldiers. Most of them were made prisoner at the Battle of La Lys. About 7,000 were 
taken to Germany and dispersed in various camps of prisoners that had been arranged for 
this purpose. The officers were fairly well treated. Indeed, they have suffered from hunger, 
like all prisoners, but also like all the German population in the last months of the war. 
Officers were grouped in Breesen (in the Germany northeaster region of Meckenburg) and 
rather well settled. They were not obliged to work and received a stipend, which allowed 
them to make small food purchases. On the other hand, the captivity of the simple soldiers 
was hard and their living conditions were difficult: facilities often precarious, and they 
did not only suffered from hunger and cold weather, but also ill-treatment, poor sanitary 
conditions, forced labour, punishments, and prison, among other things. Out of 7,000 
prisoners taken to Germany, 233 died in captivity. 

The treatment that Germany inflicted on thousands of Portuguese prisoners of war for 
eight months was a blatant violation of the International Humanitarian Law. After their 
capture, the soldiers of the Portuguese Army lost all that was worth: watches, money, 
clothes, shoes, rings, and food. Military registration plates were also taken, which had un¬ 
fortunate consequences for their identification. Some of the under officers who were taken 
the military plates were unable to prove their rank and were forced to work alongside the 
simple soldiers. They recorded in writing their conditions of detention and the duties they 
were required to perform. Near La Lys, before leaving for Germany, prisoners were used as 
manpower for the transport of munitions, the digging and repair of the trenches, and the 
burial of the German dead, in Germany, the prisoners were used as labour force for agri¬ 
culture and industrial work, but also for work which was part of the war effort: transport 
of munitions, work in foundries and weapons factories. The use of prisoners of war for the 
execution of this kind of work was formally prohibited by international conventions. 

The Portuguese delegate of the Prisoners of War Service, Major Pestana de Vasconcellos, 
who was sent to Germany after the Armistice to deal with the repatriation of prisoners, 
recorded in a report the legal violations observed (Monteiro de Oliveira, 2011). in 1919, at 
the express request of the Portuguese delegate to the Peace Conference in Versailles (Janu¬ 
ary to June 1919), Magistrate Pedro Costa was tasked with finding and interrogating former 
Prisoners of War in Germany. His report contains 52 testimonies, it is an authoritative, 
irrefutable document on the violations of international law by the German government. 
The Portuguese hoped to present it to the Reparations Committee of the Peace Conference 
in order to obtain financial compensation, at least to pay the pensions of the war widows 
and wheelchairs. The question was barely addressed by the Committee and the Portuguese 
delegation received absolutely nothing. One wonders, in fact, how little the consequences 
of these violations of international conventions would represent in the face of the millions 


142 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


of dead and the eities destroyed. At the Peace Conference, the prohlem of war crimes was 
addressed. The German war criminals were to he judged hy the allied military courts. But 
there was no agreement on a list of them. Finally, in 1920, the British Prime Minister suc¬ 
ceeded to convince the French and Belgian governments of an agreement under which the 
German war criminals could he tried hy the military and judicial authorities of their own 
country. The hrst trial opened in Leipzig in May 1921, hut the accused were either acquitted 
or sentenced to symbolic sanctions. France and Belgium then decided to prosecute war 
criminals at home, according to the initial arrangements laid down at Versailles. Between 
1922 and 1925, more than 2,000 procedures were initiated and resulted in convictions in 
absentia (Cochet &amp; Porte, 2008). 

The Armistice signed at Rethondes on 11 November 1918 provided for the immediate re¬ 
lease of all prisoners of war. France and England took immediate steps to bring their sol¬ 
diers prisoners of war in Germany. The Portuguese Government was, however, remarkable 
by its inertia and its absence in this area. Soldiers captured in the Flanders had been spread 
over dozens of prison camps, and this dispersal undoubtedly complicated the work of re¬ 
patriation. Left to themselves, many prisoners fled the internment camps and joined the 
Dutch border in search of a boat, living on a day-to-day basis from the assistance provided 
by the population of the crossed areas. Some soldiers even went home on foot. Without 
any news from Lisbon on this issue, a committee of officers went to Berlin and negotiated 
directly with the German authorities the conditions for their repatriation. They returned 
to Portugal through the Netherlands. The delegate of the Service of the Prisoners of War 
arrived in Germany, in order to deal with the repatriations, only at the end of the month of 
December 1918 (Monteiro de Oliveira, 2011). It is true that the political situation in Portugal 
remained complicated at that time, and that the President of the Republic, Sidonio Pais, 
had just been assassinated on 14 December 1918. 

Arriving in Portugal in the months of January and February 1919, the Portuguese pris¬ 
oners of war were received in Lisbon in an almost generalized indifference, both from the 
government and the population. The soldiers sometimes waited for hours in the boats or 
on the docks before someone came to tell them what they had to do. They were finally sent 
to their units, including the wounded soldiers, before demobilizing them. In the face of 
the inertia of the authorities, it is a non-governmental organization, the Liga dos Combat- 
entes, which took care of the sick and of the wounded, as well as of the elementary needs 
of former soldiers. 

The Armistice of 11 November 1918 was celebrated in Lisbon as a great victory. General 
Garcia Rosado, who had replaced General Tamagnini in August 1918, returned to Portugal 
covered with glory and then made a great career as Portuguese Ambassador to London. 
The then General Gomes da Costa will be considered the true hero of the French campaign 
and will receive the highest decorations. Leaving Cherbourg, the first Portuguese soldiers 
arrived in Lisbon on 23 November 1918, but many of the soldiers will still spend a winter 
in Flanders, removing the war infrastructure and burying the dead. The last soldiers of the 
Portuguese Expeditionary Corp will return to Portugal only in March 1919 (Monteiro de 
Oliveira, 2011). On 14 July 1919, 400 Portuguese soldiers participated in the victory parade 
in Paris under British command, on the Champs-Elysees. Exactly one year before, the city 
of Paris decided to change the name of a street in the 16th arrondissement to call it “Ave¬ 
nue des Portugais” (Figure 3.) 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


143 


• Le 14 Juillel 1918 • 

le Conseil Municipal de Paris decidail ^ue IdHcienne 

Avenue de Sofia Scippelerail desormais Avenue des Porlugais 
Un horn mage dail ainsi lendii aiix 30.000 soldals Porlugais 
qui coinbdUdienlaux coles des forces alliees 
pour la liberie de la France 
70 ans plus lard Le-25 Juin 1988 
Son Excellence iXmbassadeur dii Porliigal 
el Monsieur le Sccrelaire d Elal 
aux Anciens Coinballanls 
oiiL rappele ensemble celle page de la Memeire cemmime 

• des deux Naliens Ptrlugaise el Fraii^aise. e 

. -'i 


Figure 3. Commemorative plaque of the Avenue des Portugais in Paris 


Yet, in Portugal, the partieipation of soldiers in the hghting of Flanders did not galvanize 
the eountry, did not ereate a patriotie impetus. The sacred union that the Repuhlican gov¬ 
ernment hoped did not happened. The country continued to sink into crisis, divisions 
and political quarrels. Thinking of the soldiers of the Portuguese Expeditionary Corps in 
France, one cannot but think of the terrible expression written by the Filipe Ribeiro de 
Meneses: “The history of Portuguese participation in the Great War is marked by reckless 
gestures, by improvisations and disagreements at the highest level.” (Ribeiro de Meneses, 
2015, p. 45 ). This is certainly the greatest defeat of the Battle of the Lys. 


144 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 




References 


Afonso, Aniceto &amp; Matos Gomes, Carlos de (2010). Portugal e a Grande Guerra 1914-1918. 
Matosinhos: Quidnovi. 

Bonnefoi, Nadine (2007). Revue Les Ghemins de la Memoire, n° 168, Janvier, MINDEF/ 
SGA/DMPA. 

Brun, Andre (1919). A Malta das Trincheiras - Migalhas da Grande Guerra - 1917-1918. 
Lisboa: Guimaraes &amp; C.“ Editora. 

Castro Henriques, Mendo; Rosas Leitao, Antonio (2001). La Lys - Os Soldados Desconhe- 
cidos. Lisboa: Prefacio. 

Coehet, Fran 9 ois &amp; Porte, Remy (2008). Dictionnaire de la Grande Guerre 1914-1918. Par¬ 
is: Robert Laffont, Colleetion Bouquins. 

Do Naseimento, Manuel (2008). La Lys - Devoir de Memoire. Paris: L’Harmattan, Edition 
bilingue fran^ais/portugais. 

Do Naseimento, Manuel (2014). Premiere Guerre mondiale - Les soldats portugais dans 
les tranchees de Flandre. Paris: L’Harmattan. 

Gomes da Costa, Manuel (1920). O Gorpo de Exercito portugues na Grande Guerra - A 
Batalha de La Lys 9 de Abril de 1918. Porto: Editores Renascen^a Portuguesa. 

Gomes da Cruz, Henrique Manuel (2014). Portugal na Grande Guerra: a Gonstrugao do 

mito de La Lys na imprensa escrita between 1918 e 1940. Lisbon: FCSH/DH (Master in 

Contemporary History), available at: http://hdl.handle.net/10362/13813 

Leonard, Yves (2016). Histoire du Portugal contemporain de 1890 a nos jours. Paris: 

Chandeigne. 

Monteiro de Oliveira, Maria Jose (2011). Deste triste viver - Memorias dos prisioneiros de 
guerra portugueses naprimeira Guerra Mundial. Lisbon: FCSH/DH (Master in Contem¬ 
porary History), available at: http://hdl.handle.net/10362/7324 

Nunes Ramalho, Miguel (1998). Siddnio Pais: Diplomata e Gonspirador (1912-1917). Lis¬ 
boa: Cosmos. 

Pestana Marques, Isabel (2008). Das Trincheiras, com saudade - A vida quotidiana dos 
militares portugueses na Primeira Guerra Mundial. Lisboa: A Esfera dos Livros. 

Ribeiro de Meneses, Filipe (2000). Unido Sagrada e Sidonismo - Portugal em Guerra 
(1916-18). Lisboa: Cosmos. 

Ribeiro de Meneses, Filipe (2015). A Grande Guerra de Afonso Gosta. Lisboa: Dom Quixote. 
Zabeeki, David T. (2006) The German 1918 Offensives. A Case Study in the Operational 
Level of War. London: Routledge. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


145 



La participation du Portugal dans 
la Premiere Guerre mondiale 

Manuel do Nascimento"*^ 


Resume: La proclamation de la Republique portugaise fut proclamee le 5 octobre 1910, et 
entralne de changements radicaux dans le pays. La volonte des republicains an pouvoir, 
souhaite legitimer la Republique portugaise elle-meme. La Republique portugaise fut ac- 
cueillie froidement par une grande partie des pays europeens. Le nouveau pouvoir republi- 
cain en place promulgua la separation de I’Eglise et de I’Etat, et reforme le service militaire 
et le rendre obligatoire. L’Angleterre et I’AHemagne signent des accords (dits secrets) pour 
le partage de I’Afrique portugaise. La these pour la defense des colonies portugaises pou- 
vait se reveler exacte pour la toute jeune republique portugaise. La neutralite ambigue du 
Portugal - a la demande de I’Angleterre - est restee une neutralite non declaree, egalement 
sur le plan exterieur (ni neutralite ni belligerance). 

Mots-cles: Republique portugaise, service militaire, politique portuguaise. Premiere guerre 
mondiale. 

Abstract: Eirst Portuguese Republie, military service, Portuguese policy. World War 1. 
The proclamation of the Portuguese Republic was proclaimed on October 5th, 1910 and 
brought about radical changes in the country. The will of Republicans in power, wishes 
to legitimize the Portuguese Republie itself. The Portuguese Republic was coldly wel¬ 
comed by a large part of the European countries. The new republican power in place 
promulgated the separation of church and state, and reformed military service and made 
it compulsory. England and Germany sign so-ealled secret agreements for the sharing of 
Portuguese Africa. The thesis for the defense of the Portuguese colonies could prove true 
for the very young Portuguese Republie. The ambiguous neutrality of Portugal - at the 
request of England - remained an undeclared neutrality, also externally (neither neu¬ 
trality nor belligerence). 

Keywords: Portuguese Republic, Military Service, Portuguese Politics, World War 1. 


Manuel do Nascimeto est ne au Portugal et vit en France depuis 1970. Auteur autodidate de plusieurs ouvrages 
historiques, en langue fran^aise, portugaise ou bilingue portugais/fran(;ais. 11 collabore depuis nombreuses annees 
dans des journaux de la communaute portugaise en France et au Canada, chroniquer on line pour un magazine au 
Portugal. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


147 



La participation, dans le conflit mondiale 1914-1918, des contingents portugais du CEP, est 
meconnue en France. 

La Grande Guerre, plus connue comme Premiere Guerre mondiale, a eclate au debut du 
mois d’aout 1914, pour se terminer le 11 novembre 1918. 

L’imperialisme des nations europeennes est materialise par le traitement de la question 
coloniale. La conference de Berlin de 1885 avail permis le partage de I’Afrique entre les 
puissances europeennes 

Le detonateur du processus diplomatique aboutissant a la guerre est le double assassinat 
de I’archiduc Fran^ois-Ferdinand, heritier du trone d’Autriche-Hongrie, et de son epouse 
morganatique Sophie Ghotek, duchesse d’Hohenberg, a Sarajevo le 28 juin 1914, par un 
etudiant nationaliste serbe de Bosnie 

Des les premiers jours de la guerre, le Portugal avail pris position et offre son concou- 
rs a I’Angleterre. Celle-ci, sure que la guerre n’allait durer que quelques mois, refuse le 
concours du Portugal, pays qu’elle juge faible, et a tout fait pour persuader la toute jeune 
Republique portugaise de rester neutre au conflit. 

En meme temps, les gouvernants portugais de Manuel Arriaga (PR, 1911-1915) et Ber¬ 
nardino Machado (PR, 1915-1917) eprouvent la necessite de participer a ce conflit, mais un 
probleme subsiste! 

Comment convaincre les militaires portugais de se battre en France aux cotes des soldats 
anglais, a cause de I’ultimatum anglais de 1890 (ll janvier) impose aux Portugais par les 
ambitions anglaises en Afrique. 

En 1914, le Portugal est un pays republicain, depuis le 5 octobre 1910. En 1912, et 1913, 
I’Angleterre et I’Allemagne avaient signe des accords dits (secrets) pour le partage des co¬ 
lonies portugaises et des 1914, il y avail une guerre (qui ne portait pas le nom). Le Portugal 
envoya un premier contingent pour proteger la population en Angola (sud) et au Mozam¬ 
bique (sud), contre les attaques allemandes. 

Si la France emporte la victoire de la Marne, en 1914 (5-12 septembre), les horreurs de 
Verdun de 1916 (21 fevrier-19 decembre), et la bataille de Chemin des Dames en 1917 (16 
avril - 24 octobre) reste dans la memoire collective. 

Le 7 aout 1914, le gouvernement portugais, reafhrme au parlement I’alliance anglaise, 
sans que ce dernier sollicite du Portugal I’entree de son allie en guerre, et dans une attitude 
equivoque; pas declaration de neutralite ou de belligerance. 

Les arguments des partisans de I’entree du Portugal: assumer les devoirs de I’alliance 
(1373) entre les deux pays, la defense du patrimoine d’Outre-mer (les colonies portugai¬ 
ses), et avoir la garantie de participer aux conferences de la paix et faire partie de la nou- 
velle organisation internationale. 

L’Allemagne sans aucune declaration de guerre au Portugal, le 25 aout 1914, attaque Ma- 
ziua, a la frontiere nord du Mozambique, et les 17 et 18 octobre attaque Naulila au sud 
d’Angola et le 30 du meme mois attaque Cuangar. 

La premiere expedition militaire est envoyee le octobre pour Angola (Mogdmedes) 
sous le commandement de du colonel Alves Ro^adas. 

L’Angleterre qui avait tout fait pour persuader la toute jeune Republique portugaise de 
rester neutre au conflit, mais deux ans plus tard, apres les boucheries de Ypres, les Anglais 
demandent ofhciellement au Portugal d’entrer en guerre aux cotes des allies. 


148 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


L’Angleterre confrontee au probleme de fret, le 26 fevrier 1916, c’est sous une forte pres- 
sion anglaise pour que le gouvernement portugais bloque dans ses ports plusieurs navi- 
res allemands. L’ambassadeur allemand a Lisbonne depose une declaration de guerre au 
Portugal, le 9 mars 1916. Le gouvernement anglais demande offtciellement au Portugal de 
rejoindre les allies. Le gouvernement portugais accepte la participation du pays a la guerre. 

L’ambassadeur allemand a Lisbonne depose une declaration de guerre au Portugal, le 9 
mars 1916, et quitte Lisbonne. 

Siddnio Pais, diplomate du Portugal a Berlin, quitte egalement ses fonctions diplomati- 
ques et rentre au Portugal. 

La preparation du CEP a Tancos: Les republicains au pouvoir doivent agir tres vite, et en 
trois mois de temps, une armee equipee, entrainee au camp militaire de Tancos, est opera- 
tionnelle (OMilagre de Tancos), le miracle de Tancos. 

Le premier contingent portugais du C.E.P embarque a Lisbonne (Alcantara) le 26 Janvier 
1917, Bernardino Machado, president de la Republique portugaise, salue les troupes por- 
tugaises du Corps Expeditionnaire Portugais du premier contingent portugais, qui embar- 
quent a Lisbonne pour la France, direction le port de Brest. 

L’arrivee du premier contingent portugais du 
CEP en France au port de Brest en 1917 

Les premiers soldats portugais embarquent a Lisbonne (Alcantara) le 26 Janvier 1917. 

Apres quelques Jours en mer - pour certains tres penibles - les soldats du CEP arrivent au 
port de Brest le 2 fevrier, mais le debarquement ne se fera que deux Jours apres dans ce port 
fran^ais, et, une fois de plus, dans un total desordre. Le manque d’information donnees au 
port de Brest de I’arrivee des navires, qui transportaient les soldats portugais, engendrait 
une arrivee massive d’embarcations dans le port, empechant ainsi un debarquement inef- 
ftcace. 

Malgre le desordre sur le qua! de Brest, la population fran^aise, hommes ages - non va- 
lides pour la guerre - femmes et enfants sont en masse pour saluer les soldats et le peuple 
portugais. 

-Une femme demande a un soldat: 

«Au Portugal, mon pays est connu ? 

-Mais oui, Madame, repond le soldat portugais, nous connaissons la France.» 

-La dame est vetue de noir. 

-Le soldat lui demande: 

«Quelqu’un est mort dans votre famille? — Mon bis est tombe sur le champ d’honneur 
a Verdun, lui repond la dame, et maintenant Je suis seule» C’etait la tristesse, le soldat 
portugais etait emu. 

Ce sont de bonnes gens, les Portugais... repond la dame vetue de noir. 

Les soldats du CEP sont tres bien accueillis au port de Brest par la population fran^aise 
civile. 

A Brest, chaque soldat du CEP s’est vu attribuer deux boites de conserve de bceuf et quel¬ 
ques biscuits, et chaque Jour, une ration de cafe. 

Le debarquement des Portugais a Brest est marque aussi par la ftgure de Mme Helies, qui 
prit soin de ces soldats, de telle sorte qu’elle fut appelee la mere des Portugais. 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


149 


Apres le debarquement a Brest les militaires sont installes dans un camp provisoire dans 
la plaine de Kerangoff. 

Durante cette attente pour partir dans le nord de la France les militaires portugais errant 
dans les rues, du port de Brest. La ville de Brest et environ est transformee en ville des 
tentes militaires. 

Le petit journal du 13 mai 

“Depuis deux mois les troupes portugaises sont en France: depuis quinze 
jours seulement les journaux fran^ais sont autorises a le dire. Auparavant 
c’etait un secret impose a la presse fran^aise, ne I’etait pas a la presse portu- 
gaise. Des le mois de mars, les journaux de Lisbonne avaient annonce, avec 
des manchettes enormes, de debarquement des contingents portugais en 
France. Mais 11 parait que les Allemands ne lisent que les journaux fran^ais; 
ils ne lisent pas les journaux portugais. 

Voila pourquoi, apparemment, les Portugais sont arrives a bon port. - Notez 
que je ne dis pas quel port: 11 est toujours interdit de le nommer. 

Bref, concluons de tout ceci que, s’il est vrai, suivant un fameux refrain 
d’operette, que «les Portugais sont toujours gais», nos amis de Lusitanie 
n’ont pas du manquer, des leur arrivee chez nous, de s’egayer quelque peu 
aux depens de la censure fran^aise (...) 

Tous ceux, qui ont vu les Portugais depuis leur arrivee, ont pu constater des 
I’abord que les soldats ont I’aspect vif, vigoureux, tres crane dans leur uni¬ 
forme bleu horizon, et qu’ils equipes de faqon tres pratique. Leur allure n’a 
rien du militarisme allemand. Les offtciers sont jeunes, elegants. Tous par- 
lent le fran^ais, et la plupart meme le parlent d’une fa^on tres pure. 

C’est la, pour les Fran^ais que sont peu renseignes sur le Portugal, une preu- 
ve d’attachement sincere et profond que ce pays a pour la France et I’admi- 
ration que Ton y professe pour la civilisation fran^aise. 

11 faut que Ton sache, en effet, qu’au Portugal tout est a la mode de France et 
que, sur dix volumes qui vendent les librairies, 11 y en a neuf en langue fran- 
9 aise et un en portugais. 

Un des grands chagrins des Portugais de ne pouvoir communiquer avec la 
France, au double point de vue intellectuel et economique. La necessite de 
traverser I’Espagne equivaut pour eux a un veritable eloignement. Et puis, ils 
sentent que s’ils nous connaissent bien et nous estiment, nous autres, nous 
les connaissons mal. Nous les confondons trop souvent avec leurs voisins Es- 
pagnols. Or, les deux peuples ont des vertus diverses, des signes distinctifs 
bien differents. Le Portugais, je le repete, tiens beaucoup du Celte, alors que 
I’Espagnol est un Latin. Ernest Laut 


Dispomvel em: http:l/gaUica.bnf.frfark:ll2148/bpt6k717261n/fl.item 


150 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



Apres I’arrivee du contingent portugais an port de Brest (2 fevrier 1917), et par manque de 
trains - seul moyen de transport de Brest pour les Flandres (Aire-sur-la-Lys) - les soldats 
portugais du CEP resteront quelques jours dans les environs de Brest. 

lasses eomme sardines, les soldats portugais embarquent dans les wagons, pour une 
destination qui leur est ineonnue, loin de leur terra natale, et pour beaucoup d’entre eux, 
un voyage sans doute sans retour. Durant longs jours, un train rempli d’hommes et de 
chevaux s’eloigne du port de Brest, et les soldats portugais arrivent a Aire-sur-la-Lys, en 
ce mois de fevrier, par une matinee neigeuse. Les voila pres du front. 

Le CEP, une fois arrive dans les Flandres, a Aire-sur-la-Lys, est rattaehe au 11'= eorps de 
la l"^" armee anglaise du general Sir Henry Horne, lls vont suivre des entrainements a Ro- 
quetoire, Therouanne et Witternesse, avant que le QC du CEP s’installe a Saint-Venant et 
sur front. 

Le secteur du CEP est base au sud des Flandres, precisement dans la vallee de la Lys, entre 
Armentieres, La Bassee et de Merville a Bethune, sur une distanee maximale de 11 km et 
minimum de 4 km, en accord avec revolution de la campagne militaire. 

Entre fevrier 1917 et avril 1918, la vie des forces du CEP va se derouler dans ce seeteur au 
rythme des bombardements et assauts. Les forees du CEP ont souffert plus de 60 aetions et 
plus de 20 bombardements par les forces allemandes. 

La vie des soldats portugais dans les tranchees: des hivers qui n’en bnissent pas en Flan- 
dre (nesta terra de ningue'm, ou avenue d’Afonso Costa, «tranches’ noms donnes par les 
soldats portugais). L’eau, la boue, la pluie, la neige fondue et des rats geants sont les seuls 
veritables maitres du terrain. Faute de pouvoir ereuser des tranchees profondes pour que 
les soldats se tiennent debout, il a fallu dans la plupart des eas se eontenter de tranchees 
superftcielles couronnees de parapets faits de sacs de terre, de gravats, du sable ou de sacs 
sans le moindre creusement. 


Le secteur portugais 

En 1917, le 4 avril, Antonio Gonsalves Curado, soldat 234, dans la 12' division d’infanterie 
28, est le premier soldat portugais mort au eombat. 11 fut inhume au cimetiere de Laventie, 
avant d’etre transfere au Portugal a Figueira da Foz (31 juillet 1929). 

Le 30 mai 1917, la 1" brigade d’infanterie assume la responsabilite d’un secteur sur le 
front, et desle4juinl917, les soldats portugais affrontent une attaque allemande. Le 17juin 
1917, les Portugais attaquent les Allemands. Le 14 septembre, les soldats portugais eaptu- 
rent des soldats allemands. Apres I’entree de la 3'brigade sur le front, e’est la 4'brigade qui 
assume la responsabilite d’un secteur portugais. 

Pendant I’annee 1917, le corps expeditionnaire portugais prend sa place dans les lignes 
de feu et, pendant plusieurs mois, se eharge d’un seeteur du front britannique. Bien que 
n’ayant pas ete engages dans les offensives majeures, les ofhciers et les hommes du CEP 
se sont montres de braves et utiles soldats pendant le deelenehement de plusieurs raids et 
actions secondaires. 

Le premier eontingent de la CAPl arrive en Franee, sous le eommandant fran^ais, mais 
n’est operationnel que le 16 mars 1918. 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


151 


Les villes ou villages des Flandres garden! le souvenir de la presenee des soldats portugais: 
Armentieres, La Bassee, Merville, Bethune, La Gorgue, Le Touret, Neuve-Chapelle, Vieille- 
-Chapelle, Fleurbaix, La Lawe, Richebourg, La Couture, Saint-Vaast, Lestrom, Les Lobes, 
Aire, Laventie, Loeon, Bailleul, Therouanne, Chapigny, Estaires, Pont du Hem, Fauquis- 
sart, Ferme du Bois, Saint-Venant, Bois-Grenier, Huits Maisons, Guarbeeque, Clarques, 
Isbergues, Bocsinhena, Fillers, Ecquedecques, Enguinegatte, Ouve-Wirquin, Ecques, Eau- 
quembergues, Avroult, Coyeeques, Bomy, Erny-Saint-Julien, Enquin-les-Mines, Dohem, 
Riez, Clety, Inghen, Quiestede, Roquetoire, Marthes, Blessy, Witternesse, Ouernes, Aire- 
-sur-la-Lys, Nieppe, Estuber, Violaines, Givenchy-les-la-Bassee, Wingles, etc. 

A la demande du general portugais Tamagnini de Abreu, le 20 avril 1917, le CEP est eleve 
au rang de corps d’armee a deux divisions, mais il va rencontrer deux difhcultes pour aug- 
menter ses effectifs: d’une part, le refus anglais de mettre a disposition des transporteurs 
maritimes, alors que le gouvernement anglais avait donne son accord pour porter le CEP a 
deux divisions, et, d’autre part, le regime de Sidonio Pais (1872-1918), chef du gouverne¬ 
ment depuis le 11 decembre 1917&lt;“), contre la participation du Portugal dans le conflit, qui 
ne permet plus de maintenir sur le front les effectifs necessaires. Sidonio Pais: Abaixo a 
guerra! Ninguem mais vai para a guerra!!! A bas la guerre ! Plus personne ne sera envoye 
a la guerre. 

En ce debut d’annee 1918, la situation du CEP est critique. Le manque d’effectifs, le non- 
-remplacement des ofhciers en permission et le non-remplacement des soldats restes 
longtemps dans les tranchees contribuent a affaiblir le moral des troupes. L’opposition du 
gouvernement anglais et la position du gouvernement portugais entrainent la grande de- 
faite que les troupes portugaises du front subissent le 9 avril 1918. 

Depuis le coup d’Etat du 8 decembre 1917, par Sidonio Pais, le President de 
la Republique Bernardino Machado est destitue et est oblige de quitter le 
Portugal pour s’installer a Madrid. Le 15 decembre 1917, il demande exil a la 
France. Le 15 Janvier 1918, il s’installe a Paris. Le 19 Janvier 1918, Bernardino 
Machado est invite au Palais de I’Elysee pour un diner avec le President fran- 
9 ais Raymond Poincare. Finalement, il est de retour au Portugal le 15 aout 

1919 .( 11 ) 

Le soir du 6 avril 1918, le bataillon d’infanterie 13 (Vila Real), selon le rapport elabore par 
le capitaine portugais Bento Roma, re^oit I’ordre d’occuper le secteur de la Ferme du Bois 
(La Couture). 

La le division portugaise quitte la premiere ligne du front le 6 avril. 

Le 8 avril 1918, est prevu le remplacement de la 2e division portugaise, par une division 
anglaise, selon la note ofhcielle (n° 328 du 8 avril 1918), qui annonce le retrait de la 2e di¬ 
vision portugaise. 

Elle disait: La 2e division portugaise, depuis de trop longtemps dans les tranchees, serait 
remplacee I’armee anglaise. 

Au soir du 9 avril 1918, la 2e division re^oit un nouvel ordre, annon^ant qu’elle ne sera 
remplacee que dans la nuit du 9 au 10 avril 1918. 


152 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


La bataille de la Lys 


Le front du secteur portugais est tres long. 11 kilometres contre 7 pour la 40*= division an- 
glaise au nord, et de 3 pour la 55*= division anglaise au sud. 

L’objectif allemand etait de couper le front anglais en deux, et d’atteindre Hazebrouck, 
ncEud ferroviaire, et gagner la Manche pour encercler une partie des forces anglaises dans 
le nord de la France et en Belgique. Separer les troupes fran^aises et anglaises pour repou- 
sser les troupes anglaises jusqu’a la mer, pour atteindre les ports de Dunkerque, Calais et 
Boulogne, et les obliger a rentrer en Angleterre et de quitter la guerre. 

Ce mardi 9 avril 1918, au petit matin, par une matinee densement brumeuse, rartillerie 
allemande couvre de projectiles le secteur entre la Lys et le canal de La Bassee. 

La deuxieme division portugaise en pleine releve suivit I’assaut des forces allemandes. 

Des 6 heures du matin, les neuf divisions allemandes de I’armee de Von Quast se ruent en 
masse sur le front portugais, long de 11 kilometres. 

Les soldats portugais en premiere ligne et en pleine releve sont soudainement pris par 
surprise. 

Quand la bataille de la Lys eclate le 9 avril 1918, la 2*= division du corps expeditionnaire 
portugais, sous le commandement du general portugais Gomes da Costa (20 000 hommes), 
qui tenait son quartier general a Lestrem (chateau de la Cigale) doit affronter la 6*= armee 
de Von Quast composee de 50 000 Allemands en plusieurs lignes successives qui pilonnait 
toutes les routes, cherchant a isoler le quartier general et la division portugaise. 

Pres de 500 canons allemands de Givenchy a Bois-Grenier bombarderent pendant pres 
de quatre heures. Malgre quelques points de resistance par les soldats portugais du GEP, 
la 2*= division portugaise est presque entierement balayee par Folfensive allemande de 
I’operation «Georgette» devant Neuve-Chapelle et de Fauquissart, le 9 avril 1918. 

Eprouvees sur la Somme et mises au repos dans ce secteur (juge calme), les deux di¬ 
visions anglaises (40*=) au nord et (55*=) au sud, qui se trouvaient aux extremites du front 
(Fleurbaix et Givenchy), resisterent. 

Des le premier choc, les Portugais sont bouscules, et sans arret les troupes allemandes 
font irruption sur les deux lignes de front. 

Neuve-Ghapelle, Fauquissart, Richebourg, Bois-Grenier et Laventie tomberent. 

Les lignes portugaises sont detruites et ferment un amas heteroclite de decombres et de 
cadavres. 

Les rescapes qui ont reussi a s’echapper du front se refugient dans les trous de mortiers 
et autres denivellations du terrain. 

Malgre I’avancee allemande de 8 km sur les 23 km du secteur portugais, les Allemands 
n’avaient pas encore traverse les rivieres; la Lys et la Lawe, qui constituaient de vraies bar- 
rieres de la defense portugaise. 

Les soldats portugais tenaient au reduit de La Gouture. 

Selon certains temoignages, un lieutenant portugais, qui combattait avec une poignee de 
soldats a La Gouture, aurait repondu a un capitaine anglais qui avait demande a battre en 
retrait: daqui ninguem se retira, combatemos ate a. ultima (personne ne part d’ici, nous 
combattrons jusqu’a la derniere balle). 

Ge n’est que vers f7 heures qu’il a quitte La Couture, avec les derniers soldats portugais 
du CEP. 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


153 


- Selon le journal fran^ais Telegramme du 10 avril 1918: L’Histoire par leva unjour d’un 
bataillon portugais qui a La Couture s’est battujusqu’d la derniere cartouche. 

(in: http://www.momentosdehistoria.eom/MH_05_03_01_03_Exercito.htm) 

Des le 11 avril, la 2‘ division portugaise est remplacee par les forces britanniques. 

Le 13 avril 1918, les soldats du CEP sont en soutien de la 14*= et de la 16*= division anglaise 
a Lillers et aux environs. Elies sont alors regroupees en une seule division et participent a 
I’offensive alli&amp; de I’ete 1918. 

A partir du 22 avril 1918, I’armee anglaise va proposer au commandant portugais Ta- 
magnini de Abreu un projet de reorganisation du CEP qui prevoyait de separer les unites 
portugaises par les divisions anglaises sous son commandement. 11 conteste cette decision 
et rappelle qu’il doit informer le gouvernement portugais. Cependant, Tamagnini de Abreu 
accepte momentanement les exigences anglaises en attendant les decisions du gouverne¬ 
ment portugais. 

La bataille de la Lys, nom de la riviere locale, s’est deroulee du 9 au 29 avril 1918. 

Les chiffres des morts, blesses, malades, gazes, disparus et prisonniers, varient et sont 
toujours contro verses. 

Apres la signature de I’armistice, I’Allemagne renvoie au Portugal 6.767 prisonniers avec 
une liste de 233 soldats morts en captivite'*'*. 

L’offensive «Georgette» du 9 avril 1918 a fait perdre 13,4 % des effectifs du CEP. 

Selon ce tableau, et sur les 55 165 militaires portugais engages sur le front de Elandre: 2 
160 sont morts, 5 224 blesses, 6.678 prisonniers et disparus, pour un total de 14 062, soil 
pres de 26 % des effectifs. 

Un siecle apres la bataille de la Lys, il est impossible d’indiquer avec precision le nombre 
des pertes du CEP lors de la bataille du 9 avril, et durant toute la guerre dans les Elandres. 


Le retour des soldats portugais au pays 

A la bn de la guerre, et par manque de navires pour le retour au pays, beaucoup de soldats 
portugais du CEP sont obliges d’attendre longtemps leur retour. Beaucoup sont obliges 
de rester nesta terra de ningue'm a des travaux preventifs, de destruction de vestiges de 
guerre. Apres pres de deux ans passes dans les tranchees, ils sont forces aux taches penibles 
par manque d’infrastructures sur place, par manque de soutien aux soldats portugais, et 
a cause du grave probleme de sante de ces soldats dus au manque alimentaire en general 
pendant la guerre et apres. 

Ce fut le sort qui a ete reserve a ces soldats apres la guerre. 

Les soldats portugais furent les oublies des gouvernements portugais, anglais et fran^ais 
qui ont empeche un retour digne dans leur pays natal. 


Direction des services de statique du CEP du 19 juillet 1923. In Major Vasco de Carvalho; A 2.® Divisao Portuguesa 
na Batalha de la Lys (9 de Abril de 1918), p. 77; Lusitania Editora, Lisboa, 1924. 


154 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



Apres-guerre 

Le Portugal a participe a la conference de paix a Versailles, represente par Egas Moniz en 
novembre 1918 et par Afonso Costa en mars 1919. 

Le 6 mai 1919, a la conference de Paris, Afonso Costa reagit: Le traite de paix ne tient au- 
cun compte de la situation du Portugal. Ses sacrifices n’ont pas ete reconnus. 11 n’a meme 
pas ete appele a avoir des voix dans les groupements crees par la Conference, et a vu avec 
surprise reconnaitre des droits a des pays neutres, au detriment de ceux qui ont verse leur 
sang pour une cause commune. Je demande a ceux qui ont redige cette Charte, que mon 
pays, qui s’est battu, y soit moins compare aux neutres. Je demande que mon pays, qui 
a envoye en Prance ses soldats, soit au moins traite comme les pays qui n’ont envoye en 
Prance que leurs commis voyageurs. 

Le journal fran^ais, L’Avenir, dans un article signe Charles Chaumet, ministre de la Ma¬ 
rine, a soutenu le mecontentement d’Afonso Costa, et critique la neutralite de I’Espagne. 

Les Allies auraient privilegie la neutralite espagnole et puni la belligerance portugaise... 

Le traite de Versailles est signe le 28 juin 1919, dans le Palais de Glaces, entre I’Allemagne 
et les allies avec Parmistice de Rethondes signe le 11 novembre 1918. 


Conclusion 

L’Operation Georgette du 9 avril 1918 avait comme objectif empecher que les forces alle- 
mandes traversent la ligne d front de la Lys, tenues par le CEP, (ils avaient 24 heures pour 
aneantir le front portugais et les secteurs gauche et droit tenus par les forces anglaises). 

Ils n’ont pas ressui! Pourquoi ? 

Malgre la defaite portugaise, les forces du CEP vont tenir quelques heures, qui furent 
fondamentales pour que les forces anglaises reviennent les renforcer et empecher les for¬ 
ces allemandes de traverser la LYS. 

Un autre aspect de propagande allemande ! La presse allemande distribuee au Portugal 
va annoncer que des milliers portugais sont morts ou emprisonnes. 

Cela va causer beaucoup de souffrance pour le peuple portugais et alimenter la theorie de 
ceux qui etaient contre la participation du Portugal dans ce conflit. 

La presse anglaise n’a pas epargne non plus les forces du CEP, les tenants comme respon- 
sables de I’avancee des forces allemandes lors de la bataille de la Lys. 

Les deux annees de guerre passees dans les Llandres ont provoque des alterations de la 
sante des combattants portugais a court, moyen ou a long terme. Les conditions climati- 
ques rigoureuses; tunnels et abris inondes de boue, de gel, de rats, la durete quotidienne, 
la longue permanence dans les tranchees, le probleme de 1’alimentation, le manque de 
vetements de rechange ainsi que le manque d’hygiene, ont provoque des maladies et une 
usure physique et psychologique determinante pour les soldats portugais. 

Au-dela de la difhculte d’orientation du labyrinthe des tranchees, 11 faut vivre sous des 
abris souvent couverts par des toles ou installes dans les maisons detruites par la guerre, 
et supporter la vision de cadavres humains et animaux parfois en etat de decomposition. A 
cela, 11 faut ajouter le climat humide de la region, ou les soldats devenaient amphibies pour 


II. PORTUGAL NA GUERRA - A BATALUA DE LA LYS 


155 


survivre. Les rats, la gadoue, la neige, le froid, les longs hivers passes dans les tranchees 
affectaient les soldats portugais, non habitues au climat des Flandres. 

Des la bn de 1917 et le debut de 1918, les tranchees etaient I’endroit ou Ton vivait, dormait, 
mangeait, combattait, restait blesse et mourrait. La vie etait une vraie survie. La censu¬ 
re du courrier militaire a ete instauree. 11 etait interdit de mentionner la ville, village ou 
bourg ainsi que toute information militaire sous peine d’une grave punition et la censure 
du courrier. 

Le CEP se plaint du gouvernement portugais de ne pas respecter la promesse faite aux 
militaires: tout militaire doit avoir une permission au Portugal. Pourtant, quelques ofb- 
ciers etaient privilegies. 

Aujourd’hui, que reste-t-il de cette periode 1917-1918 ? Les derniers temoins ont dis- 
paru. 

Seuls quelques vestiges et les cimetieres militaires sont la pour nous rappeler qu’une 
bataille a eu lieu. 11 ne reste que quelques enfants ou petits-enfants de ces soldats restes 
en France apres la guerre: Felicia Gloria d’Assun^ao Pailleux, nee en 1926 a Ecquedecques, 
demeurant a Burbue, bile de Joao d’Assun^ao, soldat portugais. Depuis de nombreuses 
annees, Felicia est presente aux commemorations ofbcielles avec le drapeau portugais de 
la Ligue des Combattants de Fillers. File a dit en 2004, lors des commemorations a La Cou¬ 
ture: «A travers ce drapeau, je vois mon pere, mais il faudra qu’un jour quelqu’un prenne 
la releve». 

Apres la Premiere Guerre mondiale, un deble a lieu sur les Champs-Elysees le 14 juillet 
1919. Un contingent portugais de 400 hommes d’infanterie deble sur les Champs-Elysees 
en passant sous I’Arc de triomphe pour la «Victoire». Les marechaux vainqueurs, lobre, 
Foch et Petain, deblent a cheval sur les Champs. A leur arrivee, &lt;“) Raymond Poincare, pre¬ 
sident de la Republique, et Georges Clemenceau, president du Conseil, deposent une gerbe 
de fleurs au pied du cenotaphe eleve a la gloire des morts. Les marechaux lobre et Foch 
sont re^us Porte Maillot par le president du Conseil municipal et le prefet de la Seine. Le 
deble venant de I’avenue de la Grande Armee debouche sous I’Arc de triomphe. Les deux 
marechaux chevauchent derriere I’escadron de la Garde republicaine, suivis du marechal 
Petain. Dans la nuit du 14 au 15 juillet 1919, des feux d’artibce eclatent dans le ciel de Pa¬ 
ris illumine. C’est au tour des troupes alliees d’etre a I’honneur, avec en tete leurs chefs. 
Envoie un telegramme au chef de I’Etat portugais: «L’amitie du Portugal est tres chere a la 
France. Les braves soldats de votre Republique ont etc acclames par le peuple de Paris, le 
nom des Portugais a etc donne a une rue de la cite et je suis I’interprete du pays entier en 
vous exprimant, pres du votre, les souhaits les plus cordiaux». 

A I’heure actuelle (cent ans apres), combien de Fran^ais savent que le Portugal a envoye 
des soldats combattre dans les Flandres? 11 serait d’ailleurs cruel pour les historiens 
fran^ais d’etablir la liste d’ouvrages consacres a la Grande Guerre, ou le Portugal n’est 
meme pas mentionne, et totalement passe sous silence, le fait que nombre de soldats 
portugais y ont perdu la vie et participe pour la victoire bnale avec I’armistice signe le 
11 novembre 1918. 


156 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


References 


Carvalho; Major Vasco de (1924). A 2.^ Divisao Portuguesa na Batalha de la Lys (9 de Abril 
de 1918). Lusitania Editora, Lisboa. 

Henriques, Mendo Castro e Leitao Antonio Rosas (2001). La Lys 1918-Os Soldados Desco- 
nhecidos. Lisboa: Edi^ao de Livros e Revistas, Ld®. 

Nascimento, Manuel do (2008). La Bataille de La Lys - 9 avrill918 - Devoir de Memoire, 
(edition bilingue frangais/portugais). Paris: L’Harmattan. 

Nascimento, Manuel do (2014). Premiere Guerre mondiale (Centenaire 1914-2014), Les 
soldats portugais des tranchees de Flandre et la main-d’oeuvre portugaise a la demande 
de I’Etatfrangais, Paris: L Harmattan. 

Nascimento, Manuel do (2018). Les oublie's de la guerre des tranchees-Les soldats portu¬ 
gais de la bataille de la Lys (9 avril 1918). Paris: L’Harmattan. 


II. PORTUGAL NA GUERRA - A BATALHA DE LA LYS 


157 



III. A guerra nas colonias 




German Mittel-Africa plans and the colonial 
situation in Angola and Mozambique 

Helmut Bley 

Professor of Contemporary History. 

Leibniz University Hannover 


Abstract: The paper starts with discussing relevant literature to the state of affair in hoth 
Portuguese colonies. The situation of the labor force and even continued slavery is dis¬ 
cussed in the context of the international debates on colonial scandals especially the situa¬ 
tion in Angola was compared with the Belgian Congo around 1909, which created a severe 
image problem also for Portugal. The British German agreement of 1898 is discussed in the 
context of expected bankruptcy of Portugal and the Boer War. The South African labor 
demands towards Mozambique migrant labor to the Gold Mines in Transvaal became an 
important issue as well as Cecil Rhodes plans to annex Portuguese territories for expansion 
to the north which happened 1906. Two lines of thoughts were visible in Britain and Ger¬ 
many: territorial control versus informal economic influence. However deep resentment 
against Germany gained influenced in important political circles in Great Britain because 
Germanys official sympathy towards the Boers and hints off a potential invasion in France 
in 1912 so that it can be questioned how serious those plans were. This also applies to the 
talks on the same issue 1912-1914, which became irrelevant because of the outbreak of 
the war. The impact of the War in East Africa is discussed and the suffering of the African 
population both in Tanzania and Mozambique underlined. Finally, a section deals with the 
function of Mittel-Africa plans in the governmental discussions over war-aims before and 
during the war. 1 underline that after the defeat of Russia because of the successful British 
blockade especially the German military wanted an expansion to Eastern Europe rather 
than to overseas territories. 

Keywords: African Portuguese colonies, international context. War in East Africa, British 
and German agreement. 

Resume: Le document commence par discuter la litterature pertinente sur I’etat des choses 
dans les deux colonies portugaises. La situation de la main-d’ceuvre et meme la poursuite 
de I’esclavage est discutee dans le cadre des debats internationaux sur les scandales colo- 
niaux, en particulier la situation en Angola a ete comparee avec le Congo beige vers 1909, 
qui a cree un grave probleme d’image aussi pour le Portugal. L’accord britannique de 1898 
est discute dans le contexte de la faillite attendue du Portugal et de la guerre des Boers. Les 
demandes de main-d’ceuvre sud-africaine a I’egard du travail migrant du Mozambique 
aux mines d’or du Transvaal sont devenues une question importante, ainsi que Cecil Rho¬ 
des prevoit d’annexer les territoires portugais pour I’expansion au nord qui s’est produite 
en 1906. Deux lignes de pensee etaient visibles en Angleterre et en Allemagne: le controle 
territorial versus I’influence economique informelle. Toutefois, un profond ressentiment 
contre I’Allemagne a ete influence par des cercles politiques importants en Grande-Bre- 


III. A GUERRA NAS COLONIAS 


161 


tagne du fait de la sympathie officielle des allemands envers les Boers et laisse entrevoir 
une eventuelle invasion en France en 1912, an point qu’on puisse se demander dans quelle 
mesure ces plans etaient serieux. Cela s’applique egalement aux pourparlers sur la meme 
question 1912-1914, qui sont devenus inutiles en raison de reclatement de la guerre. L’im- 
pact de la guerre en Afrique de Test est discute et les souffrances de la population africaine 
en Tanzanie et au Mozambique soulignees. Enfin, une section traite de la fonction des plans 
pour I’Afrique Centrale dans les discussions gouvernementales sur des objectifs de guerre 
avant et pendant la guerre. On souligne qu’apres la defaite de la Russie en raison du blocus 
britannique reussi, les militaires allemands voulaient en particulier une expansion vers 
I’Europe de Test plutot que vers les territoires d’outre-mer. 

Mots-cles: Colonies portugaises en Afrique, contexte international, guerre en Afrique de 
I’Est, accord britannique et allemand. 


162 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Introduction 


Portuguese Africa was several times a German aim for revising colonial borders. Secret 
talks and agreements with Great Britain were held 1894-1898 and 1912-1914. Plans for the 
extension the German colonial empire were also formulated as war-aims during the First 
World War as a concept to demand a Mittel-Africa (Gentral Africa). The invasion of the 
troops of Lettow Vorbeck 1917 into Mozambique only aimed at gaining food and military 
material for a totally exhausted small army. 


The situation in Angola and Mozambique 

It is necessary to look at the situations in Angola and Mozambique in this period from 1894 
to the end of the War, which formed the background of German and British ambitions for 
controlling the Portuguese colonies in Angola and Mozambique. 

The older literature stressed the weaknesses and ineffectiveness of Portugal’s colonial 
regime (Hammond, 1966). Hammond for instance regarded also the state of affairs in the 
motherland as rather negative during the last years of the Monarchy, when the powers in 
Europe speculated about state bankruptcy. 

William Glarence Smith (1985) has a much more balanced view which 1 will follow here. 
His core argument is, that the Portuguese local administrators in both Colonies were man¬ 
aging the fragile economy mainly through a harsh labor-system, in order to compensate 
for the capital scarcity. In a similar way the owners of the plantations acted, including 
the “Prado” property owners. They practiced a close relationship to important African 
families. All concentrated-on rubber, sugar and coconuts production. The same applied 
to the owners of cacao plantations in Sa Tome and Principe. They used mainly slaves. Their 
import was supported by the Angolan administration. It tolerated illegal export of slaves 
and later of contract laborers as a workforce in semi-feudal relations. 

The colonial rule was mainly restricted to enclaves around the Harbors. Most other re¬ 
gions were de facto controlled by international concessions. A special case was the Nyas- 
aland concession which controlled the parts of the Zambezi which was navigable and had 
a connection with the Shire Valley. Here was the centre of the Prado landholdings. 

Because the Portuguese settler community was very small, a substantial Luso-African 
population grew. But in the framework of international hnancial influences in the colo¬ 
nies, all Portuguese groups were rather content with their minor roles. Petty trade and 
running shops by a petite Portuguese and Luso-African bourgeoisie was widespread. The 
same applied to positions in hrms, dependent positions in administration and running 
small scale tropical agriculture. In this way it was a rather stable system but with perma¬ 
nent need of international capital. 

Despite of this the Portuguese colonies in Africa stayed under Portuguese sovereignty. 
For the international capital there was no direct path available to bring areas under direct 
control. But they penetrated the colonies through huge concessions which enabled them 
to hnance and operate trade, plantation-production and road and railway constructions. 
The system was sustained also by employing corruption which also reached Lisbon. The 


III. A GUERRA NAS COLONIAS 


163 


expected State bankruptcy did not happen. The Bureaucrats in Lisbon masterfully played 
off the international hnancial groups against each other and survived all hnancial crises. 


Negative image of the colonial rule Portugal’s in Europe 

Clarence Smith and also David Birmingham (2006) argue, that even accepting the precar¬ 
ious stability in the political economy of the colonies, a very negative image of the colonial 
rule Portugal’s was perceived in Europe and the USA. 

Not only were the repeated hnancial crises in Portugal an issue, much stronger was in¬ 
ternational criticism of the labor system. Both Authors link the abuse of labor including 
large scale forced labor to the fact that the economy in both colonies despite of interna¬ 
tional capital was permanently undercapitalized. The slave trade to Brazil continued into 
the 1890ties. Also a permanent trade from Angola to Sao Tome and Principe in order to 
serve the labor demands of the cacao-plantations did not stop. So British Humanitari¬ 
an and Antislavery organizations saw these praxis’s as a scandal. It was compared with 
the scandals in King Leopold’s private colony of Congo in 1908/9. The public campaigns 
against the Portuguese colonies run parallel to the campaign against the cruelties in labor 
relations in the Congo. 


Migrant labor from Mozambique to the Goldm in es of South Africa 

The international pressure on Mozambique increased after the Goldmines in Witwaters- 
rand went in full production and also railways had to be built there. The interest of South 
Africa and Great Britain towards Mozambique was strong in the period around the Boer 
War. Gontrol of the Harbor of Louren^o Marques became important to keep the Boer re¬ 
publics off the Indian Ocean. A safe outlet for Rhodesia was sought for in Beira. 

After the Boer War control of the labor supply became the main interest of the Goldmin¬ 
es in Witwatersrand. The demand for labor from Mozambique increased. Pressure on the 
Mozambique administration was used, which tried to keep their scarce labor force inside 
the colony. But the Administration could not block the labor migration which continued 
and even grew all the time. So many Africans went to the Witwatersrand because of better 
wages and treatment. Around the Anglo-Boer war, about 50 000 workers went to Trans¬ 
vaal in the Witwatersrand. 

The situation of the local population anyhow worsened in these decades because of 
enormous spread of sleeping sickness and malaria. The people were exhausted and had no 
resistance against these diseases. The food situation anyhow was rather bad. Additional¬ 
ly, the colonial army in Mozambique was still hghting against independent African King¬ 
doms: so against the Gaza Nguni in Mozambique and the Ovambo at the border of Angola 
to Namibia. Many other incidences happened. 


164 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Negotiations between Great Britain and Germany'^^ 

Negotiations between Great Britain and Germany lasted from 1894 to 1898 and led to a 
first agreement using the colonies as a security in case of bankruptcy of Portugal which 
however did not happen. A second attempt started 1912 and was finalized in June 1914, 
but not published and the First World made it obsolete. After the Boer War was won 1902 
British interest was reduced, but some gestures toward the colonial ambitions of German 
were contemplated. But more important was the political legacy that Emperor Wilhelm 11 
hat supported the Boers 1896. Also, a broad German movement showed sympathies with 
the Boers. In Great Britain Because this created the impression that German had interests 
in Transvaal and deeply impressed the political mood of important figures in London. They 
became very skeptical if not negative, about German influence over the Boer Republic. 
Together with built up of the German Navy often with anti-British undertones, all this was 
regarded as proof that German might have ambitions towards hegemony in Europe and 
the World. 

A similar mistrust developed during the second round on Mozambique and Angola 1912. 
Minister fJaldane in his Mission to Germany 1912 wanted to stop the planned increase of 
German to start of a period of more friendly relations. But the Germans Government de¬ 
nied this, unless Britain would declare to stay neutral in an unprovoked war by Germany 
against France, which would have destroyed the British-French Entente. So, the mission 
came to a very negative result. Despite of that, the discussions about the future of the Por¬ 
tuguese colonies were continued in order to ease tensions. The same applied to the nego¬ 
tiations on the German Bagdad-Railway in the Ottoman Empire close to the Gulf of Persia. 
Anyhow the British-French entente was stabilized in the Morocco-crisis of 1911 when 
Lloyd George as exponent of friendly relations to Germany in his Mansion House speech 
warned successfully Germany to risk a war, otherwise Great Britain would support France. 

Britain anyway had intensified its expansion in Southern Africa by blocking the hinterland 
of Mozambique through annexing the Nyasaland concession 1906 to open a way for Cecil 
Rhodes plans for a Cape to Cairo railway, which never materialized. This was regarded in 
Portugal as a very heavy blow. 

In both rounds to reach an agreement the two powers mainly looked for informal pen¬ 
etration in order to avoid the change of the borders of the Portuguese colonies. As the ex¬ 
ception was agreed to use the Colonies in case of a potential Portuguese State- Bankruptcy 
as a security. 

It was possible to use informal penetration because parts of Mozambique belonged to the 
“Congo Free Trade Zone”. So, it was easier to use economic tools without changing bor¬ 
ders. Great Britain was used to informal infiltration in the period of “Free Trade Imperial¬ 
ism.” (Cain and Hopkins, 1993;. Bley and Konig, 2006; Gallagher and Robinson, 1953). But 
in the case of Mozambique later in 1906 Great Britain directly annexed large parts of the 
hinterland, especially Nyasaland now Malawi which in Portugal was regarded as a heavy 
humiliating blow. 


Gifford and Roger 1967; Willequet, J., 1971. 


III. A GUERRA NAS COLONIAS 


165 



Germany between informal penetration and ideas of territorial control'’ 

In Germany there were two tendencies running parallel and hindering each other. The 
colonial movement and its press as well as the naval movement demanded territorial en¬ 
largement of the colonial empire as late comer. Bismarck however had calculated hy fram¬ 
ing the concept of the “Congo Free Trade Zone” in 1884/5 to practice informal penetration 
even in the German colonies. He was in accordance with the hig hanking houses and the 
Hamburg trading community to use its economic and hnancial power to penetrate world¬ 
wide without territorial claims, normally in cooperation with capital of Great Britain and 
France. Both lines of political strategies continued during the whole period before World 
War I and also influenced the War aims during the First World War. In the Period until 1914 
the territorial interest for nationalist reasons became dominant. The British naval Block¬ 
ade during the First World War shifted German interest to informal control of Europe and 
territorial as well as informal control to Eastern Europe in order to counter the British 
Hegemony on the oceans. 

In both period of bargaining with Great Britain about the future of the Portuguese Af¬ 
rican colonies the German delegation was under the leadership of the Warburg Bank in 
Hamburg, which also had strong connections in Tondon. The bankers were even angry 
that the diplomats in Berlin were always eager to govern territory, whereas the bankers 
were satished with concessions and hnancial penetration. 


The discussions of 1912-to 1914 

The discussions of 1912-to 1914 to rework the treaty of 1898 started March 1912 Germany 
demanding Angola, Zanzibar and Pemba. In the hnal draft custom duties in Angola and 
Mozambique were divided between both states who wanted to place a joint loan to Portu¬ 
gal. But secession of the colonies from Portugal was dependent of its agreement. Both sides 
were reluctant to publish the results. The reasons for the reluctance were to offer time to 
the German banks for a penetration pacihque. As the German Ambassador Metternich in 
London had suggested already 1912 that economic penetration had to happen before the 
talks otherwise Great Britain would go ahead. Germany signed the Agreement at the 23 
July 1914, the day of the Austrian ultimatum to Serbia, Grey delayed to the autumn and 
the War hnished everything. One could conclude that in both periods of negations about 
Portugal’s Golonies there was no real political chance for the big powers. Anyway, Portugal 
was successful despite of the internal system being in turmoil to avoid state bankrupt¬ 
cy and had no reasons to agree to the secessions of its African Golonies. Portugal had no 
reason to give up her colonies and felt protected through the Windsor treaty which was 
concluded at the same time and stressed the continuity of British responsibility towards 
the integrity of Portugal as formed already in the 17th.century despite of the realities of 
British policies in Africa. Anyway, Portugal was successful despite of the internal system 
in turmoil to avoid state bankruptcy and had no reasons to agree to the secessions of its 


" Barth, 2004. 


166 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



African Colonies. Portugal had no reason to give up her colonies and felt protected through 
the Windsor treaty which was concluded at the same time and stressed the continuity of 
British responsibility towards the integrity of Portugal as formed already in the 17th.cen¬ 
tury despite of the realities of British policies in Africa. 

Britain tried to balance its skepticism over Germanys political aims through the fact that 
parallel to the negations on Portugal’s Colonies other negotiations were successful con¬ 
cluded, so agreement over the Bagdad-Railway of Germany, which was agreed at also in 
June 1914. It was the cooperation of both states 1912/13 that the Balkan Wars did not lead to 
a World War. It hts into the pattern of British policy of this time to minimize zones of ten¬ 
sion when they agreed with Russia to divide Persia/Iran 1907 in a Northern and Southern 
interest zone. Germany signed it at the 23 July 1914 the day of the Austrian ultimatum to 
Serbia, Grey delayed to the autumn and the War hnished everything. 


The War Period: 

Mozambique’s African population was hit hard. War brought enormous inflation and 
made food prices high. The labor was even heavier exploited because of the intensihed ex¬ 
port production. The diseases spread more than before the war and Mozambique as other 
African countries were 1918/19 suffering under the Influenza epidemic. The invasion of the 
Lettow Vorbeck troops 1917 was aiming for food and military supplies. The German troops 
at the beginning were greeted by Africans because of their hate against the Portuguese 
rule. But this changed after the Askari and German officers took many women by force and 
confiscated harvests and cattle as they had done in large scale also in German East Africa, 
which caused about 300 300 lives of people from German East Africa through hunger and 
destruction of villages. 

Lettow Vorbeck had waged this war because he had hoped to use his presence for sup¬ 
porting the German claim to the country. Later he rationalized that he could keep 130 
000 British, Indian and South African Soldiers off the Western fronts in Europe. But he 
had during this war Period no contact to Berlin. The badly organized Portuguese colonial 
army could not stop him with few exceptions. But once they regained armaments and food 
supplies. Einally, the Germans were forced by British troops to get out and retreat via the 
western borderlands of German East Africa to Northern Rhodesia. Lettow Vorbeck capitu¬ 
lated in November 1918, creating a mythos in Germany as “not being defeated in the held” 
(Pesek, 2010, p. 41-127). 


German “Mittel Afrika” plans as part of the War aims since 1914'*^ 

The war aims, articulated by the Chancellor and his closet advisors at the O* of September 
1914, the so called “September program”, which Immanuel Geiss and Eritz Eischer discov¬ 
ered, was written in the moment where the victory over Erance was expected in the battle 


Wedi-Pascha, 1992. 


111. A GUERRA NAS COLONIAS 


167 



at the Marne (Fischer, 1961 and Zechlin &amp; Bley, 1964). In my interpretation it was also a 
design to organize Europe under German hegemony for an unforeseeahle period of time, 
because the German government had accepted that Britain could not been beaten and that 
the blockade would last. They planned for a second World War. The concept of Mittel-Af- 
rika” can be compared with that of “Mittel-Europa”. There were similarities with regard 
to informal economic control. “Mittel-Europa” was discussed already before the war by 
the liberal imperialists Rathenau, Director of the AEG and also Banker and Gwinner, Di¬ 
rector oOfthe “Deutsche Bank” (Meyer, 1955 and Fischer, 1961). During the War he be¬ 
came leader of the ofhce to organize the raw materials necessary for the unexpected long 
war and the economic blockade. The concept of an enforced custom union all over Europe 
and other economic instrument was thought to achieve the German hegemony without 
waging a war. As the advisor of the Ghancellor, Riezler formulated 1913: Germany should 
“demand the impossible” but avoid a war (Riezler, 1913). In a sense it was an early concept 
for what we later called the “Cold War” 

In the situation of the war it became a new meaning. Against military and extreme na¬ 
tionalist demands for territorial control after victory the so-called moderates around the 
Chancellor were aware that territorial annexations should be avoided in Europe because of 
the general mood in Europe against it. So, the informal penetration of a large part of Eu¬ 
rope and even over its ally Austria- Hungary should achieve this. Because the Occupation 
of Erance became unlikely, the idea faded away and territorial control over Eastern Europe 
after successful military campaign an because of the German advances after the Russian 
Revolution became dominant. Sending Tenin from Switzerland to Russia in a German train 
worked in radicalizing the Russian Revolution. 

All these plans can be regarded as expressions of despair, but they survived the whole 
war period and hat its peak in the Separate peace treaty of Brest- Litowsk with Soviet Rus¬ 
sia, but now leaving the African interests, also a German colonial revisionism developed 
after the Treaty of Versailles. 

“Mittelafrika” developed as a parallel concept, but as territorial expansion. The idea was 
to form a huge colonial block from Angola, via the Congo and Northern Rhodesia or Zam¬ 
bia connected with Mozambique and off course East Africa. Southwest Africa after having 
exploited the diamonds was to be given to South Africa. The illusionistic aspects are obvi¬ 
ous, but the plans were included in German peace proposals 1916 and 1917 as options. The 
stumbling block was anyhow that the Germans gave their hrst priority to the almost total 
control of Belgium badly disguised as informal concepts, and off course of keeping Elsasser 
Lothringen. Great Britain and her allies were determined through the agreements not to 
conclude a separate peace which was published at September 4* 1914in December 1914. 
That the Erench integrity as major power should be maintained and that the German sys¬ 
tem of Prussian Militarism was even the position of the United States long before joining 
the War. Because the Western fronts did not change despite of the millions of men killed, 
the War aims of Germany which were officially secret were rather an element of the power 
relations between Government, Military Leadership if not dictatorship and the reorgan¬ 
ized ultra nationalist movements (Hagenlilcke, 1997). 


168 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


References 


Barth, B. (1995). Die deutsche Hochfinanz und die Imperialismen. Stuttgart: Franz Steine. 
Birmingham, D. (2006). Empire in Africa, Angola and its neighbors. Ohio University 
Press. 

Birmingham, D. and Martin, P. M. (eds.) (1983). History of Central Africa, vol. 2, Harlow: 
Longman. 

Bley, H. and Konig, H. (2006). Freihandelsimperialismus. In: Enzyklopadie der Neuzeit, 
Vol 3, E. Dauenhauer, pp.1139-1145. 

Cain, P.J. and Hopkins, A.G. (1993). British Imperialism: Innovation and Expansion 1688- 
1914. London: Longman. 

Clarence-Smith, W. G. (1985). Third Portuguese Empire 1825-1975. Manchester University 
Press. 

Fischer, F. (l96l). Grijf nach der Weltmacht, die Kriegszielpolitik des kaiserlichen 
Deutschland 1914-1918. Dilsseldorf. 

Fischer, F. (1962). Griff nach der Weltmacht. Dilsseldorf. 

Gifford, P and Roger, L. (ed.) (1967). Britain and Germany in Africa. New Haven. 
Hagenlucke, H. (1997). Die deutsche Vaterlandspartei. Die nationale Rechte amEnde des 
Kaiserreiches. Dilsseldorf. 

Hammond, R. J. (1966). Portugal and Africa 1815-1910, astudy of uneconomic Imperial¬ 
ism. Stanford University Press. 

Laak, D. Van (2004). Imperiale Infrastruktur. Deutsche Planungenfur Eine Erschliefiung 
Afrikas 1880-1960. Paderborn: Schoningh. 

Meyer, H. C. (1995). Mitteleuropa in the German thought. Den Haag. 

Michels, E. (2008). Der Held von Deutsch Ostafrika“ Paul von Lettoiv-Vorbeck. Pader¬ 
born: Ferdinand Schoningh 

Pesek, M. (2010). Das Ende eines Kolonialreiches, Ostafrika im Ersten Weltkrieg. Frank¬ 
furt: Campus Ver lag. 

Riezler, K. (1913). Die Erforderlichkeit des Unmoglichen, Prolegomena zu einer Theorie 
der Politik und andere Theorien. Berlin. 

Robinson, R. and Gallagher, J. (1953). The Imperialism of free trade. The Economic History 
Review, Second series, VI/1, pp.1-15. 

Wedi-Pascha, B. (1992). Die deutsche MittelafrikaPolitik 1870-1914. Pfaffenweiler. 
Willequet, J. (l97l). Anglo-German Rivalry in Belgium and Portuguese Africa. In: Gifford, 
P. and Louis, W.R. (eds), Britain and Germany, New Haven, pp 245-274. 

Zechlin, E. and Bley, H. (1964). Deutschland zwischen Kabinettskrieg undWirtschafts- 
krieg. In: Historische Zeitschrift 199 (1964), Nr. 2, S. 347-458. 


III. A GUERRA NAS COLONIAS 


169 



A Marinha na Grande Guerra. A Defesa 
Mantima das Ilhas de Cabo Verde (1914-1918) 


Cmg ref Jose Antonio Rodrigues Pereira"*® 

Academia da Marinha 
Academia Portuguesa de Historia 


Resumo: Logo que se iniciaram as hostilidades - e apesar da nao beligerancia portuguesa 
- A Marinha Portuguesa teve de tomar imediatamente medidas de defesa dos interesses 
nacionais no mar e nas colonias. 

Emhora sejam hahitualmente referidas as participa^oes do Batalhao de Marinha Expe- 
diciondrio a Angola (1914-15) e do eruzador Adamastor em Mozambique (1916-18), foi 
Cabo Verde quern recebeu a maior aten^ao da Armada Portuguesa. O porto do Mindelo 
(Cabo Verde) era um importante ponto de amarra^ao dos eabos submarinos, fundamen¬ 
tals, na epoea, para as eomunicazoes telegrabcas da Europa com a America e a Africa; o 
Mindelo era tambem naquela epoca um estrategieo porto abasteeedor de earvao. Tal situa- 
Zao obrigou a medidas de defesa especiais. Para ali foram deslocadas primeiro, o eruzador 
Sao Gabriel e depois as canhoneiras Beira e Ibo que com dois vapores armados, e utilizados 
como patrulhas, garantiram a defesa maritima daquele porto. Eoram tambem montadas, 
uma diizia de pe^as de artilharia, guarneeidas por pessoal da Forga Expediciondria de 
Marinha a Cabo Verde e por pessoal do Exercito. O porto do Mindelo viria a ser ataeado 
quatro vezes por submersiveis alemaes, ao longo do conflito, tendo ali sido afundados dois 
vapores brasileiros. Merecem ainda referenda a presen^a, naquele porto, do navio mer- 
cante holandes Kennemerland e do contratorpedeiro brasileiro Piaui da Divisao Naval de 
Operagoes de Guerra (DNOG). 

Palavras-chave: Portugal, Grande Guerra, Marinha Portuguesa, Historia Maritima, Guerra 
Naval, Arquipelago de Gabo Verde 

Abstract: As soon as the hostilities began - and despite the Portuguese non-belligerence - 
The Portuguese Navy had to immediately take measures to defend national interests at sea 
and in the colonies. 

Although the participation of the Expeditionary Navy Battalion to Angola (1914-15) and 
the cruiser Adamastor in Mozambique (1916-18) are usually mentioned, it was Cape Verde 
who received the greatest attention from the Portuguese Navy. The port of Mindelo (Cape 
Vert) was an important mooring point for submarine cables, fundamental at the time to 
telegraph communications between Europe and America and Africa; Mindelo was also at 
that time a strategic coal-supplying port. This situation led to special defense measures. 
There they were hrst displaeed, the cruiser Sao Gabriel and then the Beira and Ibo gun- 


O autor nao escreve de acordo com o AO90. Capitao-de-mar-e-guerra reformado Academico Emerito da Aca¬ 
demia de Marinha. Academico honorario da Academia Portuguesa da Historia. 


III. A GUERRA NAS COLONIAS 


171 



boats, which with two armed vapors, and used as patrols, guaranteed the maritime de¬ 
fense of that port. 

A dozen artillery pieees were also set up, staffed by personnel from the Expeditionary 
Force of the Navy to Cape Verde and by Army personnel. The port of Mindelo was to be 
attaeked four times by German submersibles during the eonflict, and two Brazilian vapors 
were sunk there. Reference should also be made to the presenee in that port of the Dutch 
merchant ship Kennemerland and the Brazilian destroyer Piaui of the Naval Operations 
Division (DNOG). 

Keywords: Portugal, World War, Portuguese Navy, Maritime History, War at Sea, Cape Vert 
Islands. 


172 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Preambulo 


O Seculo XX iniciou-se sob o espectro do confronto entre o Imperio Britanico e o Imperio 
Alemao, pela hegemonia mundial. 

Quando em Agosto de 1914 rebentou o conflito que ficaria conhecido como a Grande 
Guerra, as opinioes dividem-se em Portugal e nao ha consenso sobre a participa^ao portu- 
guesa. Defendiam alguns, os chamados nao intervencionistas, a manuten^ao da neutra- 
lidade por o pals e as Formas Armadas nao estavam preparados militar e economicamente 
para um conflito de tal intensidade. 

O grupo dos beligerantes ou intervencionistas, defendia a participa^ao portuguesa no 
conflito, ao lado da Gra-Bretanha, como forma de cumprir a alian^a Luso-Britanica, impor 
internacionalmente o regime republicano e, no final do conflito, ter assento nas negocia- 
^oes de paz, para garantir a soberania das Colonias, especialmente as que tinham fronteiras 
com os territorios alemaes. 

O Exercito conseguiu, com o chamado milagre de Tancos, organizar um Corpo de Exer- 
cito para actuar no teatro de opera^oes da Europa, a Marinha teve muita dificuldade em 
preparar-se porque, como ja dizia o padre Fernando de Oliveira, no Seculo XVI, uma Ar¬ 
mada nao pode improvisar-se. 

A Armada Portuguesa contava em 1914, com um conjunto de unidades navais muito he- 
terogenias, totalizando 25.000 toneladas de deslocamento, que a rapida evolu^ao dos ar¬ 
mament os navais, verificada nos primeiros anos do Seculo XX, tornara obsoletos. 

Algumas das unidades de menor porte, utilizadas nas Estaqoes Navais do Ultramar, ti¬ 
nham sido transferidas para a Marinha Golonial - criada em 1912 - e dependiam do Minis- 
terio das Colonias; apesar de guarnecidas por pessoal da Armada, actuavam sob as ordens 
dos Governadores dos territorios onde se encontravam. 

Os navios estavam vocacionados para o combate de superficie, num periodo em que a 
amea^a submarina, a mina e o torpedo ja representavam um novo e importante dado na 
guerra naval. A linica unidade naval capaz de executar missoes de guerra submarina era o 
Espadarte, um submersivel encomendado ainda no tempo da monarquia e que entrou ao 
serviqo em 1912. A sua eficacia operacional levou logo a encomenda de mais tres unidades 
semelhantes, que seriam entregues em fmais de 1917. 

Os efectivos da Armada rondavam os 4.200 homens (cerca de 450 oficiais e 3.700 sar- 
gentos e pranas). 

Desenvolvida a partir da decada de 1880 com a cria^ao da Empresa Nacional de Navega- 
gdo e da Mala Real Portuguesa, a Marinha de Comercio portuguesa possma, em Agosto de 
1914 uma frota de 473 navios, representando 142.241,57 toneladas de arquea^ao bruta, dos 
quais 246 (cerca de metade) eram veleiros como a barca Eerreira (ex-Gutty Sark) e a galera 
Viajante (constrmda em Damao em 1850). Com mais de 1.000 toneladas existiam apenas 
32 navios que representavam 81.549,47 toneladas de arquea^ao bruta (mais de metade do 
total). 

Em Africa, e apesar da nao-beligerancia portuguesa, as formas militares alemas hostiliza- 
vam as guarni^oes portuguesas nas fronteiras. 

A 25 de Agosto de 1914, formas alemas atravessam o rio Rovuma (Mozambique) e atacam o 
posto de Maziua, massacrando a pequena guarni^ao: seis soldados africanos da Companhia 


III. A GUERRA NAS COLONIAS 


173 


do Niassa, comandados pelo sargento de Marinha Eduardo Rodrigues da Costa, que seria o 
primeiro militar portugues morto no conflito. 

A 31 de Outubro de 1914, o posto de Cuangar (Angola) foi atacado e a sua guarni^ao cha- 
cinada. 

Esta situa^ao obrigou o governo a enviar imediatamente para Africa numerosos con- 
tingentes militares com destino aos territorios, onde existiam extensas fronteiras com a 
Alemanha: Angola e Mozambique. 

Mas Gra-Bretanha exercia pressao diplomatica sobre o Governo Portugues para que se 
mantivesse como nao-beligerante; tal possibilitava-lhe a utiliza^ao dos portos portugue- 
ses para apoio e abastecimento dos seus navios. 

A 11 de Setembro largaram de Lisboa os paquetes Mogambique e Durban Castle"*'* e o 
vapor Cabo Verde com os Corpos Expediciondrios do Exercito destinados a Angola e a 
Mozambique, escoltados pelo cruzador Almirante Reis e pelas canhoneiras Beira e Ibo. 

No Mogambique seguiam os 1.300 homens e alguma carga da expedizao comandada pelo 
Tenente-Coronel Alves Rozadas com destino ao Sul de Angola; o gado e a restante carga 
seguiriam, no vapor Cabo Verde. As tropasdesembarcaramemMozamedesaide Outubro, 
pouco antes dos incidentes de Naulila (180UT14) e do Cuangar (310UT14). 

No Durban Castle embarcaram os 1.500 homens da 1** Expedigao Militar para Mogam- 
bique comandada pelo coronel Massano de Amorim. 


1. A Entrada na Guerra 

Eoram tarefas da Armada, assumidas logo em 1914: 

• Assegurar a escolta aos transportes de tropas para Africa: 

• Participar na defesa do Ultramar, com forzas navais e batalhoes constituidos para actuar 
em terra com as forzas do Exercito. 

Um dos actos desempenhados pela Armada, neste conturbado periodo e que viria a ter 
significativas consequencias, ocorreu a 14 de Maio de 1915 quando, sob o comando do Ca- 
pitao-de-fragata Leote do Rego e conjuntamente com forzas do Exercito, depos o Governo 
Ditatorial do General Pimenta de Castro - opositor da entrada de Portugal no conflito - e 
restabeleceu o Regime Constitucional, subindo ao poder os partidarios da intervenzao por- 
tuguesa. 

Com a entrada formal de Portugal na Grande Guerra, a Armada foi chamada a assumir, 
para alem das que ja vinham sendo desempenhadas desde 1914, as seguintes tarefas: 

• Assegurar a escolta aos transportes de tropas para Eranza: 

• Assegurar a escolta dos navios mercantes nacionais para o Ultramar e as llhas adjacentes; 


Os paquetes Durban Castle (britanico) e Britannia (trances) foram os linicos navios estrangeiros utilizados no 
transporte de tropas para Africa. 


174 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



• Patrulhar e defender o litoral metropolitano, a barra do Tejo e as barras do rio Douro e de 
Leixoes e a baia de Lagos; 

• Estabelecer barreiras anti-submarinas, roeegar minas na entrada dos portos prineipais e 
lan^ar eampos de minas defensivos; 

• Patrulhar e defender as aguas dos arquipelagos dos Azores, Madeira e Cabo Verde; 

Sendo esperado, pelo menos desde Maio de 1915, data da subida ao poder dos partidos 
intervencionistas ou guerristas, que Portugal partieipasse direetamente no conflito nao 
houve qualquer esfor^o para refor^ar os meios materials e humanos da Marinha para en- 
frentar as novas amea^as da guerra naval: o submersfvel e a mina. 

Leote do Rego, comandante das Divisao Naval de Defesa e Instrugao apos o Golpe Mi- 
litar de Maio de 1915 apenas se preoeupou em manter preparadas e aprontadas as grandes 
unidades navais de superfleie - eruzadores e contratorpedeiros - que, desde 1914 vinham 
assegurando as eseoltas aos Transportes de Tropas eom destino a Afriea. 



Figura 1. Cruzador Sao Gabriel. Modelo do Miiseii de Marinha. Foto do autor. 


Nada estava feito em Portugal para enfrentar as novas amea^as da guerra naval quando, em 
9 de Mar^o de 1916, o Imperio Alemao deelarou guerra ao Governo Portugues. 

So em 29 de Fevereiro o Comandante Leote do Rego inieiara os preparativos para a pro- 
tee^ao do porto de Lisboa e de outros portos nacionais; anteriormente, apenas o Mindelo 
(Cabo Verde) tinha sido dotado de uma estrutura de defesa logo em 1914. 

Em Junho de 1916 sao estabeleeidos os cursos intensivos na Escola Naval e na Escola 
Auxiliar de Marinha, enquanto aumentava o reerutamento nas Escolas de Alunos Mari- 
nheiros. 

O Arsenal da Marinha desenvolveu esfor^os para terminar as constru^oes em curso - os 
contratorpedeiros Vouga (iniciado em 150UT14 viria ser terminado em 31DEZ20) e Tdme- 
ga (iniciado em 16NOV14 viria ser terminado em 19AG024) e as canhoneiras Bengo (JANIO 


III. A GUERRA NAS COLONIAS 


175 










- 3JUL17) e Mandovi (FEV13 - 19JUN18) - reparar as unidades existentes - nomeadamente 
o contratorpedeiro Tejo (18JUL12 - 19JUN16) - e por a navegar os navios apresados aos 
alemaes e que tinham sido sabotados pelas tripula^oes. 

Este esfor^o viria a ser dificultado pelo incendio que a 18 de Abril de 1916 deflagrou no 
Arsenal da Marinha e que destruiu parte das suas instala^oes fabris, nomeadamente a Sala 
do Risco™, e as instala^oes da Escola Naval e do Museu de Marinha. Alem do valioso pa- 
trimonio do Museu perderam-se os arquivos e o material eseolar da Escola Naval, cujo 
funcionamento foi seriamente afectado por esta ocorrencia, tendo as aulas funcionado, 
temporariamente, na sociedade de Geograba de Lisboa. 

O proprio Arsenal viu a sua capacidade seriamente diminuida por este acontecimento 
que, segundo a imprensa da epoca tera sido responsabilidade a agentes alemaes. 

Hoje sabe-se que o agente alemao Hermann Wuppermann (cujo nome de codigo era 
Arnold) actuou em Portugal naquele ano tendo destruido fabricas e depositos militares e 
planeado um ataque com gases quimicos (antrax), antes de fugir para a America do Sul 
(CALLEJA, 2014). 

2. A Defesa dos Port os 

A interven^ao da Armada foi tambem relevante na defesa dos portos de Portugal continen¬ 
tal, ilhas adjacentes e arquipelago de Cabo Verde. 

No Continente activaram-se as defesas das barras dos portos de Lisboa, do Douro e de 
Leixoes e da Baia de Lagos, que incluiu patrulhamento naval e postos de vigilancia em terra. 

A cidade do Funchal foi atacada a 03 de Dezembro de 1916, pelo submarino alemao U-83, 
tendo sido afundados a canhoneira Surprise e o vapor Kangaroo, de nacionalidade fran- 
cesa e o vapor britanico Dacia. A 17 de Dezembro de 1917 o cruzador-submarino U-155 
voltou a bombardear a cidade provocando baixas na popula^ao civil. 

A cidade de Ponta Delgada foi tambem atacada pelo submarino alemao U-155 em 4 de 
Julho de 1917. 


4. A Defesa Maritima de Cabo Verde 

O porto do Mindelo na ilha de Sao Vicente (Cabo Verde) era um importante ponto de amar- 
ra^ao dos cabos submarinos, fundamentais, na epoca, para as comunica^oes telegrabcas 
da Europa com a America e a Africa; o Mindelo era tambem naquela epoca um estrategico 
porto abastecedor de carvao para a navega^ao, e para a esquadra britanica em servi^o na¬ 
quela area do Atlantico. 


A Sala do Risco, com 80x18 metros, estava situada no extremo Oeste dos edificios pombalinos, onde hoje estao 
as messes das Instala^oes Centrals de Marinha. Em 1969, quando ali estava instalado o Instituto Hidrografico, foi 
novamente destruida por um incendio. 


176 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 




Figura 2. A localizagao das ilhas de Cabo Verde nas rotas do 
Atlantico. Desenho de Jose Manuel Cabrita. 


Foi, por isso, o primeiro porto a ser dotado com um sistema de defesa contra ataques vin- 
dos do mar. 

O cruzador Sao Gabriel (capitao-de-fragata Pinto Basto) que ja estivera em Cabo Verde 
antes do inicio do conflito (28MAI-09JUN1914) voltou para aquele territorio em 8 de Se- 
tembro de 1914 para defesa e vigilancia dos cabos submarinos que ancoravam na ilha de 
Sao Vicente. 

A 2 de Dezembro largaria do Mindelo para ir as Canarias comboiar os paquetes Peninsu¬ 
lar e Ambaca que tinham largado de Lisboa sob escolta do cruzador Vasco da Gama com 
militares do Gorpo Expediciondrio para Angola; chegou a Luanda a 26 e regressou a Sao 
Vicente onde permaneceu ate regressar a Lisboa a 20 de Abril de 1915. 

Depois, foram enviadas as canhoneiras Beira, Ibo e Bengo que, com o patrulha Briga- 
deiro Barreiros e os dois vapores armados Fendale e Mindelo, utilizados tambem como 
patrulhas, garantiram a defesa maritima daquele porto. De recordar que estas canhoneiras 
eram entao os mais modernos navios da Armada tendo entrado ao servi^o, respectivamen- 
te, em 1911,1913 e 1917. 


III. A GUERRA NAS COLONIAS 


177 




















Eram pequenas unidades construidas no Arsenal da Marinha, com 463 toneladas de des- 
locamento e 44 metros de comprimento; armadas com duas pe^as Krups de 76 mm, duas 
Hotchkiss de 47 mm e duas metralhadoras, tinham uma guarni^ao de 85 homens e a sua 
silhueta sui generis deu-lhes o epiteto de cruzadores de bolso. 



Figura 3. A canhoneira Beira. Modelo do Museu de Marinha. Foto do autor. 


A primeira a chegar foi a Ibo (l° tenente Carvalho Brandao), a 20 de Setembro de 1914 e que 
all hcaria ate 2 de Abril de 1918. 

A Beira largou de Lisboa a 14 de Dezembro de 1914 (l° Tenente Cisneiros e Faria) e mante- 
ve-se em Cabo Verde ate 24 de Junho de 1917, dab seguindo para os Azores. Voltaria a Cabo 
Verde em 26 de Janeiro de 1918 ali bcando ate ao bnal do conflito. 

Para substituir a Ibo, foi enviada a Bengo (l° Tenente Serra Guedes), um navio que aca- 
bara de ser construido no Arsenal da Marinha em Julho de 1917; chegaria a Cabo Verde em 
5 de Maio de 1918. 

Em 22 de Novembro de 1914 a Marinha enviou para Cabo Verde, embarcado no vapor 
Cazengo o seu segundo contingente para o Ultramar; a Forgo Expediciondria de Marinha 
para Cabo Verde, de 90 homens (l obcial, 9 sargentos e 80 pranas), para efectuar a vigi- 
lancia e defesa dos cabos submarinos e do porto do Mindelo, comandado pelo 1“ tenente 
Joaquim Costa. 

Em terra existiam apenas duas velhas pe^as de bronze Krups de 76 mm montadas no 
ilheu dos Passaros e que foram transferidas para o Morro Branco, na ponta Sul da baia sen- 
do guarnecidas pelo pessoal da Forgo Expediciondria de Marinha. 

Em Outubro de 1916 foram instaladas mais duas pe^as Armstrong 150mm desmontadas 
da fragata D. Fernando II e Gloria e da canhoneira Zambeze e que foram montadas tambem 
no Morro Branco. 


178 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 







Em Janeiro de 1917 foram instaladas na ponta Norte da baia 4 pe^as de montanha Canet 
de 47 mm - com apenas 3 kms de alcance - guarnecidas por pessoal do Exercito comanda- 
do pelo capitao Joao Sequeira. 

So em 1918 foi montada, no ilheu dos Passaros, outra bateria de Marinha constituida por 
tres pe^as Hotchkiss de 47mm a que se juntaria uma pe^a francesa de 90 mm que a Beira 
trouxe de Dakar. 

Na mesma data chegou tambem uma companhia de Infantaria do Exercito. 

No inicio do conflito, a Royal Navy tornara Cabo Verde numa base para a sua esquadra 
em servi^o naquela area do Atlantico - coura^ado Swiftsure, cruzador Highflyer e cruza- 
dor-auxiliar Marmora (um paquete da P&amp;O armado). 

Quando em 24 de Eevereiro de 1916 foram apresados os 8 navios alemaes estacionados 
no Mindelo, foi o pessoal das canhoneiras Beira e Ibo quern executou esta opera^ao e fez 
a guarda dos navios, cujas tripula^oes foram desembarcadas, ftcando a guarda do pessoal 
da Forga Expediciondria ate serem internados na ilha de Sao Nicolau sob a guarda de uma 
for^a de Marinha comandada pelo 1“ tenente Garces de Lencastre, Delegado Maritimo da 
Praia. 

Em 16 de Marqo de 1916, chegaram a Sao Vicente mais navios da Royal Navy, sob o co- 
mando do contra-almirante Gordon Moore - coura^ado Swiftsure, cruzadores King Al¬ 
fred, Suttley, Lancaster, Essex, Highflyer, Kent, cruzadores-auxiliares Carmania, Ophir, 
City of London e Marmora e numerosos navios auxiliares. 

Deslocada do Eunchal perante a amea^a dos submersiveis alemaes - agora com maior 
autonomia - a frota deslocou-se para Sul, acolhendo-se em Cabo Verde para patrulhar as 
aguas do Atlantico, a Sul das Canarias, o Golfo da Guine e o Atlantico Sul. 

Mas a baia do Mindelo era tambem vulneravel a ataques de submersiveis, face aos seus 5 
kms de abertura e os cerca de 100 metros de profundidade que dibcultavam a vigilancia e 
permitia a aproxima^ao dos submersiveis em imersao. 


III. A GUERRA NAS COLONIAS 


179 



Figura 4. A Baia do Mindelo e o seu porto. Desenho de Jose Manuel Cabrita. 


O alargamento para Sul, da amea^a submarina, obrigou os britanicos a retirar para Freeto¬ 
wn em Novembro daquele ano. Com a saida da esquadra deixaram tambem de frequentar 
aquele porto nacional os transportes de tropas australianos e os navios com cereals vindos 
da Argentina. 

O porto foi sujeito a varios ataques de submerslveis alemaes; a 4 de Dezembro de 1916 
(um dia depois do ataque ao Funchal), o U-47 tentou entrar na bala do Mindelo onde se 
encontrava o paquete Mogambique com 500 militares a bordo e numeroso material de 
guerra; detectado pelos vigias da canhoneira Ibo, foi atacado a tiro e obrigado a mergulhar 
ainda dentro do porto e sair em imersao. 

A canhoneira Beira, que tambem se encontrava no Mindelo, largou em auxilio da Ibo e, 
quando o inimigo voltou a superficie, ja fora da bafa, estava proximo daquela que o atacou 
com a sua artilharia obrigando-o a mergulhar novamente. 


180 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 

































A amea^a submarina na regiao levou a que a tripula^ao e os passageiros civis do paquete 
Mogdmedes, que regressava de Africa em Janeiro de 1917, protagonizassem um episodio 
de revolta, recusando-se a largar de Sao Vicente sem que Ihes fosse garantida a escolta por 
um navio militar. Amea^ados com a prisao, os tripulantes decidiram conduzir o navio para 
Lisboa. 

Recordemos que em 13 de Novembro de 1916, o vapor Machico, (ex-alemao Belmar, de 
6.118 tab) que regressava de Mozambique, foi atacado por um submersivel quando nave- 
gava a Norte das Canarias. Utilizando toda a potencia da maquina o navio conseguiu colo- 
car-se fora do alcance das pe^as do submersivel e abrigar-se entre as ilhas do Arquipelago 
das Canarias (a Espanha era um pais neutro), evitando a sua destrui^ao. 

Em 9 de Eevereiro de 1917 houve nova tentativa de ataque ao porto; avistado pelos vigias 
do ilheu dos Passaros, o submersivel desapareceria com a saida do porto da canhoneira Ibo. 

Outro ataque foi efectuado em 2 de Novembro de 1917 pelo cruzador-submarino U-151, 
que torpedeou os vapores Guahyba e Acary de nacionalidade brasileira; os dois torpedos 
foram disparados de fora da baia a cerca de 300 a 450 metros dos dois navios que atingidos 
na linha de agua se afundaram. A reac^ao da Ibo, que largou logo em sua persegui^ao, fez o 
inimigo abandonar o ataque e mergulhar. 

A presen^a frequente de submersiveis alemaes naquela regiao levou as autoridades mi- 
litares e navais portuguesas a desconfiar da existencia de uma celula ulema na ilha que 
fornecesse informazoes e outros apoios aos inimigos. 

As desconfianzas viriam a recair sobre o navio mercante holandes Kennemerland, que all 
se encontrava desde o inicio do conflito ao abrigo do estatuto da neutralidade. A sua tripu- 
lazao tinha comportamentos menos apropriados, nomeadamente quanto ao cumprimento 
das normas de seguranza do porto. 

Eram frequentes os sinais de luzes do navio para o mar, o uso indevido da TSE e o incum- 
primento das determinazoes sobre a ocultazao de luzes no periodo nocturno, permitindo a 
sua localizazao e a da entrada do porto. 

No entanto, e apesar dos argumentos apresentados pelos comandos militares - nomea¬ 
damente pelo comandante da canhoneira Ibo, capitao-tenente Henrique Monteiro Correia 
da Silva, o Governador, comandante Abel Eontoura da Costa, nao apoiava uma aczao mili¬ 
tar sobre o navio e o internamento da sua tripulazao. 

Eoi depois do ataque e afundamento dos navios brasileiros que o comandante da Ibo re- 
solveu intervir, o que Ihe Valeria uma chamada a Conselho de Guerra. O navio seria ocu- 
pado por uma forza de marinheiros da canhoneira Ibo e a sua tripulazao desembarcada e 
internada em terra. Eicou no navio uma pequena guarnizao de presa constituida por mili¬ 
tares portugueses (Silva, 1931). 

Quando a 7 de Novembro o mesmo submersivel entrou na baia a coberto da noite e acos- 
tou ao Kennemerland, foi atacado a tiro pelos militares de bordo e pela Ibo, sendo obrigado 
a largar e a mergulhar. 

A 14 do mesmo mes voltou a atacar a baia do Mindelo, apos o que abandonou a regiao; 
dois dias depois atacou, junto a ilha da Madeira, o navio americano Margaret L. Roberts. 

Com a chegada da canhoneira Beira, em Janeiro de 1918, vieram e foram instaladas bar- 
reiras submarinas. O navio trazia uma artilharia reforzada com mais uma peza de 90 mm, 
cargas de profundidade e caixas de fumo. 


III. A GUERRA NAS COLONIAS 


181 


Apos a chegada da Bengo em 5 de Maio de 1918, o service de vigilancia passou a ter a 
seguinte constitui^ao: 

Canhoneira Beira armada com 1 pe^as de 90 mm, 1 de 65 mm, 2 de 47 mm e duas cargas 
de profundidade 

Canhoneira Bengo armada com 1 pe^as de 90 mm, 4 de 47 mm e duas cargas de profun¬ 
didade. 

Patruihas Brigadeiro Barreiros, Fendale e Mindelo armados, cada um com uma pe^a de 
47 mm e uma bomba de profundidade. 

Barragem anti-submarina - constitmda por tres panos de redes com um comprimento 
de 2.340 metros com 52 minas e 3 barca^as, guarnecidos por 1 obciai, 2 sargentos, 6 pranas 
e mimeros auxiiiares civis. 

Baterias de costa nas pontas Norte (4 pe^as Canet 47 mm) e Sui (2 pe^as Armstrong de 
150 mm e 2 pe^as Krups de 76 mm), da Bafa. 

Posto de vigilancia no iiheu dos Passaros guarnecido por um sargento e 9 pranas e uma 
bateria de 1 pe^as de 90 mm e 2 de 47 mm. 

Com estas defesas, o porto voitou a ter movimento - com 80 navios em Setembro de 1918 
- e aii se organizaram, ate ao bnai da guerra, dois comboios com 24 e 19 navios sob a escoita 
de cruzadores - auxiiiares britanicos (inso, 2006). 

Em Setembro de 1918 o Almirantado Britdnico soiicitou a coiaboraejao da Divisao Naval 
de Operagoes de Guerra da Marinha brasiieira, entao estacionada em Dakar, para a patru- 
iha das aguas adjacentes ao arquipeiago de Cabo Verde, onde tinham sido avistados varios 
submersfveis inimigos. 

Os navios brasiieiros debatiam-se com a epidemia da Gripe Espanhola (pneumonica) e 
apenas os contratorpedeiros Piaui e Santa Gatarina ainda dispunham de guarniqoes sub- 
cientes para navegar; foi determinado peio Comandante-chefe brasiieiro, Aimirante Fron- 
tin, que aqueies navios iargassem para Sao Vicente a 8 de Setembro. 

Mas epidemia aiastrou-se a bordo do Santa Gatarina e apenas o Piaui se fez ao mar. Mas 
a gripe acompanhou-o e quando dois dias depois atingiu o Mindeio e fundeou junto as ca- 
nhoneiras portuguesas Beira e Bengo, o mimero de doentes existentes impossibiiitavam o 
navio de continuar a navegar (Maia, 1961). 

Enquanto a guarni^ao brasiieira melhorava, a epidemia atacou as guarni^oes dos navios 
portugueses. Seria o navio brasiieiro quern, ate 19 de Outubro, garantiu a vigilancia da en- 
trada do porto do Mindelo, enquanto as guarni^oes portuguesas recuperavam da epidemia 
que Ihes provocou nove mortos (8 na Beira e 1 na Bengo)^^ 


Faleceram ainda, naquele fatidico mes de Outubro de 1918, mais nove militares do Exercito, dos quais, dois 
oficiais. 


182 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



5. Conclusdes 


A participa^ao da Marinha Portuguesa na Grande Guerra enorme relativamente aos sens 
poueos reeursos e os marinheiros portugueses podiam orgulhar-se do trabalho realizado. 

Sem o eaminho do mar nao teria sido possivel o abastecimento do Corpo Expediciondrio 
Portugues na Flandres, a ac^ao mais visivel da partieipa^ao portuguesa no conflito, nem a 
defesa do Ultramar, abnal uma das razoes da nossa partieipa^ao no conflito. 


Referencias 

Barata, Gen. M. T. e Teixeira, N. S. (2003-2005). NovaHistdriaMilitar de Portugal (5 vol.). 
Lisboa: Clrculo de Leitores. 

Correia, L. M. (1992). Paquetes Portugueses. Lisboa: Edi^oes INAPA. 

Costa, A. R. (2006). Diciondrio de Navios e Relagd.o de Efemerides. Edi^oes Culturais da 
Marinha. 

Ferreira, J. J. B. (2002). Evolugd.o do Conceito Estrategico Ultramarino Portugues Da Con- 
quista de Ceuta a Conferencia de Berlim. Lisboa: Atena. 

Gonzalez-Calleja, E. e Alberta, P. (2014). Nidos de Espias. Espana, Erancia e la Primera 
GuerraMundial 1914-1919. Alianza Editorial, 

Inso, J. (2006). A Marinha Portuguesa na Grande Guerra. Lisboa: Edi^oes Culturais da 
Marinha. 

Junior, C. (1944). Ao Servigo da Pdtria. A Marinha Merc ante Portuguesa na I Grande 
Guerra. Lisboa: Editora Maritimo-Colonial. 

Keenan, J. (2004). Battle at Sea. Prom Man-of-war to Submarine. London: Pimlico. 
Loureiro, C. G. A. (1940). Estaleiros Navais Portugueses. I - Arsenal da Marinha. Lisboa. 
Mahan, A. T. (1987). The Influence of Sea Power upon History 1660-1805. Greenwich 
(USA): Presidio. 

Maia, P. (l96l). D.N.O.G. Uma Pdgina Esquecida da Histdria da Marinha Brasileira. Rio 
de Janeiro: Servi^os de Documenta^ao Geral da Marinha. 

Mendes, J. A. S. (1989-2005). Setenta e Cinco Anos no Mar (1910-1985) (17 Vol.). Lisboa: 
Edi^oes Culturais da Marinha. 

Momentos de Histdria (s/d). Grande Guerra. [Em linha]. Disponivel em: http://momen- 
tosdehistoria.com/001-grande_guerra (consultado em Novembro de 2016). 

Monteiro, A. S. S. (1990-97). Batalhas e Gombates da Marinha Portuguesa (8 Volumes). 
Lisboa: Sa da Costa Editora. 

Navios e Navegadores (2018). Histdria trdgico-maritima. [Em linha]. Disponivel em: na- 
viosenavegadores.blogspot.com (consultado em Dezembro de 2015) 

Nunes, A. R. P (1923). Portugal na Grande Guerra. A Acgdo da Marinha. Lisboa: Imprensa 
Nacional. 

Oliveira, F. (1983). A Arte da Guerra no Mar. Lisboa: Edi^oes Culturais da Marinha. 
Oliveira, M. (1936). Armada Gloriosa. Lisboa: Parceria Antonio Maria Pereira. 

Paeheeo, B. (2014). A Marinha na 1 Guerra Mundial. in: Anais do Glube Militar Naval, 
Jul-Dez. 


III. A GUERRA NAS COLONIAS 


183 


Pemsel, H. (1987). A History of War at Sea. Annapolis: Naval Institute Press.. 

Pereira, J. A. R. (1968). Tres Meses de Marinha Republicana. Revista Tridente. Alfeite: Es- 
eola Naval. 

Pereira, J. A. R. (1992). A Marinha Portuguesa nos Conflitos Europeus dos Seculos XVll a 
XX. In: Actas do III Coldquio de Histdria Militar - Portugal e a Europa nos Seculos XVII 
a XX. Lisboa. 

Pereira, J. A. R. (2010). Marinha Portuguesa - Nove Seculos de Histdria. Lisboa: Edi^oes 
Culturais da Marinha. 

Pinto, J. L.L. (2014). Participagdo da Marinha cm Africa na Grande Guerra, Comunica^ao 
a Aeademia de Marinha, em 14 de Outubro de 2014. 

Salgado, A. A. (2014). A See^ao de Auxiliares da Defesa Maritima. Cria^ao e Ae^oes na 1“ 
Guerra Mundial, in: XXIII Goldquio de Histdria Militar. Comissao Portuguesa de Histdria 
Militar. 

Salgado, A. A. e Russo, J. (2015). Submarinos Alemaes na Costa Portuguesa. O Caso do 
U-35, in: Actas do Goldquio Internacional “A Grande Guerra Um Se'culo Depois”. Lisboa: 
Academia Militar. 

Salgueiro, A. (l94l). A Evolu^ao da Marinha de Guerra Portuguesa nos Ultimos 50 Anos 
(1890-1910). Revista de Marinha, 132 a 137. Lisboa, Outubro/Novembro. 

Santos, J. F. (2008). Navios da Armada Portuguesa na Grande Guerra. Lisboa: Academia 
de Marinha. 

Selvagem, C. (l93l). Portugal Militar. Lisboa: Imprensa Nacional. 

Sena, C. (1926). Marinha de Guerra Portuguesa. Apontamentos para a sua Histdria. Se¬ 
parata da Revista Militar. Lisboa. 

Silva, F. A. P. (1909). O Nosso Plano Naval. Lisboa: Biblioteca da Liga Naval Portuguesa. 
Silva, F. D. (2014). A Marinha e a “Paz Armada” - Pianos Navais 1897-1916. in: Anais do 
Glube Militar Naval, lul-Dez. 

Silva, H. C. S. (Pa^o de Arcos) (l93l). Memdrias da Guerra no Mar. Coimbra: Imprensa da 
Universidade. 

Silva, J. F. (2014). A Marinha de Comercio na Grande Guerra (1914-18). Comunica^ao a 
Academia de Marinha em 28 de Outubro de 2014. 

Telo, A. J. (Coord.) (1999). Histdria da Marinha Portuguesa. Homens Boutrinas e Orga- 
nizagdo (1824-1974). Lisboa: Academia de Marinha. 

Telo, A. J. e Sousa, P. M. (2016). O GEP. Os Militares Sacrificados pela Md Politica. Porto: 
Fronteira do Caos. 

Telo, A, J., Salgado, A. A. e Russo, J. (2017). Agoes do U-35 no Algarve. 24 de Abril de 1917. 
Camara Municipal de Vila do Bispo e Escola Naval. 

U-boat.net. (s/d). U-boat War in World War One. [Emlinha]. Dispomvelem: www.uboat. 
net/wwi (consultado em Dezembro de 2015). 


184 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Para la do Rovuma mandam os que la estao: 

Ou o desastre militar em Mozambique durante 
a primeira guerra mundial de 14-18 

Jose Soares Martins 

FCHS/Universidade Fernando Pessoa 


“A memdria do men avo Manuel Baltazar Soares, sargento miliciano, com¬ 
bat ente nesta funesta empresa militar 16/18!” 


Nos finals do seculo XIX, a corrida para Africa por parte das grandes potencias colonials 
teve como principal objective aumentar o conhecimento dos colonizadores sob re a geogra- 
fta e etnograba das diversas regioes (e.g. Serpa Pinto, Livingstone, Hermenegildo Capelo). 
Claro que no fundo, o que estava em causa eram motiva^oes economicas e geoestrategicas. 
Surgindo mais tarde a Africa como um palco, para onde se prolongaria a guerra na Europa. 

Assim, com base no malfadado tratado de Berlim houve uma tentativa de satisfazer os 
paises em presen^a na mesa das negocia^oes desde a Espanha, a Portugal, passando pela 
Italia, Eran^a, Belgica, Alemanha e Gra-Bretanha e Holanda. Portugal, pequeno pais eu- 
ropeu, mas com um grande imperio africano, foi desde logo alvo da cobi^a das grandes 
potencias militares, sobretudo a Gra-Bretanha e a Alemanha. Como foi o caso do Mapa Cor 
de Rosa e do apetite ingles que sonhava com uma liga^ao costa a costa, entre o Cairo e a Ci- 
dade do Cabo. Pretensao que haveria de levar ao famoso ultimate de 1890 e que acabou por 
sublevar o orgulho dos portugueses e apressar a queda da monarquia. Tambem a queda de 
Quionga, lugarejo invadido pelo alemao, foi um exemplo mais de que os nossos interesses 
em Africa estavam paulatinamente a ser postos em causa. 

Quando a Grande Guerra eclodiu depois dos acontecimentos de Sarajevo. O pais viu-se 
na obriga^ao de zelar pela defesa das suas possessoes africanas. Sobretudo Angola, ataca- 
da a sul pelos alemaes da Namibia e Mozambique, amea^ado a norte pelas tropas de Von 
Lettow a partir do Tanganica. Assim, no inicio da guerra as defesas de Angola e sobretudo 
Mozambique, tornaram-se objectives prioritarios, tanto mais que britanicos e belgas cor- 
reram celeres para o novo palco da guerra. 

No entanto, a impreparazao militar, loglstica e sanitaria, logo surgiriam como bloqueios 
mais do que impeditivos da nossa ansia de gloria imperial. Assim o teatro das operazoes era 
nefasto para grandes empreendimentos: A estazao das chuvas de Outubro a Abril, as altas 
temperaturas, as variazoes termicas diurnas e nocturnas, a humidade, ja para nao falar das 
endemias, paludismo, doenza do sono, disenteria, febre-amarela, bilioses, pernicioses... 
aliadas a proverbial falta de cuidado higienico das tropas, ja para nao falarmos do obsoleto 
equipamento, ftzeram das campanhas africanas, nao a repetizao das glorias de Mouzinho 
Caldas Xavier ou Paiva Couceiro nas gestas contra os vatuas, mas as tragedias de quant os 
tomaram parte nas quatro expedizoes que da metropole foram enviadas, mau grade os de- 
pauperados cabedais da mae patria. 


III. A GUERRA NAS COLONIAS 


185 


A primeira expedi 9 ao 


No dia 18 d Agosto de 1914, foi decretado, o envio de uma expedi^ao a Mozambique, sendo 
destacado a frente de 1500 homens, Massano Amorim. Este eontingente, tinha como fun- 
Zao o refor^o das guarnizoes junto a fronteiro norte, mais eoncretamente, ao longo do Ro- 
vuma. Era uma eonsideravel formazao militar eonstituida por: 1 batalhao do regimento de 
infantaria 1; Bataria do regimento de artilharia de montanha; 4° esquadrao do regimento de 
eavalaria 10; pessoal de engenharia; pessoal de servizos de saiide; O embarque ocorreu a 11 
de Setembro de 1914, tendo chegado a Lourenzo Marques a 16 de Outubro de 1914. As tro- 
pas foram seguidamente transferidas para Porto Amelia a 1 de Novembro. Esta 1“ expedizao 
foi um verdadeiro fracasso. A eomezar pelo reerutamento. Imp reparados militarmente, 
sem o minimo de condizoes para se deslocar no terreno, desentendimentos inacreditaveis 
entre as chebas militares, falta de tropas indigenas e earregadores, problemas logisticos 
basieos. Eoi um verdadeiro milagre o faeto de sob o comando atribulado de Massano de 
Amorim se ter eonseguido a proeza de reforzar as nossas posizoes ao longo do Rovuma, de 
forma a ser possivel vigiar as movimentazoes inimigas. Claro que o objectivo mimero, um 
a reconquista de Quionga aos alemaes e a consequente progressao no Tanganica, ficaram 
para as calendas de Marzo. Ao cansazo das tropas mal preparadas, juntavam-se as doenzas 
endemicas e um clima inelemente que acabariam por se tornar no maior inimigo dos por- 
tugueses. Dai que em 15 de Outubro de 1915 a metropole enviasse uma segunda expedizao 
para ir em socorro de tropas exaustas e incapazes de lutar. 


A segunda expedizao 

O ano de 1914 havia sido um ano pacibeo em termos de reeontros com o inimigo, o mesmo 
nao aconteceria com a chegada do novo ano e dos novos efectivos. Esta nova expedizao era 
eonstituida por um batalhao do regimento 21; 1 bataria de metralhadoras do 7° grupo de 
metralhadoras; 1 bataria do regimento de artilharia de campanha; um esquadrao do regi¬ 
mento de eavalaria, sob o comando de Moura Mendes. Embora com a cronica imp repara- 
Zao das tropas, os caminhos abertos na anterior expedizao, para a penetrazao no terreno, 
facilitariam a movimentazao dos soldados. Em termos estrategicos o terreno de operazoes 
foi dividido em dois sectores. Sendo o primeiro sector divido em dois comandos militares, 
um com sede em Palma e o segundo em Mocimboa do Rovuma. O segundo sector tinha 
sede em Macaloge. Assim, a fronteira entre o Niassa e o Indico apresentava uma frente de 
900 km, com os postos militares dispostos ao longo do Rovuma com um maior efectivo de 
tropas em Negomano junto a foz do Rovuma. A recuperazao de Quionga, grande objecti¬ 
vo desta campanha, foi um relativo passeio para as nossas tropas, que nao encontraram 
resistencia do lado inimigo. No entanto, esta aczao foi saudada no Terreiro do Pazo com 
os maiores encomios pelos politicos de entao! Bravura sem par dos portugueses, superio- 
ridade civilizacional etc. Mas as nossas tropas ja se debatiam com as mesmas diftculdades 
de sempre. Doenzas, impreparazao militar, desorganizazao ao nivel dos comandos. E neste 
contexto que se prepara uma outra operazao de vulto, a primeira travessia do Rovuma. O 
cruzador Adamastor so chegou a foz do rio a 19 de Maio, hem como a canhoneira Chaimite. 
Houve uma tentativa mal sucedida de desembarque da marinha a 23 mas foi recebida por 

186 A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


uma salva de metralhadoras que provocaram nas nossas tropas 3 mortos e 6 feridos. Numa 
nova tentativa, tentou-se dias depois a travessia de dois vans, o de Namaca e o Namiranga. 
A primeira coluna ainda chegou a 150 metros da margem alema, mas foi violentamente 
alvejada por logo intenso das tropas alemas, o mesmo acontecendo com a segunda coluna. 
Foi um verdadeiro desastre com 3 oficiais e trinta pranas mortos, quatro oficiais e vinte 
pranas feridos. Este insucesso marcou de forma defmitiva esta segunda expedi^ao tendo se 
come^ado a pensar numa 3“ expedi^ao. 


3“ Expedi9ao 

Sob o comando de Ferreira Gil, enviou-se aquela que seria a maior de todas as expedi^oes 
a Mozambique, cerca de 4000 homens. Estas tropas nao puderam ter os refor^os dos efec- 
tivos das anteriores expedizoes, pois a sua grande maioria, estava inoperante, jazendo em 
hospitals de campanha a espera de seguir ou para Lourenzo Marques ou para a metropole. 
Nada mais sendo do que um bando de soldados maltrapilhos, famintos e doentes que se 
arrastavam penosamente como fantasmas pelo norte de Mozambique, mais propriamente 
por Palma. Assim, foi sob estes auspfcios que a 3°a expedizao chegou a Palma para iniciar 
uma ofensiva contra os alemaes, sob a forte desconftanza do comando ingles que nao sabia 
se deveria saudar esta missao portuguesa ou esperar uma nova dor de cabeza no conjun- 
to da guerra africana tal a inebcacia das nossas tropas. A ofensiva portuguesa tinha como 
principal objectivo a travessia de um Rovuma, infestado de crocodilos e vigiado de perto 
pelos alemaes. Entre 17 e 19 de Setembro de 1916, iniciou-se junto a foz a travessia dos vans 
de Namoto e Nacua, sem incidentes de maior, uma vez que as forzas alemas haviam retira- 
do estrategicamente das margens. Assim, sem um tiro as nossas tropas encontraram-se na 
outra margem perante o gaudio do Terreiro do Pazo, que classificaram esta travessia como 
um feito digno dos grandes acontecimentos da nossa histdria patria! A honra de Portu¬ 
gal estava garantida depois de Quionga! Muito embora os britanicos assim nao pensassem. 
Procurou- se chegar a Nevala, ponto estrategico alemao com um poder simbolico e militar 
para Portugal. A 26 de Outubro entrou-se em Nevala sem um tiro, devido a mais um recuo 
estrategico de Letow. Este feito foi de novo celebrado no Terreiro do Paqo como sempre. 
Mas Nevala acabaria por cair de novo em maos alemas depois de uma contra ofensiva a que 
nao resistiram as nossas forzas, que a coberto da noite e em condizoes dignas de um blme 
de guerra se dispersaram pela selva ate atingirem de novo as posizoes portuguesas junto ao 
Rovuma. Acabava assim a gloriosa investida da 3“a expedizao da qual havia tornado parte 
activa o meu avo. 


4‘‘expedi9ao 

A12 de Setembro chegou a quarta e ultima expedizao. Tinha como objectivo, sob o comando 
de Sousa Rosa, substituir as baixas e reforzar assim 3“ expedizao. Ligado a esta malfadada 
aventura esta o destino tragico do Regimento 31 do Porto. Sem treino militar, mas sob re- 
tudo sem vacinazao, sem obrigatoriedade de tomar o quinino todas as manhas, e muitas 
vezes sem a dose de quinino por falta dele, estas tropas morreram aos montes praticamente 


III. A GUERRA NAS COLONIAS 


187 


depois da chegada a Mocimboa da Praia (um erro estrategico gigantesco) com o seu clima 
hostil, os seus pantanos infectos e a malaria grassante, cerca de 500 homens tombaram 
para sempre sem sequer terem entrado em combate, vitimas nao so de paludismo, mas de 
disenteria, diarreia, pernieioses, bilioses, anemia palustre...., os sobreviventes envolve- 
ram-se nas opera^oes que se viriam a revelar desastrosas para a nossa presen^a. A come^ar 
por Negomano. Numa contra-ofensiva relampago em taetiea de guerrilhas que fustigavam 
os nossos postos fronteiri^os e tambem as posi^oes britanicas, chegaram a Negomano sob 
o eomando de Van Lettow que eonquistou este posto ehave abandonado depois de alguma 
resistencia pelas tropas nacionais. Dando-se de seguida o combate da Serra Maeula com 
igual desfecho para os germanicos. Estes eontinuaram a avan^ar para sul dando-se o re- 
eontro de Nhamacura que caiu igualmente na mao do inimigo. Este pouco tempo depois 
retirou estrategieamente para a Rodesia. Depois deu-se o armesticio sem que Lettow, o 
verdadeiro heroi destas campanhas, tivesse sofrido com os seus soldados e os askaris do 
Tanganica alguma derrota. 


Coticlusao 

Muito resumidamente, a nossa presen^a em Afriea foi um desastre para a Repiibliea, mau 
grado a histeria patriotiea que grassava no Terreiro do Pa^o e motivo de um profundo silen- 
eio depois do 28 de Maio de ma memoria. De facto, estas campanhas, foram desde sempre 
uma pedra no sapato do Estado Novo e as suas grandiosas narrativas sem sentido, quer 
sobre Africa quer sobre a Elandres, onde se procurava dourar a pilula nacional. No dizer 
de Sousa Rosa, o nosso exereito tinha telegrabstas analfabetos, pranas de engenharia que 
so sabiam eanto eoral, soldados de artilharia que nunea bzeram fogo e de infantaria que 
nao sabiam usar de espingardas, isto para nao se falar nas tropas indigenas... mas que di¬ 
zer de Sousa Rosa e da sua lideran^a? A isto some-se a indiseiplina, a desmotiva^ao geral, 
os pessimos equipamentos perfeitamente inadequados para os tropicos, falta de higiene, 
o desleixo e as doen^as. Corria o boato que o quinino tirava a potencia sexual e a grande 
maioria reeusava a dose diaria. Junte-se isto ao pessimo planeamento da guerra, com os 
soldados a fazerem caminhadas imiteis de eentenas senao milhares de quilometros contra 
um inimigo invisivel que ataeava so de surpresa numa tatiea de guerrilha desconhecida dos 
oficiais portugueses...Assim podemos dizer que dos 7 000 soldados tombados na 1“ guerra, 
tendo em conta as duas frentes, europeia e colonial, se na frente europeia tombaram em 
combate, cerca de 1800 soldados, os restantes 5000 morreram na selva africana, de com¬ 
bate, doen^as ou desaparecidos para sempre e sem rasto na selva inospita dos tropicos. Isto 
para nao falar dos infelizes carregadores que marehavam esqueleticos ate cairem mortos 
por exaustao nas bermas dos caminhos. Sem um queixume. Aos milhares. De facto, esta 
guerra foi uma guerra que no dizer de Manuel Carvalho, Portugal quis esquecer. O norte de 
Mozambique esta pejado de sepulturas e valas comuns onde jazem milhares desses infelizes 
para sempre esquecidos. Em conversas com o meu avo que esteve em Mozambique entre 
16-18, pude constatar, que ele dos tempos de Africa referia pouca coisa e a muito custo: A 
segunda travessia do Rovuma; Nevala; as longas caminhadas pela selva indspitas; as con- 
dizoes horriveis de Mocimboa da Praia; os hospitais de campanha; as pessimas condizoes 
em palhotas miseraveis e algumas operazoes em Nampula e perto de Quelimane; mas so- 


188 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


bretudo as doen^as que aos poucos matavam os soldados portugueses, sobretudo a malaria 
e as disenterias. Como linica recorda^ao de combate, um tiro de raspao numa perna. Ele 
que foi da sua companhia inicial um dos poucos que viveram para contar os seus feitos e 
que presenciou aterrado ao triste destino dos soldados do 31 e de quantos tomaram parte, 
brancos e negros neste pesadelo que ainda o assaltava muitas vezes ja na sua velhice. Onde 
a beleza da selva ou os crocodilos do Rovuma, os leoes e os leopardos na noite, os monhes 
e as negras bonitas de Moclmboa, eram exotismos que nao apagavam nem de longe, nem 
de perto, o assombro terrivel de tudo o que havia vivido bem digno do romance de Joseph 
Conrad, Coragao nas Trevas. 


Referencias 

Carvalho, M. (2015). A Guerra que Portugal quis Esquecer. Porto: Porto Editora. 

Marques, R. (2012). Os Fantasmas do Rovuma. Lisboa: Ohcina do Livro. 

Mirao, C. (2001). Kinani: Cronica de Guerra - Mogambique (17/18). Lisboa: Livros Horizonte. 
Pelissier, R. (2000). As Gampanhas Goloniais de Portugal (1844.19411) Vol 11. Editorial 
Estampa. 

Selvagem, C. (1925). Tropa de Africa: Jornal de Gampanha de um Voluntdrio do Niassa. 
Lisboa: Bertrand. 

Teixeira, N, S. (dir) (2004). Nova Histdria Militar Portuguesa. Lisboa: Circulode Leitores. 


III. A GUERRA NAS COLONIAS 


189 



IV. A historiografia da guerra 




As imagens da 1"* Guerra Mundial, na 
Historia e na Historiografia, em Portugal 

Antonio Paulo Duarte 

Institute da Defesa Nacional 

Institute de Historia Contemporanea (FCSH/UNL) 


Resumo: Esta apresenta^ao trata das imagens que em redor da participa^ao portuguesa na 
H Guerra Mundial foram elaboradas. E de eomo elas sao configuradas pela nossa maneira 
de olhar para a partieipa^ao de Portugal na 1“ Guerra Mundial. Nao obstante, nao estamos 
aqui estritamente a falar da historiograba e da sua eonstru^ao da historia, mas daquilo que 
a memoria eoletiva retem e da imagem eom que se apropria de uma dada realidade histori- 
ca, da representa^ao que faz de um dado aeonteeimento historico. Trataremos subsequen- 
temente de tres mementos temporais: o primeiro, nasce da forma eomo e feita a leitura da 
nossa interven^ao na 1“ Guerra Mundial por parte dos proprios protagonistas deste proces- 
so historieo; o segundo, na fase de resealdo e em eonsequeneia dos resultados que a a^ao 
gerada pelos intervencionistas produziu de faeto e a forma eomo a sociedade a proeura 
entao ler e dela se apropriar eomo memoria eoletiva; um terceiro memento, na atualidade 
e na reeonstru^ao eontemporanea da participa^ao de Portugal na P* Guerra Mundial e na 
revisao e refunda^ao da sua memoria eoletiva. 

Palavras-chave: Partieipa^ao portuguesa, constru^ao historiograftca, memoria coleetiva, 
reeonstru^ao contemporanea; Primeira Guerra Mundial. 

Abstract: This presentation deals with the images that around the Portuguese participation 
in World War 1 were elaborated. And how they are shaped by our way of looking at Por¬ 
tugal’s participation in World War 1. Nevertheless, we are not strictly speaking here about 
historiography and its construction of history, but about what collective memory retains 
and the image with which it appropriates a given historical reality, the representation it 
makes of a given historical event. 

We will deal subsequently with three temporal moments: the first is born of the way 
in which our own intervention in the Eirst World War is read by the protagonists of this 
historical process; the second, in the aftermath and in consequence of the results that the 
action generated by the interventionists produced in fact and the way in which the society 
then seeks to read and appropriates it as a collective memory; a third moment, nowadays 
and in the contemporary reconstruction of Portugal’s participation in World War 1 and in 
the revision and refoundation of its collective memory. 

Keywords: Portuguese participation, historiographic construction, collective memory, 
contemporary reconstruction World War 1 


IV. A HISTORIOGRAFIA DA GUERRA 


193 


Introdu^ao 

Alguns dos estrategistas mais reputados da cultura ocidental falam da guerra atraves da 
ideia de dialetica, de modo a realgar a sua logica dinamica, em que o movimento e deter- 
minante. Esta leitura da guerra pode-se observar em Clausewitz com as tres dinamicas 
da intera^ao que conduzem a ascensao aos extremos (Clausewitz, 1989, pp. 76-77) ou em 
Raymond Aron, a partir de Clausewitz, com um capitulo inteiro dedicado a dialetica da 
confronta^ao (Aron, 1976,151-287). E natural que a guerra seja um excelente motivo para 
as artes pictdricas ou grabcas, como a pintura, o desenho, mas tambem para a fotogra- 
ba. Movimento e espetacularidade combinam bem com arte, principalmente com as artes 
plasticas. Ora, quando pensamos na(s) ideia(s) de imagem(s) vemos uma representa^ao 
grabca ou plastica da realidade. Ao referirmo-nos a uma imagem, logo visualizamos um 
desenho ou uma fotograbca, um quadro ou uma escultura. Com efeito, segundo um dicio- 
nario de blosoba, uma imagem e ou deve ser uma “representa^ao mais ou menos exata de 
uma realidade qualquer” (Clement, Demonque, Hansen-Love e Kahn, 1994, p. 192). 

Nao e todavia a imagem que vemos, mas a representa^ao que o cerebro dela constroi. 
Susan Sontag observa, a proposito da fotograba, mas com mais propriedade, serve para as 
artes pictdricas e plasticas, “e sempre a imagem que algum escolheu; fotografar e enqua- 
drar e enquadrar e excluir” (Sontag, 2015, p. 50). As Imagens sao, efetivamente, elabora- 
^des sobre a realidade que as sociedades constroem. Como diz Eernando Pessoa no Livro do 
Desassossego, e o cerebro que narra o que vemos: “o romancista e todos nos, e narramos 
quando vemos, porque ver e complexo como tudo” (Pessoa, 2006, p. 56). 

E preciso, assim, observar que imagem e imagina^ao correlacionam-se e que a imagem 
nao e um espelho do real, mas uma burila^ao da realidade. De facto, imagina^ao e imagem 
tern a mesma origem etimoldgica, vindo do latim “imago”, que signibca “imita^ao” ou 
“representa^ao” ou “retrato” (Clement, Demonque, Hansen-Love e Kahn, 1994, p. 192). A 
imagem nao e por isso mera representa^ao, mas uma imagina^ao sobre o que e o real obser- 
vado e como deve ser representado. Assim, dentro deste sentido, a linguagem poetica, (tal 
como toda a linguagem), e tao produtora de imagens, como aquelas a que habitualmente 
reportamos as artes pictdricas ou grabcas ou plasticas. “As imagens poeticas sao bgura^des 
num sentido privilegiado, como formas introduzidas. Sao «imagina 9 des» (o resultado de 
pdr alguma coisa em imagens), incrusta^des (...)” (Han, 2018, p. 75). 

Esta apresenta^ao trata das imagens que em redor da participa^ao portuguesa na 1“ 
Guerra Mundial foram elaboradas. E de como elas sao conbguradas pela nossa maneira de 
olhar para a participa^ao de Portugal na 1“ Guerra Mundial. Mais do que ler a imagem des- 
pida de todo o conteudo narrativo e de todo o contexto da conjuntura em que e elaborada, 
parte-se da narra^ao que sobre a nossa participa^ao na 1“ Guerra Mundial e feita, em cada 
epoca, para nos aproximar-nos das imagens que sobre ela foram construfdas. A elabora^ao 
da narracjao sustenta de algum modo a burila^ao da imagem que sobre esse acontecimento 
e feita. A imagem aqui deve ser entendida, nao no sentido literal, de algo expresso de forma 
pictdrica, mas sim, no signibcado bnal que se extrai da representa^ao sobre essa realidade, 
no seu sentido ultimo ou na sua signibca^ao mais geral. 

Nao se trata aqui, nao obstante, de estudar a histdria da historia da nossa participa^ao 
na 1“ Guerra Mundial, mas de procurar e tentar ver como em cada epoca a sociedade por¬ 
tuguesa, genericamente, observou ou quis observar essa interven^ao e sobre ela construi 


194 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


uma narrativa, ou seja, na verdade, uma imagem, que a descrevia em termos curtos e sig- 
nificativos. Em suma, como a sociedade exprimia de forma precisa e incisiva a participa^ao 
de Portugal na Grande Guerra, tal qual o que se pretende eom a elabora^ao de uma imagem 
plastiea ou pietorica. Inegavelmente, os historiadores podem ter, de um modo ou outro, 
favoreeido mais ou menos uma dada interpreta^ao, a que a sociedade se apegou para ex- 
plicar, compreender e incorporar na sua memoria coletiva a experiencia de Portugal na 
“catastrofe seminal” que inaugurou o seculo XX. 

Nao obstante, nao estamos aqui estritamente a falar da historiografta e da sua constru^ao 
da histdria, mas daquilo que a memoria coletiva retem e da imagem com que se apropria 
de uma dada realidade historica, da representa^ao que faz de um dado acontecimento his- 
torico. Trataremos subsequentemente de tres periodos, o primeiro, no momento em que 
acontecimento em causa se desenla^a e se come^a a construir, ou seja, da leitura da nossa 
interven^ao na P* Guerra Mundial por parte dos proprios protagonistas deste processo his- 
torico; o segundo, na fase de rescaldo e em consequencia dos resultados que a a^ao gerada 
pelos intervencionistas produziu de facto e a forma como a sociedade a procura entao ler 
e dela se apropriar como memoria coletiva; um terceiro momento, na atualidade e na re- 
constru^ao contemporanea da participa^ao de Portugal na 1“ Guerra Mundial e na revisao 
e refunda^ao da sua memoria coletiva. 

Este texto nao pretende ser mais do que uma aproxima^ao a uma leitura sobre a imagem 
que ao longo do tempo se foi ediftcando na memoria coletiva sobre a interven^ao de Portu¬ 
gal na 1“ Guerra Mundial. Trata-se de procurar identiftcar alguns trains, necessariamente 
ainda incipientes, para mais num curto texto, que abram caminho para uma reflexao mais 
profunda sobre a memoria nacional e mais particularmente sobre a memoria da interven- 
(jao de Portugal na Grande Guerra. 


1. A imagem da “Grande Guerra” durante a 
1“ Guerra Mundial em Portugal 

A imagem coeva da interven^ao portuguesa na 1“ Guerra Mundial e bifocal, tal qual a situa- 
gao concreta em que o pals vivia. De um lado, “guerristas”, de outro lado “antiguerristas”. 
Para os republicanos radicals, propulsionadores da interven^ao portuguesa na 1“ Guerra 
Mundial, a participa^ao na conflagra^ao, por paradoxal que nos possa parecer, emergia 
como uma abrma^ao de modernidade e de perten^a. Portugal ao lado da “Entente” era 
um espelho de modernidade e de perten^a a Europa moderna. Para os que se opunham aos 
radicals republicanos e ao seu belicismo, o intervencionismo espelhava a vontade de uma 
minoria em servir os seus proprios interesses contra o sentimento geral da na^ao. 

Ao reler varias decadas depois a participa^ao de Portugal na 1“ Guerra Mundial, Norton 
de Matos, um dos mais importantes propulsionadores da nossa interven^ao nessa confla- 
gra^ao, come^a por realgar o que se pensava do que Portugal poderia dar as na^oes belige- 
rantes com que o pals acabaria por se aliar apos a declara^ao alema de guerra a si: 

“Nos centros militares ingleses (...), nao se julgou que a organiza^ao de uma divisao com- 
posta de cerca de 20.000 homens, importasse real sacriffcio para uma na^ao de 6 milhoes 
de habitantes, cujo or^amento de guerra e muito superior ao da maior parte dos estados 
balcanicos, os quais, com popula^oes muito inferiores a nossa, facilmente poem em pe de 


IV. A HISTORIOGRAFIA DA GUERRA 


195 


guerra muitas centenas de milhares de homens.” (Teixeira Gomes a Freire de Andrade, 
carta de 30 de novembro de 1914, citada em Matos, 2004, 3°, V, p. 5l). 

Esta parcela de uma carta exposta por Norton de Matos nas suas Memdrias e Trabalhos 
da Minha Vida, as memdrias que nos legou no ftm da vida, contem implicitamente muito 
do que foi a ideologia dos intervencionistas entre 1914 e 1918. Aqui presente esta, para alem 
da questao epidermica do mimero de efetivos que Portugal poderia ou nao fornecer, como 
combatentes, aos aliados, duas questdes implicitas: a da capacidade ou nao de Portugal 
ser uma na^ao moderna, capaz de intervir consequentemente na guerra que atravessava 
o continente, e seguidamente, de ser efetivamente parte da Europa, porquanto intervir 
na contenda, de forma substantiva, era na realidade, ser parte integrante da modernidade 
europeia, que se espelhava no poderio da violencia destrutiva da conflagra^ao. 

Por isso, para Norton de Matos, modernidade e transforma^ao de Portugal, e a rela^ao 
de Portugal com a Europa, passavam pela capacidade do pais ser capaz, estando a altura do 
desabo, como acontecera no periodo aureo dos Descobrimentos, de intervir substantiva- 
mente na guerra e no principal teatro de guerra, a Flandres em Franca. Para a peleja nao 
faltariam homens, diria Norton de Matos, considerando-se o homem como uma invariante 
universal: 

“Foi em agosto de 1915 que principiei a tratar de material, fardamentos e equipamentos 
para a o nosso exercito. Homens nao me faltavam, sabia-o bem. (...). Na guerra o essencial 
e o combatente e desse dispomos felizmente. ” 

Todavia, o “milagre”, para Norton de Matos, nao era mobilizar homens, mas apresentar 
um exercito moderno que pudesse travar uma guerra na Flandres contra os alemaes, um 
simbolo da modernidade por excelencia. Tancos foi uma demonstra^ao dessa modernidade: 

“Chamaram-lhe Milagre de Tancos. (...). Nunca se viu em Portugal tao grande mimero 
de formas militares reunidas; como apareceram, perguntava-se, todas aquelas viaturas hi- 
pomoveis e, como espanto geral, os automoveis, as centenas (...).” (Matos: 2004, 3“, V, pp. 
163 e 237). 

Como referem Antonio Telo e Marques de Sousa (Telo e Sousa, 2016, pp. 171 e 173), para 
os guerristas, as manobras de Tancos serviram para apresentar ao pais a modernidade que 
a guerra aportava ou deveria aportar. Era pura propaganda, mas espelhava o seu desejo e 
anseio ao mesmo tempo. A interven^ao portuguesa na Grande Guerra era a ponta de lan^a 
da moderniza^ao nacional e da sua inser^ao na Europa, pensavam os republicanos radicals, 
guerristas, e como tal, as manobras de Tancos foram uma enorme manobra de propaganda 
em que a imagem dessa modernidade foi explorada. Leia-se como e promovida pela llus- 
tra^ao Portuguesa, a epoca, o “Milagre de Tancos”: 

“A Jornada de Tancos foi quasi uma epopeia e uma vitoria para a Republica, Durante 
mezes 20:000 homens, beiroes, alemtejanos, durienses, transmontanos e ciladiaos ergue- 
ram alto, muito alto, o nome e o prestigio do nosso amado Portugal. Simples cavadores, 
gente rude, pobres aldeaos, trocando as alfaias agricolas, a picareta e o alviao, puzeram 
aos hombros uma espingarda e marcharam, conscios dos seus deveres, galhardamente, 
bizarramente, ensinando ao mundo inteiro que n’este velho e ignorado canto da Europa 
ha coraqoes que anceiam pela liberdade dos povos, ha homens valorosos capazes de defen- 
derem, palmo a palmo, o terreno conquistado aos que lutam pelos maiores ideaes de paz e 
de liberta^ao dos povos. Sim! Os mobilisados de Tancos, ja agora o ponto inicial do ressur- 
gimento da nossa ra^a, alem de uma preparaqao metodica, regular, cheia de patriotismo. 


196 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


ficaram sendo cidadaos robustos, homens fortes, um punhado de hercules, lembrando os 
audazes conquistadores dos tempos idos, os nossos antepassados, essa falange aguerrida 
que deslumbrou o mundo com os seus feitos e os seus rasgos de valor. De um pinhal incul- 
to, onde o mato crescia e as urzes medravam, como n’um quadro de muta^ao, em magica 
de grande espectaculo, bravos vigorosos, impulsionados por inteligencias lucidas, ergue- 
ram em poucos dias essa pequena cidade de madeira e Iona, na qual se vivia em plena liber- 
dade, na qual se respirava a plenos pulmoes.” (Martins, 1916, p. 106). 

Neste longo texto se conftgura muitos dos topos sobre o que deveria ser e que efeitos 
deveria ter a participa^ao portuguesa na 1“ Guerra Mundial: a transforma^ao da na^ao de 
incultos cavadores em novels cidadaos, num contexto de uma educa^ao moderna, “metd- 
dica, regular, patriotica, liicida”, que evidenciasse quanto Portugal era parte e pertencia a 
essa Europa moderna, tal qual ela existia no norte do continente, para onde partiriam esses 
soldados para combater e elevar o nome do pals, como o tinham feito os portugueses no 
seculo XV e XVI. Tancos era a na^ao moderna, a altura do grande desafto dessa moderni- 
dade, o da guerra, o da maior guerra de todos os tempos (ate entao obviamente), como os 
intervencionistas sabiam e nela queriam participar. 

Em nome da perten^a a modernidade e ao progresso, em nome da civilidade, Portugal 
assumia-se assim como beligerante. Este topos aparece igualmente num panfleto publica- 
do pela Junta Nacional de Propaganda Patriotica em 1916: 

“Tlnhamos de cumprir um dever: cumprimo-lo. Tlnhamos de zelar pelos mais altos in- 
teresses nacionais. Por eles lutamos. Mas os povos nao tern so deveres a cumprir e interes- 
ses a salvaguardar. Os povos tambem nutrem aspiraqoes magntaimas, tambem possuem 
um ideal, tambem defendem uma civiliza^ao. (...). Nao estamos desagregados do grande 
organismo da humanidade que luta por uma liberdade cada vez mais ampla, por um direito 
cadavez mais equitativo (...).” (JNPP, 1916, p. 7). 

Ou como diz Teixeira de Pascoaes, em 1916, precisamente sobre a guerra ou em defesa 
da guerra, “poderlamos evitar a guerra (...) se abdicassemos da nossa personalidade euro- 
peia”. (Pascoaes, 1916, p. 109). 

A imagem que assim se queria fazer passar e de um pals que com a interven^ao na guerra 
se modernizaria e se europeizaria. Este facto, paradoxalmente, seria feito atraves da inter- 
vencjao numa imensa contenda, o que contraditoriamente, na realidade, espelhava para os 
coevos a modernidade e a contemporaneidade. Os intervencionistas queriam fazer passar 
a mensagem de que a interven^ao era um salto para a perten^a a Europa civilizada e para a 
modernidade; era um processo de moderniza^ao radical da propria sociedade portuguesa, 
que se revelaria cidada patriotica e republicana. 

Pelo contrario, os seus oponentes observam a polltica intervencionista como se fosse de 
uma empresa ao servi^o de interesses pessoais ou de partido. A guerra e expressao de uma 
fa^ao que a impoe ao pals e como se um negdcio se tratasse, servindo os seus interesses e 
das potencias estrangeiras que esses proprios interesses servem, e que vao assim contra a 
vontade e os verdadeiros anseios da na^ao. Palavras como “vendilhoes” e “matadouro” sao 
comummente usadas nesta propaganda (Santos, 2013, pp. 38-40). Aqui, muitas das vezes, 
os republicanos acabam por ser vistos como “traidores” a na^ao (Santos, 2013: p. 39). 


IV. A HISTORIOGRAFIA DA GUERRA 


197 


2. A imagem da “Grande Guerra” durante “Nova 
Repiiblica Velha” e o Estado Novo 


Todavia, se durante a 1“ Guerra Mundial, o pais se ve confrontando eom duas imagens 
sobre a sua interven(;ao, apos a eonflagra^ao, de algum modo e neeessario reeonstruir 
uma imagem eonsensual sobre esta. E aqui sao as circunstaneias de ter-se de faeto par- 
ticipado na 1“ Guerra Mundial e de ter-se enviado formas expedieionarias para Afriea e 
principalmente, para a Europa, o que acabara por pesar na elabora^ao da narrativa sobre 
a guerra posterior ao fim da eontenda. O acontecido imp6e-se como realidade insobsma- 
vel. Gom o bm da 1“ Guerra Mundial, a imagem da interven^ao naeional ganha contornos 
distintos, que resultam em boa medida da edibca^ao de uma memoria da participa^ao 
portuguesa na eontenda que fosse o mais eonsensual possivel para a soeiedade e que, de 
algum modo, oeultasse a profunda dilacera^ao que o intervencionismo e o anti interven- 
eionismo engendraram durante a guerra. Aproveitando um diseurso, ainda elaborado 
durante a Grande Guerra, em torno da heroicidade sofrida do soldado portugues face 
a adversidade, elabora-se uma narrativa heroica em torno da nossa participa^ao na 1“ 
Guerra Mundial, que tern como corolario a derrota na denominada batalha de La Lys, em 
que as formas portuguesas so sao batidas gramas a dimensao (real, e certo) do ataque ale- 
mao.“ Esta visao da participa^ao portuguesa na 1“ Guerra Mundial perpassa por exemplo 
as paginas das memorias de Jaime Gortesao: 

“Goletivamente na guerra, na nossa guerra, salvou-se o soldado. Lie foi, sempre que o 
nao enganaram, paciente, sofredor, heroico. Teve na maior parte das vezes a compreensao 
das coisas mais elevadas. (...). Esse soldado e ainda o mesmo de Aljubarrota e do mar.” 
(Gortesao, 2016, pp. 161-2). 

Esta imagem da nossa participa^ao na 1“ Guerra Mundial passa de forma muito clara para 
os compendios escolares durante o Estado Novo. Note-se, adicionalmente, que sao livros 
unicos que refletem uma dtica ideologica conforme ao regime, como salientaremos mais a 
frente. 

“O mes de mar^o [de 1918] foi o mais ativo e de maior atividade dos nossos soldados, que 
se bateram heroicamente durante dias consecutivos. (...). Eoi nestas circunstaneias que, as 
4 boras da madrugada desse dia [9 de abril] os alemaes desencadearam a ofensiva da Elan- 
dres (...). Batemo-nos com a maior bravura, mas perdemos toda a artilharia, tivemos nu- 
merososmortos eforamfeitosprisioneiros 300 obciaise7.000 soldados. (...). Emsetembro 
de 1918 os batalhoes retomaram a marcha da ofensiva e a data do armisticio colaboravam 
ebcazmente com as brigadas inglesas no norte de Eran^a e na Belgica.” [Matoso, (s/d), p. 
361)]. Assim e descrito pelas maos de um dos autores mais proficuos de manuals de historia 
do Estado Novo, Antonio G. Matoso, a participa^ao de Portugal na Elandres, na 1“ Guerra 
Mundial assim como o seu corolario. 

Tal qual o texto memorial e quase historiograbco de Jaime Gortesao, o tema da bravura e 
da combatividade face a imensas formas adversas e o topico decisivo do texto de Antonio G. 
Matoso. Neste particular o que se glosa nao e tanto a elite que dirigia a for^a belica como a 


Escusa-se o texto de dissertar sobre a denominada batalha de La Lys, visto nao ser esse o foco o mesmo. O estudo 
mais completo e inovador sobre a mesma e de Telo e Sousa (2016) para o qual remetemos o leitor. 


198 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



povo humilde que espelhava, para la das debilidades inerentes a sua pobreza e a incultura 
nacional, as qualidades intrinsecas da ra^a. A bravura era um atributo do povo, do pals e 
da na^ao, algo que podia ser inserito na memoria coletiva e que earacterizava no esseneial a 
interven^ao de Portugal na Guerra Mundial. Realcemos adicionalmente que neste texto, 
como noutros tantos, assim parece, nunca se observa a imensissima discrepaneia entre o 
contributo portugues e britanieo para a conflagra^ao, eomo se ambos os paises tivessem 
feito um esfor^o de guerra com alguma similitude. 

A ideia da heroicidade, da bravura, da intrepidez nacional aparece em outros manuals 
do Estado Novo sobre a participa^ao portuguesa na 1“ Guerra Mundial, sendo ate a linica 
qualidade ou adjetiva^ao que e empregue em textos em geral muito secos e estritamente 
descritivos sobre a interven^ao portuguesa na conflagra^ao: 

“Em 1914 desencadeou-se na Europa um conflito armado que ftcou conhecido pela Gran¬ 
de Guerra, na qual se envolveram a Alemanha e a Turquia, a Bulgaria e a Austria-Hungria, 
contra a Eran^a, a Russia, a Servia, a Inglaterra, a Italia e outras na^oes aliadas, Portugal, 
a principio, manteve-se neutral: mas em Eevereiro de 1916, devido a secular alian^a que 
mantem com a Inglaterra, viu-se for^ado a entrar no conflito, que so terminou em Novem- 
bro de 1918 pela derrota da Alemanha, que entao pedira a paz, conbrmada pelo Tratado de 
Versalhes (1919). O exercito portugues, quer em Eran^a, onde suportou batalhas formida- 
veis como a de La Lis - 9 de abril de 1918 - quer em Angola e Mozambique, deu sempre as 
maisbrilhantesprovasdeheroismoedevalentia”. [Barros, (s/d), pp. 169-70). 

Neste texto, ediftcado para a 4“ classe e para a admissao aos liceus, o autor constrdi sobre 
a nossa participa^ao na 1“ Guerra Mundial um texto quase que estritamente informativo, 
de datas e intervenientes e acontecimentos, a que so escapa a frase ftnal, que qualibca e 
opina exatamente a forma como os portugueses se comportaram na contenda: heroicos e 
intrepidos. 

Os manuals do ensino da historia e da blosofia do Estado Novo tinham por ftto formar 
consciencias nacionalistas e catolicas e promover a unidade moral da na^ao, conftguran- 
do uma visao linica do passado (Garvalho, 2005, pp. 74-78). Neste sentido, a imagem que 
estes compendios de historia espelham e consonante com a base ideoldgica do regime do 
Estado Novo. E, no entanto, interessante observar que o quadro geral em que a intervenzao 
portuguesa e descrita nestes compendios tern um cariz nacional, ocultando a profunda 
divisao politico-ideoldgica que a altura da intervenzao da conflagra^ao dilacerava a socle- 
dade portuguesa, caracter nacional que se reflete depois na intrepidez com que os soldados 
portugueses se batem. Parece dbvio que o discurso narrativo do Estado Novo sobre a inter- 
ven^ao portuguesa na 1“ Guerra Mundial procurar ter um caracter consensual e nacional, o 
que encaixaria com a visao ideoldgica de os manuals proveram a unidade moral da na^ao. 
Ora, este racional coadunava-se igualmente com a leitura que a prdpria “Nova Repiiblica 
Velha” quisera formular sobre a participa^ao portuguesa na Grande Guerra. E nesse sen¬ 
tido, engendrou-se um discurso narrativo, de cariz nacional, comum a todas as partes e 
consensual sobre a interven^ao portuguesa na 1“ Guerra Mundial, muito ligado a heroici¬ 
dade e a combatividade do soldado (= povo) portugues. 


IV. A HISTORIOGRAFIA DA GUERRA 


199 


3. A imagem da “Grande Guerra” na contemporaneidade 

Coube a Nuno Severiano Teixeira abrir as portas para um repensar contemporaneo sobre 
as causas da interven^ao portuguesa na Grande Guerra, abandonando uma leitura estri- 
tamente externa dos seus motivos, e em consequencia, observada segundo criterios na- 
cionais, recolocando o olhar, a imagem da participa^ao na contenda a partir de lentes 
internas. Partindo de novas conce^oes historiograbcas e de aproxima^oes teoreticas das 
Rela^oes Internacionais, Nuno Severiano Teixeira valorizou as dinamicas e os comporta- 
mentos politicos internos na elabora^ao da politica externa de cada Estado, concluindo que 
o motivo decisivo da participa^ao de Portugal na 1“ Guerra Mundial resultara da vontade da 
corrente republicana hegemdnica, alicer^ada no Partido Republicano Portugues, ou como 
era entao denominado, “partido democratico”, de instrumentalizar a guerra em vista a 
consolida^ao da Repiiblica, e do proprio poder democratico, quer internamente (atraves da 
“ Uniao Sagrada ”, seguindo o modelo frances), quer externamente (edibcando uma parce - 
ria estrategia com as demais potencias aliadas, equiparando Portugal a estas e assegurando 
o prestigio internacional do pais no seio da “Entente”) (Teixeira, 1996). Como disse Pedro 
Aires Oliveira, com a tese de Nuno Severiano Teixeira, torna-se dominante na leitura da 
historiograba, “o primado da politica interna” como motivo e fonte da participa^ao de 
Portugal na 1“ Guerra Mundial. (Oliveira, 2011, pp. 185 e 190-193). 

Nao obstante, o proprio Nuno Severiano Teixeira logo observou que a politica intervencio- 
nista nao era consensual na sociedade portuguesa (Teixeira, 1996) e fomentou fortissimas 
resistencias internas de variado tipo ao longo da conflagra^ao. Antonio Telo diz que com a 
Grande Guerra, a disputa politica pre-existente seria enquadrada na oposi^ao entre “guer- 
ristas” e “antiguerristas”, sendo esta cisao o eixo em redor do qual todo o combate politico 
e ideologico se desenvolveria (Telo, 2014:10-14). 

A leitura de Portugal na 1“ Guerra Mundial passa entao a ser lida e observada a luz de um 
conceito mais abrangente, cunhado por Eernando Rosas, as “guerras civis intermitentes” 
(Rosas, 2007), que espelha a profunda divisao e luta politica e social por que passava o pais 
nos anos bnais do regime monarquico constitucional e pela 1“ Republica, e que muitos his- 
toriadores e cientistas sociais enquadram na crise provocada pelas transforma^oes radicais 
geradas pelos paradigmas tecno-industriais em sociedades ainda fortemente marcadas 
pelas estruturas socioecondmicas e politicas do antigo regime. Podemos e devemos dizer 
que Portugal vivia durante a 1“ Republica um estado de “guerra civil larvar” pontuado por 
“guerras civis intermitentes” e que atingiriam um paroxismo muito elevado durante o pe- 
riodo da 1“ Guerra Mundial. Com efeito, das cinco grandes batalhas que acontecem em 
Lisboa durante os 15,5 anos de 1“ Republica (3-5 de outubro de 1910; 14-15 de maio de 
1915; 5-8 de dezembro de 1917; a “Monarquia do Norte” em Janeiro e fevereiro de 1919; o 
18 de abril de 1925), tres aconteceram durante a conflagra^ao mundial ou no seu rescaldo 
(Duarte, 2015: 94-95). 

A intervencjao de Portugal na 1“ Guerra Mundial: A “guerra civil Larvar” e a sua conse¬ 
quencia nas “guerras civis intermitentes” acabam por ser um espelho daquilo que pode ser 
olhado como uma antinomia, uma oposi^ao entre o Estado e a Na^ao. Esta antinomia, se 
assim ela pode ser debnida, e expressa e comentada por Nuno Severiano Teixeira, a partir 
de um texto de Aquilino Ribeiro, referente ao inicio da guerra, e que resulta de um en- 
contro deste com um dos mais fervorosos “guerristas”, o embaixador (Ministro Plenipo- 


200 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


tenciario) de Portugal em Franca, Joao Chagas, na sede da embaixada em Paris, texto esse 
que serve para o historiador interpelar os motivos que levariam o pals a beligerancia e a 
expedi^ao para a Flandres do Corpo Expedicionario Portugues (C.E.P): 

“ Aquilino atravessou Paris, que partia para a guerra e dirigiu-se a Avenue Kleber, a Lega- 
9 ao de Portugal. Na chancelaria discutiam-se os dez mil homens, que segundo os jornais, 
Portugal la mobilizar contra os Imperios Centrals. (...). «Como a sugestao e disparatada, 
rematadamente disparatada, pode ser que vingue», retorquia Aquilino. (...). Aquilino re- 
gressa a casa e cruzando Paris, em sentido inverso e pensa para si proprio: «Em nome de 
e que justa, necessaria causa, se podem despachar para o matadoiro os meus pobres, ig¬ 
norant es e pacibcos labregos?» (...). (...) [Era] a questao da atitude de Portugal perante a 
guerra: o consenso nacional nunca conseguido.” (Teixeira, 1996: pp. 188-9). 

A questao posta era e e precisamente aquela que Aquilino Ribeiro punha em cima da mesa. 
Qual era a rela^ao real do pais com a Grande Guerra e com a participa^ao politica e militar 
de Portugal na contenda? 

Nao sera por acaso que os estudos historiograbcos recolocaram o seu olhar sobre a vida e 
o dia-a-dia e o quotidiano dos soldados Portugueses da Grande Guerra nos anos imedia- 
tamente posteriores a obra de Nuno Severiano Teixeira. A obra de Isabel Pestana Marques, 
Das Trincheiras com Saudade, a vida quotidiana dos militares portugueses na Primeira 
Guerra Mundial (Marques, 2008), sobre o C.E.P., e de Marco Fortunato Arrifes, A Primeira 
Grande Guerra na Africa Portuguesa, Angola e Mozambique (1914-1918), (Arrifes, 2004), 
essencialmente, sobre as expedi^oes enviadas para Mozambique, sao marcos essenciais 
nesta releitura, a luz “dos de baixo”, do soldado, do combatente, a partir da vida dos mo- 
bilizados, mais do que dos agentes politicos da interven^ao. Era voltar a dar vida e dando 
a conhecer a histdria dos “labregos” de que falava Aquilino Ribeiro, do povo humilde em- 
purrado para a guerra pelos politicos e pelas guerras entre os politicos. 

Todavia esta releitura da participa^ao nacional na 1“ Guerra Mundial, mais do que condenar 
e vituperar a sua memoria, deu-lhe um novo folego e uma nova legitimidade: os mobili- 
zados mais do que os politicos, pese todos os erros e incompetencias destes, cumpriram 
o melhor que puderam, e muitas vezes com grande sofrimento, a missao militar para que 
tinham sido chamados. O pais, apesar de tudo apresentou-se. A na^ao esteve presente na 
hora, o Estado, esse, nem tanto. 

Expressivo desta releitura e a obra, talvez das mais notaveis, e sem diivida, altamente ino- 
vadora no que toca ao conhecimento da histdria do C.E.P. de Antonio Telo e Pedro Marques 
de Sousa e que tern por titulo, signibcativamente: “O CEP. Os militares sacribcados pela ma 
pobtica” (Telo e Sousa, 2016). De igual modo, e a respeito do campo de convalescentes de 
Goba, em Mozambique, que mais nao seria que um verdadeiro campo de morte, escreve 
Manuel de Carvalho “um monumento pdstumo seria la construido para nao deixar esque- 
cer essa ultima afronta de Portugal aos portugueses” (Carvalho, 2015, p. 235). Nao se pode 
ser mais eloquente, se lermos a histdria da intervenzao de Portugal na 1“ Guerra Mundial, a 
luz desta dicotomia entre o Estado e a nazao. 

Reside aqui a ultima grande ironia da histdria. A guerra que os republicanos radicals qui- 
seram que os legitimasse, tornou-se, como disse Eernando Rosas em recente conferencia. 


IV. A HISTORIOGRAFIA DA GUERRA 


201 


uma “decisao suicidaria”^^ que os condenou a prazo. Nao obstante, por um extraordina- 
rio paradoxo, bem observado por Nuno Severiano Teixeira, essa mesma guerra legitimou 
a Repiiblica, no sen sentido mais geral. Os que combateram e morreram pela patria na 
guerra e a bandeira que os guiou, a republieana, ftzeram eom que a Repiiblica adquirisse 
uma legitimidade politica de cariz verdadeiramente nacional - a Repiiblica, nao os radicals 
propulsionadores do intervencionismo: 

“Entre 1914 e 1918 foi sob a bandeira verde e vermelha que as tropas portuguesas defen- 
deram os interesses nacionais e a integridade do territorio colonial em Africa”. (Teixeira, 
2015, 67). 

Teixeira conta mesmo um interessante episodio, passado no Porto, (provavelmente, pare- 
ce-nos, em 1919), em que uma mulher do povo exclama ofendida ao ver arrastar a Portu- 
guesa pelo chao: “E muito mal feito. Entao a bandeira que vi cobrir o corpo de um soldado 
que veio de Eran^a ferido e veio ca morrer e para se por all? A bandeira que cobriu o caixao 
do Sr. Presidente da Repiiblica Sidonio Pais! Isto nao se faz!” (Teixeira, 2015, pp. 67-8). 
Coaduna-se com esta visao da participa^ao portuguesa na 1“ Guerra Mundial o proposi- 
to que conduz a Comissao Coordenadora da Evoca^ao do Centenario da 1 Guerra Mundial 
no desenvolvimento das suas atividades evocativas e que e pelo seu presidente, o General 
Mario de Oliveira Gardoso, expresso na sua apresenta^ao no sitio da comissao na internet: 
“Ha cem anos atras Portugal envolveu-se - e viu-se envolvido - num conflito criado e nas- 
cido na, ja na altura, velha Europa, sacudida, uma vez mais, por convulsoes que sempre 
tiveram a ver ou com fronteiras das Na^oes ou com a tentativa de dominio dos Estados mais 
fortes (...). 

O que motivou o envolvimento de Portugal; a forma como o fez; os resultados objectivos 
de uma interven^ao que nos fez combater em Africa, no Atlantico e na Europa e que nos 
custou 7760 vidas e mais de 30000 baixas, entre feridos, desaparecidos, incapazes e prisio- 
neiros, tern sido estudado, discutido e alvo de perspectivas justibcativas diversas. 

O facto e que a Histdria nao se muda. 

Assim, assinalar este periodo deve ser para nos, portugueses de outra era, um ato de ho- 
menagem ao sacrificio pedido ao Povo, que tudo deu para alcan^ar os objetivos que os di- 
rigentes do Estado entendiam ser adequados para a sobrevivencia soberana de Portugal. 
Esse respeito pelo sacrificio e o que anima este projecto. 

Nao nos preocupa, enquanto comissao, que existam perspectivas contraditorias; que se 
revelem as nossas fraquezas na condu^ao politica e militar ou que sejam evidenciadas as 
nossas qualidades, individuals e coletivas. 

Preocupa-nos sim se a discussao, que se pretende que exista ao longo deste periodo entre 
2014 e 2018, se alheie ou ignore o sacrificissimo do Soldado de Portugal que, uma vez mais 
na sua longa caminhada, deu tudo; e tudo e a propria vida! 


Fernando Rosas, “Portugal, o quadro interno e o quadro externo”, Conferencia pronunciada no Seminario “O Fim 
da Guerra Mundial: Consequencias para a Politica Externa e a Politica de Defesa de Portugal” realizado no Instituto 
da Defesa Nacional a 27 de fevereiro de 2018. 

Mario de Oliveira Cardoso, Comissao Coordenadora da Evoca^ao do Centenario da 1 Guerra Mundial. Apre~ 
sentagao - Proposito da Comissao, apresentado pelo Presidente da Comissao, Sitio da Comissao na Internet, 
Portugal na Grande Guerra. O texto do proposito esta datado de 21 de maio de 2013. In http://ivwiv.portugalgran- 
deguerra.defesa.pt/Paginas/A%20Comissao.aspx, consultado em 30 de abril de 2018. 


202 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



O “proposito” da Comissao nao recusa que se reconhe^am as dilacera^oes por que passava 
o pais de um ponto de vista politico e ideologico ou outro, nem que nao se fala das debili- 
dades estrategieas, tatieas e logisticas das formas militares portuguesas. O seu fito e outro. 
E evocar e relembrar os que se batendo, morreram pela patria, ou seja, deram a vida pelos 
portugueses. A anamnese dos mortos em eombate, e que se eorrelaciona eom a ideia de um 
Portugal pobre, no inicio do seeulo XX, e um povo sofrido. 

A memoria da interven^ao de Portugal na Guerra Mundial nacionalizou-se, a despeito 
da eisao politiea eoeva e da oposi^ao entre “guerristas” e “antiguerristas”, e da guerra civil 
larvar e suas guerras civis intermitentes. E uma guerra naeional, nao por ser do Estado, 
mas por a na^ao a ter eombatido, atraves dos soldados mobilizados, do povo chamado as 
armas e que a eombateu em Afriea e na Europa, sofreu e morreu e por isso dela se apropriou 
e a fez parte integrante da gesta lusiada e da historia de Portugal. No fundo, a na^ao uniu-se 
em torno dos que por ela morreram em eombate. 


Conclusoes 

A narrativa tambem proporciona a constru^ao de imagens sob re a realidade. Essas imagens 
sao a ideia eharneira a partir do qual determinada realidade e lida pelos coevos e pelos 
vindouros, interpretada e apropriada pela memoria coletiva. Essa memoria eoletiva que 
procurar enquadrar a vivencia individual de eada um numa realidade coletiva mais ampla e 
signifteativa. Podemos dizer assim que em tres momentos, tres imagens foram construidas 
em redor da interven^ao de Portugal na 1“ Guerra Mundial. 

O primeiro momento da-se eom o desejo de interven^ao de Portugal na Guerra Mun¬ 
dial e a sua posterior participa^ao na eonflagra^ao. Duas imagens eontraditorias sao elabo- 
radas, uma por aqueles que visam legitimar a partieipa^ao beliea na guerra, prineipalmente 
no teatro de guerra europeu, outra que visa mobilizar a sociedade eontra essa interven^ao, 
vista eomo servindo, nao interesses naeionais, mas de fa^ao partidaria e estrangeira tam¬ 
bem. Para os interveneionistas, a participa^ao de Portugal na conflagra^ao seria um vet or 
de moderniza^ao da for^a armada e da sociedade e uma abrma^ao de perten^a a Europa. 
Para os seus opositores, o interveneionismo derivaria de dependeneia de Portugal faee a 
Gra-Bretanha e dos interesses particulares de alguns setores politicos radicais naeionais e 
resultaria na miseria e no empobrecimento de Portugal. 

Um segundo momento surge com o bm da 1“ Guerra Mundial, durante a 2’^ fase da 1“ Re- 
piiblica e o Estado Novo, e derivava de ediftca^ao naeional de uma memoria mais ou menos 
consensual em torno da participa^ao de Portugal na contenda. O mito de um povo heroico, 
que pese as imensas dibculdades impostas pela guerra, se soube bater com brio e heroi- 
camente durante a 1“ Guerra Mundial torna-se o topico axial. Esta imagem permitia, de 
algum modo consensualizar a interven^ao do pais na Grande Guerra no quadro naeional. 

Um terceiro momento emerge com a contemporaneidade e com a leitura que a contem- 
poraneidade faz de Portugal, no quadro do seu proprio desenvolvimento, e das tensoes 
politicas a ele associadas: um pals atrasado e pobre, profundamente dilacerado ideologi- 
camente, atravessada por uma conflitualidade politiea, em determinados casos metamor- 
foseada em contenda armada intracomunitaria e que foi arrastado para a guerra, para a 
qual nao dispunha de meios com que se bater, empurrado pelos politicos que puseram em 


IV. A HISTORIOGRAFIA DA GUERRA 


203 


campo uma for^a militar sem as condi^oes necessarias para uma a^ao belica consequente. 
Nao obstante, as dibculdades e os erros, os soldados, dadas as circunstancias, bateram-se 
o melhor que puderem e o sen fracasso e na verdade bem mats o fracasso do Estado que do 
pats. 

As diversas narrativas procuram de alguma forma enquadrar e inserir as experiencias 
individuals numa mais ampla vivencia coletiva, elaborando um trajeto analitico que de 
signibca^ao tanto aos atos singulares dos individuos, como a a^ao coletiva. Nesse sentido, 
trata-se de engendrar uma memdria coletiva que possa igualmente ser apropriada por cada 
individuo, sem que com isso se perca o quadro geral nacional. 


Referencias 

Aron, R. (1976). Penser la Guerre. Clausewitz. I. L'dge europeen. Paris: Edittions Galli- 
mard. 

Arrifes, M.F. (2004). A Primeira Guerra Mundial na Africa Portuguesa, Angola e Mogam- 
bique (1914-1918). Lisboa: Edi^oes Cosmos/IDN. 

Barros, T. de B. (s/d.). Histdria de Portugal. Ensino Primdrio. Editora Educa^ao Nacional 
de Adolfo Machado: Porto. 

Chantal, D. et al. (1996). Diciondrio Prdtico de Filosofia. Lisboa: Terramar. 

Cardoso, M. de O. (2018). “Apresenta^ao - Proposito da Comissao”, apresentando pelo 
Presidente da Comissao. Gomissdo Goordenadora da Evocagdo do Gentendrio da I Guerra 
Mundial - Sitio da Gomissdo na Internet, Portugal na Grande Guerra. O texto do propo¬ 
sito esta datado de 21 de maio de 2013. Disponivel em: http://www.portugalgrandeguer- 
ra.defesa.pt/Paginas/A%20Comissdo.aspx, consultado em 30 de abril de 2018. 

Carvalho, M. (2015). A Guerra que Portugal Quis Esquecer. Porto: Porto Editora. 
Carvalho, M. (2005). Poder e Ensino. Os manuais de histdria na politica do Estado Novo 
(1926-1940). Lisboa: Livros Horizonte. 

Clausewitz, Carl Von (1989). On War. Princeton: Princeton University Press. 

Duarte, Antonio Paulo (2015). “A Guerra Civil Larvar e a Beligerancia Portuguesa na Gran¬ 
de Guerra”, in Antonio Jose Telo (Coordena^ao), A Grande Guerra: Um Se'culo Depots. 
Atas. Lisboa: Academia Militar/Eronteira do Gaos, pp. 78-99. 

Han, Byung-Chul (2018). A expulsdo do outro. Lisboa: Relogio de Agua. 

Junta Naeional de Propaganda Patriotiea, (1916). A Guerra. Lisboa: Imprensa Nacional. 
Marques, Isabel Pestana, (2008). Das Trincheiras, Com Saudade, A Vida Quotidiana dos 
Militares Portugueses na Primeira Guerra Mundial. Lisboa, Esfera dos Livros. 

Martins, Nobre, (1916). “Portugal na Guerra”. Ilustragdo Portuguesa, n“ 546, pp. 105-110. 
Matos, Norton de, (2004). Memdrias e Trabalhos da Minha Vida. 3“ VoL, Tomo V. Coim¬ 
bra: Imprensa Universidade de Coimbra. 

Matoso, Antonio G., (s/d). Histdria de Portugal. Lisboa: Livraria Sa da Costa Editora. 
Oliveira, Pedro Aires, (2011). “A Repiiblica e a Guerra”, in Luciano Amaral, Org., Outubro: 
a Revolugdo Republicana em Portugal (1910-1926). Lisboa: Edi^oes 70, pp. 185 e 190-193. 
Paseoaes; Teixeirade, (1916). “A Guerra”. Portugal e a Guerra. AAguia, n“ 52, 53, 54, pp. 
109-111. 


204 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Pessoa, Fernando, (2015). OLivro do Desassossego. Lisboa: Circulo dos Leitores. 

Rosas, Fernando, (2007). LisboaRevoluciondria. Roteiro dos conflitos armados no seculo 
XX. Lisboa: Tinta-da-China. 

Rosas, Fernando, (2018). “Portugal, o quadro interno e o quadro externo”. In Semindrio In- 
ternacional “O Fim da 1^ Guerra Mundiah Consequencias para a PoHtica Externa e a Po- 
Utica de Defesa de Portugal”, Lisboa, Instituto da Defesa Nacional, 27 de fevereiro de 2018. 
Santos, Miguel Dias, (2013). “The Monarchists and the Great War: the practices and repre¬ 
sentations of counterpropaganda”. JPH. Vol. 11. Number 2. Winter, pp. 30-49. 

Sontag, Susan, (2015). Olhando o Sofrimento dos Outros. Lisboa: Quetzal. 

Teixeira, Nuno Severiano, (1996). O Poder e a Guerra, 1914-1918. Objetivos Nacionais e 
Estrategias PoHticas na Entrada de Portugal na Grande Guerra. Lisboa: Editorial Estampa. 
Teixeira, Nuno Severiano, (2015). Herdis do Mar. Histdria dos Simbolos Nacionais. Lis¬ 
boa: Esfera dos Livros. 

Telo, Antonio Jose, (2014). “Um enquadramento global para uma guerra global”. Nagao e 
Defesa, n“ 139, pp. 8-33. 

Telo, Antonio Jose e Sousa, Pedro Marques de, (2016). O GEP. Os militares sacrificados 
pela mdpolitica. Porto: Eronteira do Caos Editores. 


IV. A HISTORIOGRAFIA DA GUERRA 


205 



The war in History and the History of war: 
historiographic profiles in the syntheses of Portugal 

Judite A. Gon9alves de Freitas 

FCHS/University Fernando Pessoa 
CEPESE / ECT/ UP &amp; IPRI / UNova of Lisbon 


Abstract: The historiographic studies of the First World War in the most important syn¬ 
theses of Portugal history follows pari passu the main historiographic changes during the 
twentieth century and the Portuguese self-concept as a nation. The historiographic dis¬ 
course on war reflects the political paths and the impact of the dominant historiograph¬ 
ical perspectives. The Estado Novo (1926-74) dictatorship promoted the development of 
historical studies, especially certain helds and specialties, with emphasis on diplomatic 
and military history and the history of overseas Portuguese expansion. Both served the in¬ 
terests of the political regime. Our study will start from the History of Portugal called “de 
Barcelos”, produced in the context of dictatorship (1928-1935), and partly influenced hy 
historic traditional currents, continuing with the analysis of Joao Ameal synthesis (1940); 
go forward to the modern syntheses produced in democracy time (Antonio Henrique de 
Oliveira Marques [1972-74, and Jose Mattoso [1992-94]), that reflect important changes in 
the scope of our historiography in general, in the concept of Portugal, and mainly in the 
visions of Portuguese participation in the Great War. 

Keywords: Historiography; historiographic trends; history of war; concept / idea of Por¬ 
tugal, World War. 

Resumo: Os modelos de analise historiograhcos da 1 Guerra Mundial nas sinteses da His- 
toria de Portugal acompanham pari passu as principals mudan^as da nossa historiograha 
ao longo do seculo XX e do autoconceito de Portugal. Ao longo do seculo XX, o discurso 
historiograhco sohre a guerra, sofreu as vicissitudes da politica e a influencia da altera^ao 
das perspetivas historiograhcas dominantes. Durante a ditadura do Estado Novo (1926-74) 
foram, especialmente, desenvolvidos os estudos de determinadas tematicas e especialida- 
des, com destaque para a historia diplomatica e militar e a historia da expansao portugue- 
sa. Uma e outra serviam os interesses que mais aproveitavam ao regime. A nossa analise 
arrancara da Historia de Portugal dita “de Barcelos”, inicialmente produzida em contexto 
de ditadura (1928-1935), e em parte influenciada pelas tradicionais correntes da historia, 
prosseguindo com a analise da sintese de Joao Ameal (1940); trilhando caminho ate as mais 
recentes sinteses concebidas em tempo de democracia (Antonio Henrique de Oliveira Mar¬ 
ques [1972-74], e Jose Mattoso [1992-94]), que refletem importantes altera^Qes no ambito 
da nossa historiograha em geral, no conceito de Portugal, e das visoes da participa^ao por- 
tuguesa na Grande Guerra em particular. 


Palavras-chave: Historiograha, tendencias historiograhcas, historia da guerra, ideia de 
Portugal, 1 Guerra Mundial. 


IV. A HISTORIOGRAFIA DA GUERRA 


207 


Historiography is a discipline of history that proceeds to the critical study of historical 
production, effecting the evaluation and classihcation of historical, schooling or acade¬ 
mic knowledge, allowing to assess, in particular, the delays or historiography advances 
respecting to others. The study of historiographical currents, the professionalization wor¬ 
king levels (degrees of scholarship, specialization, knowledge and distinction), as well as 
the greater or lesser political and ideological tendencies of historical constructions. His¬ 
toriographic studies, within the branches of History, demonstrated the specihc canons 
of interpretation, distinctive conventions of writing, the historiographical currents and 
perspectives of approach, relating them with personal projects, hut also, and above all, 
with political and cultural projects connected with an academic and statehood function 
disseminated in different historical contexts. In this sense, it can be deduced that all histo¬ 
rical production depends on different ways of conceiving history, and can, from a critical 
analysis, dehne intellectual (cultural) prohles, nowadays known as paradigms (models). 

What we want to emphasize is the distinctive ideas, currents and ideology uttered in the 
authors’ discourse, relating it to the way in which they construct the historical plot on the 
First World War. In this sense, this article, therefore, contains an attempt to underlying 
our main subject emphasizing the main tendencies in the Portuguese syntheses regarding 
the historic context analysis of First World War and reviewing the Portuguese historian’s 
contributions that writes about it. So, the scope of the article includes a look at the main 
synthesis and a state of art of the twentieth century in Portugal considering the raise of 
trends in historiography that began when it became a new discipline in universities in the 
nineteenth century. That means it’s important to address this issue to the historiography 
of war and historiographical interpretations of the phenomenon, by confronting the diffe¬ 
rent perspectives in changed times. The heritage historiographical approach in the late ni¬ 
neteenth century and that came, in Portugal, until the 50’s of the twentieth century is the 
called positivism that privileged the chronological time as a linear sequence of facts. So, 
the Republican pedagogical ideal marked by positivism defended a scientihc, rational and 
secular education (Proen^a, 2009). On the other hand, in the last quarter of the nineteenth 
century, the historicism that preferred the individualist facts, flourished in various dis¬ 
ciplinary areas, namely in the held of political and military history and in the Discoveries 
like a product of Portuguese nationalism (Polanah, 2011 e Freitas, 2013). Finally, the rise 
of the national imaginary, in the nineteenth century, prove the great importance of the 
sentiment of nationality. In Portugal, the intellectual historians that are marked by diffe¬ 
rent national colors, promoted the rise of the Portuguese Discoveries and colonial policy as 
national identity factors. (Polanah, 2011) 

Beginning by analyzing the hrst history of Portugal published after the war - the history 
known as “de Barcelos”, that was published the hrst volume in 1928. 

The structure of this compilation, conforming to a monumental model, was produced to 
celebrate the eighth centenary of the nationality foundation, composed by ten volumes, 
abundantly illustrated, and the hrst seven were published between 1928 and 1935. The se¬ 
ven volumes follow a chronological narrative, by kingdoms and dynasties, maintaining a 
uniform structure, dividing in hve epochs, and within these in thematic units of: “Political 
life”, “Economic life” or “Economic organization”, “Cultural life” and “Portuguese over¬ 
seas expansion”. The seventh volume, which corresponds to the hfth period (1816-1918), 


208 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


was published in 1935, contains 799 pages, of which only 85 are dedicated to the Republic 
period, and of these only 45 pages relate to First World War under the title “Portugal na 
Primeira Guerra, 1914-1918”. 

The four authors of this section (Damiao Peres, Joaquim de Carvalho, Carlos Passos 
and Angelo Ribeiro) have an academic education in History, Philosophy or Literature, 
highlighting the hrst two that developed their teaching and research activity at the Uni¬ 
versity of Coimbra as historians. They are educated in the republican cultural spirit, which 
privileged freedom of speech and schooling as a central factor of social transformation and 
the “regeneration of homeland”. None of them adhered to anti-republican or monarchist 
movements, or even to Integralismo Lusitano (Lusitanian Integralism), an anti-parlia- 
mentary, nationalist and traditionalist current that was in vogue between 1914 and 1932. it 
seems that this synthesis is not contaminated with past (conservative) ideology. 

The work is a demand of the “EstadoNovo” (1926-1974), an authoritarian and corporatist 
political regime which ended the liberal period in Portugal. The ideological and propagan- 
distic goals of Estado Novo are evident in the self-designation, which marks the beginning 
of a new era. The sponsorship of this synthesis, commissioned by the rising Estado Novo, 
and produced in its context, can induced the most unsuspecting reader to think that it 
would he a historiographical product with credited ideological content. 

However, the work is not marked by only the performing time. The authors studied and 
began teaching and writing in times of Republic (liberal period). Their formation is repu¬ 
blican, and this fact is not insignihcant in the “free mode” as they produce the historian’s 
work. The reading of war remains free of anti-Republican bias, evoking the main internal 
and external facts that determined Portugal’s entrance into the war. 

Nevertheless, from the historiographic point of view, this synthesis is guided by the di¬ 
vision of the History of Portugal into dynasties and reigns, it privileges political, diplo¬ 
matic and military analysis, in a methodical and historicist (event) perspective (Torgal, 

. Portugal appears like a great colonialist nation - one of the most important symbols of 
national unity throughout our history -, it could not remain unrelated to the conflict, in 
addition to the ethical and historical reasons invoked, especially the solidarity of small 
peoples against then German militarist and imperialist arrogance - the most practical rea¬ 
sons for our agreement with England and the defense of a long-threatened colonial he¬ 
ritage were imposed (Damiao Peres et al, vol. Vll, p. 494). The alliance with England is 
justihed by historical treaties (since Windsor), which imposed weighty obligations, in this 
context, the imperative idea of protecting our colonies in Africa (one of the most impor¬ 
tant signs of our nationality promoted by the war party support the decision to partaken 
in the world’s conflict. 

The second compilation analyzed is a synthesis that has a different character from the 
previous one, from the responsibility of Joao Ameal, a fervent Catholic monarchist, who 
joined the Integralismo Lusitano, Ameal developed a conception of doctrinal and retro¬ 
grade history regardless of having college education in literature, and research in the helds 
of philosophy and history - areas of expertise that he bequeathed a remarkable work (Pin¬ 
to, 1995). The Histdria de Portugal. Das origens al940 (History of Portugal from its origins 
to 1940) is a synthesis that highlights the singularity of the facts, in a chronological chain, 
and qualifying them, without proceeding to any critical distance, it had the hrst edition in 
1940 and gained the Alexandre Herculano award three years later (Torgal, 1996, pp. 244- 


IV. A HISTORIOGRAFIA DA GUERRA 


209 


246). The main author’ objective is to present an ideal history, great hgures, glorihcation 
of the martyrs, soldiers and heroes of war, glorious times and past events, undervaluing 
social, economic and cultural dimensions, at a time when strong winds were blowing of 
Ecole des Annales in Western Europe^T In his point of view, history should fulhll a peda¬ 
gogical function, a mixture of art, passion, ethics and science. His way of making history 
seems like a catechism, putting forward the idea of a glorious colonial past and a great 
Portuguese empire that was in danger during the First Portuguese Republic (1910-1926). 

It blames the republican (parliamentary) regime for the constant popular uprising and 
for all politico-military misdeeds that, in its view, have called into question national honor 
and glory. 

Consequently, and in accordance with the line taken in other parts of his book, he em¬ 
phasized: “Portugal e uma na^ao que aceita os seus imperativos historicos, e visto como 
um motivo de orgulho, um modelo de atua^ao: a sua histdria espelha o seu trabalho ao 
servi^o de Deus e a vontade de construir e manter um imperio^'’. ” 

Therefore, Joao Ameal’s History of Portugal appeals to traditional Catholic values and the 
courage of the Portuguese against liberal. Masonic and anti-clerical Republican ideals that 
he classihes as intemperate, without respecting leadership, order and hierarchy. 

The following compilation was written 32 years later, it is the History of Portugal autho¬ 
red by A. H. Oliveira Marques: published in hrst time in 2 volumes (in the USA); country 
where the historian exiled and where it exerted teaching functions in several American 
universities between 1965 and 1970 (Freitas, 2009, p. 191). The compilation was reissued 
in 1972-1974 in Portugal, in 3 volumes, corresponding to a new perspective of approach 
to the evolution of Portuguese society from the hrst peoples who inhabited the Iberian 
Peninsula until the end of the Estado Novo. 

Oliveira Marques is representative of a historical approach that values the critical analy¬ 
sis of events, basing it on an exceptional knowledge of historical sources. The two periods 
of election of the studies that published are, respectively, the Middle Ages and the First 
Portuguese Republic (1910-1926). Oliveira Marques represents with Joel Serrao, Vitorino 
Magalhaes Godinho et al. a generation of historians who renewed the vision of the History 
of Portugal with new perspectives of approach, incorporating harmoniously and fluently 
the European historiographical currents of that time (Annales and Nouvelle Histoire, in 
particular). In the same way, it connects history with the social sciences, and has therefore 
renewed and expanded the framework of historical research by opening the held of his¬ 
tory to the study of all dimensions of daily human activity, hitherto little or nothing(table, 
death, hunting, clothing ...) and, in this held, was a forerunner of today’s micro-history 
(history of everyday life). He has the author of several books and articles on daily life. 

On the motivations that induced him to produce the compilation in 1972, he states: “Que 
nada havia de recomendavel que um professor de histdria de Portugal pudesse aconselhar 
aosseusalunos como obra de conjunto.”^’ (cit. inMendes, 1996, p. 321). 


This historiographical current fight against the hegemony of political history, criticize the notion of historical fact 
and focusing on economic and social history (Bourde &amp; Martin, 2003:119-135). 

“Portugal is a nation that accepts its historical imperatives, is seen as a source of pride, a model of action: its 
history mirrors its work in the service of God and willingness to build and maintain an empire” (our translation). 

” “That nothing was advisable that a professor of history of Portugal could advise his students as a joint work. ” (Our 
translation). 


210 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



The text was planned to aehieve a wide audienee, therefore was ehosen the objeetivity 
and synthesis, removing the ideological sense in the historical discourse. But what is more 
important to emphasize is that political and military history ceased to be the central core 
of the work, being referenced in articulation with the different helds of historical reality 
including economy, society and culture veins. (Mendes, 1996, pp. 322-323). 

As for the intervention of Portugal in First World War, Oliveira Marques advances with 
the three main reasons that, in his point of view, leaded Portugal’s entry into the war un¬ 
der the British pressure: 

1) The importance of protecting the Portuguese colonies, mainly in Africa, in 
face of the 1898 and 1913 agreements between England and Germany to share 
the Portuguese overseas domains, 

2) The recognition of the new republican state, in the Western European 
context of most monarchical regimes, and 

3) The demarcation vis-a-vis of Germanophilism and the neutrality of Spain 
regarding the conflict, looking for prestige and independence in Western 
Europe context. 

it does not give special emphasis to the Battle of Ta Tys, preferring to explore the social and 
economic consequences of the unsuccessful military moment. 

in summary, Oliveira Marques’s Tiistory of Portugal represents a new way of making 
history, following the international trends of similar compilations. A pattern that will re¬ 
produce, in a more detailed way, in the eleventh volume of the “Nova Tiistoria de Portugal” 
printed in 1991-1992 (thirteen volumes), that he coordinated with Joel Serrao, entitled: 
“Da Monarquia a Repiiblica”, which he wrote himself. 

East of all, a reference to the History of Portugal, coordinated by Jose Mattoso and pu¬ 
blished between 1992 and 1993. The sixth volume, entitled “A Segunda Funda^ao (1890- 
1926)”, is signed by Rui Ramos, professor of Contemporary History at the New University 
of Lisbon, representing a new generation of historians. Rui Ramos supports its analysis of 
the reasons for Portugal’s entry into First World War, namely in diplomatic corresponden¬ 
ce (his main historical source). 

At hrst, it emphasizes the troubled and difficult relationship between the Republican 
Portuguese government and the British crown, which conditioned the international op¬ 
tion to reduce Portuguese participation. The Great Britain delaying to recognize the Por¬ 
tuguese republican regime. From your point of view, it highlights the weaknesses and the 
failings of the republican regime in the diplomatic management services, especially with 
Great Britain, before and during the war. in external point of view, Portuguese military 
intervention in war alongside England it’s a way of strengthening ties with a great ‘frien¬ 
dly’ power of those who feared the alignment with Spain of Alfonso Xlll and the German 
threat in the colonies. From the internal point of view, the entry into the war conditioned 
the parties to a “sacred backward economic union” that guarantees political supremacy to 
the Portuguese Republic Party (PRP). A kind of republican opportunism! 

Following, it highlights the enormous war effort for a “limited” Portugal (scarifying of 
manpower), with a weak and backward economy (lower level of socio-economic deve¬ 
lopment): in Africa (30,000 men) and in Flanders (50,000,000 men). The idea of a small 


IV. A HISTORIOGRAFIA DA GUERRA 


211 


Portugal compared to the great imperialist powers (England, Franee et al.). This effort was 
seen as unbridled and unpopular, one of the reasons for the coup d’etat of Sidonio Pais^® 
(Deeemher 5, 1917). This overthrow of republic regime that imposing the dictatorship of 
Sidonio Pais was against the war and the demagogy of the Republiean Party. The republi- 
ean regime was interrupted by a dietatorship led by a pro-German, and this, to him, is a 
sign of the weakness of the republiean dream. 

Conclusion 

The four syntheses of Portugal examined, only one eontains an aceentuated ideologieal 
and traditionalist tendeney - that of Joao Ameal, historian of the regime. The Portugue¬ 
se History “de Bareelos”, although it plaees special place on political and military issues, 
unrevealed the ideologieal political factors of the regime that sponsored it. Despite this, 
however, the idea of Portugal as a great eolonial eountry persists as an inheritance of late 
nineteenth-eentury Portuguese nationalism. The 30’s and 40’s of the twentieth century, 
eorresponding to the heyday of the promotion of the glorious colonial Portugal (Diseo- 
veries put in the national identity profile). Only in the 70’s a first synthesis arises, made 
under the influence of the most recent historiographical European currents (Annales and 
Nouvelle Histoire), devaluing political and military factors in the traditional sense, and re¬ 
lating them with the economic and social one. The idea of Portugal is not based on regime 
ideology. Finally, in the most recent synthesis, wrote by Rui Ramos, makes an analysis of 
the Republican War policy stressing the weakness of Portugal’s position (a small and weak 
country) in the peninsular and Western European context and the failure of the opportu¬ 
nism of the Portuguese Republican Party in attempting of uniting the country around the 
Portuguese participation in the confliet. Nowadays in the Portugal synthesis, Portugal is 
seen as a poor nation with limited role-playing of ajfirmation in international politics, 
during the First Republic (1910-1926) and the Estado Novo (1926-1974). 


He was a military and politician, Ambassador of Portugal in Berlin, Minister of War, and Minister of Foreign Af¬ 
fairs. He was the fourth President of the Portuguese Republic (28 of April of 1918 to 14 of December of 1918) which 
imposed a dictatorship. 


212 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



References 


Ameal, J. (1958). Historia de Portugal. Das origens ate 1940. Porto: Livraria Tavares Martins. 
Bourde, G. e Martin, H. (2003). As Escolas Histdricas. 2“ Edi^ao, Mem Martins: Europa- 
-America. 

Freitas, J. A. G. de (2009). A. H. de Oliveira Marques (1933-2007). Intellectual biography. 
In: laume Aurell &amp; Julia Pavon (ed.). Rewriting the Middle Ages in the Twentieth Century. 
Vol. II: National Traditions, Turnhout: Brepols, pp. 183-206. 

Freitas, J. A. G. de (2013), The Memory of Prince Henry the Navigator: Genesis, Forma¬ 
tion and Classihcation of a Monumental Collection of Documents - the Monumenta Hen- 
ricina. Roda da Fortuna. Revista Eletrdnica sobre Antiguidade e Medievo, Volume 2, 
Numero2, pp. 379-400. [Emlinha]. Disponlvelem: https://docs.wixstatic.coni/ugd/3fd- 
dl8_28f9352114794505aebe448c88eca081.pdf 

Marques, A. H. de O. (1972). Historia de Portugal. 3 vols., Lisboa: Palas Editores. 

Peres, D. (1935), Historia de Portugal, vol. VII, Barcelos: Editora Civiliza^ao. 

Mendes, J. A. (1996). A renova^ao da historiograha portuguesa. In: L. R. Torgal, J. M.Ama- 
do Mendes &amp; F. Catroga, Historia da Historia de Portugal. Sec.s XIX-XX. Mem Martins: 
Circulo de Leitores, pp. 277-364. 

Pinto, A. (1995). Um ideologo no Estado-Novo: Joao Ameal, historiador. Revista de His- 
tdria das Ideias. Do Estado Novo ao 25 de Abril, vol. 17, Imprensa da Universidade de 
Coimbra, pp. 125-165. [Emlinha]. Disponlvel em: https://digitalis- dsp.uc.pt/jspui/bits- 
tream/10316.2/41949/l/Um_ ideologo_no_Estado_Novo.pdf 

Polanah, P. S. (2011). The Zenith of our National History! National identity, colonial empi¬ 
re, and the promotion of the Portuguese Discoveries: Portugal 1930s. e-Journal of Portu¬ 
guese History, vol.9, n.l, pp.40-64. [Em linha]. Disponlvel em: http://www.brown.edu/ 
Departments /Portuguese _Brazilian_Studies/e}ph/html/issuel7/html/v9nla03.html 
Proen9a, M. C. (2010). A educa^ao. In: Fernando Rosas &amp; Maria Fernanda Rollo (coord.). 
Historia da Primeira Repiiblica Portuguesa. Lisboa: Tinta-da-china, pp. 169-190. 

Ramos, R. (1993). A Segunda Funda^ao (1809-1926). In: Jose Mattoso (dir.). Historia de 
Portugal, vol. VI, Mem Martins: Circulo de Leitores. 

Rosas, F. (1995). Estado Novo, imperio e ideologia imperial. Revista de Historia das Ideias. 
Do Estado Novo ao 25 de Abril, vol. 17, Imprensa da Universidade de Coimbra, pp. 19-32. 
Torgal, L. R. (1996). A historia em tempo de «ditadura». In: In: L. R. T., J. M. A. Mendes 
&amp; F. Catroga, Historia da Historia de Portugal. Secs. XIX-XX. Mem Martins: Circulo de 
Leitores, pp. 241-276. 

Torgal, L. R.; Mendes, J. M. A. &amp; Catroga, F. (1996). Historia da Historia em Portugal: se- 
culos XIX-XX, Mem Martins: Circulo de Leitores. 


IV. A HISTORIOGRAFIA DA GUERRA 


213 



Portugal na Guerra: uma revista de infopropaganda 

Jorge Pedro Sousa 

FCHS/Universidade Fernando Pessoa 
CIC Digital/UNL 


Resumo: Na I Guerra Mundial (1914 a 1918), apenas a imprensa tinha a capacidade para 
promover uma comunica^ao ubiqua, pelo que foi usada pelos contendores para propagan¬ 
da e informa^ao. A imprensa teve, assim, um importante papel na forma como a guerra foi 
contada a sociedade e contribuiu para a produ^ao e circula^ao de ideias e pontos de vista 
sob re o conflito, ajudando a tornar as pessoas mais propensas a aceitar o esfor^o de guerra 
e a perda de vidas. As revistas ilustradas, em particular, tiveram um papel significativo na 
propaganda de guerra, porque permitiram que ate mesmo os analfabetos “vissem” a guer¬ 
ra. Portugal foi um dos paises beligerantes da I Guerra Mundial que usou as revistas ilus¬ 
tradas para propaganda. Neste artigo, tra^aremos a historia da revista Portugal na Guerra, 
patrocinada pelo governo portugues, editada em Paris e escrita em portugues e frances, e 
analisaremos como essa revista apresentou a 1 Guerra Mundial sens leitores. 

Palavras-chave: Revistas ilustradas Portuguesas, Portugal na 1 Grande Guerra, propaganda. 

Abstract; During World War 1 (1914 to 1918), only the press had the ability to promote a 
ubiquitous communication, and as such it was used by the promoters for propaganda and 
information. The press had, thus, an important role in the way the war was told to society 
and contributed to the production and circulation of ideas and views on the conflict, help¬ 
ing to make people more likely to accept the war effort and the loss of lifes. The illustrated 
magazines, in particular, played a signiheant role in the propaganda of war, because they 
allowed even the illiterates to “see” the war. Portugal was one of the belligerent countries 
of World War 1 who used the illustrated magazines for propaganda. In this article, we trace 
the history of the magazine Portugal na Guerra, sponsored by the Portuguese Government, 
edited in Paris and written in Portuguese and French, and we analyze how this magazine 
presented the World War 1 to its readers. 

Keywords: Portuguese illustrated magazines, Portugal in the 1 World War, propaganda. 


IV. A HISTORIOGRAFIA DA GUERRA 


215 


Durante a Grande Guerra, para alem da disputa belica, desenvolveu-se uma disputa sim- 
bolica no campo da comunica^ao social. Os meios de comunica^ao foram usados pro- 
pagandisticamente para os contendores convencerem e convencer-se da justeza da sua 
causa, animando-se para a luta (Lasswell, 1927; Sanders, 1975; Knightley, 1975; Sanders e 
Taylor, 1982; Messinger, 1992; Arthur, 2007; Garambone, 2003; Novais, 2013; Sousa, 2013). 
Somente a imprensa tinha a capacidade de tornar as mensagens propagandistas omni- 
presentes e, par isto, elafoi usada par todos os contendores (Marquis, 1978; Sanders e 
Taylor, 1982; Pizarroso Quintero, 1993: pp.209-234; Navarro, dir. et al., 2005: pp.226-228; 
Gilbert, 2007: p.l3). Alguma da propaganda de guerra apresentou-se, pois, em suportes 
“jornalisticos” ou que se podiam conotar com o jornalismo, com os quais os leitores esta- 
vam familiarizados. Entre a imprensa, as re vistas ilustradas tiveram um papel signibcativo 
na propaganda de guerra, ate porque permitiam mostrar a guerra, mesmo aos analfabetos 
(Marquis, 1978; Bishop, 1982; Pizarroso Quintero, 1993: pp.209-234); Sousa, 2013). 

Tera sido por iniciativa do governo portugues e, em particular, do ministro do Exercito, 
Norton de Matos, que surgiu, a 1 de junho de 1917, a revista ilustrada Portugal na Guerra. 
O lan^amento dessa revista demonstra que os portugueses tambem tentaram fazer propa¬ 
ganda de guerra que se apresentasse insinuante e inocentemente como uma revista ilus¬ 
trada “jornalistica”. 

Escrita em portugues mas com apontamentos em frances (alguns titulos, legendas e o 
cartaz de espetaculos em Paris), a revista Portugal na Guerra foi publicada entre junho de 
1917 e Janeiro de 1918, num total de oito mimeros, de periodicidade irregular. 

Projetada para ser quinzenal, conforme revelam as informaqoes sobre os preqos das as- 
sinaturas na ftcha tecnica, a revista Portugal na Guerra teve esta periodicidade em junho 
de 1917 e entre setembro e outubro de 1917. Os liltimos mimeros, de novembro de 1917 a 
Janeiro de 1918, tiveram ja periodicidade mensal. Nao se publicou em julho e agosto de 1917. 

Uma vez que o ftm da publica^ao da revista coincidiu com a ascensao de Sidonio Pais ao 
poder em Portugal, a causa mais provavel para a morte da Portugal na Guerra tera sido o 
bm do bnanciamento e a falta de interesse do novo poder, mais interessado em promover 
a bgura do novo Chefe-de-Estado. 

A revista Portugal na Guerra tinha, normalmente, 16 paginas de 38,5 cm de altura por 
28,5 cm de largura, excluindo-se a capa, a contracapa e os respetivos versos. A capa, que 
ostentava, simbolicamente, o escudo da Republica Portuguesa, conferindo-lhe um esta- 
tuto obcial, ou, pelo menos, obcioso, foi sempre impressa em papel colorido verde e, por 
vezes, incluiu palavras grafadas a cor, como aconteceu com a palavra Portugal do proprio 
titulo da revista. O interior foi composto a preto-e-branco. Os anuncios publicitarios tam¬ 
bem apresentavam, por vezes, elementos coloridos, suscetiveis de promover a aten^ao do 
leitor. 

A bcha tecnica da revista Portugal na Guerra revela que se publicava em Paris (sede no 
n.“ 3 da rua de Villejust), sob a dire^ao de Augusto Pina, um pintor (que tinha estudado 
Belas-Artes em Paris) e ilustrador e um homem do teatro, envolvido na propaganda de 
guerra. E interessante notar este facto - o governo portugues colocou um homem da arte 
e do teatro, e nao um jornalista, a dirigir uma revista ilustrada de propaganda de guerra. 

Ainda segundo os dados inseridos na revista, o secretario de reda^ao da Portugal na 
Guerra era Jose de Ereitas Bragan^a, que assinou alguns dos textos como J.B, incluindo 
varias crdnicas sobre o quotidiano parisiense em tempo de guerra. 


216 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


As fotografias em Franca ficaram a cargo de Arnaldo Garces, colaborador regular da im- 
prensa e um dos introdutores do fotojornalismo em Portugal, sendo dele o mais impressio- 
nante conjunto de imagens do quotidiano dos expedieionarios portugueses. Em Portugal, 
o correspondente fotografico era Carlos Alberto Lima, tambem ele colaborador regular da 
imprensa - mas a revista nao publicou fotograbas de Portugal, so de portugueses. 

A revista apregoava que tinha a “eolabora^ao literaria dos mais notaveis escritores por¬ 
tugueses e estrangeiros”, “eolabora^ao artlstica dos maiores artistas portugueses” e ainda 
“eartas das prineipais eapitais do mundo”. Mas o contributo literario reduziu-se a varios 
autores menores da literatura e do jornalismo: entre outros, o jornalista republicano Ma¬ 
yer Gar^ao; o jornalista, eseritor e diplomata republieano Alfredo de Mesquita Pimentel; 
o jornalista, dramaturgo, cronista e militar republieano Andre Brun, sob o pseudonimo 
misterioso de “CapitaoX” (a sua identidade e revelada no n.° 4 p. 6 ); o jornalista Jose Paulo 
Fernandes; o jornalista republieano Xavier de Carvalho, que ha largos anos vivia em Paris 
e que foi um dos expoentes da defesa da interven^ao portuguesa na 1 Guerra Mundial; e o 
jornalista, jurista e eseritor republieano Alberto de Sousa Costa. 

Por seu turno, caso se descontem as fotograbas, o contributo “artfstico” reduziu-se a 
uma aguarela colorida do diretor da revista, Augusto Pina, intitulada “Porta-bandeira 
portugues na guerra”, e a um retrato colorido do comandante do CEP, general Tamagnini, 
da autoria do pintor Ferreira da Costa. 

A revista aceitava assinantes de Franca, de Portugal e do Brasil, sinal que se destinava a 
piiblicos destes pafses. Alias, a revista tinha agentes distribuidores em Lisboa (Victor Melo) 
e no Rio de Janeiro (Casa A. Moura). 

O preqo das assinaturas e dos mimeros avulsos nao variou enquanto a revista foi publica- 
da, apesar da infla^ao. Um mimero custava 30 centavos em Portugal, um franco em Franca 
e 1500 reis no Brasil. Curiosamente, os pre^os das assinaturas - igualmente disponfveis 
para Franca, Brasil e Portugal - revela que estava prevista a publica^ao de mais edi^oes da 
revista Portugal na Guerra. Efetivamente, a publica^ao teve oito mimeros, de periodicida- 
de irregular, entre junho de 1917 e janeiro de 1918 (oito meses). Embora a revista nao tivesse 
sido publicada em julho e em agosto de 1917, em junho deste ano foram publieados os dois 
mimeros previstos, tal como em outubro. Mas a beha tecnica revela que se previa que a 
revista fosse quinzenal e durasse bastante mais tempo do que durou. Uma assinatura de um 
ano (24 mimeros) custava 6$30 para Portugal, 21 francos para Eran^a e tres mil reis para o 
Brasil. Uma assinatura de seis meses (12 mimeros) beava por 3$30, em Portugal; 16$00, no 
Brasil; e 11 francos, em Eran^a. Einalmente, uma assinatura de tres meses custava 1$80 em 
Portugal e 6 franeos em Eran^a, nao estando esta modalidade prevista para o Brasil. 

A indica^ao do preqo da revista em francos, escudos e reis tambem eontribui para de- 
monstrar que a publica^ao se destinava aos portugueses e lusofonos que estavam em Eran- 
9 a, em Portugal e no Brasil. Demonstra, igualmente, que a comunidade portuguesa no 
Brasil mantinha fortes laqos com Portugal. Alias, foi publicada pela Uniao dos Portugueses 
no Brasil, organiza^ao sedeada no Rio de Janeiro, uma revista autodesignada “patridtica” 
igualmente intitulada Portugal na Guerra. 

Os amincios, sempre inseridos na contracapa e no respetivo verso, publicitavam casas 
comerciais tais como: os alfaiates Victorino, especialistas em fardas para o exercito por¬ 
tugues; os grandes-armazens Printemps, que tinham um representante em Lisboa; um 
intermediario-comissionista frances de negdeios na Europa; a tipograba parisiense Lux, 


IV. A HISTORIOGRAFIA DA GUERRA 


217 


onde se imprimia a revista Portugal na Guerra; uma editora de Paris; os jornais de modas 
da casa A. Moura, do Rio de Janeiro, agencia da revista para o Brasil; e um suplemento para 
gasolina e petroleo de uma empresa americana. 

O amincio a Tipografia Lux podera ter resultado de um acordo entre a revista e quern a 
imprimia, eventualmente em contrapartida por uma diminui^ao do pre^o da impressao. 
Os amincios da casa A. Moura, do Rio de Janeiro, agente da revista no Brasil, tambem po- 
dem ter sido uma contrapartida negociada no ambito do acordo de representa^ao. 

No primeiro mimero da revista, o texto de apresenta^ao aos leitores - curiosamente, nao 
assinado - refere que o proposito da publica^ao seria “documentar a interven^ao militar 
dos portugueses na maior conflagra^ao de que ha memoria na historia da humanidade”, 
dai a escolha do titulo Portugal na Guerra. Mas a revista tambem tinha por bnalidade - e 
aqui enuncia-se o seu vies propagandistico - “manter elevado o espirito nacional, pelo 
exemplo glorioso dos sens”. Por outras palavras, segundo os redatores da revista, poderia 
esperar-se dela que apontasse para o exemplo dos soldados portugueses como simbolo da 
“revela^ao de energia” de Portugal, pais que renascia “para as recompensas da considera- 
9 ao que se devem aos povos vigorosos”. Esta e a chave para a leitura politica da revista: a 
participaqao de Portugal na guerra, apesar dos sacrificios, destinava-se a salvaguardar os 
interesses nacionais. A revista nao clariftca esses interesses, mas a frente deles estavam, 
certamente, a defesa das coldnias - entendidas como uma especie de retaguarda estrate- 
gica necessaria a sobrevivencia do pais e parte integrante do todo nacional - e a defesa da 
forma republicana de regime. 

Por que razao seria necessario, por outro lado, proceder a documenta^ao da participa- 
9 ao portuguesa na guerra em curso na Europa? No mesmo texto encontra-se a resposta: a 
magnitude histdrica da 1 Guerra Mundial, para o mundo e para Portugal: 

As razoes do nosso empreendimento contem-se na propria magnitude do 
acontecimento que o inspira. 

A guerra, em si mesma, e um facto de tal natureza grande que preenchera 
por largos seculos a imagina^ao dos homens. (...) Mas se a guerra em si mes¬ 
ma e um facto de consideraveis propor^oes em rela^ao a historia do mundo, a 
guerra que nos proprios vamos fazer com os nossos soldados, em campos de 
batalha comuns, e, em rela^ao a nossa historia, um acontecimento de tama- 
nha grandeza que podemos considera-lo linico nos anais da nacionalidade. 
(n.“l:p.2). 

A participa^ao portuguesa na Grande Guerra seria encarada, pois, como um acontecimen¬ 
to linico na historia do pals, ja que “Pela primeira vez e no decurso da sua longa historia” 
Portugal sala “da sua cena para a vastidao da cena politica universal”. O pais iria lutar fora 
das fronteiras, “ao lado das mais poderosas naqoes do mundo”, para defender uma “cau¬ 
sa (...) de todos”. Obviamente, o todos, aqui, refere-se retoricamente aos aliados, nao as 
potencias centrais. 

O texto enfatiza, num sentido legitimador, o esforqo nacional singular que o pals fazia - e 
que o arruinou - para lutar em Africa e na Europa contra as potencias centrais: 


218 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


[Portugal] Constitui um exercito capaz de combater ao mesmo tempo no 
continente e nas colonias, manda sessenta mil homens para Franca, trinta 
mil para Africa e encontra ainda nas suas reservas os elementos constituti- 
vos e uma guarni^ao territorial. Este exercitoe exclusivamente nacional. (...) 

Sao portugueses os seus obciais, sao portugueses os seus soldados e o mesmo 
pano dos uniformes que veste e portugues. (n.° 1: p. 2). 

O redator do texto, possivelmente Jose de Freitas Bragan^a ou Augusto Pina, aponta, fmal- 
mente, para os custos da guerra - para as “devasta^oes” e para as “carnificinas” em que 
os soldados portugueses iriam participar. Mas a revista nao mostrara os mortos nem os 
feridos, embora tenha dado conta da “devasta^ao” provocada pela guerra no patrimonio 
ediftcado, especialmente atraves de imagens. 

Curiosamente, no primeiro mimero da revista aparece um outro curto texto dirigido “Ao 
leitor” (n.° 1:15) que se referia as “dibculdades sem conta” com que se lutava para lan^ar 
uma publica^ao da natureza da revista Portugal na Guerra, que nao estavam “inteiramen- 
te vencidas”. Adiantava o texto que “prestes a entrar nos prelos”, o primeiro mimero tinha 
sofrido “o contratempo de uma greve”. Prometia-se, no entanto, que os contratempos nao 
seriam “a ultima palavra” nos “esfor^os” para lan^ar a revista. 

Depois de um tempo de suspensao da publica^ao, a revista voltou ao contacto com os 
leitores com o mimero 3, datado de 15 de setembro de 1917. Justiftcou, entao, num texto di¬ 
rigido especibcamente “Aoleitor”, a interrup^ao na publica^ao por motivo de “dibculda- 
des materials quase insuperaveis”, “contrariedades e prejulzos graves” (n.“ 3: p.l5). Nesse 
mesmo texto, a revista anuncia que tera, doravante, “a colabora^ao artlstica dum novel 
pintor portugues que, atualmente junto das nossas tropas, nos enviara os seus flagrantes 
croquis”. Tratar-se-ia, provavelmente, de Adriano de Sousa Lopes, o pintor que viajou ate 
as trincheiras para pintar o CEP, mas ele nunca chegou a ver publicados trabalhos seus na 
Portugal na Guerra. De qualquer modo, bca a referencia a possibilidade de colabora^ao de 
Sousa Lopes que os responsaveis da revista - e possivelmente o ministro da Guerra, Norton 
de Matos, que montou a maquina de propaganda de guerra portuguesa - equacionaram. 

O texto “Ao leitor” e tambem relevante por outro motivo: insere excertos de cinco cartas 
- em frances - recebidas na reda^ao, duas de publica^oes francesas e tres de politicos e jor- 
nalistas franceses. Estas cartas sao reveladoras de que os notaveis e os periodicos franceses 
se inclulam entre os piiblicos-alvo da revista e que esta Ihes chegava gratuitamente. 

As cartas de La Revue e do Excelsior anunciam o nascimento da revista “de propagan¬ 
da” (Excelsior) Portugal na Guerra e cumulam de elogios o “grande artista portugues” 
(La Revue) Augusto Pina, que a dirigia e tinha fundado. O Excelsior agradece mesmo os 
“documentos fotograbcos” relativos ao CEP que publicou e que Ihe terao sido remetidos 
pela revista ou reproduzidos a partir dela (provavelmente, fotograbas de Arnaldo Garces). 
As cartas dos politicos e jornalistas agradecem o envio da revista e felicitam a iniciativa. O 
literato e cronista Philias Lebesgue, um mediterranista que se subscreve como “um velho 
amigo de Portugal”, anuncia que iria referir-se a revista Portugal na Guerra na sua proxi- 
ma cronica no Mercure de Prance e que a usaria num estudo sobre o combatente lusitano 
que estaria a preparar. O antigo ministro e senador Jules Goden sublinha o seu interesse 
testemunhal. O jornalista e sociologo frances Jean Einot, de ascendencia polaca, sdcio- 


IV. A HISTORIOGRAFIA DA GUERRA 


219 


-correspondente da Academia Brasileira de Letras, salientava que a Portugal na Guerra 
se distinguia positivamente dos restantes periodicos similares criados durante a guerra. 

A revista quis, portanto, propagandear o esfor^o de guerra portugues junto da impren- 
sa franeesa, eom quern procurou estabeleeer la^os e a qual forneceu fotografias do CEP, e 
junto dos notaveis da politica e do jornalismo em Franca. Por essa razao, a revista publicou 
apontamentos em franees. 

Um texto intitulado “A nossa revista”, publicado no setimo mimero, salienta que era 
“pela imagem” que a publica^ao teria “feito eonhecer (...) o heroieo esfor^o militar por¬ 
tugues em Franca” e o “panorama eurioso” da eoopera^ao militar portuguesa (n.° 7: p.7). 
Os editores de Portugal na Guerra desejavam, portanto, abrma-la eomo uma revista ilus- 
trada, eapaz de documentar - e de propagandear - fotografteamente a presen^a dos ex- 
pedicionarios portugueses em Franca, e estavam autoeonvencidos dos sens “patrioticos 
esfor^os”. E nesse quadro que se compreende a politica de difusao gratuita da revista junto 
das altas individualidades franeesas e da imprensa de Franca: 

Tern sido para nos extremamente honroso as eartas que temos eontinuado a 
receber (...), eonstituindo um piiblieo testemunho de alto apre^o aos nos- 
sos patrioticos esforqos. No mes fmdo, mandamos encadernar luxuosamente 
(...) um certo mimero de cole^Qes do Portugal na Guerra para oferecer a al- 
gumas notabilidades franeesas na politica, na ciencia e nas artes. Recebemos 
as mais requintadas frases de agradecimento (...). O senhor Presidente da 
Repiibliea Franeesa (...), o senhor (...) presidente da Camara dos Deputados, 
o senhor Painleve, sabio membro do Instituto de Franca e ex-presidente do 
Conselho de Ministros, o senador e ex-ministro Jules Godin, o ex-ministro 
e ilustre economista Yves Guyot, todas essas notabilidades franeesas nos en- 
viaramcartas (...) agradecendo as cole^Qes oferecidas. Aindahapouco rece¬ 
bemos outra carta do eminente jurisconsulto franees (...) Edouard Clunet, 
agradeeendo tambem a nossa revista. 

Muitas folhas parisienses e departamentais se tern referido a nossa publiea^ao 
com elogio. E dos principals membros da coldnia portuguesa em Pais temos 
igualmente recebido palavras (...) de muito apre^o. A nossa publica^ao ob- 
teve mesmo um grande sucesso em coldnias distantes, porque entre os jor- 
nais que a nossa revista se tern referido destacamos uma folha de Tonkim^'’! A 
imprensa brasileira cita-nos amiudadas vezes e temos visto transeri^oes de 
artigos da nossa revista nos quotidianos mais lidos do Rio, de Sao Paulo, de 
Minas e do Para. (n.° 7: p.7) 

A revista Portugal na Guerra teria sido, em eonsequeneia, enearada pelos poderes piiblicos 
portugueses - designadamente, eom bastante probabilidade, por Norton de Matos - eomo 
um instrumento de propaganda nacional quer junto da nata dos portugueses em Paris quer 


O redator refere-se, possivelmente, a regiao vietnamita de Tonquim - o Vietname era, entao, uma coldnia franc- 
esa (indochina Franeesa). 


220 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 



junto dos restantes aliados, nomeadamente junto dos franceses. A publica^ao serviu, pois, 
para relembrar continuamente aos aliados o esfor^o de guerra portugues para que, quando 
a guerra terminasse, o governo portugues pudesse reivindiear para o pals as justas contra- 
partidas, em espeeial o direito a manuten^ao das colonias. 


Referencias 

Arthur, M. (2007). Faces of World War One: The Tragedy of the Great War in Words and 
Pictures. London: Cassel Illustrated. 

Bishop, J. (1982). The Illustrated London News Social History of the First World War. Lon¬ 
don: Angus &amp; Robertson Publishers. 

Garambone, S. (2003). A Primeira Guerra Mundial e a Imprensa Brasileira. Rio de Ja¬ 
neiro: Mauad. 

Gilbert, M. (2007). A Primeira Guerra Mundial. Lisboa: Esfera dos Livros. 

Knightley, P. (1975). The First Gasualty: From the War Gorrespondent as Hero, Propagan¬ 
dist and Myth-Maker. New York: Harcourt Brace Jovanovich. 

Lasswell, H. (1927). Propaganda Technique in the World War. Cambridge: The MIT Press, 
1971 (reprinted). 

Marquis, A. G. (1978). Words as weapons: propaganda in Britain and Germany during the 
First World War. Journal of Gontemporary History, vol. 13, n.° 3,1978: pp. 467-498. 
Messinger, G. S. (1992). British Propaganda and the State in the First World War. Man¬ 
chester: Manchester University Press. 

Navarro, F. (dir.) et al. (2005). Histdria Universal. Vol. 19. Lisboa: Salvat/Promoway/Pii- 
blico. 

Novais, N. M. (2013). A Imprensa Portuguesa e a Guerra. 1914-1918. Os Jornais Inter- 
vencionistas e Anti-Intervencionistas. A Agao da Gensura e da Propaganda. Tese de 
doutoramento apresentada a Faculdade de Ciencias Sociais e Humanas da Universidade 
Nova de Lisboa. 

Pizarroso Quintero, A. (1993). Histdria da Propaganda Politica. Lisboa: Planeta Editora. 
Sanders, M. L. (1975). Wellington House and British propaganda during the First World 
War. The HistoricalJournal, vol. 18, n.“ 1: pp. 119-146. 

Sanders, M. L.; Taylor, P M. (1982). British Propaganda During the First World War, 1914- 
18. London: MacMillan. 


IV. A HISTORIOGRAFIA DA GUERRA 


221 



V. Posters 




CONGRESSO INTERNACIONAl 
DA PRIMeitA GUERRA NIUNDlIi 


Faculdade de Ciencias Humanas e Socials 
Uniwersidade Fernando Pessoa | Porto I Portugal 


a 11 de abril de 2018 



V. POSTERS 


225 









226 


A PRIMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 














V. POSTERS 


227 









oo 


IB 


S' 

O 


O ^ 

S. w 
o-§jj 


tt o 


:iss 


■6 ri ■= 


O 

&lt;0 

•** 

c 

» 

E 

E 

o 

o 


SI 

•0 Qi 

« S 


0 _ 


8 ! § 
'S 3 

(Qq 


s 


S' Bt 
Bl SS 

11 

I I 

9 

■o 

s ^ 

IB Bt ^ 

M 9 «I 

® SL e 

"ll 



■a tfj ® o 

5 .2 -g a •= 

O LL 'C Q. Q. 


Jodo Casqueira Cardoso &amp; Isabel Silva 
Imagens and Comments on the Red Cross... 


228 


A PRIMEIRA GUERRA MONDIAL. NA BATALHA DE LA LYS 

















THE FIRST WORLD WAR IN THE CITY OF PORTO 

Contributions to the knowledge of daily lives and sociability's during the war (1917) 

Catarina Nogueira Pereira ^ &amp; Diogo Guedes Vidal ’’ 

^ Master Student in History and Heritage, FLUP, University of Porto, catarina nogueira@live.com.pt 

^ PhD Student and Researcher, UFP Energy, Environment and Health Research Unit, University Fernando Pessoa. diogovidal@ufp.edu.pt 



INTRODUCTION 

First World War began in 1914 and ended in 1918 [1, 2], 
Portugal went to war through the CEP - Expeditionary 
Portuguese Body - in Flanders between November 1917 
and April 1918 [3], This intervention have direct impacts 
on population daily lives, resulting in profound changes 
in multiple dimensions of Portuguese society, mobilizing 
a male contingent in an active age. This demographic 
change results in a restructuring of the women role in 
society. Porto, as a coastal and maritime municipality, 
sees the war closely [4], This research intends to share, 
supported in bibliographical and documentary 
evidences, the reality of the city in 1917 and in what 
way its participation in the war affected their dynamics. 


OBJECTIVES 

The main objectives of this work were: 

A. Analyze and explore the documentary heritage of 
the Great War period in Porto; 

B. Establish connections between the state of art and 
the quotidian's during the war presents in the 
documentary heritage; 

C. To know the quotidian's and sociability's of Porto in 
the year 1917. 


RESULTS 


METHODS 

Document analyze — historiographic 
research 

Four funds consulted at ADP - 
Porto District Archive: 

1. Immigrant Passport Processes 
and letters of call; 

2. Entrance Records at Exposed 
Hospices; 

3. Notary Registration; 

4. Porto railways company. 


TOP DESTINATIONS FOR EMIGRATION IN 1917 




Provisional Admission Final Admission 


Querida Justina saude e o que de coragao te desejo assim como ao nosso querido filho. Eu fico bem gragas a providencia. De hoje em diante continuo empregado. Tenho 
esperado pela tua chegada. E ca vens ter se Deus te der boa viagem cuando tu chegares em Santos eu bou dentro do vapor perguntar a tea compurtamento durante a viagem. 
Excerpt from the letter of call inserted In the Passport Process nS 1108, of December 10, 1917. PT / ADPRT / AC / GCPRT / J-E / 099/0441. Source: Porto District Archive. 
Description: A husband writes to his wife expiaining his situation in Brazil. Teii him that he awaits her arrivai on the steamboat where he wiil ask about her behaviour during the 


Foi a referida creanga admitida definitivamente nesta Casa-Hospi'cio na qualidade de desvalida e matriculada com o numero novecentos e nowenfo e tris da quinta serie, em 
virtude de se declarar no oficio, que a acompanhou, qua a mae se acha presa na referida cadeia (Cadeia Civil do Porto) e ndo tern leite para amamentar a filha. Excerpt from 
the admission process no. 993, at February 28,1917. PT/ ADPRT/ ACD / CRPRT / AE / 002/0536. Source: Porto District Archive. 

Description: A chiid is admitted as a disabled because her mother is trapped in jaii and does not have milk to breastfeed. 

...que ndo pode haver duvida de que ndo existindo, como ndo existe contrato, ou acordo especial em contrarlo, o prego do metro quadrado de repartigdo de 
pavimento a pagar pela Companhia ndo pode ser outro sendo o fixado na tabela (...) a redugdo feita nos termos da citada deliberagdo foi uma consessdo 
voluntaria e um acto de equidade da Camara que a Companhia melhor deveria saber apreciar, tanto mais quanto e certo e ninguem pode por em duvida que 
desde o comego da guerra (julho de mil novecentos e catorze) havia motivo para aumento e ndo demlnuigdo de taxa, em consequencia do conhecido aumento 
dos pregos ndo so da mdo de obra mas tambem dos materials de construgdo... Excerpt from correspondence in file No. 412 of March 6,1917. Source: Porto District 
Archive 

Description: An exchange of correspondence between the Porto Chamber and the coiiective transport company in which it denies the decrease of the payment of the rent due 
to the increases provoked by the entrance of Portugai in the war. 


CONCLUSIONS 

The historiographic analyse show a city marked by emigration and families hoping for a better future. Even in war, the city continue to create 
companies and public transports continue to be assumed as main tool to circulate in the city. Although this evidence, many of those live in 
poor conditions, with a high poverty level. The last hope of many children to survive was their admission to the Exposed Hospice (an 
institution with the mission to host abandoned children), but most died shortly after admission. 

REFERENCES 

[1] Arrifes, M. F. (2004). A primeira guerra mundial na Africa Portuguesa: Angola e Mozambique (1914-1918). Lisboa: Cosmos 

[2] Willmott, H. (2003). World War I. Nova lorque; Dorling Kindersley. 

[3] Fraga, L. A. (2003). Reflexos doSIdonismo: o CEP abandonado. In A. Afonso, 8i C. d. Gomes, Portugal e a Grande Guerra:1914-1918. Lisboa: Olarlo de Nob'cias 

[4] Araujo, F. M. (2014). Impressoes Jornalisb'cas sobre o Porto na Grande Guerra. Atas do Encontro " A Grande Guerra (1914-1918); Problemab'cas e Representazoes. CITCEM 


FIRSTWORLD WAR INTERNACIONAL CONGRESS-LA LYS BATTLE CENTENARY | FCHS - UNIVERSIDADE FERNANDO PESSOA | 2018 


Catarina Nogeuira Pereira &amp; Diogo Guedes Vidal 

The First World War in the eity of Porto. Contributions to the knowledge of daily lives and 
soeiability’s during the war (1917) 

















GRANDE GUERRE : LES INDIGENES DE L'ARMEE D'AFRIQUE 

Mehdi Jendoubi 

Licence en Science Politique et Relations Internationales. Universite Fernando Pessoa, Porto 
Mehdi.jendoubi.ufp(S) gmail.com 


iNTRnniirTiniy MPTHninnip 



Mehdi Jendoubi 

Grande Guerra: les indigenes de Farmee d’Afrique 


-I 

ll 

lA 









































VI. Outros 




Phenomenological exploration of emigration 
and acculturation: War and peace between 
the individual position and States 

Joaquim Castro 

Mestre em Psicologia pela FCHS/UFP 


On behalf of my maternal great-grandfather and grandfather, beeause both 
were soldiers in the World War 1 


Abstract: This exploratory and qualitative study describes the author’s personal experi¬ 
ences of emigration. A hrst part of the article focuses on the relationship between the in¬ 
dividual rights and the State violence. Colonization, wars, and discrimination increases 
the Portuguese forced emigration flows. In the second part, this exploratory work applies 
the method of phenomenological reduction, and thus attempts to be descriptive, rather 
than normative and prescriptive. The focus is on ethnic identity and on the emigration 
experience, in comparative reflection to the main acculturation models. Labeling is often 
perceived as intrusive and causes discord with the individual’s self-categorization. The 
author, from Portugal, experiences social pressure for self-categorization in France, as 
well as in Portugal. The ordinary categorizing words “immigrant” and “emigrant” are la¬ 
bels. Each label carries attributes related to otherness, and low socioeconomic status. The 
personal experience is close to fusion acculturation. 

Keywords: acculturation, ethnic identity, migration. State violence, saudade. 

Resumo: Este estudo exploratorio e qualitativo descreve as experiencias pessoais do autor. 
Uma primeira parte do artigo foca a rela^ao entre os direitos individuals e a violencia do Es- 
tado. Coloniza^ao, guerras e discrimina^ao aumentam os fluxos portugueses de emigra^ao 
for^ada. Na segunda parte, este trabalho exploratorio aplica o metodo de redu^ao fenome- 
noldgica e, portanto, tenta ser descritivo, em vez de normativo e prescritivo. O foco esta na 
identidade etnica e na experiencia de emigra^ao, em reflexao comparativa aos principals 
modelos de aculturacjao. A rotulagem e frequentemente percebida como intrusiva e causa 
discdrdia com a auto-categoriza^ao do indivfduo. O autor, de origem portuguesa, experi- 
menta pressao social por autocategoriza^ao na Eran^a, assim como em Portugal. As pala- 
vras comuns de categoriza^ao “imigrante” e “emigrante” sao rotulos. Cada rdtulo carrega 
atributos relacionados a alteridade e baixo status socioeconomico. A experiencia pessoal 
esta proxima da acultura^ao de fusao. 

Palavras-ehave: acultura^ao, identidade etnica, migra^ao, Estado violencia, saudade. 


VI. OUTROS 


233 


Introduction 


Migration may be disruptive at national and international levels. It may be also disruptive 
at individual and collective levels. Migration is an international topic (Knepper, 2010), for 
instance, between France and the United Kingdom. Migration is also a politicized topic 
(Pecoud, 2015), because it is often manipulated by the left-right political spectrum. 

Migrations are a constant in the Human evolution (Coppens, 2012). Today, due to migra¬ 
tions, almost all cultures are changing (Wihtol, 2013). Culture is made by innovation, dif¬ 
fusion, and acculturation. Innovation takes place in a particular cultural group. Diffusion 
does not need direct contact between cultures, but acculturation requires enduring di¬ 
rect contact. Migration and acculturation encompass contact between cultures, and both 
transform, and often improve cultures and individuals. 

Acculturation concept appears in anthropology in the European colonization of North 
America (Powell, 1880). Acculturation focuses on the dominated North American minori¬ 
ties, mainly on the Indigenous (Castro, 2017a). At the beginning of the 20* century, sociol¬ 
ogy focuses on immigrants (Castro, 2017b). Hence, acculturation appears in colonial times 
(Rudmin, Wang, &amp; Castro, 2017), and it has attached a past of violence. 

Acculturation research is nearly dismissed in the anthropological literature (Castro, 
2017a, 2018a, b; Winthrop, 1991), and it is replaced by social movements, and by the cul¬ 
tural change topic. The latter is, in fact, a main dimension of the acculturation concept, 
and it was employed earlier by the British anthropologists (Malinowski, 1958/1945). Ger¬ 
man anthropologists have taken a similar position (Westphal-Hellbusch, 1959). According 
to Bastide (1968), in France is employed the expression interpenetration of civilizations 
(interpenetration des civilisations), and in the Spanish (Castilian) language is also applied 
the word transculturation due to Ortiz’s work (1995/1940). 

In sociology, acculturation is studied mainly regarding immigrants, and it applies the 
assimilation model (Castro, 2017b; Portes, Fernandez-Kelly, &amp; Haller, 2005). In psychol¬ 
ogy, acculturation research appears generally after the 60s of the 20* century, and the 
multicultural model is pervasive (Berry, 2001). Nowadays, acculturation research is under 
a deadlock (Rudmin, 2009; Rudmin, et al., 2017). The deadlock is due to historical, politi¬ 
cal, and methodological reasons. 

Psychology works with intrapsychological data, for instance, emotional, and cognitive 
data. However, data comes, and it is applied in social context. In anthropology, the violent 
past of acculturation is acknowledged. However, in psychology, it is rarely acknowledged. 
The current and dominant research is grounded on attitudes (cultural preferences) con¬ 
cerning minorities, and immigrants. However, according to the current article, it is nec¬ 
essary to report the immigrant personal experiences. 

This article aimed to be a contribution to solve that deadlock. It applied personal experi¬ 
ences in a phenomenological way. It was more emic than etic, because it barely compared 
cultures, and it described personal experiences. 


234 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


1. Acculturation definition 


The acculturation phenomenon may be dehned by its main dimensions, i.e., intercultural 
contact, mutual interactions between different cultures (Castro, &amp; Rudmin, 2017; Red- 
held, Linton, &amp; Herskovits, 1936), to learn a second culture (Powell, 1880; Rudmin, 2009), 
and by individual and collective cultural changes (Boas, 1982/1940). 

1.1. Acculturation models 

The acculturation topic has four models, i.e., assimilation, multicultural, fusion, and in¬ 
tercultural (Castro, 2012, 2014a, b, 2015, 2016a, b, c, d; Taylor, 2012). According to Castro 
(2015), in the assimilation model, the minority culture is expected to disappear. The mutu¬ 
al learning will not be reported, because the minority will be completely assimilated. The 
European policies in the 19th century and the Chicago School (Park, 1928) are examples of 
the assimilation model. 

in the multicultural model, the minority culture is expected to adapt and, at the same 
time, to maintain its culture (Berry, 2001). in the model, only the minority is described as 
learning, and both cultures interact with the larger society. The Berry Model (2001) is an 
example of the multicultural approach. 

in the fusion model, there are interactions, mixtures, and mutual learning between dif¬ 
ferent cultures. Cultural mixtures are expected to produce a new culture with internal 
diversity (Castro, 2012, 2014a, b, 2015, 2016a, b). The works of Freyre (1986/1933), Ortiz 
(1995/1940), and Alexander the Great (Simons, 1901) were examples of the fusion model. 

in the intercultural model, at the private level, the minority may change or maintain its 
cultural legacy, because of the laissez-faire. However, the minority at the public level is 
expected to adapt to the majority culture, for instance, at labor and educational domains. 
At the institutional level, the interaction between different cultures is reduced. The uni¬ 
versal values of the French Republic may be an example of the model. The intercultural 
model is also connected to the Francophone culture of Quebec (Taylor, 2012). 


2 . Categories of migration 

in the Universal Declaration of Human Rights (UDHR), the article 13 stated that free dis¬ 
placement is a Universal Human Right (United Nations, 2015/1948). However, human 
migrations encountered legal limits placed on State borders (Scott, 2009). They also en¬ 
countered limits on natural barriers, e. g., seas or mountains. Besides the legal and the nat - 
ural borders, there are also cultural, ethnic, religious, linguistic, economic, lifestyle, and 
membership borders. 

Migrations may be collective, individual, forced and voluntary. Wars and colonization 
are collective and forced migrations. The history of Portugal started with the Reconquest 
(1139-1249), which resulted in the crusades, and conquest of territories in the North of 
Africa, e.g., Ceuta in 1415. Later, it drove to colonization (Dupront, 1997) and Portuguese 
Empire (1415-1999). 


VI. OUTROS 


23S 


The emigration flow that oecurred in the 19th century to Brazil was often individual. 
The Independence of Brazil, in 1822, transformed the Portuguese settlers in immigrants. 
The Portuguese emigration to Europe in the mid-twentieth century was also individual. 
However, it may be hardly called voluntary. It was done to escape from the Colonial War 
(1961-1974), and political oppression. 

Voluntary emigration encompasses free will, and the absence of social constraints. 
Transnational and cosmopolitan entrepreneurs correspond to the voluntary category. 
However, the Portuguese emigrant flows were rarely voluntary. 

Portugal is a unitary semi-presidential representative democratic republic, and it is 
considered a developed country. However, the Portuguese recent migrants are often co¬ 
erced, due to social constraints. Hence, forced migration is produced by social discrim¬ 
ination, and it is understood as deprivation of liberty, and access to opportunities, and 
services (Allport, 1954). 

The Portuguese State is in a difficult situation regarding emigration. It may stop it, for 
instance, through repression, like in the 1960s, but it would be in opposition to a uni¬ 
versal human right. However, on the contrary, the Portuguese ordoliberal government 
(2011-2015) encouraged it, and it failed as a State, because it did not keep the population 
stable. Historically, the Portuguese migrations gain collective and forced characteristics. 
State, institutions, and upper classes are responsible for emigration. Wars often increase 
migrations, for instance, during the First World War the Portuguese emigration to Brazil 
increased (Ferreira &amp; Rocha, 2013), and later it started the first emigration flow to France 
(Alves, 1988; Pereira, 2013), which boosted during the Colonial War (1961-1974). 


3. Historical events 

3.1. Agriculture, territory, ownership, and culture 

The Paleoanthropologists stated that human groups started to be sedentary in the Levant 
and the Middle East, circa 12 000 years ago. Agriculture boosted the creation of culture 
and changed completely nature. The current fauna and flora were the outcomes of the An- 
thropocene (Latour, 2015). Today, archaeological evidence reported that sedentary groups 
were surrounded by their deaths. The worship of the ancestors was a way to justified the 
collective ownership of territory, besides its religious function (Coppens, 2012). 

The sedentary lifestyle enhanced the emergence of cities, and networks among them. It 
created the first empires. The limits of the empire were its borders, which were sometimes 
blurred. The current notion of borders is grounded in the Western culture (Hobsbawm, 
1995), and it is dominant in the planet, including the Arctic, and the Amazon rainforest 
(Clastres, 1974). Hence, the migrant individual is labeled mainly by its nationality. A sov¬ 
ereign State has sovereignty over a territory and population (Scott, 2009), and the individ¬ 
ual rights may be banned under the State power. 


236 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


3.2. Collective and historical narratives 


The State creates a collective narrative, and often a characteristic culture. In Ancient 
Greece, the collective memory started to be written by Homer. In the Portuguese culture, 
Camoes (1524 or 1525-1580) wrote the epic work Os Lusiadas (The Lusiads). 

In Ancient Greece the representation of the planet was very different than the current 
representation. Currently, it is considered that the sailors under the Portuguese Crown, 
and Ferdinand Magellan (1480-1521) started the globalization process. All cultural groups 
were potentially in contact, and the isolation of the earlier Homo sapiens groups ended. 

The Portuguese Empire is ambiguous, because one part is pointing out to an epic narra¬ 
tive, and another part is pointing out to colonial rule. Sedentary life was not the norm, and 
the Portuguese outbreak in other territories caused tragic changes. 

3.3. Violence as a founder of culture 

According to Freud (1919/1913,1936), the founding act of culture was the symbolic killing 
of the father. The children were united by the guilt of father’s death. Similarly, accord¬ 
ing to Rene Girard (1972), culture emerged associated with violence. The mimetic desire 
implied to get what was owned by the other person. Another form of cultural creation 
emerged by collective violence against a scapegoat. The mimetic desire, described by Rene 
Girard, was analogous to the projective identihcation described by Melanie Klein (1964). In 
the projective identihcation an individual projected and located in another person feeling 
and thoughts that were intolerable. It worked to blame the other, and it was also similar 
to Girard’s scapegoat. Furthermore, it aimed to assimilate the other. For instance, in the 
Progressive Era (l890s-1920s), to blame the natives or the African Americans for the delay 
in the North American progress was current (Castro 2017c, 2018a, b), and the same hap¬ 
pened in the Portuguese colonies. 

Klein’s theory provides a clue to understand violence. One typical and very common 
emotion connected to projective identihcation is envy. Like projective identihcation it 
aims what other person has or is, and his or her destruction. Veblen explained how envy 
was important to explain the conspicuous consumption. Most of colonial empires and 
wars can be explained by the economic envy, e.g., Europeans were chasing El Dorado. 

3.4. Colonialism and the First World War 

It is acknowledged that that Portuguese Republic engaged in the First World War (1914 to 
1918) to safeguard its colonies. Germans were in the South of Africa, and it endangered the 
Portuguese Empire. Hence, the Portuguese Republic supported the allied powers. The First 
World War increased the emigration flow to Brazil, and it started the emigration flow to 
Europe (Alves, 1988; Mendes, 1988; Munoz, 1991). 

As stated above, the Portuguese expansion was, at the same time, epic and violent. The 
British historian Charles Boxer (1959) opposed to Portuguese dictatorship (Estado Novo, 
1933-1974). Boxer’s academic work was outstanding. However, it gave the impression that 
the Portuguese Empire was worse than the British and Dutch Protestant Empires. It drove 


VI. OUTROS 


237 


to projective identification and splitting, because violence was pervasive, and there was no 
good object, and the common violent past prevented to plan the future. 

Today, globalization, nuclear weapons, and ecologic (economic) exhaustion (Cohen, 
20159) drove humanity to a deadlock. It is necessary to act and change, but, at the same 
time, current days are perceived as a dreadful period, and it includes science and culture 
(Latour, 2015). The situation is similar to the cognitive dissonance (Festinger, 1962), and 
to klein’s splitting, and to a lack of psychological consistency, because action and new 
information contradict ideals. 

3.5. How to solve the ambivalenee of violenee 

According to Norbert Elias (2012), one of the founding questions of sociology proposed 
by Auguste Comte (1798-1857) was how individuals gathered to form social groups, and 
societies. The question was connected to violence. The question was, in fact, a phenome¬ 
nological experience, because it suspended the knowledge of the world, and took to con¬ 
sciousness the most basic that existed in minds. 

Gregory Bateson (1935) accused anthropologists, and other social scientists of participat¬ 
ing in colonial administration. The same Bateson provided a clue on how to solve the im¬ 
pairment represented by cognitive dissonance and splitting. Bateson said that only the 
introduction of new information was capable of leading to learning, and change. The split¬ 
ting behavior tends to be dichotomous, and to annihilate the difference. 

Projecting intolerable objects into the other individual may be a very short-term a func¬ 
tional strategy. However, in order to reach a higher stage of mental and interpersonal 
functioning, the individual also needs to put intolerable objects within. Hence, an individ¬ 
ual may be good and bad at the same time. 

A similar solution was provided by Arendt (1958). Extreme collective violence may be 
accomplished by ordinary people. It avoids group essentialism, because it is a rationaliza¬ 
tion. Similarly, but in the opposite direction, it is necessary to think that tolerance is not 
exclusive to the Western liberal thought. 

Other defense mechanisms that may trigger strong elaboration are; mindfulness (curiosi¬ 
ty, openness, and acceptance); sublimation; tolerance regarding what is different; and ra¬ 
tionalization with a permanent critic point of view. So, it implies to change the narrative, 
in an inclusive manner, and in different directions, which is, in fact, a dehning dimension 
of science. 


4. Methodology 

4.1. Phenomenology 

The current article is grounded in a phenomenological description of personal experienc¬ 
es about the own immigration. Edmund Husserl (1859-1938) introduces phenomenolo¬ 
gy in philosophy. Literally, phenomenology means to study what appears (from Greek 


238 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


phainomenon “that which appears” and logos “study”). It is the study of the structures of 
experience, and consciousness. In the current article, it is applied the phenomenological 
reduction. Therefore, it focuses on an analysis of experience, and the article is done taken 
into account what appeared in mind, and intentions. 

4 . 2 . Personal data 

Cultural reality is increasingly complex, and social sciences are also part of that complexity 
(Morin, 2005). Knowledge about the own culture is very partial. Some elements of cultural 
legacies are stored in oral and written devices. Written systems are recent regarding the 
human evolution, because they appeared, for instance, in Sumer only circa 5,000 years 
ago (3,000 BC). 

Westerns and other cultures, for instance, Polynesians, Mongols, and Arab Muslims 
conquered and colonized the planet. National narratives and identities are often celebrat¬ 
ed in writing devices. Today, the global history is pervasively a Western construction, due 
to the Western social dominance for circa 500 years (Goody, 2006). Those narratives are 
also collective descriptions, and they are descriptions of dominant social classes. However, 
people stories were often dismissed (Zinn, 1994), and also the individual descriptions. 

Immigration is often approached by the majority side, because the main question is how 
to incorporate immigrants (Brettel, &amp; Holliheld, 2015). The emigrant and the individual 
points of view are often dismissed. 

Personal documents are employed in the acculturation research connected to immigrants 
since the seminal work of Thomas and Znaniecki (1918). The pervasive research is ground¬ 
ed in cultural attitudes, consequently in predispositions for action. However, the current 
article argues that it requires personal descriptions. 

4.3. Ethical issues 

Migration is a politicized issue (Pecoud, 2015). Consequently, political manipulation and 
even personal and professional benehts have to be under vigilance. As stated at the outset, 
migrations are considered potentially disruptive at many stages. However, the current re¬ 
search does not provide a single solution for likely disruptions. 

Frequently, attitudes are different than historical and personal experiences (Castro, 
2017c; Navas, Garcia, Sanchez, Rojas, Pumares, and Fernandez, 2005). For instance, Her¬ 
bert Spencer, in the 19* century, preferred a minimal interaction between Japan and the 
Western cultures. However, Herbert Spencer himself learned the Japanese culture. Con¬ 
sequently, he did fusion. 

Another ethical issue is that the research should display its cultural background, and 
sociopolitical goals. This research prefers the fusion model. However, it does not state that 
it must be prescribed. It aims to be descriptive, and the goal is not to be normative and 
prescriptive (Miller, 1924). In the current article, fusion is approached as dynamic, and it 
is not studied as an expected outcome. Furthermore, fusion is considered a way of cultural 
creation with no particular content, and outcome. 


VI. OUTROS 


239 


5. Ethnic identity 


Ethnic identity is dynamic (Barth, 1969; Phinney, &amp; Ong, 2007), and it is a construction 
over the entire iife span. Ethnic identity is aiso a muitidimensionai construct that refers 
to individuai identity as a memher of an ethnic group. The concept of ethnic identity has 
three main dimensions. The hrst dimension is the seif-categorization, it may he the “I” 
and the “We”. It may he endorsed hy other persons or cultural groups. The second di¬ 
mension is the subjective sense of belonging to an ethnic group. The third dimension is 
its development by the exploratory behavior (Phinney, &amp; Ong, 2007). The current article 
focuses the seif-categorization and the subjective sense of belonging. 

5.1. Self-categorization 

Elias and Scotson (1994/1965) reported that it was the mere social relationship that pro¬ 
duced a culture near Leicester in the United Kingdom. According to Barth (1969), it was 
the mere intercultural contact that produced ethnic identities, sometimes in the same cul¬ 
ture. In the Western culture, many ethnic labels were ascribed in Ancient Greece, and in 
the Roman Empire, for instance, Germans. Today, ethnic groups are also labeled according 
to the States, for instance, the Russians. Often, religion is also a factor to create ethnic la¬ 
bels. Another frequent factor is the phenotype. 

As stated above, the ethnic identity may be a self-categorization or may be ascribed by 
other individuals or cultural groups. The latter may be perceived as intrusive. Eurther- 
more, the self-categorization, and the ascribed label may be discordant or concordant. 

Labels change according to context, for instance, in Japan the author would be labeled as 
White and European, but, in the European Union, he would be considered as Portuguese. 
Eurthermore, a person who experiences migration is often labeled in the second culture, 
but also in his or her departure country. 

The immigrant person experiences a social pressure to dehne oneself regarding other 
persons, and cultural groups. It happens especially when differences are manifest, for 
instance, due to the poor use of the second language. In the current article, the ethnic 
identity topic was described taking into account both spaces, i.e., the departure and the 
receiving States. 

5.1.1. Labeling in the seeond eulture (Franee) 

In the second culture, he experiences regular demands for self-categorization. It is often 
experienced as intrusive. However, it is normal to categorize persons (Barth, 1969), even 
at cognitive level, and everybody does it (Allport, 1954). In Erance, the label “Portuguese” 
has stereotypes attached. Stereotypes were connected to the Portuguese hrst generation 
of immigrants, and also to the Portuguese culture. Like many stereotypes, it has a part of 
reality, for instance, the dried and salted cod is a typical Portuguese food, but it is hshed 
thousands of miles away from Portugal. It reminds the so-called Portuguese discoveries, 
which are also a component of the Portuguese auto stereotypes. 

In the Schengen Area, there are free human movements between countries. However, 
the emigrant person will be considered as an immigrant. Additionally, the word “immi- 


240 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


grant” is by itself a label. In the departure culture, occurs the same, and he or she will be 
labeled as “emigrant”. 

5.1.2. Labeling in the departure eulture (Portugal) 

In the original culture, society, and State the migrant status also changes due to the mere 
absent regarding the social interactions. Abroad is perceived as farther than the internal 
migrants, and it is perceived as culturally different, and it makes that the immigrant in¬ 
dividual gains a new ascribed label, i.e., “emigrant”. The reaction regarding the absence 
encompasses curiosity about the new culture, but also a pressure for conformity regarding 
the original culture. 

According to the own experience, the emigrant person must be absent from the dai¬ 
ly relationships at least one year. After that, he returns, and his or her status changes. 
The immigrant starts to be treated as an outsider, and mainly as an emigrant. In Portugal, 
Portuguese citizens who are living in France are labeled as “French”. Another endorsed 
label is the French word “avec” (with), and it encompasses a prejudice. In the Portuguese 
literature, the character of the emigrant appeared largely in the 19* century. For instance, 
the Portuguese novelist E^a de Queiros (1845-1900) wrote about Portuguese who returned 
from Brazil (Alves, 2004), and he despised them. 

The intrusive demand to self-categorization is experienced as more intrusive in the 
departure than in the second culture. This social pressure occurs in regular social rela¬ 
tionships. In the departure culture, natives are curious about the receiving culture. It en¬ 
compasses acculturation, because they are getting information from an emigrant. It also 
encompasses intercultural comparisons, and sometimes it triggers conflicts. 

The comparison works to choose what is considered the best culture. Flence, it works 
by exclusion, because the thinking is binary. It happens, maybe, due to the lack of liberal 
background in Portugal, and also because the North of Portugal has an individualistic and 
competitive culture (Todd, 1994). All cultural groups are sharing hetero and auto stereo¬ 
types (Vassiliou, Triandis, Vassiliou, &amp; McGuire, 1972). Maybe, emigrants report new in¬ 
formation that may disconhrm the auto stereotypes. 

The current phenomenological approach has a consequence for the self-categorization 
topic. It argues that there is the self-categorization, i.e., “1” and/or “we”, and the ascribed 
labels in the departure, and in the receiving cultures. The combination of the three kinds of 
categorization provides several possible ethnic identities. All of them may appear isolated 
or at the same time (Goffman, 1959). Flowever, for the migrant person, all of them coexist 
at the same time in her or his personal experience. The hetero categorization may be dis¬ 
cordant, and it is often intrusive. Furthermore, conflicts may occur in the departure, and 
in the new culture. The disagreement reinforces his individualistic engagement, because 
the self-categorization is now more “1” than “we”. 

5.2. Subjective sense of belonging 

This section is also divided into the subjective sense of belonging regarding the departure 
culture, and regarding the second culture. 


VI. OUTROS 


241 


5.2.1. Subjective sense of belonging to the second culture 

The second culture, society, and nation-state changed due to migration. Phenotypes traits 
changed also a little. Other experienced changes were the emotional expression, architec¬ 
ture, landscape (humanized territory), food, and etiquette. However, the main chance was 
connected to language. 

Today, due to globalization, English is the lingua Franca, and the author often employs 
it. It does not help him to an immersion into the second language. He is from the northwest 
of Portugal, and that region is known to mix the spelling of the B and of the V letters. It 
is a barrier to a proper communication. The immigrant is well-educated by socialization; 
however, he does not manage the second language, mainly its written form. So, his incor¬ 
poration into the second space is not easy. 

Allport (1954) in his seminal work establishes several levels of prejudice. Antilocution is 
the hrst stage, and it encompasses negative verbal remarks against a person. The next stage 
is avoidance, and it occurs when members of the in-group actively avoid members of the 
out-group. Discrimination is the next level, and the out-group members are deprived of 
opportunities, and services. Physical attack and extermination are the additional stages. 

The experienced discrimination in the departure culture is low, and acculturative dis¬ 
tress is also low. However, it does not mean that he experiences a feeling of belonging to 
the second culture. He perceives himself mainly as an individual. Furthermore, the French 
intercultural model allows cultural maintenance at private level, and there is no social 
pressure to adhere to the French nationality, and to the supposed French ethnic identity. 

5.2.2. Subjective sense of belonging to the departure culture 

The migrant person experiences a feeling of rejection. However, that emotion, and cogni¬ 
tion is connected to the original culture, essentially to the original State. A fundamental 
State’s function is to maintain its population under its borders (Scott, 2009), because it 
needs manpower and taxpayers to keep the upper classes in their places. Hence, the State 
fails for emigrants. The emigrant person experiences rejection, and an increasing individ¬ 
ualism. Hence, social discrimination occurs in the original culture. 

Tlano (1900) wrote what may be considered an earlier example of an individualistic 
point of view about the ethnic identity topic. He wrote “... race is a matter of supreme in¬ 
difference to the individual, whose affections are conhned within a circle of small radius, 
when compared with the entire held of human life... the race is the individual’s greatest 
enemy no need to preservation. (Llano, 1900, p. 504). 

However, the individualistic point of view has limits. Contact leads to categorize the 
ethnic identity, and then it triggers comparisons (Barth, 1969), and social differentiation. 
The current individualism and/or cosmopolitism does not avoid categorization of the eth¬ 
nic identity. It is important to state that social differentiation is not discrimination by it¬ 
self. The self-categorization encompasses the “I” and the “we”, and the subjective sense of 
belonging to the “we” decreased. 


242 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


5.2.2.1. Phenomenology of saudade 

In the American Psychologist, perhaps, the first article to relate migrations and emotions 
was written by Kline (1898), and it provided a positive meaning to nostalgia. Therefore, 
nostalgia was more connected to eustress than to distress. Nostalgia enhanced continuity 
between spaces, social adaptation, group cohesion, and strong mental health (Sedikides, 
Wildschut, Gaertner, Routledge, &amp; Arndt, 2008). 

In the Portuguese culture, nostalgia gained a peculiar meaning, and it is designated as 
saudade. Saudade protects mental health at individual level. However, at collective level, 
it may work as a power device. Saudade may be promoted by the State, institutions, com¬ 
mercial business, and it also works on interpersonal relationships (Foucault, 1980). 

In the Portuguese literature, saudade appeared in Fernao Mendes Pinto (1989/1614). 
He lamented the intercultural violence and decided to return to Portugal. In the Portu¬ 
guese historical narrative, saudade is related to emigration and colonization. The latter 
took place to outside, and increased territory, and cultural influence. It provided a sense 
of superiority (Adler, 1925). The emigration to Europe did not increase the territory, but it 
displaced population, and changed culture. Hence, it provided a sense of inferiority. 

In the Portuguese historical narrative, saudade did the connection between emigration 
and colonization. In the 19* century, Brazil was already another State, but it was possible 
to maintain, and to enlarge the Portuguese culture by emigration. Another reason is that 
colonization and emigration occurred at the same time, because the colonial empire gen¬ 
erally only ended in 1974. The connection between both was clear in New England (Taft, 
1969/1923), because Portuguese and Cape Verdean immigrants were living together. 

The Portuguese accession to the European Union in 1986, and its transnational scope 
ascribed a positive meaning to the Portuguese emigration. Hence, saudade established the 
link between the supposed Portuguese communities, and it established a temporal and 
symbolic link between empire and emigration. However, the symbolic cohesion does not 
solve the social differentiations within the Portuguese State, and it does not promote the 
democratic participation. For instance, emigrants are barely included in the political par¬ 
ticipation within the Portuguese unitary semi-presidential representative democratic re¬ 
public. The symbolic cohesion operated by saudade benehts the upper social classes, for 
instance, due to remittances. 

Rocha-Trindade (1987) argued that often the relationship between emigrants and Por¬ 
tugal was limited to the village. Similarly, when the author thinks about Portugal and sau¬ 
dade, what comes to mind is his personal relationships, and it are not the State, a territory 
or an abstract population. 


6. Personal experience regarding the acculturation models 

Acculturation models are described above. However, it is important to draw additional 
commentaries, because they are useful to understand the personal experience. 


VI. OUTROS 


243 


6.1. Marginalization may be an individual choice 

In the Berry Model (2001), the cultural preference that implies, at the same time, to give up 
hoth cultures is called marginalization. It is likely at the ethnic identity level, hut it is not 
possible at cognitive level, because to be under complete deculturation is not possible. For 
the current article, marginalization corresponds to the individualistic position (Bourhis, 
et al., 1997). 

Marginalization is previously conceptualized in the literature. Park (1928) conceptual¬ 
izes the marginal man as not belonging to any culture. The marginal man is in a diaspora, 
because he is without territory, and is rejected by both cultures. Therefore, the marginal 
man concept is connected to discrimination. 

Today, marginalization is similar to the cosmopolitan position. Cosmopolitan position 
supposedly is not connected to any specihc territory, and it says that the urban and di¬ 
verse environments are its preference. However, it is hard to be cosmopolitan without any 
reference to culture and territory. 

6.2. Phenomenological experience regarding models 

The personal experience does not match in any model. Social assimilation does not take 
place, because he does not identify himself with the French State, and because it needs the 
majority acceptance. The multicultural model does not match also, because he is chang¬ 
ing, and the multicultural model requires cultural maintenance. Furthermore, his ethnic 
identity also changed, but the major changes were at cognitive and individual stages. The 
changes on the ethnic identity have to include his relationship regarding the departure 
culture. At the cognitive level, his current situation may be considered a fusion. However, 
fusion must be considered as dynamic. The intercultural model matches, but not com¬ 
pletely, because at the public level his adaptation is inclusive. 


7. Discussion and conclusion 

Cross-cultural differences are a source of information, and they may disconhrm the auto 
stereotypes, and the stereotypes about other cultures. Migratory contact implies to re¬ 
think the departure culture, and it may have repercussions over the personal worldview. 

At the ethnic identity level, it is not possible to fuse two cultural elements that are ex¬ 
cluding each other, for instance, two monotheistic religions. However, at individual and 
cognitive level, it is possible to fuse two cultural elements that are excluding each other. 
It is possible to learn a second culture, to manager it, and to keep the individual identity. 
However, the mindset and the worldview may change. 

Today, modern States act similar to enterprises, because they also try to sell a commod¬ 
ity. In the last decades, the same happens to the ethnic identities (Comaroff, &amp; Comaroff, 
2009). Ethnic identities are important in the case of colonial oppression and discrimina¬ 
tion. However, the author belongs to a previous colonial empire, and he does not belong 
to a dominated culture. 


244 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


It is possible to conclude that there is a pressure for self-categorization in the second, 
and also in the original culture. The ethnic identity may be ascribed by other persons or 
cultural group. The ascribed ethnic identity occurs at the same time than the self-cate¬ 
gorization. It occurs even when the individual does not identify himself or herself with 
an ethnic identity, and it is often experienced as intrusive. Besides, to be intrusive, the 
ascribed label may be discordant regarding the personal self-categorization. The words 
“immigrant” and “emigrant” are labels. They have attributes attached, and they are re¬ 
lated to the relationship with otherness, and low socioeconomic status. The experienced 
intercultural conflict due to immigration is acuter regarding the departure culture than 
the second culture. The “I”, the “We”, and the “You”” or the “They” are often in conflict, 
and it reminds that often individuals are under social pressures to dehne themselves as 
belonging to a cultural group. 

The social pressures to categorize are especially strong in stressful social conditions, i.e., 
colonization, civil wars, and wars. Today, the Portuguese culture and mainly the Western 
European culture are pervasive. However, the dominance was achieved by violence. The 
prevalence of violence leads to cognitive dissonance and splitting. Currently, to think the 
past in order to plan the future is a hard task. 

Intercultural relationships tended to fusion, to assimilation and to multiculturalism. The 
latter apparently had humanistic attributes. However, all the preferences and historical 
policies encompassed asymmetric power relationships, and violence. The main problem 
seems to be grounded in the difficulty to live with otherness. Psychology does not solve the 
problem at social level, because its realm is intrapsychological. However, Psychology pro¬ 
vides some clues to solve the problem. Hence, otherness must be included, and respected, 
and healthier defense mechanisms should be stressed, e.g., rationalization. 

Barth (1969) wrote that ethnic identities are independent of culture, because the same 
culture may have different ethnic identities. In the future are expected new social differ¬ 
entiations due to biotechnologies, so due to human intervention and science (Habermas, 
2002). In the 19* and 20* centuries there were strong differentiations among humans. 
Race was an obsession, and differentiation operated also amongst Europeans. Often, social 
differentiations operate within cultural groups, and it endangered individual differentia¬ 
tion and rights. 

Jane Addams (1907) lived in the Progressive Era and during the Eirst World War. She was 
a feminist and helped to create a network of international pacifists. However, it did not 
avoid the Eirst World War. Jane Addams (1907) connected immigrants to peace, and other¬ 
ness. Current times are the consequence of the Progressive Era. However, today pacifism 
seems to be weaker than in Jane Addams’s times. Hence, individuals have to stress agency 
in order to enhance pacifist societies, and intercultural relationships. 


VI. OUTROS 


245 


References 


Addams, J. (1907). Newer ideals of peace. Syracuse, NY: The Macmillan Company. 

Adler, A. (1925). The practice and theory of individual psychology. London: Routledge &amp; 
Kegan Paul. 

Allport, G. (1954). The nature of prejudice. Cambridge, MA, Addison-Wesley. 

Alves, J. F. (1988). ‘Operarios para Franca e Inglaterra (1914-1918) - Experiencias da em- 
igra^ao portuguesa intra-europeia’ in Revista da Faculdade de Letras - Histdria, V, pp. 
317-333. 

Alves, J. F. (2004). ‘O «brasileiro» oitocentista - representa^oes de umtipo social’ in Vieira 
B. M. D. (org.) Grupos sociais e estratificagao social em Portugal no Se'culo XIX. Lisboa: 
ISCTE, pp. 193-199. 

Arendt, A. (1958). The human condition. Chicago: University of Chicago Press. 

Barth, F. (1969). Ethnic groups and boundaries. The social organization of culture differ¬ 
ence. Oslo: Universitetsforlaget. 

Bastide, R. (1968). ‘Acculturation’ in Encyclopaedia Universalis. Paris: La Societe d’Edi- 
tion Encyclopaedia Universalis, pp. 102-107. 

Bateson, G. (1935). ‘Culture contact and schizmogenesis’ in Bateson G. (Ed.), Steps to an 
ecology of mind; Collected essays in Anthropology, Psychiatry, evolution, and episte¬ 
mology. London, lason Aronson, pp. 71-82. 

Berry, J. (2001). ‘A psychology of immigration’. Journal of Social Issues, 57, pp. 615-631. 
Boas, F. (1982/1940). Race, language, and culture. Chicago: University of Chicago Press. 
Boxer, C. R. (1959). The tragic History of the Sea. Cambridge: Hakluyt Society. 

Brettel, C., and Hollifield, J. (2015). Migration theory. Taking across disciplines. New York 
and London: Routledge. 

Castro, J. F. P. (2012). ‘The Portuguese tile in the Rudmin Acculturation Learning Model: A 
fusion case’ in Gaiser. L., &amp; Curcic D. (Eds.), EMUNl, bridging gaps in the Mediterranean 
research space. Conference proceedings of the 4th EMUNl Research Souk, 17-18 April. El. 
Knjiga/Portoroz: EMUNl University, pp. 618-625. 

Castro, J. F. P. (2014a). O contexto da aculturagdo portugues atraves do modelo de Rud¬ 
min: do encontro intercultural com o Japao ate ao Luso-Tropicalismo. Unpublished doc¬ 
toral dissertation Universidade Fernando Pessoa, Porto, Portugal. 

Castro, J. F. P (2014b). ‘O contexto da aculturagdo portugues atraves do modelo de Rud¬ 
min: do encontro intercultural com o Japao ate ao Luso-Tropicalismo’ in Gabinete de 
Rela^oes Internacionais e Apoio ao Desenvolvimento Institucional (Coord.), Atas dos Dias 
da Investiga^ao na UFP Research Days Proceedings. Porto: Gabinete de Rela^Qes Internac¬ 
ionais e Apoio ao Desenvolvimento Institueional. 

Castro, J. F. P (2015). ‘Towards a Psychology of fusion in the acculturation phenomenon’ 
in Gabinete de Rela^Qes Internacionais e Apoio ao Desenvolvimento Institucional (Co¬ 
ord.), Atas dos Dias da Investiga^ao na UFP Research Days Proceedings. Porto: Gabinete de 
Rela^oes Internacionais e Apoio ao Desenvolvimento Institucional. 

Castro, J. F. P (2016a). ‘Acculturation in the Portuguese overseas experience with Japan: A 
Rudmin Model application’, Daxiyangguo: Revista Portuguesa de Estudos Asidticos, 20, 
pp.89-120. 


246 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Castro, J. F. P. (2016b). ‘A aprendizagem duma segunda cultura e a identidade etnica dos 
indigenas brasileiros atraves duma rede soeial: estudo exploratdrio’ Religacion, Revista de 
Ciencias Sociales y Humanidades, 2, pp. 75-94. 

Castro, J. F. P. (2016e). ‘A literature review on the Portuguese emigration literature and 
aceulturation’ in Gabinete de Rela^oes Internacionais e Apoio ao Desenvolvimento In- 
stitucional (Coord.). Atas dos Dias da Investiga^ao na UFP Research Days Proceedings. 
Porto: Gabinete de Rela^oes Internacionais e Apoio ao Desenvolvimento Institucional. 
Castro, J. F. P. (2016d). ‘The contributions of Gilberto Freyre for the acculturation research’, 
The Portuguese Studies Review, 24, pp. 247-257. 

Castro, J. F. P. (2017a). ‘A short literature review about acculturation in the American An¬ 
thropologist’ RevistaFluxos eRiscos, 2, pp. 145-153. 

Castro, J. F. P. (2017b). ‘A review on the early The American Journal of Sociology’, Revista 
Latina de Sociologia, 7, pp.16-37. 

Castro, J. F. P. (2017c). Wenceslau de Moraes: Acculturation between ideals and life experi¬ 
ences. Religacion, Revista de Ciencias Sociales y Humanidades, 5, pp. 207-235. 

Castro, J. F. P. (2018a). ‘Lifelong education on Portuguese emigrants and their accultura¬ 
tion’ Sisyphus - Journal of Education, 6, pp. 97-119. 

Castro, J. F. P. (2018b). ‘Education on Portuguese emigrants and their acculturation’ E- 
Methodology, 4, pp. 55-74. 

Castro, J., and Rudmin, F. (2017). ‘Acculturation, Acculturative Change, and Assimilation: 
A Research Bibliography With URL Links’ Online Readings in Psychology and Culture, 
8 ( 1 ). 

Clastres, P. (1974). Lasociete contre I’e'tat. Paris: Les Editions de Minuit. 

Cohen, D. (2015). Le monde est clos etle de'sir infini. Paris: Editions Albin Michel. 
Comaroff, J., and Comaroff, J. (2009). Ethnicity, Inc: On indigeneity and its interpella¬ 
tions. Chicago: The University of Chicago Press. 

Coppens, Y. (2012). Le present du passe' au carre: La fabrication de la prehistoire. Paris: 
Odile Jacob. 

Dupront, A. (1997). Le my the de croisade, vol I. Paris: Gallimard. Originally published in 
1956. 

Elias, N. (2012). What is Sociology? Dublin: UCD Press. 

Elias, N., and Scotson, J. (1994/1965). The established and the outsiders: A sociological 
enquiry into community problems. London: Sage Publications. Eirst published in 1965. 
Ferreira, D., and Roeha, R. (2013). ‘A emigra^ao do distrito do Porto para o Brasil durante a 
1 RepiiblicaPortuguesa (1910-1926)’ inArrudaJ. J. A., FerliniV. L. A., Matos M. 1. S., &amp; Sou¬ 
sa F. (Eds.), De colonos a imigrantes: l(E)migragdo portuguesa para o Brasil Sao Paulo: 
Alameda, pp. 165-187. 

Festinger, L. (1962). ‘Cognitive dissonance’. Scientific American, 207, pp. 93-107. 
Foueault, M. (1980). Power/Knowledge. Brighton: Harvester. 

Freud, A. (1936). The ego and the mechanisms of defense. New York: Int. Universities Press. 
Freud, S. (1919/1913). Totem and Taboo: Resemblances between the psychic lives of sav¬ 
ages and neurotics. New York: Moffat Yard and Company. 

Freyre, G. (1986/1933). The masters and the slaves: A study in the development of Brazil¬ 
ian civilization. Berkeley: University of California Press. 

Girard, R. (1972). La violence et le sucre. Paris: Bernard Grasset. 


VI. OUTROS 


247 


Goffman, E. (1959). The presentation of self in everyday life. New York: Anchor Books. 
Goody, J. (2006). The theft of History. Cambridge, UK: Cambridge University Press. 
Habermas, J. (2002). L’avenir de lanature humaine Vers un eugenisme liberaU Paris: Gal- 
limard. 

Hobsbawm, E. J. (1995). The age of extremes: The short twentieth century, 1914-1991. 
London: Abacus. 

Klein, M. (1964). Contributions to psychoanalysis: 1920-1945. New York: McGraw-Hill. 
Kline, L. W. (1898). ‘The migratory impulse vs. love of home’. The American Journal of 
Psychology, 10, pp. 1-81. 

Knepper, P. (2010). The invention of international crime: A global issue in the making, 
1881-1914. New York: Palgrave Macmillan. 

Latour, B. (2015). Face a Gaia. Huit conferences sur le nouveau regime climatique. Paris: 
La Decouverte. 

Llano, A. (1900). ‘Race preservation dogma’. The American Journal of Sociology, 5, pp. 
488-505. 

Malinowski, B. (1958). The dynamics of cultural change: An inquiry into race relations in 
Africa. New Haven: Yale University Press. Originally published in 1945. 

Mendes, J. A. M. (1988). ‘A emigra^ao portuguesa, nas opticas de Alexandre Herculano, 
Oliveira Martins e Afonso Costa’, Revista Portuguesa de Historia, XXIV, pp. 293-308. 
Miller, H. A. (1924). Races, nations and classes: The psychology of domination and free¬ 
dom. Philadelphia: Lippincott. 

Morin, E. (2005). Introduction a lapense'e complexe. Paris: Editions du Seuil. 

Munoz, M. C. (l99l). ‘Les relations franco-portugaises de 1916 a 1918’, Hommes &amp; Migra¬ 
tions, 1148, pp. 15-18. 

Navas, M., Gareia, M. C., Sanehez, J., Rojas, A. J., Pumares, P, and Eernandez, J. S. (2005). 
‘Relative acculturation extended model (RAEM): New contributions with regard to the 
study of acculturation’ International Journal of Inter cultural Relations, 29, pp. 21-37. 
Ortiz, E. (1995). Cuban counterpoint: Tobacco and sugar. Durham and London: Duke Uni¬ 
versity Press. Originally published in 1940. 

Park, R. (1928). ‘Human migration and the marginal man’. The American Journal of Soci¬ 
ology, 33, pp. 881-893. 

Peeoud, A. (2015). Depoliticizing migration: Global governance and international migra¬ 
tion narratives. New York: Palgrave Macmillan. 

Pereira, M. H. (2013). ‘A emergencia do conceito de emigrante e a politica de emigra^ao’ 
ini. 1. A. Arruda., V. L. A. Eeruni., M. A. 1. S. Matos., &amp; E. Sousa (Orgs.), De colonos a imi- 
grantes. l(E)migragao portuguesa para o Brasil. Sao Paulo: Alameda, pp. 37-45. 

Phinney, J., and Ong, A. (2007). ‘Conceptualization and measurement of ethnic identity: 
Current status and future directions’ Journal of Counseling Psychology, 54, pp. 271-281. 
Pinto, E. M. (1989/1614). The travels of Mendes Pinto (R. Catz, Ed.). Chicago: The Univer¬ 
sity of Chicago Press. Originally published in 1614. 

Portes, A, Eernandez-Kelly, P, and Haller, W. (2005). ‘Segmented assimilation on the 
ground: The new second generation in early adulthood’ Ethnic and Racial Studies, 28, pp. 
1000-1040. 

Powell, J. (I88O). Introduction to the study of Indian languages: With words phrases and 
sentences to be collected. Washington: Government Printing Ofhce. 


248 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Redheld, R., Linton, R., and Herskovits, M. (1936). ‘Memorandum for the study of accul¬ 
turation’, American Anthropologist, 38, pp. 149-152. 

Rocha-Trindade, M. B. (1987). ‘As micropatrias do interior portugues’, Andlise Social, 
XXIII, pp. 721-732. 

Rudmin, F. W. (2009). ‘Constructs, measurements and models of acculturation and accul- 
turative stress’. International lournal of Inter cultural Relations, 33, pp. 106-123. 
Rudmin, F., Wang, B., and Castro, J. (2016). ‘Acculturation research critiques and alterna¬ 
tive research designs’ in Schwartz S. J. and Unger J. B. (Eds.), Handbook of acculturation 
and health. Oxford: Oxford University Press, pp. 75-95. 

Scott, J. (2009). The art of not being governed: An anarchist history of upland Southeast 
Asia. New Haven and London: Yale University Press. 

Sedikides, C., Wildschut, T., Gaertner, L., Routledge, C., and Arndt, J. (2008). ‘Nostalgia 
as enahler of self continuity’ in Sani F. (Ed.), Self-continuity: Individual and collective 
perspectives. New York: Psychology Press, pp. 227-239. 

Simons, S. (l90l). ‘Social assimilation, 1’ American lournal of Sociology, 6, pp. 790-822. 
Taft, D. R. (1969/1923). Two Portuguese communities in New England. New York: Arno 
Press. Originally Published in 1923. 

Thomas, W, and Znaniecki, F. (1918). The Polish peasant in Europe and America. New 
York: Dover. 

Todd, E. (1994). Le destin des immigres assimilation et segregation: Dans les democraties 
occidentales. Paris: Editions du Seuil. 

Triandis, H. C. (1972). The analysis of subjective culture. New York, US: Wiley-lntersci- 
ence. 

United Nations (2015/1948). Universal Declaration of Human Rights. New York, United 
Nations. Originally Published in 1948. 

Vassiliou, V., Triandis, H. C., Vassiliou, G., and McGuire, H. (1972). ‘interpersonal contact 
and stereotyping’ in Triandis H. C. (Ed.), The analysis of subjective culture. New York, US: 
Wiley-lnterscience, pp. 89-116. 

Westphal-Hellbusch, S. (1959). ‘Trends in Anthropology: The present situation of ethno¬ 
logical research in Germany’. American Anthropologist, 61, pp. 848-874. 

Wihtol, de W. C. (2013). La question migratoire au XXIeme siecle. Migrants, refugies et 
relations internationales. Paris: Presses de Sciences Po. 

Winthrop, R. (l99l). Dictionary of concepts in cultural Anthropology. New York: Greeen- 
wood Press. 

Zinn, H. (1994). A people’s history of the United States. New York: Longman. 


VI. OUTROS 


249 



La chute de I’Empire ottoman et ses 
consequences pour le Proche-Orient 


Mehdi Jendoubi 

Estudante de Ciencia Politica e Relacjoes Internacionais 
FCHS/ Universidade Fernando Pessoa 


Vers rOrient complique, je volais avec des idees simples. Je savais que; an 
milieu de facteurs enchevetres, une partie essentielle s’y jouait. Il fallait done 
enetre. (Charles de Gaulle) 


Resume: Pour la commemoration du centenaire de la fm de la Premiere Guerre mondiale, 
plusieurs retrospectives ont mis I’accent sur le chemin parcouru par I’Europe qui, en un 
siecle, est passee des rivalites imperiales et des nationalismes exarcerbes a la reconciliation 
et I’unification du continent. Get accomplissement ne s’est pas fait sans heurts, le second 
conflit mondial, encore plus devastateur que le premier, en est la preuve eclatante. Si I’ori- 
gine de la seconde guerre mondiale trouve son origine dans le declenchement des hostilites 
en 1914, les guerres qui embrasent le Proche-Orient moderne y trouvent egalement leur 
source. En effet, en une decennie, cette region du monde, dominee et administree par les 
Ottomans pendant quatre siecles, au sein d’un empire multi-ethnique et multi-confes- 
sionnel, est devenue une mosaique heterogene de nations fondees sur des appartenances 
linguistiques, religieuses ou tribales. Du demembrement de I’Empire Ottoman a I’aboli- 
tion du Califat Islamique, en passant par les accords Sykes-Picot et la declaration Balfour, 
cette periode est un tournant majeur pour ce carrefour civilisationnel, point de rencontre 
de I’Europe, de I’Afrique et de I’Asie. Au moment ou se fragilisent les entites creees il y a 
tout juste cent ans, la perspective d’un nouveau redecoupage frontalier est, de nouveau, 
discutee pour tenter de repondre aux multiples crises qui secouent ce foyer d’instabilite 
qui constitue I’epicentre des tensions internationales actuelles. 11 est done plus que jamais 
necessaire de revenir sur cette periode charniere de la «question d’Orient». 

Mots-cles: Empire ottoman; Proche-Orient; nationalisme; Premiere guerre mondiale. 

Abstract: For the commemoration of the centenary of the end of the First World War, sev¬ 
eral retrospectives have focused on the path done by Europe which, in a century, has gone 
from imperial rivalries and exacerbated nationalisms to reconciliation and the uniheation 
of the continent. This achievement has not been done smoothly, the Second World War, 
even more devastating than the First, is the obvious proof of this. While the origin of the 
Second World War originated in the outbreak of hostilities in 1914, the wars that embrace 
the modern Near East also hnd their source there. Indeed, in a decade, this region of the 
world, dominated and administered by the Ottomans for four centuries, in a multi-ethnic 
and multi-confessional empire, has become a heterogeneous mosaic of nations based on 


VI. OUTROS 


251 


linguistic, religious or tribal affiliations. From the dismemberment of the Ottoman Empire 
to the abolition of the Islamic caliphate, through the Sykes-Picot accords and the Balfour 
declaration, this period is a major milestone for this civilizational crossroads, a meeting 
point for Europe, Africa and Asia. At a time when the entities created just a hundred years 
ago are getting weaker, the perspective of a new border design is again being discussed in 
an attempt to respond to the many crises that are shaking this area of instability that is 
the epicenter of current international tensions, it is therefore more than ever necessary to 
return to this crucial period of the “question d’Orient “. 

Keywords: Ottoman Empire; Middle East; nationalism; World War 1. 


252 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


Introduction 


II est inutile de repeter ce que des milliers d’auteurs ont eerit depuis un siecle, a savoir que 
la Grande Guerre fut un eataclysme eomme rhumanite n’en avait jamais connu. 

Chaque commemoration, au-dela de son noble recueillement et de sa pedagogic propre, 
nous obligent a certaines figures rhetoriques, morales et politiques. 

Posant un regard erudit sur la ‘Der des Der’, depuis sa bibliotheque d’archives, d’ou- 
vrages et d’analyses, I’historien et I’etudiant ont peut-etre dans ce contexte, un role par- 
ticulier. Parmi les angles d’approche, les innombrables batailles et acteurs, ainsi que le 
flot de donnees qui menacent de nous engloutir, il reste des questions problematiques peu 
traitees qu’il est opportun de circonscrire, notamment sur ‘la guerre extra - europeenne ’. 

Le citoyen curieux et I’eleve europeen ne connaissent que trop cette epithete de ‘mon¬ 
diale’ accole aux deux grands conflits du XX'= siecle. Si pour le conflit de 1939-45, cette 
mondialite est une evidence palpable, ne serait-ce que par I’ampleur des operations na- 
vales sur les cinq oceans, pour celui qui nous occupe, on observe une plus grande circons- 
pection. En effet, le centrage sur les fronts europeens ecrase la bibliographic ainsi que la 
memoire collective. Ce phenomene s’amplifie demesurement lorsque Ton se penche sur 
I’inegale ferveur des commemorations au travers des pays engages dans la guerre. L’ob- 
servateur attentif notera meme une quasi absence de ceremonies officielles, dans une large 
patrie du monde dechiree par la guerre. 

Au premier rang de cette amnesic memorielle, on trouve le monde arabe, islamique et 
moyen oriental. Ce conflit decrit souvent eomme une sorte de guerre civile europeenne, 
pent egalement se definir eomme une gigantesque operation de liquidation des empires 
‘centraux’, et dans le meme mouvement, une competition coloniale sans egal. Depuis le 
milieu du XlXe s., soit bien avant le debut des hostilites, les appetits s’aiguisaient autour 
de I’agonisant ‘Homme malade de I’Europe’. Contrairement a une vision teleologique fort 
repandue, I’engagement de I’empire Ottoman aux cotes de I’Allemagne ne fut ni evident, 
ni enthousiaste. Croulant sous les dettes, perclus de lutes internes et affaibli dans son 
prestige, la Sublime porte perdait entre 1911 et 1913 sa derniere possession nord-africaine 
ainsi que I’integralite de ses riches provinces europeennes. La combinaison du choc de 
la Grande Guerre, des velleites nationalitaires, reves ininterromp us de tous les peuples, 
ainsi que les manceuvres habiles des ‘colonialismes’, achevera de desintegrer un empire 
hegemonique tri-continental. 11 est parfois necessaire de se rappeler, que I’epoque dont 
il est question ne connaissait ni la radio, ni la television, et que les nouvelles de I’orient 
parvenaient aux publics europeens non-inities, avec retard, lacunes, et disons-le embuees 
du halo des legendes. 

De grands soldats fran^ais, Bugeaud d’abord et Lyautey ensuite parmi les plus eminents, 
avaient cependant amend en Erance un parfum d’exotisme colonial et presente aux fran- 
9 ais une maniere de marier le destin de I’empire qu’ils representaient, a celui des ‘indi¬ 
genes’ d’Afrique du nord et du Levant. C’est d’un autre empire, plus maritime celui-la, et 
obsede par la route des Indes que naitra un autre aventurier-soldat, certainement une des 
figures les plus romantiques de la Grande Guerre, T.E Lawrence plus connu sous le nom de 
‘Lawrence d’Arabic’. Son surnom offre quasiment une description du programme poli¬ 
tique qu’il defendra passionnement pour le compte de I’empire britannique. Si on ne pent 
surestimer le role de la revolte arabe menee par ‘Lawrence d’Arabie’ sur Tissue du conflit. 


VI. OUTROS 


253 


il est difficile de nier son impact au cceur des tempetes ‘ nationalistes’ qui enflammeront la 
region apres la guerre. 

Si certains ehapitres de cette sequenee historique sont bien connus, la choregraphie 
d’ensemble des ambitions et des luttes est pen lisible, sans se peneher avec soin sur les 
principaux acteurs et leurs turpitudes. A I’image du trop fameux accord Sykes-Picot qui il- 
lustrera parfaitement la competition franco-anglaise. De meme, la penetration allemande 
fulgurante dans les affaires Ottomanes aura son importance crueiale; s’y melangeront les 
tentatives desesperees de I’empire pour sauver ee qui peut I’etre, sans oublier les aspi¬ 
rations autonomistes des peoples de la region ainsi que I’hostilite conquerante tsariste. 
On retrouvera cette dialectique des ambitions coloniales et des questions nationalitaires 
aux marges des grands empires, pendant tons le XIXe siecle. Ces questions s’y exprime- 
ront d’une maniere souvent plus violente que dans les Etats-nations plus aneiennement 
formes. Paradoxalement les nationalismes arabes, ne seront que les derniers de la longue 
procession des revoltes, qui fragiliseront puis acheveront de dissoudre I’Empire a Tis¬ 
sue de la guerre. La raison de Tetude historique nous conduit cependant a relativiser les 
mythes fondateurs des nations en question, si brillamment ehantes par les livres scolaires. 
Dans une region sans eesse agitee depuis la bn de la premiere guerre, il plus que jamais 
utile d’eclairer la naissance chaotique des nations qui oceupent la Une des aetualites au- 
jourd’hui encore. Comment est done nee la eonscience nationale dans les provinees arabes 
de Tempire Ottoman en pleine decomposition? Et quels ont ete les ferments d’autonomi- 
sation de ees peuples, qui ont conduit apres la guerre a cette mosaique d’Etats convulsifs 
plus ou moins instables? 

Pour mener cette reflexion nous observerons Tetat de TEmpire et de ses provinees a 
la vieille du eonflit. Nous analyserons ensuite le eheminement de la Sublime Porte dans 
la guerre avant d’etudier les conditions de son demembrement par les puissanees vie- 
torieuses. Enbn, nous poserons un regard prudent sur les perspeetives presentes dans le 
Proche-Orient arabe. 

Le gouvernement imperiale Ottoman n’a pas attendu le regard impatient des puissances 
oecidentale pour laneer a partir de 1839, une vaste serie de reformes dont le flux sera inin- 
terrompu jusque 39; a la guerre. Le systeme imparfait mais fluide des millets, qui mettait 

au centre Tappartenance religieuse, sera mis en question a mesure que les idees nees de la 
revolution fran^aise s’infuseront dans les esprits et les eceurs. La fameuse revolution grec, 
premiere d’une longue serie reprendra les coneeptions romantiques d’un people soude par 
une langue et des traditions eommunes notamment religieuses. Si Tindependanee de la 
Grece negoeiee a Londres en 1822, est insignibante d’un point de vue territoriale, elle n’en 
sera pas moins le signal donne des insurreetions balkaniques. 

Ces perils renforceront la volonte les reformateurs a agir vite et fermement. 

L’apaisement de la situation pour un temps, suite aux traites de Londres en 1840, et qui 
reglent la question de TEgypte et des detroits, sera de courte duree. La situation bnaneiere 
catastrophique et done la pression bscale excessive dans les provinees de Tempire, favori- 
sera le cycle infernal de revokes, contre massaeres et deportations dans tons les Balkans. 
Sous pressions internes des troubles et ext ernes des puissances indignees, le Sultan dotera 
TEmpire d’une eonstitution et d’un parlement en 1876. 

Celle-ci eonbrmera Tegalite des sujets deja mentionnee en 1839. 


254 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Le chapelet sanglant des guerres russo-turques qui se conclura par la defaite ottomane 
de 1878, fera definitivement passer les Balkans dans le giron de I’empire Russe. Meme si 
les traites de San Stefano et de Berlin la meme annee, parlent d’autonomie et non d’inde- 
pendance, La Serbie, la Bosnie, la Roumanie et la Bulgarie, seront definitivement perdues 
pour le Sultan. Mentionnons egalement une partie des territoires peuples de Georgiens et 
d’Armeniens, octroyee directement au Tsar. On assiste alors au debut de I’ere moderne des 
refugies, environ 100000 musulmans seront chasses de ces territoires. 

Le Sultan humilie referme alors la parenthese liberale, 11 tentera d’agir en despote eclaire 
en poursuivant les reformes dans I’armee, la sante et reduction. En 1888 I’Orient Express 
reliant Paris Vienne et Istanbul, amenera un vent de modernite occidental sans precedent. 
On parlera d’une Belle epoque stambouliote. Depuis 1881,1’empire avail perdu le controle 
de sa dette publique et done de son autonomic bscale, passee aux mains des banques de 
Londres, Paris Berlin et Vienne. Le regime historique des ‘Capitulations’ offrait une im- 
munite totale pour les investisseurs etrangers, ce privilege presque colonial permis aux 
fran^ais et aux anglais de monopoliser la quasi-totalite des secteurs bancaires, miniers, 
ferroviaires et pour la Prance d’installer un reseau educatif influent. La perte de la Tunisie 
passee sous tutelle fran^aise en 1881 et de I’Egypte I’annee suivante devenue colonie an- 
glaise, s’inscrit egalement dans cette competition imperiale, qui verra bientot I’AHemagne 
entrer la course aux marches. 

En 1889 le Kaiser fera une tournee triomphale, representant un empire au C.V sans 
tache, absent du ‘Grand leu ’jusqu’ici, le projet d’une ligne ferroviaire Berlin-Bagdad est 
alors enterinee. Sur le plan interieur, Le Sultan decide de se recentrer sur le Califat et de 
faqonner un grand empire musulman, son statut de commandeur des croyants le conduit 
a promouvoir une elite arabe dans I’armee et 1’administration. A cette date, les leaders 
arabes preferent le joug familier et islamique de I’empire, et sont conscient du risque colo¬ 
nial europeen, qu’ils jugent avec crainte et dedain. 

Le reveil national armenien ecrase dans la revolte de 1894, marquera une rupture dans 
I’Histoire des massacres par son envergure et ses consequences, la mort de 200000 arme¬ 
niens sur une population d’un million et demi, provoquera une indignation universelle. 
Les armeniens qui avaient soutenus massivement les Tanzimats seront desormais comme 
on pent rimaginer, d’une hostilite irreductible a I’egard de I’empire. 

Ce message sanglant assombrira les relations des ottomans avec ses minorites desormais 
mebantes et ebrayees. 

Dans ce contexte trouble, emergera a partir de 1889 un mouvement, issu de I’essaim 
des societes secretes qui s’agitent dans toutes les provinces. Les leunes Turques (Comite 
Union et Progres), sont un groupe d’etudiants en medecine militaire qui s’agregeront sur 
le modele des ‘carbonari’ italiens. Ceux-ci font partie de ce que Ton pourrait appeler la 
‘generation tanzimat’, farouchement liberaux, leur objectif central est le retablissement la 
constitution de 1876. Leur succes est foudroyant dans I’armee, Tadministration et chez les 
etudiants de Constantinople. L’embryon de presse qui prend son essor aidera ces agitateurs 
a deployer leurs idees revolutionnaires, au point d’inquieter la vieille garde du Sultan. 
Cette mosaique ethnico religieuse et rebelle, sera integree aux abaires par le gouvernement 
abn de les neutraliser, les plus virulents sont pousses a Pexile. Trois grands courants se 
degagent du premier congres de Paris en 1902, le premier positiviste et jacobin se distingue 
des liberaux decentralisateurs, et des conservateurs religieux. Le ralliement de certaines 


VI. OUTROS 


25S 


elites imperiales et les mutineries massives dans les armees balkaniques font basculer tout 
I’empire dans une ambiance pre revolutionnaire, dans la greve et dans la secession bscale. 

Le Sultan n’a pas d’autre choix que d’autoriser le retour des exiles et de retablir la consti¬ 
tution. Selection de 1908 qui suivra sera une victoire ecrasante du Comite Union et Pro- 
gres. Une contre revolution, tentee par I’entourage conservateur du Sultan en 1909, sera 
reprimee dans le sang par les Jeunes turques qui depose Abdulhamid 11 pour installer un 
fantoche sur le trone, son frere Mehmet V. Enver Pacha, ftgure emblematique des Jeunes 
Pure sera nomme attache militaire a Berlin, et subjugue alors par la puissance des armees 
allemande decidera par la suite de conber la gestion des armees ottomane a des ofhciers 
du Reich. Malgre cette ferme reprise en mains, I’empire continu de se deliter. Suite a de 
nouveaux revers militaires dans les Balkans qui consacrent le depart debnitif des ottomans 
du continent europeen en 1913, un nouveau coup d’etat verra I’avenement des trois Pacha 
Jeunes Turcs, Enver, Talaat et Djemal. Le triumvir du CUP met le parlement en vacances et 
regne desormais sans partage. La politique brutale de turquisation conduite par le gouver- 
nement verra en reaction naitre un antagonisme arabo-turque inevitable. Malgre toutes 
les tentatives modernistes de reformes, la structure ethnique, economique et sociale de 
I’empire pose une equation quasi insoluble. 

Les triumvirs durcissent alors leur politique, ce qui poussera les Arabes a I’image des 
peuples balkaniques a trouver leur propre chemin vers une conscience nationale eman- 
cipatrice. La debnition classique de la nation, forgee par la revolution fran^aise et reprise 
par Renan dans son fameux ouvrage ‘Qu’est-ce qu’une nation ne fut pas celle qui pris 
racine dans les empires. Tandis que la debnition fran^aise oriente I’appartenance sur un 
contractualisme, et une communaute de ‘destin’, la debnition plus romantique donnee 
entre autres par Eichte, s’appuie davantage sur les notions de langues, et de race, ainsi que 
de culture populaire commune. Une troisieme debnition qui adjoint a la debnition alle¬ 
mande I’appartenance religieuse et a un passe mythique, sera celle qui essaimera dans les 
empires, comme dans le cas de I’empire Ottoman. On devine alors les ferments de discorde 
que ces principes poseront. 

Le mouvement de renaissance culturelle arabe, la ‘Nahda’ tentera d’amenager ces prin¬ 
cipes aux problemes des peuples arabes. Ce mouvement fut theorise, aussi bien, par des 
Chretiens arabes que par musulmans reformistes, ce qui donne une coloration laique, en 
tout cas CECumenique a ce mouvement. L’unibcation par la langue et la culture sera por- 
tee par des families chretiennes et dont les obsessions educatives donneront naissance a 
une profonde reflexion sur le passe des grands empires arabo-islamiques du |Moyen-Age, 
ainsi que sur la sociologie des peuples. La religion islamique sera posee comme un facteur 
culturelle primordial mais qui doit etre separee du gouvernement civil. Le clerge est un 
gouvernement moral, et la forme ideale du califat souhaitee n’a aucun droit sur la gestion 
des affaires publiques. Un livre fondateur ecrit par Negib Azoury, un chretien maronite, 
‘Le reveil de la nation arabe dans I’Asie turque’ public en fran^ais en 1904 synthetise les 
orientations inspirees par la ‘Nahda’ au sein des elites arabes. Les reformateurs musulmans 
comme Rachid Rida tente de reconcilier cette vision panarabe avec le caractere panisla- 
mique dans des constructions syncretiques modernistes. 

Contrairement a une vision convenue, I’entree en guerre de I’empire ottoman aux cotes 
de I’Allemagne ne fut pas I’evidence qu’on a pu soup^onner. Son entree dans la premiere 


256 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


guerre fut hesitante, deja aux prises avec de nombreuses agitations interieures, la Sublime 
Porte ne se sentait pas vraiment concernee par cette guerre a venir. Elle avait cependant 
conscience du risque d’un debordement en sa defaveur en cas de conflit generalise. L’ar- 
mee etait en vole de la modernisation aux mains de hauts officiers allemands, ce tropisme 
germanique, s’il s’exprimait avec enthousiasme chez Enver Pacha etait loin d’etre una- 
nime dans I’Elite militaire ottomane. Meme si cette collaboration etait etroite, la Prance 
faisait de meme avec la gendarmerie. Ce sont plutot une cascade de facteurs meles, qui 
vont faire basculer la Porte du cote de I’Allemagne, meme si cette cooperation militaire 
en fait partie. C’est un empire affaibli, craintif et crible de dettes, qui fait le tour des chan¬ 
celleries afm d’obtenir des garantis. Pour I’ambitieuse Angleterre et la Prance 9 a sera un 
refus net, il en sera de meme pour son plus grand ennemi la Russie qui negocie en secret la 
possibilite d’internationaliser Constantinople et de se partager les Detroits avec les britan- 
niques. C’est un empire isole qui se tourne enfm vers I’Allemagne, qui par ailleurs offre les 
meilleurs garantis fmancieres et impressionne par ses qualites militaires. L’accord est tar- 
dif et secret en Aout 1914. Meme si I’engrenage des alliances est en train de faire son ceuvre, 
c’est a la faveur d’une escarmouche maritime, que les ottomans basculent defmitivement 
du cote allemand. En effet le Goeben et le Breslau, deux croiseurs allemands sont impliques 
dans une poursuite decisive politiquement mais insignifiante du point de vue militaire. 

La Porte sans illusions tenta de maintenir une neutralite dans un engagement que tout 
le monde imaginait de courte duree. Churchill cherchait un casus belli, tandis que les alle¬ 
mands entamaient des demarches de seduction de plus en plus fermes. Selon les sources, 
on pent neanmoins signaler que les motifs ottomans dans la guerre sont toujours debattus 
par les specialistes. 

Une fois la guerre declaree le 3 novembre 1914, soit bien apres le debut des hostilites 
en Europe en juillet, le bouillant ministre de la guerre Enver Pacha part dans le Caucase 
a la tete des meilleures unites ottomanes bien que sous equipees. L’assaut des territoires 
d’Anatolie centrale et du Caucase perdus lors des guerres de 1877-1878, etait soutenue par 
les allemands, et avait pour objectifs de couper la route des petroles de la mer caspienne 
d’une part, et d’y embourber I’armee russe deja engagee en Pologne et en Prusse orientale 
d’autre part. Enver pensait pouvoir soulever les musulmans de la region et nourrissait des 
fantasmes d’un vaste empire panturc vers I’Asie. Les pertes ottomanes seront enormes, 
notamment a la bataille de Sarikamis ou 80 000 soldats mourront geles, de faim ou maladie 
en decembre 1914. Meme si les campagnes du Caucase dureront jusqu’a la desertion Russe 
du front en 1917, on pent signaler qu’elles auront ete aussi feroces qu’inutiles, d’autant 
plus que c’est le ralliement massif des armeniens aupres du tsar qui servira de pretexte au 
genocide de 1915. 

La fameuse bataille de Gallipoli est consideree comme une des plus grandes victoires otto¬ 
manes de la 1 er guerre, ajoutons que les australiens la celebre davantage que le 11 novembre 
et voient en elle, la naissance de leur nation. Apres Pechec franco-britanniques du passage 
en force maritime le 18 mars 1915, Churchill propose un debarquement terrestre d’une en- 
vergure historique. A la suite du minage sous-marin du detroit et a la defense acharnee des 
troupes de Mustapha Kemal; le futur createur de la Turquie nouvelle; les combats s’enlisent 
et les pertes sont colossales des deux cotes. 


VI. OUTROS 


257 


Gallipoli peut egalement etre consideree comme le prototype d’une bataille opposant de 
grands empires par troupe ‘coloniales’ interposees. De nombreux arabes furent engages 
du cote ottoman face aux troupes franco-britanniques composees en partie d’algeriens, 
de pieds-noirs, d’australien et de neozelandais. L’objectif afhche par les britannique est de 
soulager les russes dans le Caucase et de prendre Constantinople avant eux. 

En 1916, en Mesopotamie ce sont les armees indiennes de la couronne britannique qui 
sont envoyes pour securiser les petroles Koweitiens, prendre possession des gisements 
de Bassora et avancer vers Bagdad. Malgre les difhcultes logistiques de tons ordres et un 
serieux revers a Ctesiphon, les ‘anglo-indiens’ parviennent a repousser les armees otto- 
manes, puis les chasser de Bagdad qu’ils occupaient depuis 8 siecles. 

Depuis le debut de la guerre, le panislamisme activiste germanique effraye les fran^ais et 
les anglais, inquiets pour leurs empires respectifs composes d’une forte population musul- 
mane. C’est done un ‘contre Djihad’, qu’il s’agit de provoquer dans la population arabe au 
Hedjaz. L’epopee Lawrencienne si elle a popularise cette revolte, n’en pas moins occultee 
qu’une ecrasante majorite des arabes restait bdeles a I’empire. Environ 300 000 arabes ont 
servi dans les armees ottomanes. Seuls quelques milliers suivront la rebellion du Cherif 
Elussein soutenu par les anglais, meme si on peut raisonnablement penser que le desastre 
des famines de des deuils dans la population arabe aura un impact important sur les choix 
d’allegeance, sans exclure la repression par la Porte de nombreux notables suspects de tra- 
hison. La bataille mineure d’Aqaba sera remportee par Eay^al, le bis de Hussein et futur roi 
d’lrak, face a une armee peu combattive. 

Meme si statiquement les effectifs ainsi que les pertes sont peu impressionnants, Fimpact 
de cette revolte sera notable sur Fapres-guerre et les espoirs des peuples. La campagne de 
Palestine qui suivra verra la prise de Jerusalem le 11 decembre 1917, puis la prise de Damas 
en septembre et d’Alep un mois plus tard. Cette serie de victoire fera naitre Fembryon 
d’un etat national arabe tant attendu, mais cette effervescence reveuse sera interrompue 
par Fexil du roi Eay^al apres le traite de Sanremo en 1920 et le debut du mandat fran^ais 
en Syrie. 

Les questions proprement diplomatiques de la premiere guerre mondiale en orient, 
sont surement Faspect le moins facile a decrire. Ces questions melent en effet, les objectifs 
avoues des grandes nations belligerantes, une multitude d’accord secrets et autant de pro¬ 
messes trahis. Au tournant de la guerre, Fempire britannique passe sur la question d’orient 
d’une gestion ‘Disraelienne’ de ses interets bases sur la sauvegarde des equilibres, a une 
politique plus audacieuse et conquerante a Fimage de celle proposee par Churchill. Si Fob- 
session de la route des Indes reste au centre de la politique de la couronne Britannique, a 
partir de 1911 et sous impulsion du lord de Famiraute Churhill, la precieuse marine anglaise 
fait desormais tourner ses moteurs au petrole et non plus au charbon. Les gisements soup- 
9 onnes ou decouverts dans tout le proche orient deviennent un enjeu vital. En outre, 11 est 
important de s’entendre sur le partage avec les allies en cas de victoire, et notamment de 
contenir les ambitions russes dans la region. La revolution bolchevique de 1917, viendra 
mettre un terme a cette inquietude et la posterite ira jusque 39; a effacer le troisieme signa- 
taire de Faccord Sykes-Picot-Sasonov. 

En mai 1915 le prince Eay^al rencontre des societes secretes arabes influentes, qui lui 
transmettent un document appele ‘protocole de Damas’. Celui-ci propose aux anglais un 
soutient contre les ottomans en echange de la creation d’un grand etat arabe unibe. Une 


258 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


correspondance est alors entamee entre le Cherif Hussein et Macmahon haut-commissaire 
britannique en Egypte, ces lettres ‘officialisent’ la promesse anglaise. 

Signalons le menagement des interets fran^ais dans le doeument, que I’accord Sykes-Pi- 
cot un an plus tard, viendra de toute fa^on reviser en ruinant I’espoir arabe. 

Ce fameux aecord de partage eolonial prevoit le deeoupage en zone d’influence anglaise 
et fran^aise, les provinces arabes de I’empire. Apres d’apres negociations, une limite entre 
les deux zones est tracee, elle deviendra I’enibleme des frontieres arbitrages pour la crea¬ 
tion des futurs etats. On voit bien que cet accord contredit les promesses faites a Hussein, 
et met en exergue la duplicite britannique et fran^aise. Notons egalement que I’internatio- 
nalisation de la Palestine prevue dans I’accord, celui-ci contredisant la ‘declaration Bal¬ 
four’ de novembre 1917, document sans entete, qui prevoit la creation d’un foyer national 
juif en Palestine. Le document stipule naivement, qu’il ne sera pas porte prejudice aux 
populations non juives de ces territoires. Remis dans son contexte, autant le mouvement 
sioniste ressemble aux mouvements d’emancipations des ‘nationalites’ du XlXe siecle, au¬ 
tant la declaration Balfour ressemble aux promesses faites par les belligerants aux minori- 
tes, afm d’en obtenir I’appui. 

La defaite ottomane ineluctable sera confirmee par I’armistice de Moudros signe entre le 
nouveau ministre de la marine ottomane et I’amiral britannique representant les allies vic- 
torieux, le triumvirat jeune Turcs ayant pris la fuite. Une flotte alliee entre dans le Bosphore 
quinze jours plus tard, I’occupation militaire s’installe sur tout le territoire a I’exception 
de la peninsule anatolienne. A la conference de Paris qui debute le 18 janvier 1919, et qui 
consacre la dissolution des trois grands empires defaits, et la creation de nombreux etats 
nouveaux, I’accord Sykes-Picot est quant a lui confirme sous I’egide de la SDN. 

Afm de regler le sort de I’empire, le nouveau Grand vizir Damas Lerid fut envoye a la 
conference de Paris, pour representer la Porte dans les negociations avec les allies. Mehmet 
VI nouveau le sultan etait pret a de larges concessions a condition de sauver ce qu’il pour- 
rait de son empire et de son pouvoir. Son argumentaire etait clair, rendre responsable les 
‘Jeunes Turcs’ de I’entree en guerre et des massacres, demander la restitution de quelques 
territoires indument enleves au cours de la guerre. Meme s’il etait favorable a la creation 
d’une grande Armenie independante, et particulierement attentif aux exigences alliees 
concernant les dettes de I’empire. 


VI. OUTROS 


259 


Bibliographic 

Barr, J. (2011). A line in the sand: Britain, Franee and the struggle that shaped the Middle 
East. Simon and Sehuster. 

Benoist-Mechin, J. (1954). Mustapha Kemal ou La Mort d&amp;#39;un empire. Alhin Michel. 
Benoist-Mechin, J. (1955). Ihn-Seoud ou la naissance d&amp;#39;un royaume. Alhin Michel. 
Benoist-Mechin, J. (1959). Un printemps arahe. Alhin Michel. 

College de France. Disponivel em https;//imuiu. coiiege-de-/rance./r/site/henrp - iaurens/ 
course-2015-2016.htm (acedido em 4 de mar^o 2018) 

Fromkin, D. (1989). A peace to end all peace: The fall of the Ottoman Empire and the crea¬ 
tion of the modern Middle East. Macmillan. 

Khoury, G. D. (2016). La France et l&amp;#39;Orient arahe: Naissance du Lihan moderne 1914- 
1920. Alhin Michel. 

La fin des Ottomans - Le Moyen-Orient en eclats. Disponlvel em 

https://boutique.arte.tv/detail/fin_ottomans_moyen_orient_en_eclats (acedido em 4 
de mar^o 2018) 

Les Cles Du Moyen Orient. Disponlvel em https://www.lesclesdumoyenorient.com 


260 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Amours suspendues: Correspondances de guerre 

Aurore Rouffelaers 

Office de Tourisme de Bethune-Bruay 


Resume: Les recherches actuellement conduites dans le cadre de la coordination des eve- 
nements du centenaire de la bataille de la Lys ont amene a la decouverte d’un fond de 
correspondance de plus de cinquante lettres et cartes postales entre des membres du Corps 
Expeditionnaire Portugais et leurs families. Ce fond provient de I’inventaire de la collec¬ 
tion de Mr. Da Silva Mai'a, historien et decede Fete dernier. Le fond propose en particulier 
la correspondance de deux soldats avec leurs epouses. Le fond n’ayant pas encore ete com- 
pletement exploite, 11 est envisageable que d’autres lettres soient decouvertes. D’autres 
part, dans le cadre de la collecte de temoignages et d’objets de descendants de soldats por¬ 
tugais ayant choisi de faire leur vie en France, 11 est probable que d’autres lettres inedites 
soient mises au jour. Entre 1914 et 1918, plus de 20 milliards de lettres ont ete echangees 
entre des soldats de 52 nationalites et leurs families. 

Mots-cles: Histoires familiales; Corps Expeditionnaire Portugais; Correspondances de 
guerre; Premiere guerre mondiale. 

Abstract: The research currently carried out in the framework of the coordination of the 
events of the Centenary of the Battle of La Lys led to the discovery of a hie of correspond¬ 
ence of more than fifty letters and postcards between members of the Portuguese Expedi¬ 
tionary Corps and their families. These hie sources belong to the inventory of Mr. Da Silva 
Maia’s collection, historian that deceased last summer. The hie deals with correspondence 
of two soldiers with their wives. As the hie source has not yet been fully exploited, it is 
conceivable that other letters will be discovered. On the other hand, as part of the collec¬ 
tion of testimonies and objects of descendants of Portuguese soldiers who chose living in 
France, it is likely that other unpublished letters will he brought to light. Between 1914 and 
1918, more than 20 billion letters w ere exchanged between soldiers of 52 nationalities and 
their families. 

Keywords: Family stories; Portuguese Expeditionary Corp; War Correspondences; World 
Warl. 


VI. OUTROS 


261 


Au cours des quatre annees de guerre, le front et I’arriere echangent plus de 10 milliards 
de lettres. Ces missives, souvent eourtes assurent le lien entre la vie reelle et la sublime 
horreur du front. Si le rythme d’une lettre par jour etait courant, eertains soldats ecrivent 
et resolvent deux lettres, voire plus. 


Genese 

L’exposition Amours Suspendues a ete possible grace a la precieuse collaboration de 
la mairie de Vielle- Chapelle, de I’offtce du tourisme de Bethune-Bruay et de la famille 
d’Afonso Da Silva Mala. L’historien, decede recemment a passe une bonne partie de sa vie 
a etudier Thistoire du CEP et a creer une importante collection proteiforme. Au cceur de 
la collection, un lot de 150 lettres et cartes postales totalement inedites. De formes multi¬ 
ples I’ensemble et constitue de lettres isolees mais aussi d’echanges entre un soldat et des 
membres de sa famille. L’etat de conservation absolument impeccable a permit de realiser 
des fac-similes a rechelle 1,5. Ce travail a ete aussi Foccasion de numeriser la collection 
et de demarrer un inventaire de la collection de Mr Mala. L’exposition a ete installee dans 
Fecole de Vieille-Chapelle dans le cadre de Fevenement mis en place par Fofhce du tou¬ 
risme Bethune-Bruay- Artois-Lys-Romane: Les Portugais dans la Grande Guerre. Elle est 
visible tous les jours de 14 a 18h du 7 avril au 6 mai 2018. 


Esthetique de rexposition 

La lettre est un message en mouvement. La metaphore du courrier volant de son emetteur 
vers son destinataire est une image souvent retenue dans Fimagerie populaire. 11 etait aus¬ 
si necessaire de creer une mise en scene particuliere liee aux doubles faces des supports. 
L idee de suspendre les missives est devenue peu a peu une evidence et faisait tout a fait 
sens eues-egards au titre de Fexposition. Le choix des rubans multicolores est lie a Fesprit 
du manage, du lien et des rubans que les femmes du temps portaient dans les cheveux. 11 
a egalement ete decide de suspendre des fleurs sechees; les soldats font en effet souvent 
parvenir des petits bouquets preleves sur le front. 


Mediation 

Une des limites de ce travail est liee a la langue, 11 a done ete necessaire de faire appel a 
des traducteurs. Une dizaine de benevoles ont oeuvre a la realisation de ce travail souvent 
difhcile a cause de la calligraphie, de la syntaxe et de Fabsence de punctuation. Des me- 
diateurs de Fofhce du tourisme ont ete formes et repondent ainsi au mieux aux questions 
des visiteurs. 


262 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


1. La correspondance de guerre 

Le couple dans la guerre 

Aussi etonnant que cela puisse paraitre a I’aune de notre societe, il est necessaire de rap- 
peler que le eouple du debut du XX ° est un eouple qui s’aime. En effet dans les siecles 
preeedents, le eouple etait avant tout un moyen de transmission des biens. Les mariages 
arranges donnaient parfois lieu a une histoire d’amour. Le XlXeme sieele et le debut du 
XXeme sieele font evoluer eette situation vers plus d’amour. L’usage strategique reste ee- 
pendant d’actualite en Lrance. 

Au Portugal, il est encore monnaie courante jusque dans les armees 60. Le Portugal d’avant 
guerre etant encore et sur un grand nombre de points un pays aux usages medievaux. 

De maniere globale, I’entree des nations dans la guerre voit une recrudescence du 
nombre de mariages. Les families hatent les unions et ainsi assurent un avenir materiel aux 
jeunes biles. 

La famille dans la guerre 

Envoye au front, le soldat, meme forme, ne pent imaginer la realite. Le champ de bataille 
n’est pas le monde reel, c’est la quintessence de I’horreur a echelle humaine. Difbcile alors 
pour les soldats de trouver des points d’encrages et de stabilites pour ne pas verser dans 
la folie. La lettre en est un. Le soldat se raccroche a la materialite du monde et a sa famille. 
Dans le cadre du lot exploite, il est a remarquer que la correspondance n’est pas exclusi- 
vement conjugale. Les families et amis se mobilisent et assurent la continuite du flot epis- 
tolaire. 

Du point de vue familial, les lettres sont aussi d’une grande importance. Leur premier but 
est de conbrmer la bonne sante du soldat. C’est aussi I’occasion pour ce dernier de temoi- 
gner a son echelle de son experience. Il convient d’ailleurs de nuancer immediatement 
eette occurrence car la censure veille en particulier au fait que les soldats ne livrent pas 
de donnees strategiques pouvant etre recuperees par I’ennemi. Il leur est ainsi interdit de 
preciser leur position geographique. 

Le courrier est la plupart du temps source de joie pour les families, mais aussi source 
d’angoisse, en particulier lorsque les facteurs distribuent les lettres emises par les etats 
majors et annon^ant, via une lettre type la mort d’un soldat. 


Fonctionnement de la poste en temps de guerre 

Un tel volume de courriers necessite une organisation importante. Les armees mettent 
done en place des moyens a la hauteur de I’enjeu. Il est a noter que la correspondance est 
gratuite, c’est La «franchise militaire». La collection de Mr Maia Presente d’ailleurs une 
majorite de courriers en port franc, des courriers sont cependant timbres. 


VI. OUTROS 


263 


Les lettres postees par les soldats ou les families, sont triees par secteurs postaux. Vers 
le front la chose est plus complexe car 11 est difficile d’identiher la position exacte des re¬ 
giments. 

Les families inscrivent les numeros et les regiments. Chaque regiment correspond a un 
QG Les sacs diriges ensuite sur des «bureaux frontieres», a la limite de la zone des armees, 
etaient transportes par des «ambulants d’armee» vers les «vaguemestres d’etapes» qui 
les repartissaient ensuite entre les «bureaux divisionnaires» ou s’effectuait le tri par re¬ 
giment. Les enveloppes etaient remises enbn aux vaguemestres des compagnies qui s’ef- 
for^aient, quelle que soit la situation, de les faire parvenir et assuraient, le cas echeant, la 
triste mission de renvoyer le courrier avec la mention : «le destinataire n’apu etre touche' 
d temps» (ce qui, en termes choisis, signibait qu’il etait mort, blesse, disparu ou prison- 
nier). 

Comment ecrire 

Du cote des civils cela parait simple, il sufbt de s’asseoir de prendre du papier et de I’encre 
et de commencer a evoquer la vie quotidienne. Cela ne Lest pas tant que 9 a en realite, les 
proches se doivent en effet d’assurer, de commenter le quotidien, de donner des nouvelles 
des proches mais tout en donnant une certaine distance a I’ensemble, il faut etre precis 
tout en allant a I’essentiel. De plus la redaction quasi quotidienne de missives releve parfois 
de la performance tant la vie banale et reguliere se doit d’etre mise en valeur. Les lettres 
et cartes postales prennent done la forme d’instantanes comme autant de mails, posts ou 
SMS. Cette demarche necessite chez les proches la mise en place d’un rituel d’ecriture le 
soir ou le matin. 11 est a remarquer que bon nombre de portugais de I’epoque ne savent ni 
lire ni ecrire, le flot est en ce cas plus reduit puisqu’il sous-entend la presence d’un tiers. 

Pour les soldats du CEP la redaction de missives n’est guerre plus aisee. 11 y a bien sur 
les memes difbcultes de lecture et d’ecriture. Mais au dela, il y a aussi un certain nombre 
de problemes materiels. En un premier temps le papier est une denree rare et difficile a 
proteger. La distribution de cartes postales facilitera ce probleme mais en creera un autre: 
la necessite de concision. Les cartes et lettres presentes dans le lot sont redigees toutes a 
I’encre et a la plume. La encore il est peu aise d’imaginer le soldat dans sa tranchee redi- 
geant en plein et delies, sans table ou chaise et au beau milieu de la boue. Les soldats vont 
rivaliser d’ingeniosites, le dos du camarade etant souvent mis a contribution. Une autre 
difficulte s’ajoute encore: les mouvements de troupes, lei aussi les soldats seront contraints 
de ritualiser les seances d’ecriture. L’image d’une tranchee silencieuse affairee a la lecture 
ou a I’ecriture deviendra alors un fait banal. 


2. La difficulte de preservation 

Nous venons d’evoquer la difficulte de produire; il faut lui ajouter la question de la conser¬ 
vation. Les conditions sur le champ de bataille sont loin d’y etre favorables ainsi la majeure 
partie des lettres disparait. Mais il faut aussi evoquer la question de la conservation dans 
le temps. Le soldat de retour chez lui et sa famille eprouvent t’ils le besoin de conserver 
des objets lies a une periode si complexe de I’existence. Les descendants ont t’il releves 


264 


A PRiMEIRA GUERRA MUNDIAL. NA BATALHA DE LA LYS 


le caractere precieux de ces echanges ? La collection reunie par Mr Mala est d’autant plus 
precieuse qu’elle presente un grand nombre de pieees mais aussi des echanges eomplets 
avee emetteurs et recepteurs. 

Les contenus 

• Cote famille 

Du point de vue des families, les lettres donnent des nouvelles des proches, et du deroule- 
ment de la vie quotidienne, des moissons, des animaux, des evenements financiers et des 
enfants. Sur ee dernier point, les epouses prennent un soin particulier a la deseription et 
a la description physique des enfants et a detailler les conditions d’education, la mere se 
substituant 

• Les dits et non-dits du soldat 

Le eontrole du courrier: Dans chaque sac un certain nombre de lettres sont prelevees pour 
analyse. Une fois par semaine, un rapport indique «le moral des troupes». 

Certains soldats utilisaient de codes pour eommuniquer avee les leurs en contournant le 
eontrole postal. D’autres redigeaient leurs courriers dans un patois parfois difheile a tra- 
duire pour les ofheiers responsables de la relecture. 

La eensure fut etablie des la mobilisation. Elle eoncernait le eourrier de tout militaire en 
campagne et nul n’etait cense y echapper. Le eontrole s’appliquait aussi de fa^on stricte a 
la eorrespondance allant en direction du front qu’a celle qui en provenait. Ofhciellement. 
11 s’agissait d’eviter la divulgation des mouvements de troupes. Les correspondanees pri- 
vees et eommereiales a destination de I’etranger etaient done toutes lues avant de franehir 
les frontieres et les telegrammes tres etroitement surveilles. 

En regie generale, les carnets de route rediges au ftl des jours, sans ce souci de eamoufler 
la realite, cement mieux la realite que les correspondanees. De plus le contraste entre le 
front et I’arriere etait si grand que deux mondes se cotoyaient sans se comprendre. 

• Les contenus du lot 

Le lot appartenant a la famille da Silva Mala est d’une remarquable qualite. Les eonditions 
d’acquisitions ne sont a ce jour pas connues. Mais les eonditions de eonservations furent 
optimales. L’ensemble se compose de cartes postales a illustrations variees et de lettres. La 
majorite des courriers est redige a I’encre noire, bleu turquoise ou violette. Les lettres sont 
contenues dans leurs enveloppes d’origine. 

Les textes sont de natures diverses. A ce jour tout n’a pas encore ete traduit. Sur ce point 11 
est a noter que certains contenus posent un probleme de dechiffrement. Les textes posent 
parfois des problemes de comprehension lies aux manques de ponctuation, et aux deb- 
ciences orthographiques et grammaticales. Enfm, certains passages presentent un vrai 
manque de coherenee interdisant toute interpretation. Ce dernier element pent etre lie 
au fait que les soldats ecrivaient une langue orale, que nous manquons du contexte et 


VI. OUTROS 


265 


qu’un alignement de mots sans liens entre-deux pent contribuer a constituer une «longue 
lettre» toujours bien accueillie par le destinataire. 

Les propos tenus dans les echanges vont de la simple earte de remereiement pour un 
anniversaire, une fete, ou un colls, a des descriptions a la maniere d’instantanes de scenes 
de la vie quotidienne ou d’evenements sur le front. Les propos sont aussi souvent lies aux 
questions ftnancieres. Les families donnent aussi des nouvelles des proches. Une lettre, en- 
voye par un frere evoque les conquetes amoureuses de ce dernier a Porto. 11 est egalement 
a noter, une mention particuliere «j’en ai marre de la France». Cette phrase a echappe aux 
services de la censure mais est a coup sur le reflet d’un etat d’esprit. 

Un groupe de lettres en cours de traduction attire particulierement I’attention: les cour- 
riers envoyes depuis la Grande Bretagne. L’iconographie des cartes relate en effet les diffe- 
rentes peregrinations de I’auteur dans le pays de Galles. 


266 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Bataille de La Lys - Exposition Racine: 
19 memoire vivantes 

Aurore Rouffelaers 

Office de Tourisme de Bethune-Bruay 


Resume: Cette exposition, a I’occasion du centenaire de la bataille de la Lys, promue par 
la ville de Richebourg et I’offtce du tourisme de Bethune Bruay Artois Lys Romane, repose 
sur les temoignages de descendants fran^ais de soldats du Corps Expeditionnaire Portu- 
gais. Au-dela du temoignage, I’exposition revet un caractere inedit. En effet 11 n’existe a 
ce jour aucune archive sur cette vague migratoire. Aussi, les archives constituees seront 
au terme de I’exposition donnees aux musees portugais et fran^ais de I’histoire des im¬ 
migrations. But de Fexposition: Valoriser les histoires individuelles; Rendre hommage et 
temoigner de la presence portugaise apres la guerre; Mettre en lumiere un pan meconnu de 
I’histoire regionale; Creer de Farchive et ainsi aider les futures generations de chercheurs. 
Les temoignages seront blmes, mis en lumiere le temps de Fexposition puis deposes sur 
internet via une base de donnees specialisee. En parallele des temoignages, Fexposition 
entend solliciter les families pour le pret d’objets relatifs a Fhistoire familiale. Utiliser Fob- 
jet comme vecteur de temoignage pent permettre d’assurer des ponts tangibles dans une 
histoire hautement vernaculaire, et ainsi de faire prendre conscience de Fimportance de 
ce mouvement dans Fhistoire migratoire fran^aise. Plus simplement, de mettre en lumiere 
des recits de vie consideres comme banal par Fentourage de ceux qui Fon vecut. Les objets 
attendus seront de diverses natures ; la seule exigence etant Fevocation de Fancetre portu¬ 
gais auquel il a appartenu. Cette exposition a Fambition d’engager une veritable demarche 
de conservation de patrimoine immateriel. 

Mots-cles: Exposition; Bethune Bruay; Histoires familiales; 1 Guerre mondiale. 

Abstract: The exhibition, on the occasion of the centenary of the Battle of La Lys, promoted 
by the town of Richebourg and the Tourist Office of Bethune Bruay Artois Lys Romane, is 
based on the testimonies of Erench descendants of soldiers of the Portuguese Expedition¬ 
ary Corps. Beyond the testimony, the exhibition is of an unprecedented character. Indeed, 
there is no archive to date on this wave of migration. The archives constituted will be at the 
end of the exhibition given to the Portuguese and Erench museums in the History of im¬ 
migration. Purpose of the exhibition: Valuing Individual stories; Aknowledge and testify 
the Portuguese presence after the war; Highlighting a forgotten aspect of regional history; 
Create the archive and help the future generations of researchers. The testimonies will be 
filmed, showed during the time of the exhibition and then deposited on the Internet via a 
specialized database. In parallel to the testimonies, the exhibition intends to ask families 
to loan objects relating to family history. Using the object as a means of testimony can 
ensure tangible bridges in a highly vernacular history, and thus raise awareness on the 
importance of this movement in Erench migratory history. Or, put simply, to highlight 
life stories considered as casual by the persons around of those who lived the events. The 


VI. OUTROS 


267 


expected objects will be of various natures; the only requirement being the evocation of 
the Portuguese ancestor to which it belonged. This exhibition has the ambition to embark 
on a genuine approach to the conservation of an intangible heritage. 

Keywords: Exhibition; Bethune Bruay; Family stories; World War 1. 


268 


A PRIMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Quand les planetes s’alignent 


En quoi le fait d’avoir un ancetre portugais fait de vous un fran^ais different. Voici en une 
phrase la problematique de I’exposition Racines, presentee a Richebourg du 7 avril an 6 mai 
2018, dans le cadre de la programmation culturelle mise en place par les communes et I’of- 
bce du tourisme de Bethune-Bruay. L’ofbce du tourisme a, depuis le 2014 la volonte d’axer 
ses actions culturelles sur Thumain et a travers les histoires et les parcours, commemorer 
le sacribce de ces milliers de soldats tombes sur le territoire pendant la grande guerre. 
Au cours des annees precedentes d’autre actions ont ete menees notamment autours des 
indiens et de la bataille de Neuve-Chapelle et de la presence Canadienne. Le centenaire de 
la bataille de la lys etait done un moment propice a la creation d’un nouvel evenement. 
L’exposition racine est nee de deux temoignages. En 2017, madame Brigitte Dubois entre 
en relation avec la ville de Richebourg et I’Ofbce du tourisme pour faire part de son his- 
toire. Elle est la petite bile d’un soldat portugais et a entame des recherches pour localiser 
sa tombe et sa famille. De cette aventure est ne un recit. Parallelement Anne Moitel, chef 
du pole patrimoine a I’ofbce du tourisme de Bethune-Bruay rencontre Aurore Rouffelaers 
vice-presidente de la Liga dos combatentes section de Lillers et environs, et par ailleurs 
coordinatrice regionale des evenements du centenaire aupres du consul honoraire des 
Hauts-de-Erance. Apres echanges avec la ville de Richebourg, il est decide que la ville 
accueillera une exposition sur les soldats portugais ayant epouse des francjaises et leurs 
descendants. Le commissariat est conbe a Aurore Roubelaers. 


La methode 

Des le debut de la mission, il apparait que le sujetn’a jamais etc traite. Aussi par dela la pro¬ 
grammation, il s’agit ici de produire de I’archive. Conduire un tel projet necessite d’autre 
part une matiere abondante. 11 faut aller a la rencontre des temoins. En effet, I’exposi- 
tion s’appuie sur une memoire vivante qu’il est urgent de repertorier. Le precede choisi 
est I’appel a contribution, une collecte de temoignages est done programmee. Le service 
de communication de I’ofbce du tourisme est done mobilise, ainsi que divers media. Sur 
ce point, I’objectif principal n’etait pas de recevoir des appels volontaires mais de faire 
connaitre la demarche et de faciliter la prise de contact. Abn de limiter les refus. 


Les canaux de collecte 

En parallele de I’operation mediatique, un travail de recherche commence. Une partie des 
recherches se basent sur les informations donnees par Eelicia Assun^ao Pailleux, bile du 
soldat Joao Assun^ao. Les souvenirs de Eelicia furent une precieuse ressource. Un relais a 
aussi ete fait a I’interieur de la communaute portugaise et Luso-descendante du secteur. Ce 
canal est de loin le plus efbcace et a permit la collecte de If temoignages. 

L’examen des registres de I’etat civil et des recensements mis en ligne par les archives 
depart ementales du Pas-de-Calais ont permit de recenser des manages franco- portugais. 
11 a ete decide de limiter la recherche aux manages celebres entre 1918 et 1935. Ce travail 


VI. OUTROS 


269 


est, a ce jour, toujours en cours. A partir des donnees collectees, une genealogiste, Sylvie 
Barlet, a reussi a localiser quelques descendants dont un seul, a ce jour a accepter de te- 
moigner. 

Le troisieme vecteur d’information, et sans conteste le plus ingrat fut I’examen des pages 
blanches en y relevant systematiquement tons les noms a consonance portugaise. Plus de 
300 appels on ete emis et on permit d’obtenir 6 temoignages 

Deux temoins se sont manifestes de fa^on spontanee dont un via facebook. 

La mediatisation de I’evenement a entraine cinq nouveaux temoignages spontanes. 


Les rencontres 

Abn d’obtenir un faisceau coherent et exploitable, un questionnaire base sur I’experience 
du commissaire (luso descendante) a ete realise. Au cours des trois premieres interviews, 
de nouvelles thematiques sont apparues et ont ete ajoutees. Par la suite le questionnaire n’a 
plus change mais chaque temoin a ete libre d’ajouter des commentaires. 

Chaque premier contact a vu naitre une recurrence, celle d’avoir I’impression de n’avoir 
rien a dire, c’est une impression de normalite et de banalite de I’histoire familiale que j’ai 
moi-meme autrefois ressenti. 

Les interviews ont dure de 45 minutes a trois heures et furent orientes via un question¬ 
naire. Le questionnaire aborde differentes thematiques autour de la vie quotidienne, des 
usages specibques lies a un ancetre d’origine portugaise en France, la religion, les habitu¬ 
des alimentaires, le racisme, les engagements mais aussi I’histoire de la vie et des echanges 
avec les autres. 

Chaque entretient a ete enregistre de fa^on a retranscrire le temoignage de la fa^on la 
plus bdele possible. 


Les temoins 

Felicia Assu^ao ^ Joao Assun^ao et Antero Soares 
Paulette Senechal ^ Joao Assun^ao 
Collette et Johanes da Costa Prudentio da costa 
Colette Six ^ Arthur Gomes 

Patrick Ratto et Jeanne marie Lemoine Alvaro Ratto 

Michel et Gerard Bento Soares ^ Garlos Bento Soares 

Michel D’almeida ^ Jose de Almeida 

Nicole de Andrade ^ Joaquim de Andrade 

Rene et de Moraes ^ Jean de Moraes 

Ginette dekens ^ Jose Simao 

Jose Pinto Jose Pinto 

Brigitte dubois ^ Jose Parracho 

Desire Diaz Antonio Dias 

Yvette Gombert ^ Manuel Lopes 

Mathieu fontaine mateus leira 


270 


A PRiMEIRA GUERRA MUNDIAL. NA BATALUA DE LA LYS 


Frederic Dumoulin ^ soldat inconnu 
Beatrice Lecocq ^ Narcisse Maia 


Le traitement de I’information 

Chaque temoignage a donne lieu a deux biographies. Si le sujet est evoque, la biographic du 
temoin prend en compte son regard sur son ancetre. La retranscription s’est attachee a un 
rapport strict des propos des temoins sans tentatives d’analyse. Les textes ont ete rediges 
par aurore Rouffelaers a I’exception du portrait de Felicia Assun^ao, redige par Isabelle 
Mastin, Journaliste a ma Voix du Nord, et le portrait de Flelena Briche, redige par Mathieu 
Fontaine, son arriere petit Ms. 

Les differentes interviews on d’autre part fait emerger des thematiques se pretant a ana¬ 
lyse. L’exposition propose d’un traiter trois: 

- La protuguitude; 

- L’integration face au racisme; 

- L’engagement au cours de la seconde guerre mondiale. 

L’espace et le temps n’ont pas permis de traiter d’autres thematiques pour le moment. 
Mais il est envisage de developper des themes supplementaires: la raison du non retour, 
la specificite du lien familial, les demarches familiales des descendants, le passe militaire. 


VI. OUTROS 


271 






c ^ 
&lt; ^ 
\A 


^ &lt; 

is 

n 


''"fFERl*"'^ 


UN^/ERSmAD^^I 

FERNANDO PES50A 


WWW.UFP.PT~~| 



InstitutG 

da Defesa Nacional 


</pre>        </div><!--/.container-->
              </main>
    </div><!--/#wrap-->
    
    
    <!-- Timing ...
     rendered on: www30.us.archive.org
 seconds diff sec               message   stack(file:line:function)
=========================================================
  0.0000   0.0000         petabox start   var/cache/petabox/petabox/www/sf/download.php:1:require
                                             |common/ia:66:require_once
                                             |setup.php:384:log
  0.0062   0.0062      redis_read start   var/cache/petabox/petabox/www/sf/download.php:80:main_wrap
                                             |download.php:101:main
                                             |download.php:322:getItem
                                             |common/Item.inc:77:parseMetadata
                                             |Item.inc:132:get_obj
                                             |Metadata.inc:538:_get_obj
                                             |Metadata.inc:568:run_pipeline
                                             |Metadata.inc:1433:read
                                             |Metadata/AdapterPipeline.inc:259:_read
                                             |AdapterPipeline.inc:292:pipeline
                                             |AdapterPipeline.inc:377:fetch
                                             |AdapterPipeline.inc:613:fetch
                                             |RecordServer.inc:144:execute
                                             |RecordServer/FetchRecordOp.inc:52:log
  0.0076   0.0014     redis_read finish   var/cache/petabox/petabox/www/sf/download.php:80:main_wrap
                                             |download.php:101:main
                                             |download.php:322:getItem
                                             |common/Item.inc:77:parseMetadata
                                             |Item.inc:132:get_obj
                                             |Metadata.inc:538:_get_obj
                                             |Metadata.inc:568:run_pipeline
                                             |Metadata.inc:1433:read
                                             |Metadata/AdapterPipeline.inc:259:_read
                                             |AdapterPipeline.inc:292:pipeline
                                             |AdapterPipeline.inc:377:fetch
                                             |AdapterPipeline.inc:613:fetch
                                             |RecordServer.inc:144:execute
                                             |RecordServer/FetchRecordOp.inc:58:log
  0.0126   0.0049   begin session_start   var/cache/petabox/petabox/www/sf/download.php:80:main_wrap
                                             |download.php:101:main
                                             |download.php:536:stream
                                             |download.php:946:head
                                             |common/Nav.inc:123:__construct
                                             |Nav.inc:193:session_start
                                             |Cookies.inc:56:log
  0.0128   0.0003    done session_start   var/cache/petabox/petabox/www/sf/download.php:80:main_wrap
                                             |download.php:101:main
                                             |download.php:536:stream
                                             |download.php:946:head
                                             |common/Nav.inc:123:__construct
                                             |Nav.inc:193:session_start
                                             |Cookies.inc:62:log
  0.0913   0.0784              bug dump   var/cache/petabox/petabox/www/sf/download.php:80:main_wrap
                                             |download.php:101:main
                                             |download.php:536:stream
                                             |download.php:976:footer
                                             |common/setup.php:153:footer
                                             |Nav.inc:1100:dump
                                             |Bug.inc:102:log
    -->
    <script type="text/javascript">
if (window.archive_analytics) {
  var vs = window.archive_analytics.get_data_packets();
  for (var i in vs) {
    vs[i]['cache_bust']=Math.random();
    vs[i]['server_ms']=91;
    vs[i]['server_name']="www30.us.archive.org";
      }

  if ($(".more_search").size()>0) {
    window.archive_analytics.send_scroll_fetch_base_event();
  }
}
</script>
              <img src="//analytics.archive.org/0.gif?kind=track_js&track_js_case=control&cache_bust=102999357" />
          <noscript>
        <img src="//analytics.archive.org/0.gif?kind=track_js&track_js_case=disabled&cache_bust=1870548039" />
      </noscript>
          <script>
        document.addEventListener('DOMContentLoaded', function() {
          if (typeof AJS === 'undefined') {
            return;
          }
          AJS.createTrackingImage('in_page_executes');
        });
      </script>
      </div>
  </body>
</html>
    
