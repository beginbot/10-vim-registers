# The Named Registers

## Useful For

- Storing Whatever you want!
- You're macros are stored here automatically
- You can edit your macros!
- Storing snippets of common snippets of text
- Saving things on the fly, that you don't want clobbered
