# The Selection Register

Keys: "+ "*

https://gitlab.com/beginbot/geoboard

There is not a joke. This is seriously annoying if you don't figure it out

Depends on your OS

To ALWAYS use the clipboard for ALL operations
(instead of interacting with the '+' and/or '*' registers explicitly):

```
set clipboard+=unnamedplus
```
