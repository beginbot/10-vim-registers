# The Black hole Register

## Key: "_

When writing to this register, nothing happens.

This can be used to delete text without affecting the normal registers.

When reading from this register, nothing is returned.
