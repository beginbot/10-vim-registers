# Vim Registers

Save text for later retrieval

## The 10 Registers

1. The unnamed register ""
2. 10 numbered registers "0 to "9
3. The small delete register "-
4. 26 named registers "a to "z or "A to "Z
5. Three read-only registers ":, "., "%
6. Alternate buffer register "#
7. The expression register "=
9. The black hole register "_
8. The selection registers "* and "+
10. Last search pattern register "/

- 3 Readonly


```vim
```

:reg
gg

Normal Mode: "<Register Key>

Insert Mode: CTRL-r <Register Key>
