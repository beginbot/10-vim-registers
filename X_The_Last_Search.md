# The Last Search Register

## Key: "/

This makes sense as the Last!

## Useful for

- Correcting a Search
- Insert last Search as you are typing
- Using the last search for search and replace functions

Search Mode:  / CTRL-r /
Insert Mode:    CTRL-r /
Execute Mode: / CTRL-r /

durf
durf
durf
dorf
