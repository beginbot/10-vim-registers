# The 3 Read Only Registers

## Keys ". "% ":

% - Current File
. - Last Insert Mode
: - Last Command

What File I am in??
What Did I just Type??
What Did I just Run??

```vim
:help filename-modifiers
```

%:p -> Make file name a full path
%:h -> Head of the Filename
%:t -> Tail of the Filename
%:r -> Root
%:e -> Extension
