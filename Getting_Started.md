# Getting Started

Saves text for later retrieval

```vim
:reg

:registers

:reg a

:reg b

:help registers
```

Normal Mode: "<Register Key>

Insert Mode: CTRL-r <Register Key>
