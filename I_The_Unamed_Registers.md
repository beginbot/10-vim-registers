# The Unnamed Register

```vim
```

Things that Populate The Unnamed Register:
Things that Populate The Unnamed Register:

- d/D
- c/C
- s/S
- x/X
- y/Y

p/P pastes from the Unnamed Register

Delete a word
Change a word
Substitute a Line
Delete a Character!
Yank a Line

In Normal Mode: `"<INSERT REGISTER KEY>`

In Insert Mode: `CTRL-r<INSERT REGISTER KEY>`

:call ClearRegs()

---

> Vim fills this register with text deleted with the "d", "c", "s", "x" commands
> or copied with the yank "y" command, regardless of whether or not a specific
> register was used (e.g.  "xdd).
>
> This is like the unnamed register is pointing to the last used register.
> Thus when appending using an uppercase register name,
> the unnamed register contains the same text as the named register.
>
> An exception is the '_' register: "_dd does not store the deleted text in any register.
>
> Vim uses the contents of the unnamed register for any put command (p or P)
> which does not specify a register.
>
> Additionally you can access it with the name '"'.
> This means you have to type two double quotes.
> Writing to the "" register writes to register "0.  "'"')
