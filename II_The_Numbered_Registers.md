# The Numbered Registers

Line 1
Line 8
Line 5
Line 9
Line 10

## Useful For

- "0 always has the last yank
- "1 always has the last delete (d, c, s, x)
- Nice for finding that item you deleted 3 commands ago and forgot

An exception is made for the delete operator with these movement commands:
|%|, |(|,
|)|, |`|,
|/|, |?|, |n|, |N|, |{| and |}|.
