class User:
    # qa                -> record macro under a
    # yyp               -> duplicate the line
    # df.               -> delete to the dot
    # i"<ESC>A":<ESC>J^ -> Surround "user" in quotes plus a `:`,
    #                      bring lower word to same line,
    #                      and jump to the beginning of the line
    # q                 -> stop recording

    # :let @a='<Ctrl-r a>''

    def as_json(self):
        "name": self.name
        "dream": self.dream,
        "vision": self.vision,
        "goals": self.goals,
        "ideas": self.ideas,
        "inspirations": self.inspirations,
